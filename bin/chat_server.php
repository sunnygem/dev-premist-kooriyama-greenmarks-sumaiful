<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Chat\Chat;

var_dump($argv);

if ( isset( $argv[1] ) && isset( $argv[2] ) )
{
    $port             = $argv[1];
    $unique_parameter = $argv[2];

    require_once dirname(__DIR__) . '/vendor/autoload.php';
    $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new Chat( $unique_parameter )
                    )
                ),
            $port
            );
    $server->run();
}
else
{
    echo 'Error: チャットのポート番号とunique_parameterを指定してください' . "\n";
}
