$(function(){

    // 画像を削除
    $(document).on('click', '.delete_btn', function(){
        var target     = this.parentNode.parentNode,
            targetWrap = this.parentNode.parentNode.parentNode;
            fileInput  = targetWrap.querySelector('[type="file"]');

            $(target).remove();
            $(this).next().remove();

            fileInput.value = '';
            getFileSelect(targetWrap);
            // 次へボタンの確認
            check_next();
    });

    // 写真の拡大
    $(document).on('click', '.expanion', function(){
        //var index = this.parentNode.parentNode.parentNode.dataset.idx;
        var index = this.parentNode.parentNode.parentNode.getAttribute('data-idx');
        // オーバーレイ用の要素を追加
        //var overlay = setModalOverlay( preview );
        var overlay = setModalOverlay( preview );
        // オーバーレイをフェードイン
        $(overlay).fadeIn();

        var preview = setModalPreview( this.src, index );
        var target = document.querySelector('body');
        target.insertBefore(preview, overlay);

    });

    $(document).on('click', '.modal-overlay, .close_btn', function(){

        // モーダルコンテンツとオーバーレイをフェードアウト
        //$('#'+modal).fadeOut();
        $('.modal-overlay, .preview_wrap').fadeOut('normal',function(){
            // オーバーレイを削除
            $('.modal-overlay, .preview_wrap').remove();
        });
    });
    // 次へボタンの確認
    check_next();

});
function getPictureWrap( num ){
    var pics_wrap = document.createElement( 'div' );
    pics_wrap.setAttribute( 'class', 'picture_wrap' );
    pics_wrap.setAttribute( 'data-idx', num+1 );

    var input_file = document.createElement( 'input' );
    input_file.setAttribute( 'type',  'file' );
    input_file.setAttribute( 'name',  'photo[' + num + ']' );
    input_file.setAttribute( 'style', 'display:none;' );
    input_file.setAttribute( 'id', 'file_input'+(num+1) );

    pics_wrap.appendChild(input_file);
    getFileSelect(pics_wrap);

    return pics_wrap;
}
function getFileSelect( parent_elem ){

    var file_select = document.createElement( 'div' );
    file_select.setAttribute('class', 'file_select');
    file_select.innerHTML = 'ファイルを選択';
    var file_text = document.createElement( 'p' );
    file_text.setAttribute('class', 'file_text');
    file_text.innerHTML = 'ファイルが選択されていません';

    parent_elem.appendChild(file_select);
    parent_elem.appendChild(file_text);

    return parent_elem;

}

function getPreviewImage( fileSrc, idx ){
    var preview_image = document.createElement( 'li' );
    preview_image.setAttribute('class', 'preview_image');

    //var label = document.createElement('p');
    //label.setAttribute('class', 'label');
    //label.innerHTML = '添付画像'+idx;

    var text_c = document.createElement( 'div' );
    text_c.setAttribute('class', 'text_c');

    var image = document.createElement('img');
    image.setAttribute('src', fileSrc );
    image.setAttribute('class', 'expanion' );

    var del_btn = document.createElement( 'div' );
    del_btn.setAttribute('class', 'delete_btn');
    del_btn.innerHTML = '<span class="icon"></span>';

    text_c.appendChild(image);
    text_c.appendChild(del_btn);

    //preview_image.appendChild(label);
    preview_image.appendChild(text_c);
    return preview_image;
}

function setModalPreview( img_path, idx ){
    var wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'preview_wrap');

    //var index = document.createElement('p');
    //index.innerHTML = '添付資料'+idx;
    var close_btn = document.createElement('div');
    close_btn.setAttribute('class', 'close_btn');
    close_btn.innerHTML = '<p>×</p>';

    //wrapper.appendChild(index);
    wrapper.appendChild(close_btn);

    if( img_path.length > 0 ){
        var preview_wrap = document.createElement('div');
        preview_wrap.setAttribute('class', 'preview_img');

        var preview = document.createElement('img');
        preview.setAttribute('class', 'preview');
        preview.src = img_path;

        var p_width  = preview.width;
        var p_height = preview.height;
        if( p_width >= p_height ){
            preview.style.width = '100%';
        }

        preview_wrap.appendChild(preview);
        wrapper.appendChild(preview_wrap);

    }

    return wrapper;
}
function setModalOverlay( contents ){
    var modal_overlay = document.createElement('div');
    modal_overlay.setAttribute('class', 'modal-overlay');

    if( contents ){
        modal_overlay.appendChild( contents );
    }
    var body = document.querySelector('body');
    body.appendChild(modal_overlay);

    return modal_overlay;
}

function check_next()
{
    var res = $('.automobile_picture').find('img');
    if ( res.length > 0 )
    {
        $('#automobile_step2_submit').removeClass('disabled');
        $('#automobile_step2_submit').attr('disabled', false);

    }
    else
    {
        $('#automobile_step2_submit').addClass('disabled');
        $('#automobile_step2_submit').attr('disabled', true);
    }
}
