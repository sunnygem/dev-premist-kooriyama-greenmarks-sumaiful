$(function(){
    var $frame = $('#frame');
    if( $frame[0] ) {
        $frame.find('.control button[type=submit]').on('click', function(){
            $frame.find('.loader')[0].classList.remove('hidden');
        });
    }
});
