$(function(){
/*
    $('.menu_trigger span').on('click',function(){
        $('#menu').css('left','0');
    });
    $('#menu_close').on('click',function(){
        $('#menu').css('left','-100%');
    });
    // モーダル関係
    //$('.modal-open').on('click',function(){
    $('.container').on('click', '.modal-open', function(){
        // オーバーレイ用の要素を追加
        $('body').append('<div class="modal-overlay"></div>');
        // オーバーレイをフェードイン
        $('.modal-overlay').fadeIn();

        // モーダルコンテンツのIDを取得
        var modal = $(this).data('target');
        // モーダルコンテンツの表示位置を設定
        // modalResize( modal );
        // モーダルコンテンツフェードイン
        $('#'+modal).fadeIn();

        // 「.modal-overlay」あるいは「.modal-close」をクリック
        $('.modal-overlay, .modal .btn .cancel').click(function(){
            // モーダルコンテンツとオーバーレイをフェードアウト
            $('#'+modal).fadeOut();
            $('.modal-overlay').fadeOut('normal',function(){
                // オーバーレイを削除
                $('.modal-overlay').remove();
            });
        });

        // リサイズしたら表示位置を再取得
        // $(window).resize(function(){
        //         modalResize( modal );
        //         });

    }); 
    $('.modal').on('click', '.modal-complete', function(){
        $('.modal').fadeOut();
        // モーダルコンテンツのIDを取得
        var modal = $(this).data('target');
        // モーダルコンテンツの表示位置を設定
        // modalResize( modal );
        // モーダルコンテンツフェードイン
        $('#'+modal).fadeIn();
        $('.modal-overlay, .modal .btn .cancel').click(function(){
            // モーダルコンテンツとオーバーレイをフェードアウト
            $('#'+modal).fadeOut();
            $('.modal-overlay').fadeOut('normal',function(){
                // オーバーレイを削除
                $('.modal-overlay').remove();
            });
        });
    }); 
*/

});

/*
 * Ajax関数
 * @param  object param
 * ex )
 * param = {
 *     url  : 'xxx',
 *     data : { key : 'xxx' }
 * }
 * @return deferred object
 */
function fnAjax(request){
    var defaultParam = {
        async : true,
        type  : 'POST',
        url   : '/',
        scriptCharset : 'utf-8',
    };
    if($ === jQuery){
        var param = Object.assign( defaultParam, request );
        console.log( param );
        var d = $.Deferred();
        $.ajax(param)
        .done(function(data){
            console.log('ajax success.');
            d.resolve(data);
        })
        .fail(function(data){
            console.log('ajax fail.');
            console.log( data );
            d.reject(data);
        })
        .always(function(){
            console.log('ajax complete.');
        });
        return d.promise();
    }
}
