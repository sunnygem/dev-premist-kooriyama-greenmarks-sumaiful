(function (){
    window.onload = function() {
        CKEDITOR.config.toolbar = [
            ["Source"],                                    // ソースコード表示切り替え
            "/",                                           // 改行
            ["Undo", "Redo"],
            ["Link", "Image", "Table", "HorizontalRule"],  // リンク、画像配置、テーブル、水平線
            ["Bold", "Italic", "Strike", "RemoveFormat"],  // 太字、イタリック、打ち消し線、書式を解除
            ["NumberedList", "BulletedList"],
            ["Image", "-", "Styles"]
        ];
        CKEDITOR.replaceAll( 'ck_editor' );
    }
})();
