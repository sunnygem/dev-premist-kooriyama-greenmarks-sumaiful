
//$(function(){
//    var search_flg = false;
//    $(document).on('keyup', 'input[name="search"]',function(){
//console.log('keyup');
//        if ( search_flg === false )
//        {
//            search_flg = true;
//            var value = $(this).val();
//            members.request.search = value;
//            members.getData(members, members.request);
//            search_flg = false;
//        }
//    });
//});
    // チャットのテンプレート
    //Vue.component( 'member-list', {
    //    template : '<a class="clearfix" href="{{url}}"> 
    //                    <div class="photo"><img src="/img/{{img}}" alt=""></div>
    //                    <div class="line">
    //                        <div class="name">{{name}</div>
    //                    </div>
    //                </a>',
    //    props    : [ 'url', 'img', 'name' ]
    //});
    var page = 1;

    var mixin = {
        ajax: {
            data:{
                error:0,
                loading:true,
                result:{}
            },
            methods: {
                getData:function(obj, request){
                    var _this = this;
console.log('getData-----');
console.log(request);
                    _this.loading = true;
                    $.ajax({
                        url: request.url,
                        type: 'get',
                        dataType: 'JSON',
                        timeout: 30000,
                        data: request.data
                    })
                    .done(function(data){
                        _this.error = 0;
                        _this.loading = false;
                        console.log(page);
                        if ( page === 1 )
                        {
                            obj.result = data;
                            console.log(data);
                        }
                        else
                        {
                            // Vue.jsが分かってないので苦肉の策
                            $(data).each(function(i,e){
                                var nickname =  ( e.front_user.nickname ) ? e.front_user.nickname : '';
                                html = '<li>'
                                     + '<a class="clearfix" href="/messages/detail/' + e.chat_group_id + '/' + e.thread_id + '/' + e.id + '/' + e.front_user.id + '">'
                                     + '<div class="photo_wrap">'
                                     + '<div class="photo" style="background-image: url(' +  e.front_user.thumbnail_path + ')">'
                                     + '</div>'
                                     + '</div>'
                                     + '<div class="line">'
                                     + '<div class="name">' + nickname + '</div>';
                                if ( e.unread_flg )
                                {
                                    html += '<div class="unread">●</div>';
                                }
                                html += '</div>'
                                     + '</a>'
                                     + '</li>';
                                $('#member_list').append( html );
                            });
                        }
                        page++;
                    })
                    .fail(function(error){
                        if ( _this.error <= 3 ){
                            _this.error++;
                            _this.getData();
                        }else{
                            _this.error = true;
                            _this.loading = false;
                        }
                    })
                }
            },

        }
    }

    var members = new Vue({
        el: '#member_list',
        mixins: [ mixin.ajax ],
        data: {
            request:{
                url: '/messages/get',
                data:{
                    page: page,
                    search: ''
                }
            }
        },
        mounted:function(){
console.log('mounted');
            this.request.data.search = $('input[name="search"]').val();
            mixin.ajax.methods.getData(this, this.request);
            this.request.data.page++;
            window.addEventListener( 'scroll', this.pagination );
            this.pagination();
console.log('scroll');
        },
        methods: {
            pagination: function() {
console.log('pager');
                var doch = $(document).innerHeight(); //ページ全体の高さ
                var winh = $(window).innerHeight();   //ウィンドウの高さ
                var bottom = doch - winh;             //ページ全体の高さ - ウィンドウの高さ = ページの最下部位置
                if ( bottom <= $(window).scrollTop() )
                {
console.log('bottom');
                    //一番下までスクロールした時に実行
                    this.request.search = $('input[name="search"]').val();
                    mixin.ajax.methods.getData(this, this.request);
                    this.request.data.page++;
                }
            }
        }
    });

