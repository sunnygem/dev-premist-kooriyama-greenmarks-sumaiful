var param = {
    domain                      : document.getElementById( 'domain' ).value,
    wss                         : document.getElementById( 'wss' ).value,
    contractant_id              : document.getElementById( 'contractant_id' ).value,
    chat_group_id               : document.getElementById( 'chat_group_id' ).value,
    contractant_service_menu_id : document.getElementById( 'contractant_service_menu_id' ).value,
    thread_id                   : document.getElementById( 'thread_id' ).value,
    user_id                     : document.getElementById( 'user_id' ).value,
    front_user_id               : document.getElementById( 'front_user_id' ).value,
    number                      : document.getElementById( 'number' ).value,
    limit                       : document.getElementById( 'limit' ).value
};

$(function(){
    // var obj = document.getElementById('chat_posts');
    // メッセージ表示エリア
    let $obj = $('.main');

    $(window).on('load', function(){
        $(this).scrollTop( $obj[0].scrollHeight );
    });

    var nav = $('#chat_detail_head');
    var load = false;
    $(window).on('scroll',function(){

        //if($(window).scrollTop() > 125 ) {
        //    nav.addClass('fixed');
        //} else {
            //nav.removeClass('fixed');
            if ( $(this).scrollTop() <= 700 && load === false )
            {
                load = true;
                console.log('load start');
                param.number = Number( param.number ) + 1;
                request_data = {
                    chat_group_id: param.chat_group_id,
                    thread_id:     param.thread_id,
                    user_id:       param.user_id,
                    front_user_id: param.front_user_id,
                    page:          param.number,
                    reverse: true
                };
                $.ajax({
                    url: 'get_message',
                    type: 'get',
                    dataType: 'JSON',
                    timeout: 30000,
                    data: request_data
                })
                .done(function(data){
                    if ( data )
                    {
                        var pre_height = $('#chat_wrap').height();
                        var html = '';
                        $(data).each(function(i,e){
                            var d = new Date( e.created );

                            html = '<div class="time">' + ( d.getMonth() + 1 ) + '月' + d.getDate() + '日 ' + d.getHours() + ':' + d.getMinutes() + '</div>'
                                + '<div class="' + ( Number( e.front_user_id ) !== Number( param.front_user_id ) ? 'left' : 'right' ) + '">';
                            if ( Number( e.front_user_id ) !== Number( param.front_user_id ) )
                            {
                                html += '<div class="photo_wrap">'
                                    + '<div class="photo" style="background-image:url(/' + e.user.front_user.thumbnail_path + ')"></div>'
                                    + '</div>';
                            }
                            if ( e.file_path )
                            {
                                html += '<p class="file"><img src="' + e.file_path + '"></p>';
                            }
                            else
                            {
                                html += '<p>' + h( e.message ).replace(/\r?\n/g, '<br>'); + '</p>';
                            }
                            $('#chat_wrap').prepend( html );
                        });
                        var post_height = $('#chat_wrap').height();
                        $('#top').scrollTop(post_height - pre_height );
                    }
                })
                .fail(function(error){
                })
                .always(function(){
                    load = false;
                });
            }
        //}
    });

    $(window).keydown(function(e){
        if(event.ctrlKey){
            if(e.keyCode === 13){
                var message = $('#message').val();
                $('#message').val('');
                chats.chatSend( message, null, null, null );
                return false;
            }
        }
    });
});

// vue.js
var websocket_url = 'wss://' + param.domain + '/' + param.wss + '/?contractant_id=' + param.contractant_id + '&chat_group_id=' + param.chat_group_id + '&contractant_service_menu_id=' + param.contractant_service_menu_id + '&thread_id=' + param.thread_id + '&user_id=' + param.user_id + '&front_user_id=' + param.front_user_id;
console.log(websocket_url);
var websocket = {
    wsUri: websocket_url,
    socket:null,
    cryptKey:null,
    init:function(){
        try {
            if (typeof MozWebSocket == "function")
            {
                WebSocket = MozWebSocket;
            }

            if ( websocket.socket && websocket.socket.readyState == 1 )
            {
                websocket.socket.close();
                websocket.socket = null;
            }

            websocket.socket = new WebSocket( websocket.wsUri );
            var text;
            switch( websocket.socket.readyState )
            {
                case websocket.socket.CONNECTING:
                    break;
                case websocket.socket.OPEN:
                    break;
                case websocket.socket.CLOSING:
                    break;
                case websocket.socket.CLOSED:
                    break;
            }
            websocket.socket.onopen = function(e){
                console.log("Connection established!");
            }
            websocket.socket.onclose = function(e){
                console.log("Connection close!");
            }
            websocket.socket.onmessage = function(e){
                console.log('onmessage----------');
                var data = JSON.parse( e.data );
                console.log(data);
                chats.depiction( data );
            }
            websocket.socket.onerror = function(e){
                console.log("Connection fail!");
                websocket.init();
                conn = websocket.socket;
            }
        } catch (exception) {
            console.log("ERROR: " + exception);
        }
    }
}
// チャット起動
websocket.init();
var conn = websocket.socket;

// チャットのテンプレート
Vue.component( 'chat-list', {
    template : '<div>'
             + '<div class="time">{{date}}</div>'
             + '<div v-bind:class="own">'
             + '<div v-if="img" class="photo_wrap"><div class="photo" :style=\'{ backgroundImage: "url(" + img + ")", }\'></div></div>'
             //+ '<p v-if="img" v-bind:class="file" :style=\'{ backgroundImage: "url(" + img + ")", }\'></p>'
             + '<p v-if="photo" class="file"><img :src="photo"></p>'
             + '<p v-if="message" v-html="message">{{message}}</p>'
             + '</div>'
             + '</div>',
    props    : [ 'own', 'img', 'message', 'date', 'photo' ]
});

var chats = new Vue({
    el: '#chat_wrap',
    data: {
        own: '',
        img: '',
        message: '',
        date: '',
        photo: '',
        lists: [
        ],
        user_id: param.user_id,
        front_user_id: param.front_user_id
    },
    methods: {
        // 自分側への書き込み
        chatSend: function( message, image, audio, movie ) {
            var tmp_data = {
                message: message,
                image: image,
                audio: audio,
                movie: movie
            }

            console.log( image );
            console.log('ok');            
            // 相手に送信
            conn.send( JSON.stringify( tmp_data ) );

            // chat_wrapの書き換え
            var now = new Date();
            var msg = '';
            var own = 'right';
            this.message = message;
            var tmp = wrapMsg( param, this, own );
            msg = tmp[0];
            own = tmp[1];
            var img = null;
            var photo = null;
            if ( image )
            {
                //img = 'data:image/jpeg;base64,' + image;
                photo = 'data:image/jpeg;base64,' + image;;
            }

            this.lists.push({
                own: own,
                message: msg,
                img: img,
                photo: photo,
                //date: now.getFullYear() + '.' + ( '0' + ( now.getMonth() + 1 ) ).slice(-2) + '.' + ( '0' + now.getDate() ).slice(-2) + ' ' + ( '0' + now.getHours() ).slice(-2) + ':' + ( '0' + now.getMinutes() ).slice(-2)
                date: ( '0' + ( now.getMonth() + 1 ) ).slice(-2) + '月' + ( '0' + now.getDate() ).slice(-2) + '日' + ( '0' + now.getHours() ).slice(-2) + ':' + ( '0' + now.getMinutes() ).slice(-2)
            })
            
            // スクロールさせる
            this.$nextTick(function(){
                scrollToBottom();
            })
        },
        // チャットサーバーからのresponse
        depiction: function( data ) {
console.log('depiction');
console.log(data);
            // chat_wrapの書き換え
            var now = new Date();
            var own = 'left';
            var tmp = wrapMsg( param, data, own );
            msg = tmp[0];
            own = tmp[1];

            var obj = {
                own: own,
                message: msg,
                img: '/' + data.front_user.thumbnail_path,
                date: ( '0' + ( now.getMonth() + 1 ) ).slice(-2) + '月' + ( '0' + now.getDate() ).slice(-2) + '日' + ( '0' + now.getHours() ).slice(-2) + ':' + ( '0' + now.getMinutes() ).slice(-2)
            };
            // 画像
            if( data.file_path ) {
                obj.photo = data.file_path;
            }
            this.lists.push(obj);

            // スクロールさせる
            this.$nextTick(function(){
                scrollToBottom();
            });
        }
    }

});

function wrapMsg( param, data, own )
{
console.log(data);            
    tmp = data.message;
    msg = ( tmp ) ? tmp.replace( /\n/g, '<br>' ) : tmp;

    if ( param.user_id === data.user_id && param.front_user_id === data.front_user_id )
    {
        own = 'right';
    }
    else
    {
        own = 'left';
    }
    return [ msg, own ];
}
function handleFileSelect(evt)
{
    var files = evt.target.files; // FileList object
console.log(files);
console.log(files.length);

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

console.log(f.type);
        // Only process image files.
        if (
                f.type.match('image.*')
                ||
                f.type.match('audio.*')
                ||
                f.type.match('video.*')
                ||
                f.type.match('application.*')
           )
        {
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
console.log(theFile.type);
                    return function(e) {
console.log(e.target);
                        data = e.target.result;
console.log(data);
                        var message = null;
                        var image   = null;
                        var audio   = null;
                        var video   = null;

                        if ( theFile.type.match('image.*') )
                        {
                            tmp = data.split(',');
                            image = tmp[1];
                        }
                        else if ( theFile.type.match('audio.*') )
                        {
                           audio = data;
                        }
                        else if ( theFile.type.match('video.*') )
                        {
                           video = data;
                        }
                        chats.chatSend( message, image, audio, video );
                    }
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }
}
function h(str){
    return (str + '').replace(/&/g,'&amp;')
        .replace(/"/g,'&quot;')
        .replace(/'/g,'&#039;')
        .replace(/</g,'&lt;')
        .replace(/>/g,'&gt;'); 
}

function scrollToBottom()
{
    var element = document.documentElement;
    var bottom = element.scrollHeight - element.clientHeight;
    window.scroll(0, bottom);
}
