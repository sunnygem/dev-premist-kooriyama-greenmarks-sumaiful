//Grobal
var current_view = 'pc'; //Viewport State
var ww = $(window).width(); //Window Width
var wh = $(window).height(); //Window Height
var user_scroll = $(window).scrollTop(); //Scroll Position

/////////////////////////////////////////////
/* Init */
/////////////////////////////////////////////
$(document).ready(function(){
    ww = $(window).width();
    wh = $(window).height();
    user_scroll = $(window).scrollTop();

    checkViewsizeAndOverflow();
    checkAnimation();
    
    setTimeout(function(){
    	$(window).resize();
    	$('body').css('opacity', '1');
    }, 300);

    // 確認
    $('.jsConfirm').on('click', function(){
        var msg = $(this).data('msg');
        fnConfirm( msg );
    });
    function fnConfirm( msg )
    {
        if( window.confirm( msg ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /* Post */
    $('#post .tab a').on('click', function(){
    	$('#post .tab a, #post .tab_inner section').removeClass('active');
    	var target = $(this).attr('class');
    	$(this).addClass('active');

        var $placeholder = $(this).data('placeholder');
    	$('#post .tab_inner section').removeClass().addClass(target);
        $('#post .tab_inner section').find('textarea[name=content]').attr({'placeholder':$placeholder});

    });

    var isPost = false;
    $('#post button.submit').on('click', function(){
        // 二重クリック禁止
        if( isPost === false ){
            isPost = true;

            $form = $(this).closest('form');

            $category_id = $form.find('.tab .active').data('category-id');
            $form[0].post_category_id.value = $category_id;

            //$content = $form.find('.tab_inner .active textarea[name=dummy_content]').val();
            //$form[0].content.value = $content;

            $(this).closest('form').submit();
        }
    });

    $('.fnPostDelete').on('click', function(){
        var msg = $(this).data('msg');
        if( window.confirm( msg ) )
        {
            var $form = $(this).closest('form');
            console.log($form);
            $targetId = $func_modal.data('target-id');
            //Object.keys($datalist).forEach(function(key) {
            //    var val = this[key];
            //    $form.append( '<input type="hidden" name="' + key + '" value="' + val + '">');
            //}, $datalist);
            $form.append( '<input type="hidden" name="target_id" value="' + $targetId + '">');
            $form[0].submit();
        }
        else
        {
            return false;
        }
    });

    /*  Community */
    $('#community .tab a').on('click', function(){
        var $form = $('#search');
    	if($(this).hasClass('active')) {
    		$(this).toggleClass('active');
            $form[0].post_category_id.value = null;
    	} else {
    		$('#community .tab a').removeClass('active');
    		$(this).addClass('active');
            var $target_id = $(this).data('category-id');
            $form[0].post_category_id.value = $target_id;
    	}
        $form.submit();
    });

    /* モーダル */
    var $win = $(window);
        $modal_bg = $('.modal_bg'),
        isModalOpen = false;
    // 背景モーダル
    $modal_bg.on('click', function() {
        var $this = $(this);

        $gift_modal.removeClass('open');
        $gift_modal.find('.open').removeClass('open');

        // メニューを閉じる
        $menu.removeClass('open');
        $menu.find('.titles').removeClass('open');

        $func_modal.removeClass('open');

        $activity_modal.removeClass('open');

        isModalOpen = false;

        setTimeout(function(){
            $this.fadeOut('fast');
        }, 100);
    });

    /* menu */
    var $menu = $('#menu');
    //メニューをWindowの高さいっぱいにする
    //$win.on('load resize orientationchange', function(){
    //    WindowHeight = $win.height();
    //    //$('#menu').css('height', WindowHeight);
    //});

    $menu.on('click', function(e) {
        e.stopPropagation();
    });
    $('.bt_menu').on('click',function(){
        if ( isModalOpen === false ){
            isModalOpen = true;
            $modal_bg.fadeIn('fast');
            $menu.addClass('open');
        }
    });

    //// 背景モーダル
    //$('.menu_bg').on('click', function() {
    //    var $this = $(this);

    //    $menu.removeClass('open');
    //    $menu.find('.open').removeClass('open');

    //    setTimeout(function(){
    //        $this.fadeOut('fast');
    //    }, 100);
    //});

    /* 荷物 */
    var $gift_modal = $('#gift_modal');
    $(window).on('load', function(){
        // 荷物
        $('.jsConfirmBaggage').on('click',function(){
            isModalOpen = true;
            if ( $(this).hasClass('active') ){
                $modal_bg.fadeIn('fast');
                $gift_modal.addClass('open');
            }
        });
    });
    $('.jsReceiveBaggase').on('click',function(){
        if( isModalOpen === true )
        {
            var $form = $(this).closest('form');
            fnAjax({
                url      : $form.data('action'),
                dataType : 'json',
                type     : 'get',
                data     : {
                    user_id       : $form[0].user_id.value,
                    front_user_id : $form[0].front_user_id.value,
                }
            })
            .done(function(data){
                if( data.result > 0 )
                {
                    $('.jsConfirmBaggage').removeClass('active');
                }
                else
                {
                    // todo DB更新失敗
                }
            })
            .always(function(){
                $modal_bg.click();
            })
        }
    });


    /* 投稿サブメニュー */
    var $bt_other = $('.bt_other'),
        $func_modal = $('#func_modal');
    $bt_other.on('click', function(){
        if( isModalOpen === false ){
            isModalOpen = true;
            $func_modal.find('.post_menu').show();
            $func_modal.find('.report_menu').hide();
            $modal_bg.fadeIn('fast');
            $func_modal.addClass('open');

            var login_user_id = $(this).data('login-user-id'),
                login_front_user_id = $(this).data('login-front-user-id'),
                target_id   = $(this).data('target-id'),
                target_type = $(this).data('target-type'),
                user_id = $(this).data('user-id'),
                front_user_id = $(this).data('front-user-id');
                //console.log(target_id, login_user_id, login_front_user_id, user_id, front_user_id);

            if( user_id === login_user_id ){
                $func_modal.find('.post_menu .item.report').hide();
                $func_modal.find('.post_menu .item.message').hide();
                $func_modal.find('.post_menu .item.delete').show();
            }
            else
            {
                $func_modal.find('.post_menu .item.delete').hide();
                $func_modal.find('.post_menu .item.message').show();
                $func_modal.find('.post_menu .item.report').show();
            }

            $func_modal.data('login-user-id',       login_user_id);
            $func_modal.data('login-front-user-id', login_front_user_id);
            $func_modal.data('target-id',           target_id);
            $func_modal.data('target-type',         target_type); // 報告に使用
            $func_modal.data('user-id',             user_id);
            $func_modal.data('front-user-id',       front_user_id);

        }
    });
    $('.bt_message', $func_modal).on('click', function(e){
        //e.preventDefault();
        var href = '/messages/start-message/' + $func_modal.data('login-user-id') + '/' + $func_modal.data('login-front-user-id') + '/' + $func_modal.data('user-id') + '/' + $func_modal.data('front-user-id');
        $(this).attr('href', href);
    });
    // 報告する
    $('.bt_report').on('click', function(){
        $func_modal.find('.post_menu').hide();
        $func_modal.find('.report_menu').show();
    });
    $('.report_item').on('click', function(){
        $msg = $(this).data('msg');
        res = fnConfirm( $msg );
        if( res === true )
        {
            var target_type    = $func_modal.data('target-type');
            var target_id      = $func_modal.data('target-id');
            var login_user_id  = $func_modal.data('login-user-id');
            var login_front_user_id = $func_modal.data('login-front-user-id');
            var type           = $(this).data('report-type');
            //console.log( target_id, login_user_id, login_front_user_id, type );
            fnAjax({
                url      : '/tops/set-presentation',
                dataType : 'json',
                type     : 'get',
                data     : {
                    target_id     : target_id,
                    user_id       : login_user_id,
                    front_user_id : login_front_user_id,
                    target_type   : target_type,
                    type          : type,
                }
            })
            .done(function(data){
                if( data.result ) {
                    alert( data.result );
                }
            })
            .always(function(){
                $func_modal.removeClass('open');
                $modal_bg.click();
            });


        }
    });

    // キャンセル
    $('.bt_cancel').on('click', function(){
        $(this).parents('.modal_wrap').removeClass('open');
        $modal_bg.click();
    });

    // イイね・参加一覧
    $activity_modal = $('#activity_modal');
    $('.bt_box .favorite').on('click', function(){
        if( $(this).find('.count').data('count') > 0 ){
            $id = $(this).data('target-id');

            var list = json[$id];
            var fragment = document.createDocumentFragment();
            list.forEach(function(e, i){
                var li = document.createElement('li');
                li.innerHTML =  '<a href="/mypages/index/' + e.user.id + '">'
                                + '<div class="photo_wrap"><div class="photo" style="background-image: url( /' + e.user.front_user.thumbnail_path + ');"></div></div>'
                                + '<div class="name' + ( ( e.user.front_user.leave_flg === 1 )  ? ' leaved' : '' ) + '">'
                                + e.user.front_user.nickname + '</div>'
                                + '</a>';

                fragment.appendChild( li );
            });

            // 中身の削除しつつ追加
            var e_list = $activity_modal.find('.list')[0];
            var clone = e_list.cloneNode(false);
            clone.appendChild( fragment );
            e_list.parentNode.replaceChild( clone, e_list );

            isModalOpen = true;
            $modal_bg.fadeIn('fast');
            $activity_modal.addClass('open');
        }
    });

    // 起動画面
    $(window).bind('touchstart mousemove keydown',function(){
        removeModal();
    });
    setTimeout(function(){
        removeModal();
    },1000);

    //アルバム詳細
    $('.album_inner .thumb .thumb_img').on('click', function(){
        $('.album_inner .thumb li').removeClass('active');
        $(this).parent('li').addClass('active');

        var src = $(this).css('background-image')
        src = src.replace('url(','').replace(')','').replace(/\"/gi, "");
        $('#img_area img').attr( 'src', src );

        var $target_id = $(this).data('target-id');
        $('.bt_other').data('target-id', $target_id );
    });

    // Application
    if ( $('#agree').prop('checked') )
    {
        $('#ok').css('opacity','1');
        $('#ok').attr('disabled',false);
    }
    else
    {
        $('#ok').css('opacity','0.6');
        $('#ok').attr('disabled',true);
    }
    $('#agree').on('click',function(){
       if ( $(this).prop('checked') )
       {
            $('#ok').css('opacity','1');
            $('#ok').attr('disabled',false);
       }
       else
       {
            $('#ok').css('opacity','0.6');
            $('#ok').attr('disabled',true);
        }
    });

    // ファイルのプレビュー
    $('[type=file]').on('change', function(){
        var current_file = this.files[0];
        if( current_file ) {
            var $target = $(this).closest('.file').find('.preview_target');
            $target.show();
            if( $target[0] ){
                // Posts
                if( this.classList.contains('posts') ){
                    if( $target.find('img')[0] ){
                        $target.find('img')[0].src = window.URL.createObjectURL(current_file);
                    }else{
                        var img = document.createElement( 'img' );
                        img.src = window.URL.createObjectURL(current_file);
                        $target.append( img );
                    }
                    $(this).closest('.file').find('.bt_box').hide();
                    $target[0].classList.add('on');
                }
                //// imgタグ
                //else if( $target[0].tagName === 'IMG' )
                //{
                //    $target[0].src = window.URL.createObjectURL(current_file);
                //}
                // divタグで背景
                else if( $target[0].tagName === 'DIV' )
                {
                    $target.css('background-image', 'url("' + window.URL.createObjectURL(current_file) + '")');
                }
            }
        }
    });

    $(document).on('click', '#img_remove', function(e){
        $target = $(this).closest('.file');
        $target.find('input[type=file]').val('');
        $target.find('.preview_target').hide();
        $target.find('.bt_box').show();
    });

    $(document).on('click','.fnSetActivity', function(e){
        var $targetBtn = $( this );
        var controller    = $(this).data('controller');
        var action        = $(this).data('action');
        var target_id     = $(this).data('id');
        var user_id       = $(this).data('user-id');
        var front_user_id = $(this).data('front-user-id');
        var comment_id    = $(this).data('comment-id');
        fnAjax({
            url      : '/' + controller + '/set-activity',
            dataType : 'json',
            type     : 'get',
            data     : {
                action          : action,
                target_id     : target_id,
                user_id       : user_id,
                front_user_id : front_user_id,
                comment_id    : comment_id,
            }
        })
        .done(function(data){
            if( data.result === true )
            {
                switch( action )
                {
                case 'like':
                case 'join':
                    var $target = $targetBtn.closest('.bt_box').find('.favorite .count');
                    break;
                case 'comment_like':
                    var $target = $targetBtn.closest('.inner').find('.info .like .count');
                    break;
                }
                var count = $target.data('count');
                var innerHtml = $target.html().trim();
                console.log(innerHtml);
                if( $targetBtn.hasClass('active') )
                {
                    if( innerHtml === '1 like' )
                    {
                        $target.data('count', ( count - 1 )).html('none');
                    }
                    else
                    {
                        $target.data('count', ( count - 1 )).html( innerHtml.replace(/\d/g, (count - 1) ) );
                    }
                }
                else
                {
                    if( innerHtml === 'none' )
                    {
                        $target.data('count', ( count + 1 )).html( '1 like' );
                    }
                    else
                    {
                        $target.data('count', ( count + 1 )).html( innerHtml.replace(/\d/g, (count + 1) ) );
                    }
                }
                $targetBtn.toggleClass('active');
            }
            else
            {
                // todo DB更新失敗
            }
        })
        .always(function(){
            $modal_bg.click();
        })

        
    });

    $('.article_footer .comment input[type=text]').on('click', function(){
        var $controller = $(this).data('controller');
        var $id = $(this).data('id');
        window.location.href = '/' + $controller + '/comment/' + $id;

    });

    // コメント
    $(document).on('keyup', 'textarea[name=comment]',function(){
        var str = $(this).val();
        if( str.trim().length > 0 ){
            $(this).closest('form').find('button[name=complete]').prop('disabled', false );
        }
        else
        {
            $(this).closest('form').find('button[name=complete]').prop('disabled', true );
        }
    });
    $(document).on('click', '.bt_reply', function(){
        $('#comment .inner').removeClass('active');
        $(this).closest('.inner').addClass('active');

        var $form = $('form[name=form_comment]');
        var $parent_comment_id = $(this).data('comment-id');
        console.log($form );
        $form[0].parent_comment_id.value = $parent_comment_id;
        $form[0].comment.focus();
    });

});


/////////////////////////////////////////////
/* Resizing */
/////////////////////////////////////////////
$(window).on('resize', function(){
	ww = $(window).width();
	wh = $(window).height();
	
	var past_view = current_view;
	checkViewsizeAndOverflow();
	
	if(ww >= 768){
		
	} else {
		
	}
});

/////////////////////////////////////////////
/* Scrolling */
/////////////////////////////////////////////
$(window).on('scroll', function (){
	checkAnimation();
});

function checkAnimation(user_scroll){
	user_scroll = $(window).scrollTop();
	
	if(ww >= 768){
	}
	else {
	}
}

/////////////////////////////////////////////
/* Other Functions */
/////////////////////////////////////////////
function scrollActive(selector, offset) {
	user_scroll = $(window).scrollTop();
	
	if($(selector).length) {
		if(user_scroll > $(selector).offset().top - wh + offset) $(selector).addClass('active');			
	}
}
function checkViewsizeAndOverflow() {
	if(ww >= 768){
		$("body").addClass("pc_view").removeClass("sp_view");
		current_view = 'pc';
	} else {
		$("body").addClass("sp_view").removeClass("pc_view");
		current_view = 'sp';
	}	
	if(wh >= 500){
		$("body").removeClass("overflow_menu");
	} else {
		$("body").addClass("overflow_menu");
	}
}
function removeModal(){
    if ( $('#start') )
    {
        $('#start').fadeOut(2000, function(){
            $(this).remove();
        });
    }
}
