// common.jsに依存
$(function(){
    var $target = $('.autoPager');
    if( $target[0] ){
        var isLoad = false;
        var page   = 1;

        $(window).on('scroll', function (){
            if( isLoad === false )
            {
                var scrollHeight = $(document).height();
                var scrollPosition = wh + $(window).scrollTop();
                if ( (scrollHeight - scrollPosition) / scrollHeight === 0 ) {
                    isLoad = true;

                    //var offset = $('[name="data_offset"]').val();

                    loadPage();
                }
            }
        });
    }

    function loadPage()
    {
        fnAjax({
            url      : $target.data('action'),
            dataType : 'json',
            type     : 'get',
            data     : {
                page : page,
            }
        })
        .done(function(data){
            if( data.length > 0 )
            {
                setElement( data, $target.data('element'), $target[0] );
                //ページをカウントアップ
                page++;
            }
        })
        .always(function(){
            isLoad = false;
        });
    }

    // domの作成
    function setElement( data, target_element, target ){
        switch( target_element )
        {
        case 'post_list' :
            var fragment = document.createDocumentFragment();
            for( var i=0; i<data.length; i++ ) {
                var article = getPostElement( data[i] );
                fragment.appendChild( article );
            }
            target.appendChild( fragment );

            break;
        }
    }

    function getPostElement( data ) {
        var article = document.createElement('article');
        var category;
        switch( data.post_category_id )
        {
            case 1: category = 'cat_tweet';  break;
            case 2: category = 'cat_invite'; break;
            case 3: category = 'cat_ask';    break;
            case 4: category = 'cat_event';  break;
        }
        article.classList.add( category ); 
        article.setAttribute( 'date-label', data.post_category.label );

        var inner = document.createElement('div');
        inner.classList.add('inner');

        var article_h = document.createElement('div');
        article_h.classList.add( 'article_header', 'clearfix');
        var article_h_inner = '<a href="/mypages/index/' + data.user.id + '" class="author">'
                            + '<div class="photo_wrap"><div class="photo" style="background-image:url( /' + data.user.front_user.thumbnail_path + ');"></div></div>'
                            + '<div class="name">' + data.user.front_user.nickname + '</div>'
                            + '<div class="date">' + data.created + '</div></a>';
        article_h.innerHTML = article_h_inner;
        inner.appendChild( article_h );

        var article_m = document.createElement('div');
        article_m.classList.add( 'post_menu_wrap');
        var article_m_inner = '<span class="bt_other">その他</span>'
                            //+ '<div class="post_menu"> <div class="inner"> <ul> <li> <a href="#">編集する</a> </li> <li> <a href="#">削除する</a> </li> <li class="report"> <a href="#" class="bt_report">報告する</a> </li> </ul> </div> </div>';
        inner.appendChild( article_m );

        var article_d = document.createElement('div');
        article_d.classList.add('content');
        article_d_inner = data.content;
        if( data.rel_post_files.length > 0 ) article_d_inner += '<img src="/' + data.rel_post_files[0].file.file_path + '">';
        article_d.innerHTML = article_d_inner;
        inner.appendChild( article_d );

        var article_f = document.createElement('div');
        article_f.classList.add('article_footer');
        article_f_inner = '<div class="bt_box clearfix">'
                        + '<a class="bt_favorite" href="#">いいね</a><a href="/communities/comment/' + data.id + '" class="bt_comment">コメント</a>'
                        + '<div class="favorite">' + data.post_likes.length + '人</div>'
                        + '</div>';
        article_f.innerHTML = article_f_inner;
        inner.appendChild( article_f );

        var article_c = document.createElement('div');
        article_c.classList.add('comment');
        article_c_inner = '<div class="photo"><img src="" alt=""></div><input type="text" placeholder="Comment...">';
        article_c.innerHTML = article_c_inner;
        article_f.appendChild( article_c );

        article.appendChild(inner);

        return article;
    }
});
