    // vue.js test
    var param = {
        domain                      : document.getElementById( 'domain' ).value,
        wss                         : document.getElementById( 'wss' ).value,
        contractant_id              : document.getElementById( 'contractant_id' ).value,
        chat_group_id               : document.getElementById( 'chat_group_id' ).value,
        contractant_service_menu_id : document.getElementById( 'contractant_service_menu_id' ).value,
        thread_id                   : document.getElementById( 'thread_id' ).value,
        user_id                     : document.getElementById( 'user_id' ).value,
        front_user_id               : document.getElementById( 'front_user_id' ).value,
        number                      : document.getElementById( 'number' ).value,
        limit                       : document.getElementById( 'limit' ).value
    };
    var websocket_url = 'wss://' + param.domain + '/' + param.wss + '/?contractant_id=' + param.contractant_id + '&chat_group_id=' + param.chat_group_id + '&contractant_service_menu_id=' + param.contractant_service_menu_id + '&thread_id=' + param.thread_id + '&user_id=' + param.user_id + '&front_user_id=' + param.front_user_id;
console.log(websocket_url);
    var websocket = {
        wsUri: websocket_url,
        socket:null,
        cryptKey:null,
        init:function(){
            try {
                if (typeof MozWebSocket == "function")
                {
                    WebSocket = MozWebSocket;
                }

                if ( websocket.socket && websocket.socket.readyState == 1 )
                {
                    websocket.socket.close();
                    websocket.socket = null;
                }

                websocket.socket = new WebSocket( websocket.wsUri );
                var text;
                switch( websocket.socket.readyState )
                {
                    case websocket.socket.CONNECTING:
                        break;
                    case websocket.socket.OPEN:
                        break;
                    case websocket.socket.CLOSING:
                        break;
                    case websocket.socket.CLOSED:
                        break;
                }
                websocket.socket.onopen = function(e){
                    console.log("Connection established!");
                }
                websocket.socket.onclose = function(e){
                    console.log("Connection close!");
                }
                websocket.socket.onmessage = function(e){
                    console.log('onmessage----------');
                    var data = JSON.parse( e.data );
                    console.log(data);
                    chats.depiction( data );
                }
                websocket.socket.onerror = function(e){
                    console.log("Connection fail!");
                    websocket.init();
                    conn = websocket.socket;
                }
            } catch (exception) {
                console.log("ERROR: " + exception);
            }
        }
    }
    // チャット起動
    websocket.init();
    var conn = websocket.socket;

    // チャットのテンプレート
    Vue.component( 'chat-list', {
        template : '<li v-bind:class="own"><div class="msg_content"><div class="img"><div class="icon"></div><div class="name">{{name}}</div></div><div class="txt_wrap"><div class="txt" v-html="message"></div><div class="date">{{date}}</div></div></div></li>',
        props    : [ 'own', 'img', 'name', 'message', 'date' ]
    });

    var chats = new Vue({
        el: '#chat_wrap',
        data: {
            own: '',
            img: '',
            name: '',
            message: '',
            date: '',
            lists: [
            ]
        },
        methods: {
            // 自分側への書き込み
            chatSend: function() {
                // 相手に送信
                conn.send( JSON.stringify( this.message ) );

                // chat_wrapの書き換え
                var now = new Date();
                var msg = '';
                var own = 'own';
                var tmp = wrapMsg( param, this, own );
                msg = tmp[0];
                own = tmp[1];

                this.lists.push({
                    own: own,
                    name: param.nickname,
                    message: msg,
                    date: now.getFullYear() + '.' + ( '0' + ( now.getMonth() + 1 ) ).slice(-2) + '.' + ( '0' + now.getDate() ).slice(-2) + ' ' + ( '0' + now.getHours() ).slice(-2) + ':' + ( '0' + now.getMinutes() ).slice(-2)
                })
            },
            // チャットサーバーからのresponse
            depiction: function( data ) {
                // chat_wrapの書き換え
                var now = new Date();
                var own = '';
                var tmp = wrapMsg( param, data, own );
                msg = tmp[0];
                own = tmp[1];
                var nickname = ( own === 'own' ) ? param.nickname : 'test';

                this.lists.push({
                    own: own,
                    name: nickname,
                    message: msg,
                    date: now.getFullYear() + '.' + ( '0' + ( now.getMonth() + 1 ) ).slice(-2) + '.' + ( '0' + now.getDate() ).slice(-2) + ' ' + ( '0' + now.getHours() ).slice(-2) + ':' + ( '0' + now.getMinutes() ).slice(-2)
                })
            }
        }

    });
function wrapMsg( param, data, own )
{
    tmp = data.message;
    msg = tmp.replace( /\n/g, '<br>' );

    if ( param.user_id === data.user_id && param.front_user_id === data.front_user_id )
    {
        own = 'own';
    }
    return [ msg, own ];
}
