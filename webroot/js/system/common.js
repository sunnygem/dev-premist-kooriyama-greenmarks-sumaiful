// 共通処理を記述

// IE対策
if (typeof(window.console) === "undefined") {
    window.console = {
        log: function(){}, // 何もしない関数 とする
    }
}

$(function(){

    // datetimepicker
    // 日付だけ
    $('.datepicker').datetimepicker({
        timepicker: false,
        format: 'Y/m/d',
        scrollMonth : false,
    });
    // 時刻も
    $('.datetimepicker').datetimepicker({
        format: 'Y/m/d H:i',
        scrollMonth : false,
    });

    // ファイルチェンジ
    $('[type="file"]:not([multiple="multiple"]').on('change', function( e ){
        var current_file = this.files[0];
        var text    = $(this).parent().find('.file_name');
        if( current_file ) {
            var $preview = $(this).parent().find('.preview');
            if( $preview[0] ){
                if( $preview.find('img, embed')[0] )
                {
                    $preview.find('img, embed').remove();
                }

                if ( current_file.type === 'application/pdf' )
                {
                    var embed = document.createElement( 'embed' );
                    embed.src = window.URL.createObjectURL(current_file);
                    $preview.append( embed );
                }
                else
                {
                    var img = document.createElement( 'img' );
                    img.src = window.URL.createObjectURL(current_file);
                    $preview.append( img );
                }
            }
            text.text(current_file.name);
        } else {
            text.text('');

        }
    });

    // 複数選択
    $('[type="file"][multiple="multiple"]').on('change', function(){
        var file = this,
            files = file.files;

            $(files).each(function(i,e){
                var fileRdr = new FileReader();
                if( !e.length ){
                    if(e.type.match('image.*') && e.type.match( /(gif|gi\_|jpeg|jpg|jp\_|png|)/ ) ){

                        fileRdr.onload = function(k){

                            var target = $('#image_area');
                            var span = document.createElement('span');
                            span.innerHTML = ['<img class="thumb" src="', k.target.result, '" title="', escape(e.name), '"/>'].join('');

                            target.append( getPreviewImage( fileRdr.result, i ) );
                            // 次へボタンの確認
                            //check_next();
                        }
                        fileRdr.readAsDataURL(e);
                    }
                    else
                    {
                        alert( '添付画像のファイル形式（拡張子）はjpeg・png・bmp・tiff・gifのいずれかで受付けております。' );
                        this.value = '';
                    }
                }
            });

    });

    // 重複選択
    $duplicateSelect = $('.duplicate_select');
    if( $duplicateSelect[0] ){
        $duplicateSelect.find('.drag_items li').draggable({
            helper: 'clone',
        });
        $duplicateSelect.find('.drop_area').droppable({
            accept: $duplicateSelect.find('.drag_items li'),
            drop: function( e, ui ) {
                var item = ui.draggable;
                var itemValue = item.data('value');
                var hidden = document.createElement('input');
                hidden.type = 'hidden';
                hidden.name = 'service_menu_id[]';
                hidden.value = item.data('value');

                var delSpan = document.createElement('span');
                delSpan.classList.add('delete', 'fas', 'fa-times');

                var li = document.createElement('li')
                li.innerText = item.text();
                li.appendChild(hidden);
                li.appendChild(delSpan);
                $(this).find('ul').append( li );
                $(this).animate({scrollTop: $(this).find('ul')[0].scrollHeight});
            },
         
        });
        // 削除
        $(document).on('click', '.delete', function(e){
            $(this).parent('li').remove();
        });
    }

    // リンクタイプの設定
    $selectLink = $('.select_link');
    if( $selectLink[0] ) {
        $checked = $selectLink.find('input[type="radio"]:checked').val();
        console.log($checked);
        // 外部リンク
        //if( $checked === 0 ){
        //    
        //// pdf
        //}else if( $checked === 1 ) {

        //}

        $radio = $selectLink.find('input[type="radio"]');
        $radio.on('change', function(){
            console.log(this.value);
        });
        console.log($radio);

    }

    $innerTalble = $('.inner_table_wrap');
    if( $innerTalble[0] ){
        // テーブルのインデックスを取得
        var tbl_index = countIndex();
        // アプリテーブルの追加
        $('.add_table').on('click', function(e){
            // コピー元の取得
            var $org = $('#index\\[' + tbl_index + '\\]');

            tbl_index++;
            // コピー
            var $clone = $org.clone(true).insertAfter( $org ).attr('id', 'index[' + tbl_index + ']');

            //クローン対象ではないhidden属性は削除
            $clone.find('[type="hidden"]:not(.clone)').remove();

            //キーのカウントアップ
            $clone.find('input, select, textarea, label, .file_name, img, .delete_table').each(function(idx, obj){
                // フォーム
                if( obj.name ){
                    $(obj).attr({ name : $(obj).attr('name').replace(/\[[0-9]+\]/, '[' + tbl_index + ']') });
                }
                if( obj.id ) {
                    $(obj).attr({ id : $(obj).attr('id').replace(/\[[0-9]+\]/, '[' + tbl_index + ']') });
                }

                // その他要素(タグネーム)
                switch( obj.tagName ){
                case 'LABEL':
                    $(obj).attr({ for : $(obj).attr('for').replace(/\[[0-9]+\]/, '[' + tbl_index + ']') });
                    break;
                case 'IMG':
                    obj.src = false;
                }
                //削除ボタン
                if( obj.classList.contains('delete_table' ) ) {
                    $(obj).attr({ 'data-index' : tbl_index });
                }

                console.log($(obj));
                console.log($(obj)[0].type);
                // チェックボックス以外の値はクリア
                if( obj.type !== 'checkbox' || obj.classList.contains('clone') !== true )
                {
                    $(obj).val('');
                }
                if( obj.classList.contains('default_check') !== true )
                {
                    $(obj).prop( 'checked', false );
                }

                if( obj.classList.contains('file_name'))
                {
                    $(obj).text('');
                }
            });
        });

        $(document).on('click', '.delete_table', function(e){
            if( window.confirm('削除しますか') ){
                var index = $(this).data('index');
                $('#index\\[' + index + '\\]').remove();

                // インデックスの振り直し
                reassignIndex();
                tbl_index = countIndex();
            }else{
                return false;
            }

        });

        function reassignIndex(){
            $innerTalble.find('.inner_table').each(function(new_idx, tbl){
                // テーブルID
                $(tbl).attr({ id : $(tbl).attr('id').replace(/\[[0-9]+\]/, '[' + new_idx + ']') });
                // テーブル内の各要素
                $(tbl).find('input, textarea, label, .file_name, img, .delete_table').each(function(idx, obj){
                    if( obj.name ){
                        $(obj).attr({ name : $(obj).attr('name').replace(/\[[0-9]+\]/, '[' + new_idx + ']') });
                    }
                    if( obj.id ) {
                        $(obj).attr({ id : $(obj).attr('id').replace(/\[[0-9]+\]/, '[' + new_idx + ']') });
                    }

                    // その他要素
                    switch( obj.tagName ){
                    case 'LABEL':
                        $(obj).attr({ for : $(obj).attr('for').replace(/\[[0-9]+\]/, '[' + new_idx + ']') });
                        break;
                    }

                    //削除ボタン
                    if( obj.classList.contains('delete_table' ) ) {
                        $(obj).attr({ 'data-index' : new_idx });
                    }
                });
            });

        }

        function countIndex(){
            return $innerTalble.find('.inner_table').length - 1;
        }
   }

   // 汎用の確認
   $('.remove_contractant_menu, .jsConfirm').on('click', function(e){
       var $type = $(this).data('type');
       var msg;
       switch( $type ){
       case 'album':
           msg = "削除しますか?\n※この操作は取り消しできません。";
           break;
       case 'hidden':
           msg = "非表示にしますか?";
           break;
       case 'leave':
           msg = "居住ステータスを変更しますか?";
           break;
       default:
           msg = "削除しますか?\n※この操作は取り消しできません。";
           
       }
       
       if( window.confirm( msg ) ){
           if( $(this).data('action') !== void 0 )
           {
               var $action = $(this).data('action');
               window.location.href = $action;
           }
       } else{
           return false;
       }
   });

   $('.autoSubmit').on('change', function(e){
       $form = $(this).closest('form');
       $form.submit();
   });

   /*** 排他チェック ***/
   // name属性でチェックしてます
   $('[data-exclusive-check] input[type=checkbox]').on('change', function(){
       if( this.checked === true ){
           var $parentElm = $(this).closest('[data-exclusive-check]');
           var $needle = $parentElm.data('exclusive-check');
           if( $needle === this.name )
           {
               $parentElm.find('[type=checkbox]:not([name="'+$needle+'"])').prop('checked', false);
           }
           else
           {
               $parentElm.find('[type=checkbox][name="'+$needle+'"]').prop('checked', false);
           }
       }
   });


});
