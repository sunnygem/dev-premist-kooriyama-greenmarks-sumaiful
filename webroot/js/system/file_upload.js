$(function(){
    var $droppable = $("#accept_area");

    // File API が使用できない場合は諦めます.
    if(!window.FileReader) {
        alert("File API がサポートされていません。");
        return false;
    }

    // イベントをキャンセルするハンドラです.
    var cancelEvent = function(event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    }
    // dragenter, dragover イベントのデフォルト処理をキャンセルします.
    $droppable.on("dragenter", cancelEvent);
    $droppable.on("dragover",  cancelEvent);

    var handleDroppedOver = function(event) {
        $droppable[0].classList.add('drop_on');
    }
    var handleDroppedLeave = function(event) {
        $droppable[0].classList.remove('drop_on');
    }
    var handleDroppedEnd = function(event) {
        $droppable[0].classList.remove('drop_on');
    }

    // ドロップ時のイベントハンドラを設定します.
    var handleDroppedFile = function(event) {
        $droppable[0].classList.add('dropped');

        // ファイルは複数ドロップされる可能性がありますが, ここでは 1 つ目のファイルを扱います.
        var file = event.originalEvent.dataTransfer.files[0];

        // ファイルの内容は FileReader で読み込みます.
        var fileReader = new FileReader();
        fileReader.onload = function(event) {
            var formData = new FormData();
            formData.append( 'upload_file', file );

            // csrfトークン
            var csrf = $('input[name=_csrfToken]').val();

            // event.target.result に読み込んだファイルの内容が入っています.
            // ドラッグ＆ドロップでファイルアップロードする場合は result の内容を Ajax でサーバに送信しましょう!
            $.ajax({
                type : "post",
                async : true,
                data : formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function( xhr ) {
                    xhr.setRequestHeader( 'X-CSRF-Token', csrf );
                },
                url : '/system/buildings/room-number-drag-upload',
            })
            .done(function(data){
                console.log( data );
                html = '<div class="list_table">';
                html += '<table class="table">';
                html += '<tr class="bottom_border">';
                html += '<th>物件番号</th>';
                html += '</tr>';
                for( var i = 0; i < data.length; i++ )
                {
                    html += ( i % 2 === 0 ) ? '<tr>' : '<tr style="background-color:#eee">';
                    for( var j = 0; j < data[i].length; j++ )
                    {
                        row = data[i][j];
                        if ( row === null )
                        {
                            row = '';
                        }
                        html += '<td>' + row + '</td>';
                    }
                    html += '</tr>';
                }
                html += '</table></div>';
                $droppable.html(html);
                $('[name=drag_data]').val( JSON.stringify( data ) );
                $('.btn_upload').attr('disabled', false);
                //$('#upload_data').val( data );
            })
            .fail( function(){
                console.log('error');
            });
        }
        fileReader.readAsText(file);

        // デフォルトの処理をキャンセルします.
        cancelEvent(event);
        return false;
    }


    // ドロップイベントの各イベントハンドラを設定.
    $droppable.on({
        'drop':      handleDroppedFile,
        'dragover':  handleDroppedOver,
        'dragleave': handleDroppedLeave,
        //'dragend':   handleDroppedEnd,
    });


});
