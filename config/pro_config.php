<?php
use Cake\Core\Configure;

return [
    define( 'SYSTEM_DOMAIN',     'system.sumaiful.com' )
    ,define( 'SYSTEM_SITE_NAME', 'システム管理画面' )
    ,define( 'ADMIN_SITE_NAME',  '管理画面' )

    ,define( 'SHELL_COMMAND', ROOT . DS . 'bin' . DS . 'cake' )

    ,define( 'FAVICON_PATH', WWW_ROOT . 'favicon' . DS )

    // token設定
    ,define( 'ACCESS_TOKEN_EXPIRATION_DATE', 3600 )
    ,define( 'REFRESH_TOKEN_EXPIRATION_DATE', 86400 * 365 ) // 一年

    // 暗号化iv,salt有効期限
    ,define( 'ENCRYPTION_PARAMETER_EXPIRATION_DATE', 3600 )

    // Cookie設定
    ,define( 'COOKIE_NAME',   'sumaiful' )
    ,define( 'COOKIE_EXPIRE', '+90 days' )

    // プッシュ証明書
    ,define( 'PUSH_CER_PATH', CONFIG . 'push_cer' . DS )
    ,define( 'PUSH_AUTHORITY_PEM_FILE', CONFIG . 'entrust_root_certification_authority.pem' )
    ,define( 'PUSH_LOCK_FILE', '/tmp/pro_push_lock' )

    ,define( 'PUSH_API_KEY',      'AAAAtrkG000:APA91bH_5_kUZmdm3qT2BhLnJR72Afu-MDTJe1sKEcM0nncivtu1XCr4BJ8zo6rVDLnSEkd1PiqDcWuknxtvnRrRBvdnE3f6DrYk7ZUc70LiKFVhZmOvhbrWNeFGM2JNHJP9MzLpZIUi' )
    ,define( 'PUSH_ANDROID_URL',  'https://fcm.googleapis.com/fcm/send' )

    // chat設定
    ,define( 'WSS', 'chats' ) // TOOD.動的にする予定
    ,define( 'CHAT_MESSAGE_LIMIT', 50 )

    ,define( 'CAKE_APP_DIR',    '/home/nginx/htdocs/pro-sumaiful/' )
    ,define( 'CHAT_IMAGE_DIR',  ROOT . DS . 'webroot/img/chat/' )
    ,define( 'CHAT_AUDIO_DIR',  ROOT . DS . 'webroot/audio/chat/' )
    ,define( 'CHAT_VIDEO_DIR',  ROOT . DS . 'webroot/video/chat/' )
    ,define( 'CHAT_IMAGE_PATH', '/img/chat/' )
    ,define( 'CHAT_AUDIO_PATH', '/audio/chat/' )
    ,define( 'CHAT_VIDEO_PATH', '/video/chat/' )

    ,define( 'INFORMATION_LIMIT', 10 )
    ,define( 'CONTENT_PAGE_LIMIT', 20 )
    ,define( 'GENERAL_PAGE_LIMIT', 20 )
];
