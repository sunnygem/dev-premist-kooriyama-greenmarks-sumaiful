<?php
return [
    'nextActive' => '<li class="next"><a rel="next" href="{{url}}"><span class="arrow"></span>{{text}}</a></li>',
    'nextDisabled' => '<li class="next disabled"><a href="" onclick="return false;"><span class="arrow"></span>{{text}}</a></li>',
    'prevActive' => '<li class="prev"><a rel="prev" href="{{url}}"><span class="arrow"></span>{{text}}</a></li>',
    'prevDisabled' => '<li class="prev disabled"><a href="" onclick="return false;"><span class="arrow"></span>{{text}}</a></li>',
 ];
