<?php
use Cake\Core\Configure;

return [
    define( 'SYSTEM_DOMAIN',     'dev-premist-kooriyama-greenmarks-system-sumaiful.sgx.jp' )
    ,define( 'SYSTEM_SITE_NAME', 'DEV システム管理画面' )
    ,define( 'ADMIN_SITE_NAME',  'DEV 管理画面' )

    ,define( 'SHELL_COMMAND', ROOT . DS . 'bin' . DS . 'cake' )

    ,define( 'FAVICON_PATH', WWW_ROOT . 'favicon' . DS )

    // token設定
    ,define( 'ACCESS_TOKEN_EXPIRATION_DATE', 3600 )
    ,define( 'REFRESH_TOKEN_EXPIRATION_DATE', 86400 * 14 ) // 2週間

    // 暗号化iv,salt有効期限
    ,define( 'ENCRYPTION_PARAMETER_EXPIRATION_DATE', 3600 )

    // Cookie設定
    ,define( 'COOKIE_NAME',   'sumaiful' )
    ,define( 'COOKIE_EXPIRE', '+90 days' )

    // プッシュ証明書
    ,define( 'PUSH_CER_PATH', CONFIG . 'push_cer' . DS )
    ,define( 'PUSH_AUTHORITY_PEM_FILE', CONFIG . 'entrust_root_certification_authority.pem' )
    ,define( 'PUSH_LOCK_FILE', '/tmp/dev_push_lock' )

    ,define( 'PUSH_API_KEY',      'AAAAKBE4wxM:APA91bE_TQBLuNZ5HTPs9LmFWlb6W5oaJ69b4FhpR4Ax9QhIhd1u98a94RRzmRdmsZSgvJCVyznA8w_HSN7Jxz6BHzsio8yYc-n5u-S0RxDfYH-Lggfqy1pIatVLlcrmuLT-SyIsVmYq' )
    ,define( 'PUSH_ANDROID_URL',  'https://fcm.googleapis.com/fcm/send' )

    // chat設定
    ,define( 'WSS', 'chats' ) // TOOD.動的にする予定
    ,define( 'CHAT_MESSAGE_LIMIT', 50 )

    ,define( 'CAKE_APP_DIR',    '/home/nginx/htdocs/dev-premist-kooriyama-greenmarks-sumaiful/' )
    ,define( 'CHAT_IMAGE_DIR',  ROOT . DS . 'webroot/img/chat/' )
    ,define( 'CHAT_AUDIO_DIR',  ROOT . DS . 'webroot/audio/chat/' )
    ,define( 'CHAT_VIDEO_DIR',  ROOT . DS . 'webroot/video/chat/' )
    ,define( 'CHAT_IMAGE_PATH', '/img/chat/' )
    ,define( 'CHAT_AUDIO_PATH', '/audio/chat/' )
    ,define( 'CHAT_VIDEO_PATH', '/video/chat/' )

    ,define( 'INFORMATION_LIMIT', 10 )
    ,define( 'CONTENT_PAGE_LIMIT', 20 )
    ,define( 'GENERAL_PAGE_LIMIT', 20 )
];
