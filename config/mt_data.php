<?php
return [
    'mt_residence_type' => [
        0 => '契約'
        ,1 => '入居'
        ,2 => '検討'
    ],

    'mt_information_type' => [
        'general'         => '一般'
        ,'event'          => 'イベント'
        ,'line_open_chat' => 'LINE オープンチャット'
    ],

    'mt_point' => [
        'access_point'  => 1,
        'post_point'    => 1,
        'comment_point' => 1,
        'like_point'    => 1,
    ],

    'mt_point_popup_message' => [
        'message'  => 'ポイントアップ2倍キャンペーン中'
    ],
    
    'mt_first_campaign_fisrt_login_point' => [
        'campaign_point'  => 20
    ],
    
    'mt_campaign_date' => [
        'start_date'  => '2021-03-27 00:00:00'
        ,'end_date'  => '2021-04-11 00:00:00'
    ],

    'mt_equipment_category' => [
        1 => '生活用品',
        2 => 'レンタルサイクル',
        3 => 'ルーム',
    ],

    'mt_faq_category' => [
        1 => 'アプリ全体について',
        2 => 'ポイントについて',
        3 => '投稿について',
        4 => '備品の貸し出しについて',
        5 => 'オープンチャットについて'
    ],
];
