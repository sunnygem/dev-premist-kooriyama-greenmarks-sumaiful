<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateFaqs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('faqs');
        $table->addColumn('contractant_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]) 
        ->addColumn('faqs_question_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('answer', 'text', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('answer_en', 'text', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('icon_url_path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
        ->addColumn('display_flg', 'integer', [
            'default' => 1,
            'limit' => MysqlAdapter::INT_TINY,
            'null' => true,
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
