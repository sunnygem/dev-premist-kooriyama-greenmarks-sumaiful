<?php
use Migrations\AbstractMigration;

class AddTypeToInformations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('informations');

        // 使ってないtypeがすでに存在していたのでchangeColumnします。
        $table->changeColumn('type', 'string', [
            'default' => null,
            'limit' => 20,
            'null' => true,
        ]);
        $table->update();
    }
}
