<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateFaqsQuestions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('faqs_questions');        
        $table->addColumn('contractant_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])
        ->addColumn('faq_category_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
        ->addColumn('question', 'text', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('question_en', 'text', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('display_flg', 'integer', [
            'default' => 1,
            'limit' => MysqlAdapter::INT_TINY,
            'null' => true,
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
