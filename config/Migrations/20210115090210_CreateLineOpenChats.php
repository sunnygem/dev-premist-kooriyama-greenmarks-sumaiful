<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateLineOpenChats extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('line_open_chats');

        $table->addColumn('contractant_id', 'integer', [
            'default' => null,
        ])
        ->addColumn('line_open_chat_category_id', 'integer', [
            'default' => null,
        ])
        ->addColumn('title', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
        ->addColumn('url', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
        ->addColumn('caption', 'text', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('icon_url_path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])
        ->addColumn('display_flg', 'integer', [
            'default' => 1,
            'limit' => MysqlAdapter::INT_TINY,
            'null' => true,
        ])
        ->addColumn('pickup_flg', 'integer', [
            'default' => 0,
            'limit' => MysqlAdapter::INT_TINY,
            'null' => true,
        ])
        ->addColumn('release_date', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('close_date', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->create();
    }
}
