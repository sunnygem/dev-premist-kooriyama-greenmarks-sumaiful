<?php
use Migrations\AbstractMigration;

class AddPointToFrontUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('front_users');
        $table->addColumn('point', 'integer', [
            'default' => 0,
            'null'    => false,
            'after'   => 'push_image_sharing_flg'
        ])
        ->addColumn('last_access_date', 'datetime', [
            'default' => null,
            'null'    => true,
            'after'   => 'point'
        ])
        ->addColumn('last_post_date', 'datetime', [
            'default' => null,
            'null'    => true,
            'after'   => 'last_access_date'
        ])
        ->addColumn('last_comment_date', 'datetime', [
            'default' => null,
            'null'    => true,
            'after'   => 'last_post_date'
        ])
        ->addColumn('last_like_date', 'datetime', [
            'default' => null,
            'null'    => true,
            'after'   => 'last_comment_date'
        ]);
        $table->update();
    }
}
