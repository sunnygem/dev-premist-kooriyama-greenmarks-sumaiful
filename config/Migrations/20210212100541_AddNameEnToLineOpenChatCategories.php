<?php
use Migrations\AbstractMigration;

class AddNameEnToLineOpenChatCategories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('line_open_chat_categories');
        $table
        ->addColumn('name_en', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->update();
    }
}
