<?php
// 管理画面用定数の設定
use Cake\Core\Configure;

return [
    define( 'SYSTEM_ADMIN', 'system_admin' )
    ,define( 'ADMIN',       'admin' )
    ,define( 'GENERAL',     'general' )
];
