<?php
use Migrations\AbstractSeed;

/**
 * LineOpenChats seed.
 */
class LineOpenChatsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 1,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/IGyZBFj74bkcPUi7SPPHPw?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
                'pickup_flg'     => 1
            ],
            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 2,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
                'pickup_flg'     => 1
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 3,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/IGyZBFj74bkcPUi7SPPHPw?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 4,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 5,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/IGyZBFj74bkcPUi7SPPHPw?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 6,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 7,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => date('Y-m-d H:i:s'),
                'release_date'    => date('Y-m-d H:i:s'),
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 8,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => '2021-01-01 00:00:00',
                'release_date'    => '2021-01-01 00:00:00',
                'pickup_flg'     => 1
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 9,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => '2021-01-01 00:00:00',
                'release_date'    => '2021-01-01 00:00:00',
                'pickup_flg'     => 1
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 10,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => '2021-01-01 00:00:00',
                'release_date'    => '2021-01-01 00:00:00',
            ],            [
                'contractant_id' => 4,
                'line_open_chat_category_id' => 11,
                'title'          => 'testtest',
                'url'            => 'https://line.me/ti/g2/EHkpYDZcgajlo9c3LYR7Xg?utm_source=invitation&utm_medium=link_copy&utm_campaign=default',
                'created'        => '2021-01-01 00:00:00',
                'release_date'    => '2021-01-01 00:00:00',
            ],
        ];

        $table = $this->table('line_open_chats');
        $table->insert($data)->save();
    }
}
