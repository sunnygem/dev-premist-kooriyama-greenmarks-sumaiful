<?php
use Migrations\AbstractSeed;

/**
 * LineOpenChatCategories seed.
 */
class LineOpenChatCategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'contractant_id' => 4,
                'name' => 'アニメ・漫画',
                'orderby' => 1,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '音楽',
                'orderby' => 2,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '映画・舞台',
                'orderby' => 3,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => 'TV・VOD',
                'orderby' => 4,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => 'スポーツ',
                'orderby' => 5,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '料理・グルメ',
                'orderby' => 6,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '写真',
                'orderby' => 7,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '本',
                'orderby' => 8,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '働き方・仕事',
                'orderby' => 9,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '地域・暮らし',
                'orderby' => 10,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '同世代',
                'orderby' => 11,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '学校・同窓会',
                'orderby' => 12,
                'created' => date('Y-m-d H:i:s')
            ],
            [
                'contractant_id' => 4,
                'name' => '研究・学習',
                'orderby' => 13,
                'created' => date('Y-m-d H:i:s')
            ],
          
        ];

        $table = $this->table('line_open_chat_categories');
        $table->insert($data)->save();
    }
}
