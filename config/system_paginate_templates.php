<?php
return [
    'nextActive'   => '<span class="next"><a rel="next" href="{{url}}">{{text}}</a></span>',
    'nextDisabled' => '<span class="next disabled">{{text}}</span>',
    'prevActive'   => '<span class="prev"><a rel="prev" href="{{url}}">{{text}}</a></span>',
    'prevDisabled' => '<span class="prev disabled">{{text}}</span>',
    'number'       => '<span><a href="{{url}}">{{text}}</a></span>',
    'current'      => '<span class="current">{{text}}</span>',
    'first'        => '<span><a href="{{url}}">{{text}}</a></span>',
    'last'         => '<span><a href="{{url}}">{{text}}</a></span>',
];
