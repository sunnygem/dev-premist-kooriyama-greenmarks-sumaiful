<?php
namespace App\Service;

/**
 * mvcに拠らない共通メソッドがあれば
 * こちらに書いてみる
**/

class CommonService
{
    // urlがあったら<a>で囲って出力
    public function filterUrlStr( $str=null, $parrten=null )
    {
        $pattern = ( $parrten === null ) ? "/((?:https?|ftp):\/\/[-_.!~*\'()a-zA-Z0-9;\/?:@&=+$,%#]+)/" : $parrten;
        $replace = '<a href="$1" target="_blank" rel="noopener noreferrer">$1</a>';
        return preg_replace( $pattern, $replace, $str );
    }
}

