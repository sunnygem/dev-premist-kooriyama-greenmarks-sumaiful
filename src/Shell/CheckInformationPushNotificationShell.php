<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Log\Log;

class CheckInformationPushNotificationShell extends Shell
{
    public function main()
    {
        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') InformationPushNotification Batch Start ---', 'cli-debug');

        $this->Contractants = TableRegistry::getTableLocator()->get( 'Contractants' );
        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $this->Informations            = TableRegistry::getTableLocator()->get( 'Informations' );
        $this->LabelInformations       = TableRegistry::getTableLocator()->get( 'LabelInformations' );

        $res = $this->Contractants->find()
            ->where([
                'deleted IS' => null
                ,'status_id'  => 1 // 契約中
            ]);

        if ( $res->isEmpty() === false )
        {
            foreach( $res as $key => $val )
            {
                $res2 = $this->ContractantServiceMenus->getServiceMenuIdsByContractId( $val->id );

                // インフォメーション(本文あり)が有効になっているか
                // インフォメーション(ラベルのみ)が有効になっているか
                if( in_array( 14, $res2, true ) || in_array( 13, $res2, true ) )
                {
                    // 通知してないデータがあるか
                    if ( in_array( 14, $res2, true ) )
                    {
                       $informations = $this->Informations->getUnannouncedData( $val->id ); 
                    }
                    else if ( in_array( 13, $res2, true ) )
                    {
                       $informations = $this->LabelInformations->getUnannouncedData( $val->id ); 
                    }

                    if( count( $informations ) > 0 )
                    {
                        // PUSH通知
                        $cmd = SHELL_COMMAND . ' push ' . $val->unique_parameter . ' information ' . $val->id;
                        exec( $cmd, $out, $ret );

                        if( $ret === 0 )
                        {
                            $set = [ 'notified_flg' => 1 ];
                            $conditions = [
                                'contractant_id' => $val->id
                                ,'display_flg'   => 1
                                ,'notified_flg'  => 0
                                ,'release_date <='  => date( 'Y-m-d H:i:s' )
                            ];
                            // 通知済みにする
                            if ( in_array( 14, $res2, true ) )
                            {
                                $this->Informations->updateAll( $set, $conditions );
                            }
                            else if ( in_array( 13, $res2, true ) )
                            {
                                $this->LabelInformations->updateAll( $set, $conditions );
                            }
                            Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') InformationPushNotification Batch Success ---', 'cli-debug');
                        }
                        else
                        {
                            $err_msg = '';
                            foreach( $out as $val )
                            {
                                $err_msg .= strip_tags( $val ) . "\n";
                            }
                            Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') InformationPushNotification Batch Error ---' . $err_msg, 'cli-error');
                        }

                    }
                }
                
            }
        }

        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') InformationPushNotification Batch End ---', 'cli-debug');
    }
}
