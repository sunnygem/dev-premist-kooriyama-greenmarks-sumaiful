<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Log\Log;

class CheckImageSharingPushNotificationShell extends Shell
{
    public function main()
    {
        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') ImageSharingPushNotification Batch Start ---', 'cli-debug');

        $this->Contractants = TableRegistry::getTableLocator()->get( 'Contractants' );
        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $this->ImageSharings           = TableRegistry::getTableLocator()->get( 'ImageSharings' );

        $res = $this->Contractants->find()
            ->where([
                'deleted IS' => null
                ,'status_id'  => 1 // 契約中
            ]);

        if ( $res->isEmpty() === false )
        {
            foreach( $res as $key => $val )
            {
                 $res2 = $this->ContractantServiceMenus->getServiceMenuIdsByContractId( $val->id );

                 // インフォメーション(本文あり)が有効になっているか
                 if( in_array( 15, $res2, true ) )
                 {
                     // 通知してないデータがあるか
                     $image_sharings = $this->ImageSharings->getUnannouncedData( $val->id ); 
                     if( count( $image_sharings ) > 0 )
                     {
                         // PUSH通知
                         $cmd = SHELL_COMMAND . ' push ' . $val->unique_parameter . ' image_sharing ' . $val->id;
                         exec( $cmd, $out, $ret );

                         if( $ret === 0 )
                         {
                             // 通知済みにする
                             $this->ImageSharings->updateAll(
                                 [
                                     'notified_flg' => 1
                                 ]
                                 ,[

                                     'contractant_id' => $val->id
                                     ,'display_flg'   => 1
                                     ,'notified_flg'  => 0
                                     ,'release_date <=' => date( 'Y-m-d H:i:s' )
                                 ]
                             );
                             Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') ImageSharingPushNotification Batch Success ---', 'cli-debug');
                         }
                         else
                         {
                             $err_msg = '';
                             foreach( $out as $val )
                             {
                                 $err_msg .= strip_tags( $val ) . "\n";
                             }
                             Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') ImageSharingPushNotification Batch Error ---' . $err_msg, 'cli-error');
                         }

                     }
                 }
                
            }
        }

        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') ImageSharingnPushNotification Batch End ---', 'cli-debug');
    }
}
