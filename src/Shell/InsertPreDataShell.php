<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Log\Log;

use App\Controller\AppController;

/****
 **
 **  $this->arg[0] => テーブル
 **
 ***/
class InsertPreDataShell extends Shell
{
    public $_contractant_id;
    public $_building_id;

    public function main()
    {
        $this->_contractant_id = 4;
        $this->_building_id    = 4;
        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Insert Pre Data Start ---', 'push');
        try
        {
    
            if ( isset( $this->args[0] ) )
            {
                $type = $this->args[0];
                switch( $type )
                {
                case 'image_sharings':
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') ImageSharings---', 'debug');
                    $ImageSharings        = TableRegistry::getTableLocator()->get('ImageSharings');
                    $ImageSharingBuildings = TableRegistry::getTableLocator()->get('ImageSharingBuildings');
                    $res = $ImageSharings->find()->select(['id'])->where([
                        'contractant_id' => $this->_contractant_id
                        ,'all_building_flg' => 0
                        ,'deleted IS' => null
                    ])
                    ->all();

                    foreach( $res as $val )
                    {
                        $data = [
                            'contractant_id'    => $this->_contractant_id
                            ,'image_sharing_id' => $val->id
                            ,'building_id'      => $this->_building_id
                        ];
                        debug( $data );

                        $ImageSharingBuildings->saveData( $data );
                    }

                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') ImageSharings--- END---', 'push');

                    break;

                case 'presentations':
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Presentations Start---', 'push');
                    $Presentations = TableRegistry::getTableLocator()->get('Presentations');
                    $PresentationBuildings = TableRegistry::getTableLocator()->get('PresentationBuildings');
                    $res = $Presentations->find()->select(['id'])->where([
                        'contractant_id' => $this->_contractant_id
                        ,'deleted IS' => null
                    ])
                    ->all();

                    foreach( $res as $val )
                    {
                        $data = [
                            'contractant_id'    => $this->_contractant_id
                            ,'presentation_id'  => $val->id
                            ,'building_id'      => $this->_building_id
                        ];
                        debug( $data );

                        $PresentationBuildings->saveData( $data );
                    }


                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Presentations End---', 'push');
                    break;

                case 'informations':
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Informations Start---', 'push');
                    $Informations = TableRegistry::getTableLocator()->get('Informations');
                    $InformationBuildings = TableRegistry::getTableLocator()->get('InformationBuildings');
                    $res = $Informations->find()->select(['id'])->where([
                        'contractant_id'    => $this->_contractant_id
                        ,'all_building_flg' => 0
                        ,'deleted IS'       => null
                    ])
                    ->all();

                    foreach( $res as $val )
                    {
                        $data = [
                            'contractant_id'    => $this->_contractant_id
                            ,'information_id'  => $val->id
                            ,'building_id'      => $this->_building_id
                        ];
                        debug( $data );

                        $InformationBuildings->saveData( $data );
                    }

                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Informations END---', 'push');

                    break;

                default:
                    exit( 'error' );
                    break;
                }
            }
        }
        catch( \Exception $e )
        {
            var_dump($e->getMessage());
            Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Exception::' . $e->getMessage(), 'debug');
        }
    }

}
