<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Log\Log;

use App\Controller\AppController;

/****
 **
 **  $this->arg[0] => ユニークパラメータ
 **  $this->arg[1] => pushタイプ
 **  $this->arg[3] => ターゲットのIDなど 
 **
 ***/
class PushShell extends Shell
{
    public $_push_pem_file;
    public $_contractant;
    public $_contractant_id;
    public $_unique_parameter;
    static private $_ios_exec_flg = true;

    public function main()
    {
        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Start ---', 'push');

        // PUSH証明書の確認
        $this->_unique_parameter = $this->args[0];
        $this->_push_pem_file = PUSH_CER_PATH . ENV . '_' . $this->_unique_parameter . '.pem';
        if( file_exists( $this->_push_pem_file ) === false )
        {
            Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') push証明書がありません', 'push');
            self::$_ios_exec_flg = false;
        }

        try
        {
    
            // テスト用
            //if ( count( $this->args ) === 2 )
            //{
            //    $os              = $this->args[0];
            //    $user_id = $this->args[1];
    
    
            //    $device_tokens = [];
            //    if ( $user_id !== null )
            //    {
            //        $res = $this->Users->findById( $user_id );
            //        if ( $res === false ) return;
            //        $device_tokens = [ $res->device_token ];
            //        $this->{$os}( $device_tokens, [ 'push-test ' . date( 'Y-m-d H:i:s' ) ] );
            //    }
            //    else
            //    {
            //        // pushする対象のdevice_tokenを取得する
            //        // 対象があった場合のみ、処理を実行する
            //    }
    
            //}
            if ( isset( $this->args[1] ) )
            {
                switch( $this->args[1] )
                {
                case 'luggage_strage':
                    $front_user_id = $this->args[2];
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Luggage Start---', 'push');
                    self::luggage_strage( $front_user_id );
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Luggage End---', 'push');
                    break;
                case 'post_activity':
                    $post_activity_id = $this->args[2];
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch PostActivity Check Start---', 'push');
                    self::post_activity( $post_activity_id );
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch PostActivity Check End---', 'push');
                    break;
                case 'information':
                    $contractant_id = $this->args[2];
                    $this->_contractant_id = $contractant_id;
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Information Check Start---', 'push');
                    self::information( $contractant_id );
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Information Check End---', 'push');
                    break;
                case 'image_sharing':
                    $contractant_id = $this->args[2];
                    $this->_contractant_id = $contractant_id;
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Image Sharing Check Start---', 'push');
                    self::image_sharing( $contractant_id );
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Image Sharing Check End---', 'push');
                    break;
                case 'message':
                    $chat_messages_id = $this->args[2];
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Massage Check Start---', 'push');
                    self::message( $chat_messages_id );
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Message Check End---', 'push');
                    break;
                case 'unread':
                    $contractant_id = $this->args[2];
                    $this->_contractant_id = $contractant_id;
                    $chat_group_id  = $this->args[3];
                    $thread_id      = $this->args[4];
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Unread Check Start---', 'push');
                    self::unread( $contractant_id, $chat_group_id, $thread_id );
                    Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Push Batch Unread Check End---', 'push');
                    break;
 
                default:
                    exit( 'error' );
                    break;
                }
                //$this->{$os}( $device_tokens, $messages );
            }
            // lockファイルの削除
        }
        catch( \Exception $e )
        {
            var_dump($e->getMessage());
            Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') Exception::' . $e->getMessage(), 'push');
            // lockファイルの削除
        }
    }

    // 荷物の預かりを対象の居住者にpush通知を行う
    private function luggage_strage( $front_user_id )
    {
        $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );
        $front_user = $this->FrontUsers->findById( $front_user_id )->first();
        $this->_contractant_id = $front_user->contractant_id;
        if( $front_user->push_baggage_flg === 1 )
        {
            $this->UserDeviceTokens = TableRegistry::getTableLocator()->get( 'UserDeviceTokens' );
            $data = $this->UserDeviceTokens->findByFrontUserId( $front_user_id );

            $pushDataLists = self::createPushDataList( $data, 'luggage_strage' );
            $this->pushExec( $pushDataLists );
        }
    }
    
    // 投稿へのアクティビティを push通知する
    private function post_activity( $post_activity_id )
    {
        $this->PostActivityLogs = TableRegistry::getTableLocator()->get( 'PostActivityLogs' );
        $res = $this->PostActivityLogs->getPushData( $post_activity_id );

        $pushDataLists = self::createPushDataList( $res, 'post_activity' );
        $this->pushExec( $pushDataLists );
    }

    // 運営からのインフォメーション追加のお知らせ push通知する
    private function information( $contractant_id )
    {
        $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );

        if( $this->_unique_parameter === 'willing' )
        {
            // 建物の公開範囲をチェックする
            $Informations = TableRegistry::getTableLocator()->get( 'Informations' );
            $informations = $Informations->getUnannouncedData( $contractant_id );

            foreach( $informations as $val )
            {
                if( $val && $val->all_building_flg === 1 )
                {
                    $Buildings = TableRegistry::getTableLocator()->get( 'Buildings' );
                    $buildings = $Buildings->getOptions( $contractant_id );
                    $arr_building_id = array_keys( $buildings );
                }
                elseif( count( $val->information_buildings ) > 0 )
                {
                    $arr_building_id = array_column( $val->information_buildings, 'building_id' );
                }
                $res = $this->FrontUsers->getPushDataByBuildingId( $arr_building_id, 'information' );
                $pushDataLists = self::createPushDataList( $res, 'information' );
                $this->pushExec( $pushDataLists );
            }

        }
        else
        {
            $res = $this->FrontUsers->getPushData( $contractant_id, 'information' );

            $pushDataLists = self::createPushDataList( $res, 'information' );
            $this->pushExec( $pushDataLists );
        }
    }

    // 運営からのアルバム追加のお知らせ push通知する
    private function image_sharing( $contractant_id )
    {
        $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );
        if( $this->_unique_parameter === 'willing' )
        {
            // 建物の公開範囲をチェックする
            $ImageSharings = TableRegistry::getTableLocator()->get( 'ImageSharings' );
            $image_sharings = $ImageSharings->getUnannouncedData( $contractant_id );

            foreach( $image_sharings as $val )
            {
                if( $val && $val->all_building_flg === 1 )
                {
                    $Buildings = TableRegistry::getTableLocator()->get( 'Buildings' );
                    $buildings = $Buildings->getOptions( $contractant_id );
                    $arr_building_id = array_keys( $buildings );
                }
                elseif( count( $val->image_sharing_buildings ) > 0 )
                {
                    $arr_building_id = array_column( $val->image_sharing_buildings, 'building_id' );
                }
                $res = $this->FrontUsers->getPushDataByBuildingId( $arr_building_id, 'image_sharing' );
                $pushDataLists = self::createPushDataList( $res, 'image_sharing' );
                $this->pushExec( $pushDataLists );
            }
        }
        else
        {
            $res = $this->FrontUsers->getPushData( $contractant_id, 'image_sharing' );
            $pushDataLists = self::createPushDataList( $res, 'image_sharing' );
            $this->pushExec( $pushDataLists );
        }
    }

    // 新茶メッセージをpush通知する
    private function message( $chat_messages_id )
    {
        $this->ChatMessages = TableRegistry::getTableLocator()->get( 'ChatMessages' );
        $this->ChatGroupMembers = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $objChatMessage = $this->ChatMessages->get( $chat_messages_id );
        $res = $this->ChatGroupMembers->getPushDataByObjChatMessage( $objChatMessage );

        $pushDataLists = self::createPushDataList( $res, 'message' );
        $this->pushExec( $pushDataLists );
    }
    // 未読をpush通知する
    private function unread( $contractant_id, $chat_group_id, $thread_id )
    {
        $this->ChatUnreads = TableRegistry::getTableLocator()->get( 'ChatUnreads' );
        $this->ChatGroupMembers = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->ChatMemberlists  = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
        $this->UserDeviceTokens = TableRegistry::getTableLocator()->get( 'UserDeviceTokens' );
        $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );
        $res = $this->ChatUnreads->getChatUnreadDeviceTokenData( $contractant_id, $chat_group_id, $thread_id );
        $chat_members = $this->ChatGroupMembers->getChatGroupMembers( $contractant_id, $chat_group_id );
        foreach( $chat_members as $key => $val )
        {
            $front_user = $this->FrontUsers->findById( $val->front_user_id )->first();
            if( $front_user->push_message_flg === 1 )
            {
                // 未読があるかどうかチェックする
                if ( $this->ChatMemberlists->check_unread( $contractant_id, $val->user_id, $val->front_user_id ) )
                {
                    $data = $this->UserDeviceTokens->findByFrontUserId( $val->front_user_id );
                    $pushDataLists = self::createPushDataList( $data, 'unread' );
                    $this->pushExec( $pushDataLists );
                }
            }
        }
//Log::debug($res, 'push');
    }


    // push用のデータリストを作成する
    private function createPushDataList( $data, $case )
    {
        $ios_device_tokens     = [];
        $android_device_tokens = [];
        $ios_messages          = [];
        $android_messages      = [];
        if ( ( is_object( $data ) && $data->isEmpty() === false ) || ( is_array( $data ) && count( $data ) > 0 ) )
        {
            foreach( $data as $val )
            {
                switch( $case )
                {
                case 'luggage_strage':
                    $os           = $val->os;
                    $device_token = $val->device_token;
                    $message      = "荷物が届きました。" . "\n" . 'The package arrived';
                    break;
                case 'post_activity':
                    $os           = $val->target_user->front_user->user_device_token->os;
                    $device_token = $val->target_user->front_user->user_device_token->device_token;

                    $this->_contractant_id = $val->contractant_id;

                    switch( $val->activity_type )
                    {
                    case 'like':
                        $message  = 'あなたの投稿に' . $val->user->front_user->nickname . 'さんがいいねをつけました。' . "\n" . $val->user->front_user->nickname . ' liked your post.';
                        break;
                    case 'comment':
                        $message  = 'あなたの投稿に' . $val->user->front_user->nickname . 'さんがコメントをしました。' . "\n" . $val->user->front_user->nickname . ' commented on your post.';
                        break;
                    case 'join':
                        $message  = 'あなたの投稿(イベント)に' . $val->user->front_user->nickname . 'さんが参加しました。' . "\n" . $val->user->front_user->nickname . ' joined your post(event).';
                        break;
                    case 'comment_like':
                        $message  = 'あなたのコメントに' . $val->user->front_user->nickname . 'さんがいいねをつけました。'. "\n" . $val->user->front_user->nickname . ' liked your comment.';
                        break;
                    }
                    break;
                case 'information':
                    $os           = $val->user_device_token->os;
                    $device_token = $val->user_device_token->device_token;
                    $message      = "Informationが追加されました。" . "\n" . 'Information has been added.';
                    break;
                case 'image_sharing':
                    $os           = $val->user_device_token->os;
                    $device_token = $val->user_device_token->device_token;
                    $message      = "アルバムが追加されました。" . "\n" . 'Album has been added.';
                    break;
                case 'message':
                    $os           = $val->user->front_user->os;
                    $device_token = $val->user->front_user->device_token;
                    $message      = $val->chat_message->user->front_user->nickname . "さんからメッセージ\n" . $val->chat_message->message;
                    break;
                case 'unread':
                    $os           = $val->os;
                    $device_token = $val->device_token;
                    $message      = 'メッセージが届きました' . "\n" . 'received a message.';
                    break;
                }

                if ( isset( $device_token ) && strlen( trim( $device_token ) ) === 0 ) continue;

                switch( $os )
                {
                case 'ios':
                    $ios_device_tokens[] = $device_token;
                    $ios_messages[]      = $message;
                    break;
                case 'android':
                    $android_device_tokens[] = $device_token;
                    $android_messages[]      = $message;
                    break;
                }
            }
        }
        return [ $ios_device_tokens, $android_device_tokens, $ios_messages, $android_messages ];
    }
    // pushの実行
    /*
     * data: array
     *          0: ios_device_tokens
     *          1: android_device_tokens
     *          2: ios_messages
     *          3: android_messages
     */
    private function pushExec( $data )
    {
        // ios push
        if ( count( $data[0] ) !== 0 )
        {
            $this->ios( $data[0], $data[2] );
        }
        // android push
        if ( count( $data[1] ) !== 0 )
        {
            $this->android( $data[1], $data[3] );
        }
    }

    private function ios( $device_tokens, $messages )
    {
        if ( self::$_ios_exec_flg === false ) return;
        switch( Configure::read( 'ENV' ) )
        {
            case 'dev':
            case 'test':
            case 'stg':
                $push = new \ApnsPHP_Push(
                    \ApnsPHP_Abstract::ENVIRONMENT_SANDBOX
                    ,$this->_push_pem_file
                 );
            break;
            case 'pro':
                $push = new \ApnsPHP_Push(
                    \ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION
                    ,$this->_push_pem_file
                 );
            break;
        }
        $push->setRootCertificationAuthority( PUSH_AUTHORITY_PEM_FILE );
        $push->connect();

        //$device_token = '1426942236ad66ae61efbaf0d4283de2131285249e7f942e9eca1f9f21ed0bf0';
        //$device_token = '3c9ab393bcfd714ffec9aa686477b13f99d9a7821a298b150a876d0a7fcf8c0a';
        //device_tokens = [
        //    '1426942236ad66ae61efbaf0d4283de2131285249e7f942e9eca1f9f21ed0bf0'
        //    //,'3c9ab393bcfd714ffec9aa686477b13f99d9a7821a298b150a876d0a7fcf8c0a'
        //];

        // ---- ここからループ
        foreach( $device_tokens as $key => $device_token )
        {
            $message = new \ApnsPHP_Message( $device_token );
            $message->setContentAvailable( true );
            $message->setExpiry(30); // 有効期限
            $message->setBadge(1); // バッジ
            $message->setSound();
            $message->setText( $messages[$key] );

            $push->add( $message );
            $push->send();
        }

        // ---- ここまで
        $push->disconnect();
    }

    private function android( $device_tokens, $messages )
    {
        if ( $this->_contractant_id !== null )
        {
            $this->Contractants = TableRegistry::getTableLocator()->get( 'Contractants' );
            $contractant_data = $this->Contractants->get( $this->_contractant_id );
            $api_key = ( strlen( $contractant_data->android_push_api_key ) > 0 ) ? $contractant_data->android_push_api_key : PUSH_API_KEY;
        }
        $headers = [
            'Authorization: key=' . $api_key
            ,'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, PUSH_ANDROID_URL );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        foreach( $device_tokens as $key => $device_token )
        {
            $fields = [
                'registration_ids' => [ $device_token ]
                ,'notification' => [
                    'text' => $messages[$key]
                ]
            ];
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec( $ch );
            Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ')' . $result, 'push');
            $res = json_decode( $result, true );
            // 失敗している場合、デバイストークンを削除する
            if ( $res['failure'] === 1 )
            {
                if ( $res['results'][0]['error'] === 'MismatchSenderId' || $res['results'][0]['error'] === 'NotRegistered' )
                {
                    $this->UserDeviceTokens = TableRegistry::getTableLocator()->get( 'UserDeviceTokens' );
                    if ( $this->_contractant_id !== null )
                    {
                        $this->UserDeviceTokens->deleteDeviceToken( $this->_contractant_id, $device_token );
                    }
                }
            }
        }

        curl_close( $ch );
        return $result;
    }

}
