<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class TermsOfServicesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
    }

    public function validation( $data=[] )
    {
        $err=[];

        return $err;
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getDataByContractantId( $contractant_id=null )
    {
        $res = $this->find()
            ->select([
                'id', 'content' ,'content_en', 'url', 'url_en'
            ])
            ->where([
                'contractant_id' => $contractant_id
            ])
            ->order(['id' => 'ASC'])
            ->first();

        return $res;
    }

}
