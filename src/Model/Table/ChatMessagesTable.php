<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Log\Log;
use App\Service\CommonService;

class ChatMessagesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Users');
        $this->hasMany('ChatGroupMembers', [
            'foreignKey'  => 'chat_group_id'
            ,'bindingKey' => 'chat_group_id'
        ]);
    }
    public function getData( $contractant_id, $chat_group_id, $contractant_service_menu_id, $thread_id, $page=1, $limit=null, $tag_flg=false )
    {
        $res = $this->find()
            ->where( [
                'ChatMessages.contractant_id'               => $contractant_id
                ,'ChatMessages.chat_group_id'               => $chat_group_id
                ,'ChatMessages.contractant_service_menu_id' => $contractant_service_menu_id
                ,'ChatMessages.thread_id'                   => $thread_id
                ,'ChatMessages.deleted IS'                  => null
            ] )
            ->contain( [
                'Users' => [
                    'fields' => [
                        'id', 'login_id', 'nickname' 
                    ]
                ]
                ,'Users.FrontUsers' => [
                    'fields' => [
                        'id', 'nickname' , 'thumbnail_path'
                    ]
                ]
            ] )
            ->order( [ 'ChatMessages.id' => 'DESC' ] );

        if ( $limit === null ) $limit = CHAT_MESSAGE_LIMIT;
        $res->limit( $limit )
            ->page( $page );

        // 返すときにmessageのタグの置換を実行
        if( $tag_flg === true )
        {
            $CommonService = new CommonService();
            $res->formatResults( function( $results ) use ( $CommonService ){
                return $results->map(function ($row) use ( $CommonService )  {
                    $row->message = $CommonService->filterUrlStr( $row->message );
                    return $row;
                });
            });
        }

        if ( $res->isEmpty() === false )
        {
            return $res->all();
            //$data = [];
            //foreach( $res->all() as $val )
            //{
            //    array_unshift( $data, $val );
            //}
            //return $data;
        }
        else
        {
            return null;
        }
    }
    public function saveData( $data, $parent_id=null )
    {
        return parent::saveData( $data );
    }

    // push用のデータを生成
    public function getPushDataById( $id=null )
    {
        $res = $this->find()
            ->where([
                'ChatMessages.id'          => $id
                ,'ChatMessages.deleted IS' => null
            ])
            ->contain([
                'ChatGroupMembers.Users.FrontUsers'
            ])
            ->all();

        //if( $res && count( $res->chat_group_members ) > 0 )
        //{
        //     foreach( $res->chat_group_members as $val )
        //     {
        //         
        //     }
        //}


        
        return [];
    }
}

