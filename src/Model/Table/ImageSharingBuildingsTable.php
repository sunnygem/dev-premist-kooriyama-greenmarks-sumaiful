<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ImageSharingBuildingsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('ImageSharings');
        $this->belongsTo('Buildings');
    }

    public function getEditDataByImageSharingId( $contractant_id, $id )
    {
         return $res = $this->find('list', [
                 'valueField' => 'building_id'
             ])
             ->where([
                 'contractant_id'  => $contractant_id
                 ,'image_sharing_id' => $id

             ])
             ->toList();
    }

}
