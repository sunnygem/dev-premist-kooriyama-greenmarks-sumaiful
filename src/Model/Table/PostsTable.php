<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Log\Log;

class PostsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('PostCategories');
        $this->belongsTo('Users', [
            'conditions' => [
                'Users.deleted IS' => null
            ]
        ]);
        $this->hasMany( 'PostLikes' );
        //$this->hasOne( 'FirstPostLikes', [
        //    'className'   => 'PostLikes'
        //    ,'foreignKey' => 'post_id'
        //    ,'conditions' => function (\Cake\Database\Expression\QueryExpression $exp, \Cake\ORM\Query $q) {
        //        $sub_q = $q->connection()
        //            ->newQuery()
        //            ->select(['MIN( created )'])
        //            ->from(['TmpPostLikes' => 'post_likes'])
        //            ->where([
        //                'TmpPostLikes.post_id = Posts.id'
        //                ,'TmpPostLikes.deleted IS' => null
        //            ]);
        //        return $exp->add([
        //            'FirstPostLikes.created =' => $sub_q
        //        ]);
        //    }
        //]);
        $this->hasMany('PostComments');
        $this->hasMany('PostJoins');

        $this->hasMany('RelPostFiles', [
            'conditions' => [
                'RelPostFiles.deleted IS' => null
            ]
        ]);

        // 公開範囲
        $this->hasMany('PostBuildings');
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['post_category_id'] ) && $data['post_category_id'] === '' )
        {
            $err['post_category_id'] = 'カテゴリーIDの取得に失敗しました。';
        }

        if( isset( $data['content'] ) && $this->check_str_length( $data['content'], 1000 ) )
        {
            $err['content'] = '1,000文字以内入力してください';
        }

        return $err;
    }

    public function api_validation( $data=[] )
    {
        $err = [];

        if( isset( $data['user_id'] ) === false )
        {
            $err['user_id'] = 'user_idがありません';
        }

        if( isset( $data['post_category_id'] ) === false )
        {
            $err['post_category_id'] = 'post_category_idがありません';
        }

        if( isset( $data['content'] ) === false )
        {
            $err['content'] = 'contentがありません';
        }

        //if( isset( $data['event_date'] ) === false )
        //{
        //    $err['event_date'] = 'contentがありません';
        //}

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'Posts.deleted IS' => null
            ])
            ->contain([
                'RelPostFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
                ,'Users.FrontUsers'
            ])
            ->order([
                'Posts.id' => 'ASC'
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;

    }

    public function saveData( $data )
    {
         $associated = [];
         if( isset( $data['post_buildings']['building_id'] ) )
         {
             // 全公開
             if( $data['post_buildings']['building_id'] === "" || in_array( '0', $data['post_buildings']['building_id'], true ) || count( $data['post_buildings']['building_id'] ) === 0 )
             {
                 unset( $data['post_buildings'] );
             }
             else
             {
                 $associated[] = 'PostBuildings';

                 $post_buildings = [];
                 foreach( $data['post_buildings']['building_id'] as $val )
                 {
                     $post_buildings[] = [
                         'building_id'    => $val
                         ,'contractant_id' => $data['contractant_id']
                    ];
                 }
                 $data['post_buildings'] = $post_buildings;
             }
         }
         $entitiy = $this->newEntity( $data, ['associated' => $associated ] );
         $res = $this->save( $entitiy );
         return $res->id;
    }
 
    public function getPosts( $where=[], $limit=null, $page=0 )
    {
        $q = $this->find()
            ->where([
                'Posts.deleted IS' => NULL
            ])
            ->select([
                'Posts.id'
                ,'Posts.building_id'
                ,'Posts.user_id'
                ,'Posts.front_user_id'
                ,'Posts.content'
                ,'Posts.created'
                ,'post_category_id'
                ,'Users.id'
                ,'Users.nickname'
                ,'FrontUsers.id'
                ,'FrontUsers.nickname'
                ,'FrontUsers.thumbnail_path'
                ,'FrontUsers.leave_flg'
                //,'FirstPostLikes.created'
                //,'FirstPostLikes.user_id'
                ,'PostCategories.label'
                ,'published_building_id' => 'PostBuildingsLeftJoin.building_id'
            ])
            ->contain([
                'PostCategories'
                ,'Users.FrontUsers'
                ,'Users.FrontUsers.Buildings' => [
                    'fields' => [ 'Buildings.name', 'Buildings.icon_path' ]
                ]
                // 画像
                ,'RelPostFiles' => [
                    'fields' => [ 'RelPostFiles.post_id', 'RelPostFiles.file_id' ]
                ]
                ,'RelPostFiles.Files' => [
                    'fields' => [ 'Files.id', 'Files.file_path' ]
                ]
                //いいね
                ,'PostLikes' => [
                    'fields' => [ 'post_id', 'user_id' ]
                    ,'conditions' => [
                        'PostLikes.deleted IS ' => null 
                        ,'FrontUsers.id IS NOT' => null
                    ]
                ]
                ,'PostLikes.Users' => [
                    'fields' => [ 'id' ]
                ]
                ,'PostLikes.Users.FrontUsers' => [
                    'fields' => [ 'leave_flg', 'nickname', 'thumbnail_path' ]
                ]
                // イイねの件数
                //,'PostLikes' => [
                //    'fields' => [ 'post_id', 'user_id'  ]
                //    ,'conditions' => [ 'PostLikes.deleted IS' => null ]
                //    ,'order' => [ '' ]
                //]
                // 最初に言い値をしたユーザー
                //,'FirstPostLikes'
                //,'FirstPostLikes.Users' => [
                //    'fields' => [ 'id', 'nickname' ]
                //]
                // 参加者
                ,'PostJoins' => [
                    'fields' => [ 'post_id', 'user_id' ]
                    ,'conditions' => [
                        'PostJoins.deleted IS' => null
                        ,'FrontUsers.id IS NOT' => null
                    ]
                ]
                ,'PostJoins.Users' => [
                    'fields' => [ 'id' ]
                ]
                ,'PostJoins.Users.FrontUsers' => [
                    'fields' => [ 'leave_flg', 'nickname', 'thumbnail_path' ]
                ]
                //コメント
                ,'PostComments' => [
                    'fields' => [ 'id', 'post_id', 'comment' ]
                    ,'conditions' => [
                        'PostComments.deleted IS ' => null 
                        ,'FrontUsers.id IS NOT' => null
                    ]
                    ,'sort' => [ 'PostComments.created' => 'ASC' ]
                ]
                ,'PostComments.Users.FrontUsers' => [
                    'fields' => [ 'Users.id', 'FrontUsers.user_id', 'FrontUsers.nickname' ]
                ]
           ])
           ->group('Posts.id');

        // 物件公開範囲
        $q->join([
            'table'  => 'post_buildings'
            ,'alias' => 'PostBuildingsLeftJoin'
            ,'type'  => 'LEFT'
            ,'conditions' => [
                'PostBuildingsLeftJoin.Post_id = Posts.id'
            ]
        ]);


        if( isset( $where['Posts.id'] ) )
        {
            $q->where( $where );
            return  $q->first();
        }
        else
        {
            $q->order(['Posts.created' => 'DESC']);

            if( count( $where ) > 0 ) $q->where( $where );
            if( $limit !== null ) $q->limit( $limit );
            if( $page > 0 )       $q->page( $page );

            return $q->all();
        }
    }

    public function getPostsByUserId( $user_id=null, $limit=null, $page=0, $admin=false, $where=[] )
    {
        $q = $this->find()
            ->where([
                'Posts.user_id'      => $user_id
                ,'Posts.deleted IS'  => null
            ])
            ->contain([
                'PostCategories'
                ,'RelPostFiles' => [
                    'fields' => [ 'RelPostFiles.post_id', 'RelPostFiles.file_id' ]
                ]
                ,'RelPostFiles.Files' => [
                    'fields' => [ 'Files.id', 'Files.file_path' ]
                ]
                //,'PostLikes' => [
                //    'fields' => [ 'post_id', 'user_id' ]
                //    ,'conditions' => ['PostLikes.deleted IS ' => null ]
                //]
                //,'PostLikes.Users' => [
                //    'fields' => [ 'id', 'nickname' ]
                //]
                //,'PostLikes.Users.FrontUsers' => [
                //    'fields' => [ 'thumbnail_path' ]
                //]
            ])
            ->order(['Posts.created' => 'DESC']);

        if( $admin === false ) 
        {
            $q->select([
                'Posts.id'
                ,'Posts.user_id'
                ,'Posts.content'
                ,'Posts.created'
                ,'post_category_id'
                ,'PostCategories.label'
            ])
            ->where([
                'Posts.display_flg' => 1
            ]);

            // 公開物件の範囲
            $q->group('Posts.id')
            ->select([
               'published_building_id' => 'PostBuildingsLeftJoin.building_id'
            ])
            ->join([
                'table'  => 'post_buildings'
                ,'alias' => 'PostBuildingsLeftJoin'
                ,'type'  => 'LEFT'
                ,'conditions' => [
                    'PostBuildingsLeftJoin.Post_id = Posts.id'
                ]
            ]);
        }

        if( count( $where ) > 0 )
        {
            $q->where($where);
        }

        if( $user_id !== null ) $q->where([ 'Posts.user_id' => $user_id ]);

        if( $limit !== null ) $q->limit( $limit );
        if( $page > 0 )        $q->page( $page );

        return $q->all();
    }

}
