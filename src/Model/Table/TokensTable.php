<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Log\Log;

class TokensTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('FrontUsers');
    }
    public function getToken( $access_token, $refresh_token )
    {
        $data = [
            'access_token' => $access_token
            ,'access_token_expiration_date' => date( 'Y-m-d H:i:s', time() + ACCESS_TOKEN_EXPIRATION_DATE )
            ,'refresh_token' => $refresh_token
            ,'refresh_token_expiration_date' => date( 'Y-m-d H:i:s', time() + REFRESH_TOKEN_EXPIRATION_DATE )
        ];
        self::saveData( $data );
        unset( $data['refresh_token_expiration_date'] );
        return $data;
    }
    public function checkAccessToken( $access_token )
    {
        $res = $this->find()
            ->where([
                'access_token' => $access_token
            ]);
        if ( $res->count() === 1 )
        {
            // 有効期限以内の場合
            if ( $res->first()->access_token_expiration_date->format( 'U' ) >= time() )
            {
                return true;
            }
        }
        return false;
    }
    public function checkRefreshToken( $refresh_token )
    {
        $res = $this->find()
            ->where([
                'refresh_token' => $refresh_token
            ]);
        if ( $res->count() === 1 )
        {
            // 有効期限以内の場合
            if ( $res->first()->refresh_token_expiration_date->format( 'U' ) >= time() )
            {
                return true;
            }
        }
        return false;
    }
    public function getAccessTokenFromRefreshToken( $refresh_token, $new_tokens )
    {
        $res = $this->find()
            ->where([
                'refresh_token' => $refresh_token
            ]);
        if ( $res->count() === 1 )
        {
            $front_user_id = $res->first()->front_user_id;
            $this->updateAll(
                [
                    'front_user_id' => $front_user_id
                ]
                ,[
                    'access_token'   => $new_tokens['access_token']
                    ,'refresh_token' => $new_tokens['refresh_token']
                ]
            );
            return $res->first()->access_token;
        }
        return false;
    }
    public function getFrontUserIdByAccessToken( $access_token )
    {
        $res = $this->find()
            ->where([
                'access_token' => $access_token
            ]);
        if ( $res->count() === 1 )
        {
            return $res->first()->front_user_id;
        }
        return false;
    }
    public function updateFrontUserIdFromRefreshToken( $refresh_token, $access_token )
    {
        $res = $this->find()
            ->where([
                'refresh_token' => $refresh_token
            ]);
        if ( $res->count() === 1 )
        {
            $data = [
                'front_user_id' => $res->first()->front_user_id
                ,'access_token' => $access_token
            ];
            self::saveFrontUserId( $data );
        }
        return false;
    }
    public function saveData( $data, $parent_id=null )
    {
        parent::saveData( $data );
    }
    public function saveFrontUserId( $data )
    {
        $this->updateAll(
            [
                'front_user_id' => $data['front_user_id']
            ]
            ,[
                'access_token' => $data['access_token']
            ]
        );
    }

    public function getFrontUserByAccessTokens( $access_token )
    {
        $res = $this->find()
            ->where([
                'Tokens.access_token'    => $access_token
                ,'FrontUsers.deleted IS' => null
            ])
            ->contain(['FrontUsers']);
        if ( $res->count() === 1 )
        {
            return $res->first()->front_user;
        }
        return false;
    }

    // ログインユーザーのポイントを取得
    public function getFrontUserPoint( $access_token )
    {
        $res = $this->find()
            ->select(['point' => 'FrontUsers.point'])
            ->where([
                'Tokens.access_token'          => $access_token
                ,'Tokens.front_user_id IS NOT' => null
                ,'FrontUsers.deleted IS'       => null
            ])
            ->contain(['FrontUsers'])
            ->first();
        //Log::debug( json_decode( json_encode( $res, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
        return ( $res ) ? $res->point : 0;

    }

    public function checkFrontUserPointDate( $access_token, $target_date, $today )
    {
        $res = $this->find()
            ->select([ 'front_user_id' => 'FrontUsers.id' ])
            ->where([
                'Tokens.access_token'                 => $access_token
                ,'Tokens.front_user_id IS NOT'        => null
                ,'FrontUsers.deleted IS'              => null
                ,'OR' => [
                    'FrontUsers.' . $target_date . ' <'   => $today
                    ,'FrontUsers.' . $target_date . ' IS' => null
                ]
            ])
            ->contain(['FrontUsers'])
            ->first();

        return ( $res ) ? $res->front_user_id : false;
    }
}

