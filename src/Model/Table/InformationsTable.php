<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class InformationsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        // 画像
        $this->hasOne( 'RelInformationFiles', [
            'conditions' => [
                'RelInformationFiles.deleted IS' => null
                ,'RelInformationFiles.contractant_id = Informations.contractant_id'
            ]

        ]);
        // 投稿者
        $this->belongsTo( 'Users', [
            'conditions' => [
                'Users.deleted IS' => null
            ]
        ]);
        // いいね
        $this->hasMany( 'InformationLikes', [
            'conditions' => [
                'InformationLikes.deleted IS' => null
            ]
        ]);
        // 参加
        $this->hasMany( 'InformationJoins', [
            'conditions' => [
                'InformationJoins.deleted IS' => null
            ]
        ]);
        // コメント
        $this->hasMany( 'InformationComments', [
            'conditions' => [
                'InformationComments.deleted IS' => null
            ]
        ]);

        // 建物
        $this->HasMany('InformationBuildings');

    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }

        if( isset( $data['type'] ) && $data['type'] === '' )
        {
            $err['type'] = '選択してください';
        }

        // API仕様書1.3.0へのアップデートに伴い変更
        //if( isset( $data['event_flg'] ) && $data['event_flg'] === '1' && $data['event_date'] === '' )
        if( isset( $data['type'] ) && $data['type'] === 'event' && $data['event_date'] === '' )
        {
            $err['event_date'] = '設定してください';
        }

        if( isset( $data['release_date'] ) && mb_strlen( $data['release_date'] ) === 0 )
        {
            $err['release_date'] = '入力してください';
        }

        if( isset( $data['image_file'] ) && $data['image_file']['size'] > 0 && $data['image_file']['error'] === UPLOAD_ERR_INI_SIZE )
        {
            $err['image_file']  = 'ファイルサイズが大きすぎます';
        }
        elseif ( isset( $data['image_file'] ) && $data['image_file']['size'] > 0 && $data['image_file']['error'] > 0 && $this->check_image( $data['files']['image'] ) === false )
        {
            $err['image_file']  = '画像ファイル( png, jpg, gif )以外は受け付けません';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'Informations.deleted IS' => null
            ])
            ->contain([
                'RelInformationFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
            ])
            ->order([
                'Informations.release_date' => 'DESC'
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        // 仕様書v1.3.0以降の対応
        if( isset( $data['type'] ) )
        {
            $data['event_flg'] = ( $data['type'] === 'event' ) ?  1 : 0;
        }
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'Informations.id'          => $id
                ,'Informations.deleted IS' => null
            ])
            ->contain([
                 'RelInformationFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
            ])
            ->first();
    }

    public function getFrontInformations( $params=[] )
    {
        $q = $this->find()
            ->select([
                'Informations.id'
                ,'Informations.user_id'
                ,'Informations.title'
                ,'Informations.type'
                ,'Informations.content'
                ,'Informations.release_date'
                ,'Informations.event_flg'
                ,'Informations.event_date'
                ,'Informations.all_building_flg'
                //,'RelInformationFiles.information_id'
                ,'RelInformationFiles.file_id'
                ,'Users.id'
                ,'AdminUsers.name'
                ,'Files.id'
                ,'Files.name'
                ,'Files.file_path'
                ,'Files.mime_type'
            ])
            ->where([
                'Informations.contractant_id'   => $params['contractant_id']
                ,'Informations.display_flg'     => 1
                ,'Informations.release_date <=' => date( 'Y-m-d' )
                ,'Informations.deleted IS'      => null 
                ,'OR' => [
                    'Informations.close_date IS' => NULL
                    ,'Informations.close_date >' => date( 'Y-m-d H:i:s' )
                ]
            ])
            ->order([
                'Informations.release_date' => 'DESC'
                ,'Informations.created'     => 'DESC'
            ]);
        $building_cond = [];
        // 建物の条件を追加
        if( isset( $params['building_id'] ) && preg_match( '/^[0-9]+$/', $params['building_id'] ) )
        {
            $building_cond = [
                'InformationBuildings.building_id' => $params['building_id']
            ];
            $q->leftJoin(
                ['InformationBuildingsLeftJoin' => 'information_buildings'],
                [
                    'InformationBuildingsLeftJoin.information_id = Informations.id',
                ]
            )
            ->leftJoin(
                ['BuildingsLeftJoin' => 'buildings'],
                [ 'InformationBuildingsLeftJoin.building_id = BuildingsLeftJoin.id' ]
            )
            ->where([
                // 全公開もしくはユーザーのbuilding_idに合致するもの
                'OR' => [
                    'Informations.all_building_flg'     => 1
                    ,'InformationBuildingsLeftJoin.building_id' => $params['building_id']
                ]
            ]);
        }

        // 退去者の判別
        if( isset( $params['Informations.reject_leave_readable_flg'] ) )
        {
            $q->where([ 'Informations.reject_leave_readable_flg' => $params['Informations.reject_leave_readable_flg'] ]);
        }

        // カテゴリ
        if( isset( $params['type'] ) )
        {
            // 1.3.0前の後方互換
            switch( $params['type'] )
            {
            case 'general':
                $q->where([
                    'OR' => [
                        'Informations.type' => $params['type'],
                        'Informations.event_flg' => 0,
                    ]
                    ,'Informations.type !=' => 'line_open_chat',
                ]);
                break;
            case 'event':
                $q->where([
                    'OR' => [
                        'Informations.type' => $params['type'],
                        'Informations.event_flg' => 1,
                    ]
                    ,'Informations.type !=' => 'line_open_chat',
                ]);
                break;
            case 'line_open_chat':
                $q->where(['Informations.type' => $params['type'] ]);
                break;
            }
        }

        $q->contain([
            // 投稿者
            'Users.AdminUsers' => [
                'conditions' => [
                    'AdminUsers.deleted IS' => null
                ]
            ]
            // サムネイル
            ,'RelInformationFiles.Files' => [
                'conditions' => [
                    'Files.deleted IS' => null
                ]
            ]
            // 公開建物
            ,'InformationBuildings' => [
                'fields' => [
                    'InformationBuildings.information_id'
                    //,'InformationBuildings.building_id'
                ]
                ,'conditions' => $building_cond
            ]
            ,'InformationBuildings.Buildings' => [
                'fields' => [
                    'Buildings.name'
                    ,'Buildings.icon_path'
                ]
            ]
        ]);

        if( isset( $params['id'] ) && preg_match( '/^[0-9]+$/', $params['id'] ) )
        {
            return $q->where(['Informations.id' => $params['id']])
                ->contain([
                    // いいね
                    'InformationLikes' => [
                        'fields' => [ 'id', 'information_id', 'user_id' ]
                        ,'conditions' => [ 'FrontUsers.id IS NOT' => null ]
                        ,'sort' => [ 'InformationLikes.created' => 'ASC' ]
                    ]
                    ,'InformationLikes.Users' => [
                        'fields' => [ 'id' ]
                    ]
                    ,'InformationLikes.Users.FrontUsers' => [
                        'fields' => [ 'thumbnail_path', 'nickname', 'leave_flg' ]
                    ]
                    // 参加
                    ,'InformationJoins' => [
                        'fields' => [ 'id', 'information_id', 'user_id' ]
                        ,'conditions' => [ 'FrontUsers.id IS NOT' => null ]
                        ,'sort' => [ 'InformationJoins.created' => 'ASC' ]
                    ]
                    ,'InformationJoins.Users' => [
                        'fields' => [ 'id' ]
                    ]
                    ,'InformationJoins.Users.FrontUsers' => [
                        'fields' => [ 'thumbnail_path', 'name', 'nickname', 'room_number', 'leave_flg' ]
                    ]
                    // コメント
                    ,'InformationComments' => [
                        'fields' => [ 'id', 'information_id', 'user_id', 'comment', 'created' ]
                        ,'conditions' => [
                            'InformationComments.parent_comment_id IS' => null
                            ,'FrontUsers.id IS NOT' => null
                        ]
                        ,'sort' => [ 'InformationComments.created' => 'ASC' ]
                    ]
                    ,'InformationComments.Users' => [
                        'fields' => [ 'id' ]
                    ]
                    ,'InformationComments.Users.FrontUsers' => [
                        'fields' => [ 'thumbnail_path', 'nickname', 'leave_flg' ]
                    ]
                    // コメントのいいね
                    ,'InformationComments.InformationCommentLikes' => [
                        'fields' => [ 'comment_id', 'user_id' ]
                        ,'conditions' => [ 'FrontUsers.id IS NOT' => null ]
                    ]
                    ,'InformationComments.InformationCommentLikes.Users.FrontUsers' => [
                        'fields' => [ 'Users.id', 'FrontUsers.id', 'FrontUsers.nickname', 'FrontUsers.leave_flg', 'FrontUsers.thumbnail_path' ]
                    ]
                    ,'InformationComments.ChildInformationComments' => [
                        'fields' => [ 'id', 'parent_comment_id', 'comment', 'created' ]
                        ,'conditions' => [ 'FrontUsers.id IS NOT' => null ]
                        ,'sort' => [ 'ChildInformationComments.created' => 'ASC' ]
                    ]
                    ,'InformationComments.ChildInformationComments.Users' => [
                        'fields' => [ 'id' ]
                    ]
                    ,'InformationComments.ChildInformationComments.Users.FrontUsers' => [
                        'fields' => [ 'thumbnail_path', 'nickname', 'leave_flg' ]
                    ]
                    ,'InformationComments.ChildInformationComments.InformationCommentLikes' => [
                        'fields' => [ 'comment_id', 'user_id' ]
                        ,'conditions' => [ 'FrontUsers.id IS NOT' => null ]
                    ]
                    ,'InformationComments.ChildInformationComments.InformationCommentLikes.Users.FrontUsers' => [
                        'fields' => [ 'Users.id', 'FrontUsers.id', 'FrontUsers.nickname', 'FrontUsers.leave_flg', 'FrontUsers.thumbnail_path' ]
                    ]

                ])
                ->first();
        }
        else
        {
            $q->order([ 'Informations.release_date' => 'DESC' ]);

            // 件数
            if( isset( $params['limit'] ) && preg_match( '/^[0-9]+$/', $params['limit'] ) === 1 )
            {
                $q->limit( $params['limit'] );
            }
            else
            {
                $q->limit( INFORMATION_LIMIT );
            }
            // ページング
            if( isset( $params['page'] ) && $params['page'] > 0 )
            {
                $q->page( $params['page'] );
            }

            if( isset( $params['search'] ) )
            {
                $q->where( $params['search'] );
            }

            return $q->all();
        }

    }
    
    //表示フラグ:1 通知フラグ:0 で公開日を過ぎているもの
    public function getUnannouncedData( $contractant_id=null )
    {
         return $this->find()
             ->where([
                 'contractant_id'   => $contractant_id
                 ,'display_flg'     => 1
                 ,'notified_flg'    => 0
                 ,'release_date <=' => date( 'Y-m-d H:i:s' )
                 ,'deleted IS'      => null
             ])
             ->contain([ 'InformationBuildings' ])
             ->all();
             //->count();
    }

    public function getDataWithJoinUser( $id, $contractant_id )
    {
        return $this->find()
            ->where([
                'Informations.id' => $id
                ,'Informations.contractant_id' => $contractant_id
            ])
            ->contain([
                // サムネイル
                'RelInformationFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
                ,'InformationJoins' => [
                //    'fields' => [ 'id', 'information_id' ]
                    'sort' => [ 'InformationJoins.created' => 'ASC' ]
                ]
                ,'InformationJoins.Users' => [
                    'fields' => [ 'id', 'encryption_parameter_id' ]
                ]
                ,'InformationJoins.Users.FrontUsers' => [
                    'fields' => [ 'id', 'thumbnail_path', 'name', 'nickname', 'room_number', 'building_id', 'encryption_parameter_id', 'leave_flg' ]
                ]
                ,'InformationJoins.Users.FrontUsers.EncryptionParameters'
                ,'InformationJoins.Users.FrontUsers.Buildings' => [
                    'fields' => [ 'name' ]
                ]
            ])
            ->first();

    }

}
