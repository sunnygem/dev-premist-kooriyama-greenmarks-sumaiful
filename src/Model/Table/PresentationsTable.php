<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PresentationsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('ImageSharings');
        $this->belongsTo('Files');
        $this->belongsTo('Posts');
        $this->belongsTo('Users');

        $this->belongsTo('Buildings');
        $this->hasMany('PresentationBuildings');
    }

    public function validation( $data=[] )
    {
        $err = [];
        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'Presentations.deleted IS' => null
             ])
             ->order([
                 //'created' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data=[] )
    {
        // アソシエーションごと保存
        if( isset( $data['presentation_buildings'] ) )
        {
            $res = $this->newEntity( $data, ['associated' => [ 'PresentationBuildings' ]] );
            return $this->save( $res );
        }
        else
        {
            return parent::saveData( $data );
        }
    }


    public function getDataById( $id )
    {
        return $this->find()
        ->where([
            'Presentations.id' => $id
        ])
        ->contain([
            'Posts'
            ,'Files'
        ])
        ->first();

    }

    public function getNonCompatibleCount( $contractant_id )
    {
        return $this->find()
            ->where([
                'Presentations.contractant_id' => $contractant_id
                ,'Presentations.checked IS'    => null
                ,'Presentations.deleted IS'    => null
            ])
            ->leftJoin(
                ['PresentationBuildings' => 'presentation_buildings']
                ,['PresentationBuildings.presentation_id = Presentations.id']
            )
            ->all();
    }

}
