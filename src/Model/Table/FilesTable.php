<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class FilesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsToMany('Informations', [
            'joinTable' => 'rel_information_files'
        ]);
        $this->belongsToMany('Content', [
            'joinTable' => 'rel_information_files'
        ]);
        $this->hasOne('RelImagesharingFiles', [ ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'deleted IS' => null
             ])
             ->order([
                 'created' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id = null )
    {
        // バイナリをthumailカラムに保存する
        //if( isset( $data['thumbnail_binary'] ) )
        //{
        //    $data['thumbnail'] = $data['thumbnail_binary'];
        //}
        return parent::saveData( $data );
    }

    // 論理削除
    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    // 物理削除
    public function physiacalDeleteData( $id )
    {
        $entity = $this->get( $id );
        $this->delete( $entity );
    }

    public function getImageSharingData( $id )
    {
        return $this->find()
        ->where([
            'Files.id' => $id
        ])
        ->contain([
            'RelImagesharingFiles.ImageSharings'
        ])
        ->first();

    }
}
