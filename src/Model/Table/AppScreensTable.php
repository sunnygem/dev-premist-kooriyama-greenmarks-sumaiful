<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class AppScreensTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->hasMany('ContractantServiceMenus');
    }

    public function validation( $data=[] )
    {
        $err=[];

        if( isset( $data['label'] ) && strlen( $data['label'] ) === 0 )
        {
            $err['label'] = '入力してください';
        }
        return $err;
    }


    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'deleted IS' => null
             ]);
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getScreenByContractantId( $contractant_id=null )
    {
        return $this->find()
            ->where([
                'contractant_id' => $contractant_id
                ,'deleted IS'    => null
            ])
            ->contain([ 'ContractantServiceMenus' ])
            ->order(['orderby ASC'])
            ->all();
    }

    public function getFooterListByContractantId(  $contractant_id=null )
    {
        return $this->find('list', [
                'keyField'   => 'id',
                'valueField' => 'label'
            ])
            ->where([
                'contractant_id' => $contractant_id
                ,'deleted IS'    => null
            ])
            ->order(['orderby ASC'])
            ->toArray();

    }

    public function getOrder( $contractant_id=null )
    {
        $res = $this->find()
            ->where([
                'contractant_id' => $contractant_id
                ,'deleted IS'    => null
            ]);

        return $res->count();
    }
}
