<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class QuestionsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
    }

    public function validation( $data=[] )
    {
        $err=[];

        return $err;
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getData( $contractant_id=null )
    {
        $res = $this->find()
            ->select([
                'id', 'content' ,'content_en'
            ])
            ->where([
                'contractant_id' => $contractant_id
            ])
            ->first();

        return $res;
    }

}
