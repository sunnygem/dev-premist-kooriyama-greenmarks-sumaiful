<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class LuggageStragesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo( 'Users' );
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['user_id'] ) && $data['user_id'] === '' )
        {
            $err['user_id'] = '設定してください';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'LuggageStrages.deleted IS' => null
            ])
            ->contain([
                'Users.FrontUsers.EncryptionParameters'
                ,'Users.FrontUsers.Buildings'
            ])
            ->order([
                'LuggageStrages.created' => 'DESC'
            ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'Informations.id'          => $id
                ,'Informations.deleted IS' => null
            ])
            ->contain([
                 'RelInformationFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
            ])
            ->first();
    }

    public function getTopIonformations( $params=[] )
    {
        $res = $this->find()
            ->select([
                'Informations.id'
                ,'Informations.user_id'
                ,'Informations.title'
                ,'Informations.content'
                ,'Informations.release_date'
                ,'Informations.event_flg'
                ,'Informations.event_date'
                //,'RelInformationFiles.information_id'
                ,'RelInformationFiles.file_id'
                ,'Users.id'
                ,'AdminUsers.name'
                ,'Files.name'
                ,'Files.file_path'
                ,'Files.mime_type'
            ])
            ->where([
                'Informations.contractant_id'   => $params['contractant_id']
                ,'Informations.display_flg'     => 1
                ,'Informations.release_date <=' => date( 'Y-m-d' )
                ,'OR' => [
                    'Informations.close_date IS' => NULL
                    ,'Informations.close_date >' => date( 'Y-m-d H:i:s' )
                ]
            ])
            ->contain([
                // 投稿者
                'Users.AdminUsers' => [
                    'conditions' => [
                        'AdminUsers.deleted IS' => null
                    ]
                ]
                // サムネイル
                ,'RelInformationFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
            ])
            ->order([ 'Informations.modified' => 'DESC' ]);

        // 件数
        if( isset( $params['limit'] ) && preg_match( '/^[0-9]+$/', $params['limit'] ) === 1 )
        {
            $res->limit( $params['limit'] );
        }
        else
        {
            $res->limit( INFORMATION_LIMIT );
        }
        // ページング
        if( isset( $params['page'] ) && preg_match( '/^[0-9]+$/', $params['page'] ) === 1 )
        {
            $res->page( $params['page'] );
        }

        return $res->all();

    }

     // 未受け取り件数を返す
     public function checkUnreceive( $user_id=null )
     {
         $q = $this->find()
             ->where([
                 'LuggageStrages.user_id'      => $user_id
                 ,'LuggageStrages.receive_flg' => 0
                 ,'LuggageStrages.deleted IS'  => null
             ]);

         return $q->count();
     }

     // 受け取りフラグ立てる
     public function setFlagOn( $data=[] )
     {
         $fields = [ 'receive_flg' => 1 ];
         $cond = [
             'contractant_id' => $data['contractant_id']
             ,'user_id'       => $data['user_id']
             ,'front_user_id' => $data['front_user_id']
             ,'receive_flg'   => 0
         ];
         return $this->updateAll( $fields, $cond );
     }

}
