<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class RelMenuManualFilesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'deleted IS' => null
             ])
             ->order([
                 'created' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    //public function saveData( $data, $id = null )
    //{
    //    return parent::saveData( $data );
    //}

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    public function deleteByMenuManualId( $menu_manual_id=null )
    {
         $res = $this->find()
         ->where([
              'menu_manual_id' => $menu_manual_id
         ])
         ->first();
         if( $res )
         {
             $this->delete( $res ); // 物理削除
             return $res->file_id;
         }
         else
         {
             return false;
         }
    }
}
