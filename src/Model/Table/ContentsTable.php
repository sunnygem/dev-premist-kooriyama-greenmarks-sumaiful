<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ContentsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        // 画像
        $this->belongsToMany('Files', [
            //'className' => 'Files'
            'joinTable' => 'rel_content_files'
            ,'targetForeignKey' => 'file_id'
            ,'conditions' => [
                'Files.mime_type LIKE' => 'image/%'
                ,'RelContentFiles.deleted IS' => null
            ]
        ]);

    }

    public function validation( $data=[] )
    {
        $err = [];
        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }


        if( isset( $data['release_date'] ) && mb_strlen( $data['release_date'] ) === 0 )
        {
            $err['contractant_id'] = '入力してください';
        }

        // todo 本文の文字数縛り
        if( isset( $data['contents'] ) && mb_strlen( $data['contents'] ) > 99999 )
        {
            $err['contents'] = '文字数が超過しています';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'Contents.deleted IS' => null
             ])
             ->contain([
                 'Files' => function( $q ){
                    return $q->where([
                        'Files.deleted IS' => null
                    ])->order(['Files.created DESC']);
                 }
             ])
             ->order([
                 'Contents.release_date' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null, $contractant_id )
    {
        return $this->find()
            ->where([
                'Contents.id'          => $id
                ,'Contents.contractant_id' => $contractant_id
                ,'Contents.deleted IS' => null
            ])
            ->contain([
                'Files' => [ 
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                    ,'sort' => ['Files.created DESC']
                ]
            ])
            ->first();
    }

    public function getDetailData( $id=null )
    {
        return $this->find()
            ->where([
                'Contents.id'           => $id
                ,'Contents.deleted IS'  => null
                ,'Contents.display_flg' => 1
                ,'Contents.release_date <=' => date( 'Y-m-d' )
                ,'OR' => [
                    'Contents.close_date IS'    => NULL
                    ,'Contents.close_date >'    => date( 'Y-m-d H:i:s' )
                ]
            ])
            ->contain([
                'Files' => [ 
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                    ,'sort' => ['Files.created DESC']
                ]
            ])
            ->first();
    }

    public function getContentsList( $params=[] )
    {
        $res = $this->find()
            ->where([
                'Contents.contractant_id'   => $params['contractant_id']
                ,'Contents.display_flg'     => 1
                ,'Contents.release_date <=' => date( 'Y-m-d' )
                ,'Contents.deleted IS'      => null
                ,'OR' => [
                    'Contents.close_date IS' => NULL
                    ,'Contents.close_date >' => date( 'Y-m-d H:i:s' )
                ]
            ])
            ->contain([
                // サムネイル
                'Files' => [
                   'conditions' =>  [
                       'Files.mime_type LIKE' => 'image/%'
                       ,'Files.deleted IS' => null
                   ]
                   ,'sort' => ['Files.created DESC']
                ]
            ])
            ->order([ 'Contents.modified' => 'DESC' ]);

        // 件数
        if( isset( $params['limit'] ) && preg_match( '/^[0-9]+$/', $params['limit'] ) === 1 )
        {
            $res->limit( $params['limit'] );
        }
        else
        {
            $res->limit( CONTENT_PAGE_LIMIT );
        }
        // ページング
        if( isset( $params['page'] ) && preg_match( '/^[0-9]+$/', $params['page'] ) === 1 )
        {
            $res->page( $params['page'] );
        }

        return $res->all();

    }

}
