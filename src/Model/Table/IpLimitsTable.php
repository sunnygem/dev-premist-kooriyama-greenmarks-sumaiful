<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class IpLimitsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Contractants', []);
    }

    public function validation( $data=[] )
    {
        $err = [];
        foreach( $data as $key => $val )
        {
            if( isset( $val['ip_address'] ) && strlen( $val['ip_address'] ) !== 0 &&  $this->checkIpAddress( $val['ip_address'] ) == false )
            {
                $err[$key]['ip_address'] = '入力した値を確認してください';
            }
        }

        return $err;
    }

    // paginator
    //public function findSearch( \Cake\ORM\Query $query, array $options )
    //{
    //     return $query
    //         //->select( [
    //         //    'id'
    //         //] )
    //         ->where( [
    //             'deleted IS' => null
    //         ]);
    //    
    //}

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function check( $remote_address=null, $contractant_id=null )
    {
        $flg = false;
        if( $remote_address !== null && $contractant_id !== null )
        {
            $res = $this->find()
                ->where([
                    'contractant_id' => $contractant_id
                    ,'deleted IS'    => null
                ])
                ->all();
            if( $res->count() > 0 )
            {
                foreach( $res as $val )
                {
                    $ip_address = $val->ip_address;
                    // 255.255.255.0/24という形式ではない場合
                    if ( strpos( $ip_address, '/' ) === false )
                    {
                        if ( $remote_address === $ip_address )
                        {
                            $flg = true;
                        }
                    }
                    else
                    {
                        list( $permit_ip, $mask ) = explode( '/', $ip_address );
                        $accept_long = ip2long( $permit_ip )      >> ( 32 - $mask );
                        $remote_long = ip2long( $remote_address ) >> ( 32 - $mask );
                        if ( $accept_long === $remote_long )
                        {
                            $flg = true;
                        }
                    }
                }
            }
            else
            {
                // 登録がなければ制限はしない
                $flg = true;
            }
        }
        return $flg;
    }

    public function getIpAddressByContractantId( $contractant_id=null )
    {
        return $this->find()
        ->where([
            'contractant_id' => $contractant_id
            ,'deleted IS'   => null
        ])
        ->limit( 10 )
        ->all();
    }

    public function truncateByContractantId( $contractant_id=null  )
    {
        if( $contractant_id !== null )
        {
            $this->updateAll(
                ['deleted' => date( 'Y-m-d H:i:s' )]
                ,[
                    'contractant_id' => $contractant_id
                    ,'deleted IS'    => null
                ]
            );
        }
    }

}
