<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class BuildingRoomNumbersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Buildings');

        $this->hasMany('FrontUsers', [
            'conditions' => [
                'FrontUsers.deleted IS' => null
            ]
        ]);

        $this->addBehavior('CounterCache', [
            'Buildings' => [
                'room_count' => [
                    'conditions' => [
                        'BuildingRoomNumbers.deleted IS' => null
                    ]
                ]
            ]
        ]);
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             ->where( [
                 'Buildings.deleted IS' => null
             ])
             ->contain([
                 'Buildings' 
                 ,'FrontUsers' 
                 ,'FrontUsers.EncryptionParameters' 
                 ,'FrontUsers.Users' 
             ]);
    }

    public function uploadSaveData( $data=[] )
    {
        $entities = $this->newEntities( $data );
        $model = $this;
        // トランザクション
        $this->getConnection()->transactional( function () use ( $entities ) {
            foreach( $entities as $entity )
            {
                $exists = self::getDataByEntity( $entity );
                if( $exists !== false )
                {
                    $entity->id  = $exists->id;
                    $entity->new = false;
                }
                // エンティティーを保存
                $res = $this->save( $entity, ['atomic' => false] );

                if( $res === false ) return false;
            }
        });

        return true;

    }
    public function getDataByEntity( $entity )
    {
        $res = $this->find()
            ->where([
                'building_id'  =>  $entity->building_id
                ,'room_number' =>  $entity->room_number
                ,'deleted IS'  =>  null
            ]);
        return ( $res->isEmpty() ) ? false : $res->first();
    }

    public function checkExists( $contractant_id, $building_id, $room_number )
    {
        $res = $this->find()
            ->where( [
                'contractant_id' => $contractant_id
                ,'building_id'   => $building_id
                ,'room_number'   => trim( mb_convert_kana( $room_number, 'KVas' ) )
                ,'deleted IS'    => null
            ] );

        return ( $res->count() === 1 ) ? true : false;
    }

    public function getIdByContractantIdAndBuildingIdAndRoomNumber( $contractant_id, $building_id, $room_number )
    {
        $res = $this->find()
            ->where( [
                'contractant_id' => $contractant_id
                ,'building_id'   => $building_id
                ,'room_number'   => trim( mb_convert_kana( $room_number, 'KVas' ) )
                ,'deleted IS'    => null
            ])->first();

        return ( $res ) ? $res->id: false;
    }

    public function getDataByBuildingId( $building_id=null )
    {
        return $this->find()
            ->where([
                'building_id' => $building_id
                ,'deleted IS' => null
            ])
            //->order(['id' => 'ASC'])
            ->order(['room_number' => 'ASC'])
            ->all();
    }
}
