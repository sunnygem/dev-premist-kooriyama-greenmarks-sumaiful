<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Datasource\ConnectionManager;

class ChatGroupMembersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Users');
        $this->belongsTo('ChatGroups');
        $this->belongsTo('ChatMessages', [
            'foreignKey'  => 'chat_group_id'
            ,'bindingKey' => 'chat_group_id'
        ]);
    }

    public function getByUserId( $chat_group_id, $user_id )
    {
        $res = $this->find()
            ->where( [
                'chat_group_id' => $chat_group_id
                ,'user_id'      => $user_id
                ,'deleted IS'   => null
            ] );
        return ( $res->isEmpty() === false ) ? $res->first() : null;
    }
    public function checkForDM( $contractant_id, $user_id, $front_user_id, $partner_user_id, $partner_front_user_id )
    {
        // select count( t3.group_id ) from test t3
        // where t3.group_id = (
        //     select t1.group_id from test t1 
        //         where t1.user_id = 2 and t1.group_id = ( select t2.group_id from test t2 where t2.user_id = 11 )
        // );

        //$match_groups = [];
        //// 自分の所属グループを取得
        //$res = $this->find()
        //    ->select( [ 'group_id' ] )
        //    ->where( [
        //        'contractant_id' => $contractant_id
        //        ,'user_id' => $user_id
        //        ,'front_user_id' => $front_user_id
        //    ] );


        //$this->alias = 't3';
        $subquery2 = $this->find()
            ->select( [ 'chat_group_id'] )
            ->where(
                function( $exp, $q ) use( $partner_user_id, $partner_front_user_id ) {
                    return $exp->eq( 'user_id', $partner_user_id )
                        ->eq( 'front_user_id', $partner_front_user_id );
                }
            );
        //$this->alias = 't2';
        $subquery1 = $this->find()
            ->select( [ 'chat_group_id'] )
            ->where(
                function( $exp, $q ) use( $user_id, $front_user_id, $subquery2 ) {
                    return $exp->eq( 'user_id', $user_id )
                        ->eq( 'front_user_id', $front_user_id )
                        //->eq( 'chat_group_id', $subquery2 );
                        ->in( 'chat_group_id', $subquery2 );
                }
            );
        //$this->alias = 'ChatGroupMembers';
        $res = $this->find()
            ->select( [ 'chat_group_id'] )
            ->where(
                function( $exp, $q ) use( $subquery1 ) {
                    //jwreturn $exp->eq( 'chat_group_id', $subquery1 );
                    return $exp->in( 'chat_group_id', $subquery1 );
                }
            );

        if ( $res->count() === 2 )
        {
            return $res;
        }
        return false;
    }

    // オブジェクトのUser_id以外のuserデータを取得
    public function getPushDataByObjChatMessage( $obj )
    {
        return $this->find()
        ->where([
            'ChatGroupMembers.chat_group_id'     => $obj->chat_group_id
            ,'ChatGroupMembers.id !='            => $obj->user_id
            ,'ChatGroupMembers.front_user_id !=' => $obj->front_user_id
            ,'ChatGroupMembers.deleted IS'       => null
        ])
        ->contain([
            'Users.FrontUsers.UserDeviceTokens'
            ,'ChatMessages' => [
                'conditions' => [
                    'ChatMessages.id' => $obj->id
                ]
            ]
            ,'ChatMessages.Users.FrontUsers'
        ])
        ->all();
    }

    // メンバーを取得する(自分以外の場合はuser_idとfront_user_idを渡す
    public function getChatGroupMembers( $contractant_id, $chat_group_id, $user_id=null, $front_user_id=null )
    {
        $res = $this->find()
            ->where([
                'contractant_id' => $contractant_id
                ,'chat_group_id' => $chat_group_id
                ,'deleted IS'    => null
            ] );
        if ( $user_id !== null && $front_user_id !== null )
        {
            $res->where( [
                'user_id !=' => $user_id
                ,'front_user_id !=' => $front_user_id
            ] );
        }
        return $res->all();
    }
}
