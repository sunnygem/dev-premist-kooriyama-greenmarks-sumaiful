<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ContractantsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->hasOne( 'mtContratantStatuses', [
            'bindingKey' => 'status_id'
        ]);
        $this->hasMany( 'AdminUsers', []);
        $this->belongsToMany('ServiceMenus', [
            'joinTable' => 'contractant_service_menus'
            ,'through' => 'ContractantServiceMenus'
        ]);

    }

    public function validation( $data=[] )
    {
        $err = [];
        if( isset( $data['domain'] ) && mb_strlen( $data['domain'] ) === 0 )
        {
            $err['domain'] = '入力してください';
        }

        if( isset( $data['name'] ) && mb_strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        if( isset( $data['status_id'] ) && $data['status_id'] === '' )
        {
            $err['status_id'] = '選択してください';
        }


        if( isset( $data['unique_parameter'] ) && mb_strlen( $data['unique_parameter'] ) === 0 )
        {
            $err['unique_parameter'] = '入力してください';
        }
        elseif( isset( $data['unique_parameter'] ) && $this->checkAlphabet( $data['unique_parameter'] ) === false )
        {
            $err['unique_parameter'] = '半角英数ハイフン(-)アンダースコア(_)以外は登録できません';
        }
        elseif( isset( $data['unique_parameter'] ) && $this->check_duplication( $data, 'unique_parameter', false ) )
        {
            $err['unique_parameter'] = '既に登録されている識別子は使用できません';
        }

        if( isset( $data['service_start_date'] ) && mb_strlen( $data['service_start_date'] ) === 0 )
        {
            $err['service_start_date'] = '入力してください';
        }



        return $err;
    }

    public function admin_validation( $data=[] )
    {
        $err = [];
        return $err;
    }
    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'deleted IS' => null
             ]);
        
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getValidList()
    {
        return $this->find( 'list' )
            ->where([
                'deleted IS' => null
            ])
            ->order([
                'status_id' => 'ASC'
                ,'contract_date' => 'DESC'
                ,'modified' => 'DESC'
            ])
            ->toArray();
    }

    public function getDataByDomain( $domain )
    {
        return $this->find()
            ->where( [
                'domain'      => $domain
                ,'status_id'  => 1 // 契約中
                ,'deleted IS' => null
            ] )->first();
    }

    public function getDataByUniqueParameter( $unique_parameter )
    {
        return $this->find()
            ->where( [
                'unique_parameter' => $unique_parameter
                ,'status_id'       => 1 // 契約中
                ,'deleted IS'      => null
            ] )->first();
    }

    public function getDetailData( $id=null )
    {
        return $this->find()
            ->where([
                'Contractants.id' => $id
            ])
            ->contain([
                'AdminUsers'
                //,'ServiceMenus' => [
                //    'conditions' => [
                //        'ServiceMenus.deleted IS' => null
                //    ]
                //]
            ])
            ->first();
    }

}
