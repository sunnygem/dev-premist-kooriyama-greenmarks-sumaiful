<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ChatUnreadsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Threads');
        $this->belongsTo('ChatGroups');
        $this->hasMany('ChatGroups.ChatGroupMembers');
        $this->hasMany( 'UserDeviceTokens', [
                'foreignKey' => 'front_user_id'
                ,'bindingKey' => 'front_user_id'
        ] );
    }
    public function checkUnread( $contractant_id, $user_id, $front_user_id )
    {
        return $this->find()
            ->where( [
                'contractant_id' => $contractant_id
                ,'user_id'       => $user_id
                ,'front_user_id' => $front_user_id
                ,'deleted IS'    => null
            ] )
            ->count();
    }
    public function getMemberlist( $contractant_id, $user_id, $front_user_id )
    {
        $res = $this->find()
            ->where( [
                'ChatUnreads.contractant_id'         => $contractant_id
                ,'ChatUnreads.user_id'               => $user_id
                ,'ChatUnreads.front_user_id'         => $front_user_id
                ,'ChatUnreads.deleted IS'            => null
            ] )
            ->contain([
                'ChatGroups'
                ,'ChatGroups.ChatGroupMembers' => [
                    'conditions' => [
                        'user_id !=' => $user_id
                        ,'front_user_id !=' => $front_user_id
                    ]
                ]
            ])
            ->all();
        $data = [];
        if ( $res->isEmpty() === false )
        {
            foreach( $res as $key => $val )
            {
                foreach( $val->chat_group->chat_group_members as $key2 => $val2 )
                {
                    $data[] = $val2->front_user_id;
                }
            }
            $data = array_unique( $data );
        }
        return $data;
    }

    public function getChatUnreadDeviceTokenData( $contractant_id, $chat_group_id, $thread_id )
    {
        $res = $this->find()
            ->where( [
                'ChatUnreads.contractant_id' => $contractant_id
                ,'ChatUnreads.chat_group_id' => $chat_group_id
                ,'ChatUnreads.thread_id'     => $thread_id
                ,'ChatUnreads.deleted IS'    => null
            ] )
            ->contain([
                'UserDeviceTokens'
                //'UserDeviceTokens' => [
                //    'conditions' => [
                //        'UserDeviceTokens.deleted IS' => null
                //    ]
                //]
            ])->all();
        if ( $res->isEmpty() === false )
        {
            $data = [];
            $front_user_ids = [];
            foreach( $res as $key => $val )
            {
                if ( $val->user_device_tokens !== null )
                {
                    foreach( $val->user_device_tokens as $key2 => $val2 )
                    {
                        if ( in_array( $val2->front_user_id, $front_user_ids, true ) === false )
                        {
                            $data[] = $val2;
                            $front_user_ids[] = $val2->front_user_id;
                        }
                    }
                }
            }
            return $data;
        }
        return $res;

    }

    public function saveChatUnreadData( $contractant_id, $chat_group_id, $thread_id, $user_id, $front_user_id )
    {
        $data = [
            'contractant_id' => $contractant_id
            ,'chat_group_id' => $chat_group_id
            ,'thread_id'     => $thread_id
            ,'user_id'       => $user_id
            ,'front_user_id' => $front_user_id
        ];
        self::saveData( $data );
    }
    public function deleteChatUnreadData( $contractant_id, $chat_group_id, $thread_id, $user_id, $front_user_id )
    {
        $this->updateAll(
            [
                'deleted' => date( 'Y-m-d H:i:s' )
            ]
            ,[
            'contractant_id' => $contractant_id
            ,'chat_group_id' => $chat_group_id
            ,'thread_id'     => $thread_id
            ,'user_id'       => $user_id
            ,'front_user_id' => $front_user_id
            ]
        );
    }
    public function updateChatAlreadyReadData( $contractant_id, $user_id, $front_user_id, $thread_id, $chat_message_id )
    {
        $this->updateAll(
            [
                'chat_already_read_flg' => 1
            ]
            ,[
                'contractant_id'         => $contractant_id
                ,'user_id'               => $user_id
                ,'front_user_id'         => $front_user_id
                ,'thread_id'             => $thread_id
                ,'chat_message_id'       => $chat_message_id
            ]
        );
    }
    public function updateChatAlreadyReadForGetMessage( $contractant_id, $user_id, $front_user_id, $thread_id )
    {
        $this->updateAll(
            [
                'chat_already_read_flg' => 1
            ]
            ,[
                'contractant_id'         => $contractant_id
                ,'user_id'               => $user_id
                ,'front_user_id'         => $front_user_id
                ,'thread_id'             => $thread_id
            ]
        );
    }
    public function saveData( $data, $parent_id=null )
    {
        parent::saveData( $data );
    }
}

