<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class EnqueteAnswersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('MenuEnquetes', [
            'foreignKey' => 'enquete_id'
        ]);
        $this->hasMany('EnqueteAnswerItems');

        $this->addBehavior('CounterCache', [
            'MenuEnquetes' => ['answer_count']
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['type'] ) && $data['type'] === null )
        {
            $err['type'] = '選択してください';
        }

        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query->where( [
                 'EnqueteAnswers.deleted IS' => null
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }
   

    //public function saveData( $data, $id = null )
    //{
    //    return parent::saveData( $data );
    //}

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    //public function getDataByContractantSeriviceMenuId( $id=null, $display_flg=false )
    //{
    //    $res = $this->find()
    //    ->where([
    //        'contractant_service_menu_id' => $id
    //        ,'deleted IS' => null
    //    ]);

    //    if( $display_flg === true ) $res->where(['display_flg' => 1]);
    //    return $res->all();
    //}

}
