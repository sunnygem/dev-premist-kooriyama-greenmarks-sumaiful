<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class MenuCategoriesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('MenuManuals');
    }

    public function validationDefault( Validator $validator )
    {
        $validator->notEmpty('name', '入力してください');
        $validator->notEmpty('contractant_service_menu_id', 'サービスメニューIDが指定されていません');

        return $validator;
    }

    public function getCategoryByServiceMenuId( $service_menu_id=null, $where=[], $list_flg=false, $select=null )
    {
        $query = ( $list_flg === true ) ? $this->find( 'list' ) : $this->find();

        $query
            ->where([
                 'MenuCategories.contractant_service_menu_id' => $service_menu_id
                 ,'MenuCategories.deleted IS'                 => NULL
            ])
            //->contain([ ])
            ->order(['orderby' => 'ASC']);

        if( $select !== null )    $query->select( $select );
        if( count( $where ) > 0 ) $query->where( $where );

        return $query->all();
    }

    public function getOrder( $service_menu_id=null )
    {
        $res = $this->find()
            ->where([
                'contractant_service_menu_id' => $service_menu_id
                ,'deleted IS'    => null
            ]);

        return $res->count();
    }

}
