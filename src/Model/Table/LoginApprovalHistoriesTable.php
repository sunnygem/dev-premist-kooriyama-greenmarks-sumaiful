<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class LoginApprovalHistoriesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Contractants', []);
    }

    public function validation( $data=[] )
    {
        $err = [];
        return $err;
    }

    // paginator
    //public function findSearch( \Cake\ORM\Query $query, array $options )
    //{
    //     return $query
    //         //->select( [
    //         //    'id'
    //         //] )
    //         ->where( [
    //             'deleted IS' => null
    //         ]);
    //    
    //}

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getIpAddressByContractantId( $contractant_id=null )
    {
        return $this->find()
        ->where([
            'contractant_id' => $contractant_id
            ,'deleted IS'   => null
        ])
        ->limit( 10 )
        ->all();
    }

}
