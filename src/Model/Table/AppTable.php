<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;
use Cake\Datasource\ConnectionManager;

class AppTable extends Table
{
    public function __construct( array $config=[] )
    {
        parent::__construct( $config );
        $this->_session = new Session();
    }

    public function initialize( array $config )
    {
        // created と modified を自動挿入
        $this->addBehavior('Timestamp', []);
        // バリデーションメソッド読み込み
        $this->addBehavior('CommonValidation');
    }

    // validate前に一括trimする
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach( $data as $key => $val )
        {
            if ( is_string( $val ) )
            {
                $data[$key] = trim( $val );
            }
        }
    }

    public function truncate( $table )
    {
        $connection = ConnectionManager::get('default');
        $connection->execute( 'TRUNCATE TABLE ' . $table );
    }

    public function saveData( $data )
    {
        //$data['modified_user_id']   = $this->_session->read( 'Auth.User.id' );
        //$data['modified_user_name'] = $this->_session->read( 'Auth.User.name' );
        $data['modified']         = date( 'Y-m-d H:i:s' );
        if ( isset( $data['id'] ) === false )
        {
            //$data['created_user_id']   = $this->_session->read( 'Auth.User.id' );
            //$data['created_user_name'] = $this->_session->read( 'Auth.User.name' );
            $data['created']           = date( 'Y-m-d H:i:s' );
        }
        $res = $this->newEntity( $data );
        $res = $this->save( $res );
        return $res->id;
    }
    public function deleteData( $id )
    {
        $data = [];
        $data['id']               = $id;
        $data['modified_user_id']   = $this->_session->read( 'Auth.Admin.id' );
        $data['modified_user_name'] = $this->_session->read( 'Auth.Admin.name' );
        $data['deleted']          = date( 'Y-m-d H:i:s' );
        $res = $this->newEntity( $data );
        $this->save( $res );
    }
}
