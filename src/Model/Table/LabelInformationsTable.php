<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class LabelInformationsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->hasMany('LabelInformationUserTypes');

        // 画像
        $this->belongsToMany('Images', [
            'className' => 'Files'
            ,'joinTable' => 'rel_label_information_files'
            ,'foreignKey' => 'label_information_id'
            ,'targetForeignKey' => 'file_id'
            ,'conditions' => [
                'Images.mime_type LIKE' => 'image/%'
                //,'Images.contractant_id = LabelInformations.contractant_id'
            ]
        ]);

        // PDF
        $this->belongsToMany('Pdfs', [
            'className' => 'Files'
            ,'joinTable' => 'rel_label_information_files'
            ,'foreignKey' => 'label_information_id'
            ,'targetForeignKey' => 'file_id'
            ,'conditions' => [
                'Pdfs.mime_type' => 'application/pdf'
                //,'Pdfs.contractant_id = LabelInformations.contractant_id'
            ]
        ]);

        $this->belongsToMany('Files', [
            'joinTable' => 'rel_label_information_files'
            ,'foreignKey' => 'label_information_id'
            ,'targetForeignKey' => 'file_id'
            ,'conditions' => [
                //'Files.contractant_id = LabelInformations.contractant_id'
            ]
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }

        if( isset( $data['contractant_id'] ) && $data['contractant_id'] === '' )
        {
            $err['contractant_id'] = '選択してください';
        }

        if( isset( $data['release_date'] ) && mb_strlen( $data['release_date'] ) === 0 )
        {
            $err['contractant_id'] = '入力してください';
        }

        if( isset( $data['image_file'] ) && $data['image_file']['size'] > 0 && $data['image_file']['error'] === UPLOAD_ERR_INI_SIZE )
        {
            $err['image_file']  = 'ファイルサイズが大きすぎます';
        }
        elseif ( isset( $data['image_file'] ) && $data['image_file']['size'] > 0 && $data['image_file']['error'] > 0 && $this->check_image( $data['files']['image'] ) === false )
        {
            $err['image_file']  = '画像ファイル( png, jpg, gif )以外は受け付けません';
        }

        //if( $data['type'] === '0' && isset( $data['link_url'] ) && strlen( $data['link_url'] ) === 0 )
        //{
        //    $err['link_url']  = '入力してください';
        //}

        if( $data['type'] === '1' && isset( $data['files']['pdf'] ) && isset( $data['files']['upload'] ) === false && $data['files']['pdf']['size'] === 0 && $data['files']['pdf']['error'] === 4 )
        {
             //$err['pdf']  = 'ファイルが添付されていません';
        }
        elseif( isset( $data['files']['pdf'] ) && $data['files']['pdf']['size'] > 0 && $data['files']['pdf']['error'] === UPLOAD_ERR_INI_SIZE )
        {
             $err['pdf_file']  = 'ファイルサイズが大きすぎます';
        }
        elseif ( isset( $data['files']['pdf'] ) && $data['files']['pdf']['size'] > 0 && $data['files']['pdf']['error'] > 0 && $this->check_pdf( $data['files']['pdf'] ) === false )
        {
            $err['pdf']  = 'PDFファイル以外は受け付けません';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'LabelInformations.deleted IS' => null
             ])
             ->contain([
                 'Files' => [
                    'conditions' =>  [
                        'Files.mime_type LIKE' => 'image/%'
                        ,'Files.deleted IS' => null
                    ]
                    ,'sort' => ['Files.created DESC']
                 ]
             ])
             ->order([
                 'LabelInformations.release_date' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'LabelInformations.id'          => $id
                ,'LabelInformations.deleted IS' => null
            ])
            ->contain([
                'Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                    ,'sort' => ['Files.created DESC']
                ]
            ])
            ->first();
    }

    public function getDataWithPdf( $id=null )
    {
        return $this->find()
            ->select(['id'])
            ->where([
                'LabelInformations.id'          => $id
                ,'LabelInformations.deleted IS' => null
            ])
            ->contain([
                'Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                        ,'Files.mime_type' => 'application/pdf'
                    ]
                    ,'sort' => ['Files.created DESC']
                ]
            ])
            ->first();
    }

    public function getFrontInformations( $params=[] )
    {
        $query = $this->find()
            ->select([
                'LabelInformations.id'
                ,'LabelInformations.title'
                ,'LabelInformations.content'
                ,'LabelInformations.type'
                ,'LabelInformations.link_url'
                ,'LabelInformations.release_date'
                ,'LabelInformations.created'
            ])
            ->where([
                'LabelInformations.contractant_id'   => $params['contractant_id']
                ,'LabelInformations.display_flg'     => 1
                ,'LabelInformations.release_date <=' => date( 'Y-m-d' )
                ,'LabelInformations.deleted IS'      => null
                ,'OR' => [
                    'LabelInformations.close_date IS' => NULL
                    ,'LabelInformations.close_date >' => date( 'Y-m-d H:i:s' )
                ]
            ])
            ->contain([
                // サムネイル
                'Images' => [
                   'conditions' =>  [
                       'Images.mime_type LIKE' => 'image/%'
                       ,'Images.deleted IS' => null
                   ]
                   ,'fields' => [
                       'RelLabelInformationFiles.label_information_id', 'Images.file_path'
                   ]
                   ,'sort' => ['Images.modified DESC']
                ]
                // PDF
                ,'Pdfs' => [
                   'conditions' =>  [
                       'Pdfs.mime_type'   => 'application/pdf'
                       ,'Pdfs.deleted IS' => null
                   ]
                   ,'fields' => [
                       'RelLabelInformationFiles.label_information_id', 'Pdfs.file_path'
                   ]
                   ,'sort' => ['Pdfs.created DESC']
                ]
            ])
            ->order([ 'LabelInformations.release_date' => 'DESC' ]);

        if( isset( $params['user_type'] ) )
        {
             $usertype = $params['user_type'];
             $query->where([
                 'OR' => [
                     'LabelInformationUserTypes.user_type IS' => null
                     ,'LabelInformationUserTypes.user_type'   => $params['user_type']
                 ]
             ]);
             $query->leftJoinWith( 'LabelInformationUserTypes' );
        }

        // 件数
        if( isset( $params['limit'] ) && preg_match( '/^[0-9]+$/', $params['limit'] ) === 1 )
        {
            $query->limit( $params['limit'] );
        }
        else
        {
            $query->limit( INFORMATION_LIMIT );
        }
        // ページング
        if( isset( $params['page'] ) && $params['page'] > 0 )
        {
            $query->page( $params['page'] );
        }

        return $query->all();

    }

    //表示フラグ:1 通知フラグ:0 で公開日を過ぎているもの
    public function getUnannouncedData( $contractant_id=null )
    {
         return $this->find()
             ->where([
                 'contractant_id'   => $contractant_id
                 ,'display_flg'     => 1
                 ,'notified_flg'    => 0
                 ,'release_date <=' => date( 'Y-m-d H:i:s' )
                 ,'deleted IS'      => null
             ])
             ->all();
             //->count();
    }
}
