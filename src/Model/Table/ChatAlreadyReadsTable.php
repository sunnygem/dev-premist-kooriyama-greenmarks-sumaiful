<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ChatAlreadyReadsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Threads');
    }
    public function checkUnread( $contractant_id, $user_id, $front_user_id )
    {
        return $this->find()
            ->where( [
                'ChatAlreadyReads.contractant_id' => $contractant_id
                ,'ChatAlreadyReads.user_id'       => $user_id
                ,'ChatAlreadyReads.front_user_id' => $front_user_id
                ,'ChatAlreadyReads.deleted IS'    => null
                ,'Threads.write_update_date >'    => 'ChatAlreadyReads.readtime'
            ] )
            ->contain( [ 'Threads'] )
            ->count();
    }
    public function getUnreadUsers( $contractant_id, $user_id, $front_user_id )
    {
        $res = $this->find()
            ->where( [
                'contractant_id'         => $contractant_id
                ,'user_id'               => $user_id
                ,'front_user_id'         => $front_user_id
                ,'chat_already_read_flg' => 0
                ,'deleted IS'            => null
            ] )
            ->contain( [ 'Threads' ] )
            ->all();
    }

    // flg::時間を更新
    public function saveChatAlreadyReadData( $contractant_id, $thread_id, $user_id, $front_user_id, $flg=true )
    {
        $res = $this->find()
            ->where( [
            'contractant_id' => $contractant_id
            ,'thread_id'     => $thread_id
            ,'user_id'       => $user_id
            ,'front_user_id' => $front_user_id
        ] )->first();
        if ( $res->isEmpty() === false )
        {
            $data = [];
            if ( $flg )
            {
                $data = [
                    'id' => $res->id
                ];
            }
        }
        else
        {
            $data = [
                'contractant_id' => $contractant_id
                ,'thread_id'     => $thread_id
                ,'user_id'       => $user_id
                ,'front_user_id' => $front_user_id
            ];
        }
        if ( $flg === true )
        {
            $data['readtime'] = date( 'Y-m-d H:i:s' );
        }
        if ( count( $data ) > 0 )
        {
            self::saveData( $data );
        }
    }
    public function updateChatAlreadyReadData( $contractant_id, $user_id, $front_user_id, $thread_id, $chat_message_id )
    {
        $this->updateAll(
            [
                'chat_already_read_flg' => 1
            ]
            ,[
                'contractant_id'         => $contractant_id
                ,'user_id'               => $user_id
                ,'front_user_id'         => $front_user_id
                ,'thread_id'             => $thread_id
                ,'chat_message_id'       => $chat_message_id
            ]
        );
    }
    public function updateChatAlreadyReadForGetMessage( $contractant_id, $user_id, $front_user_id, $thread_id )
    {
        $this->updateAll(
            [
                'chat_already_read_flg' => 1
            ]
            ,[
                'contractant_id'         => $contractant_id
                ,'user_id'               => $user_id
                ,'front_user_id'         => $front_user_id
                ,'thread_id'             => $thread_id
            ]
        );
    }
    public function saveData( $data, $parent_id=null )
    {
        parent::saveData( $data );
    }
}

