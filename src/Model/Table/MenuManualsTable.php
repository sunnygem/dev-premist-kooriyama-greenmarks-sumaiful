<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MenuManualsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('ContractantServiceMenus');

        $this->belongsToMany('Files', [
            'joinTable' => 'rel_menu_manual_files'
            ,'conditions' => [
                'RelMenuManualFiles.deleted IS' => null
            ]
            ,'order' => [
                'Files.modified' => 'DESC'
            ]
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    //public function saveData( $data, $id = null )
    //{
    //    return parent::saveData( $data );
    //}

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    public function setEditFlgByContractantServiceMenuId( $contractant_service_menu_id=null )
    {
        return $this->updateAll(
            [ 'edit_flg' => 1 ]
            ,[
                'contractant_service_menu_id' => $contractant_service_menu_id
                ,'deleted IS'                 => null
            ]
        );
    }
    public function setDeletedOnEditFlg( $contractant_service_menu_id=null )
    {
        $res = $this->find()
           ->select( 'id' )
           ->where([
               'contractant_service_menu_id' => $contractant_service_menu_id
               ,'edit_flg'                   => 1
               ,'deleted IS'                 => null
           ])
           ->all();
        $arr_id = array_column( $res->toArray(), 'id' );

        if( count( $arr_id ) )
        {
            $this->updateAll(
                ['deleted' => date('Y-m-d H:i:s')]
                ,[ 'id IN ' => $arr_id ]
            );
        }

        return $arr_id;
    }

    public function getDetailById( $id=null )
    {
         return $this->find()
         ->where([
             'deleted IS' => null
         ])
         ->first();
    }
    public function getDetailByContractantSeriviceMenuId( $id=null, $where=[], $select=null )
    {
         $query = $this->find()
         ->where([
             'MenuManuals.contractant_service_menu_id' => $id
             ,'MenuManuals.deleted IS' => null
         ])
         ->contain([
             'Files' => function($query){
                 return $query->select(['id', 'file_path', 'mime_type' ]);
             }
         ]);

         if( $select !== null ) $query->select( $select );
         if( count( $where ) > 0 ) $query->where( $where );

         return $query->all();
    }

    public function checkDataExistByCategoryId($contractant_service_menu_id = null, $category_id = null)
    {
        $res = $this->find()
            ->where([
                'contractant_service_menu_id' => $contractant_service_menu_id
                ,'category_id'                => $category_id
                ,'deleted IS'                 => null
            ])
            ->count();

        return ( $res > 0 ) ? true : false;
    }

}
