<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class LineOpenChatCategoriesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        // カテゴリ
        $this->hasMany( 'LineOpenChats', [
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['name'] ) && mb_strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where([
                'LineOpenChatCategories.deleted IS' => null
            ])
            ->leftJoinWith('LineOpenChats', function ($q) {
                return $q->select(['total_registered' => 'COUNT( LineOpenChats.id )'])
                    ->where(['LineOpenChats.deleted IS' => null]);
            })
            ->group(['LineOpenChatCategories.id'])
            ->order([
                'LineOpenChatCategories.orderby' => 'ASC'
            ])
            ->enableAutoFields(true);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );

        // todo 欠番したorderbtyをオフセットする
    }

    public function getOptions()
    {
        return $this->find('list')
            ->where([
                'deleted IS' => null
            ])
            ->order(['orderby' => 'ASC'])
            ->toArray();
    }

    public function getData( $options=[] )
    {
        $q = $this->find()
            ->where([
                'LineOpenChatCategories.deleted IS' => null
            ]);

        if( isset($options['fields']) )  $q->select( $options['fields'] );
        if( isset($options['where']) )   $q->where( $options['where'] );
        if( isset($options['contain']) ) $q->contain( $options['contain'] );
        if( isset($options['order']) )   $q->order( $options['order'] );
        if( isset($options['group']) )   $q->group( $options['group'] );

        return $q->all();
    }

    public function getNextOrderBy()
    {
        $q = $this->find()
            ->where([
                'LineOpenChatCategories.deleted IS' => null
            ]);

        return ( $q->count() ) ? $q->count() + 1 : 1;
    }

    public function checkRegistered( $id )
    {
        $q = $this->find()
               ->where([ 'LineOpenChatCategories.id' => $id ])
               ->leftJoinWith('LineOpenChats', function ($q) {
                return $q->select(['total_registered' => 'COUNT( LineOpenChats.id )'])
                    ->where(['LineOpenChats.deleted IS' => null]);
            })
            ->first();
        return ( $q->total_registered > 0 ) ? false : true;
    }

    public function getDataForFront( $contractant_id, $domain=null )
    {
        $options = [
            'fields' => [
                'id',
                'name',
                'name_en',
                'icon_url_path',
            ]
            ,'where' => [
                'LineOpenChatCategories.contractant_id' => $contractant_id
                ,'LineOpenChatCategories.display_flg' => 1
            ]
            ,'order' => ['LineOpenChatCategories.orderby' => 'ASC' ]
        ];
        if( $domain !== null )
        {
            $options['fields']['full_icon_url_path'] = "CONCAT( 'https://" . $domain . DS . "', LineOpenChatCategories.icon_url_path )";
        }

        return self::getData( $options );
    }

}
