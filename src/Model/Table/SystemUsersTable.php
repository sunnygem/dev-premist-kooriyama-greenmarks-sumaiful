<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class SystemUsersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo( 'Users' );
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'deleted IS' => null
             ]);
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function deleteDataByUserId( $id )
    {
        $this->query()
            ->update()
            ->set(['deleted' => date('Y-m-d H:i:s')])
            ->where(['user_id' => $id ])
            ->execute();
    }

    // cookieのkeyを保存
    public function setCookieKey( $id, $cookie_key )
    {
        $data = [
            'id' => $id
            ,'cookie_key' => $cookie_key
        ];
        return self::saveData( $data );
    }
 
}
