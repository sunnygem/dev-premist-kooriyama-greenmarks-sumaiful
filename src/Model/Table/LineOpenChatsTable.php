<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class LineOpenChatsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        // カテゴリ
        $this->belongsTo( 'LineOpenChatCategories', [
        ]);

    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }

        if( isset( $data['url'] ) && mb_strlen( $data['url'] ) === 0 )
        {
            $err['url'] = '入力してください';
        }
        else if( mb_strlen( $data['url'] ) > 256 )
        {
            $err['url'] = 'URLが長すぎます。';
        }

        if( isset( $data['line_open_chat_category_id'] ) && $data['line_open_chat_category_id'] === '' )
        {
            $err['line_open_chat_category_id'] = '選択してください';
        }

        if( isset( $data['release_date'] ) && mb_strlen( $data['release_date'] ) === 0 )
        {
            $err['release_date'] = '入力してください';
        }

        return $err;
    }

    // paginator 汎用
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'LineOpenChats.deleted IS' => null
            ])
            ->contain([
            ])
            ->order([
        //        'LineOpenChats.release_date' => 'DESC'
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    // paginator フロント
    public function findFront( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->select([
                'id',
                'title',
                'title_en',
                'url',
                'icon_url_path',
                'pickup_flg',
                'release_date',
            ])
            ->where( [
                'LineOpenChats.deleted IS' => null
                ,'LineOpenChats.display_flg'   => 1
                ,[
                    'OR' => [
                        'LineOpenChats.release_date IS' => null
                        ,'LineOpenChats.release_date <=' => date('Y-m-d H:i:s')
                    ]
                ]
                ,[
                    'OR' => [
                        'LineOpenChats.close_date IS'   => null
                        ,'LineOpenChats.close_date >=' => date('Y-m-d') . ' 23:59:59'
                    ]
                ]
            ])
            ->contain([
                'LineOpenChatCategories' => [
                    'fields' => [
                        'category_name' => 'LineOpenChatCategories.name'
                    ]
                ]
            ])
            ->order([
                'LineOpenChats.release_date' => 'DESC'
                ,'LineOpenChats.pickup_flg'  => 'DESC'
                ,'LineOpenChats.modified'    => 'DESC'
            ]);

        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }



    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'LineOpenChats.id'          => $id
                ,'LineOpenChats.deleted IS' => null
            ])
            ->contain([])
            ->first();
    }

}
