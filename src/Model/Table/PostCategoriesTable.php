<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PostCategoriesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('ContractantServiceMenus');
    }

    public function validation( $data=[] )
    {
        $err = [];


        if( isset( $data['label'] ) && mb_strlen( $data['label'] ) === 0 )
        {
            $err['label'] = '入力してください';
        }

        return $err;
    }

    public function getCategoryByServiceMenuId( $service_menu_id=null, $where=[] )
    {
        $query = $this->find()
            ->where([
                 'PostCategories.contractant_service_menu_id' => $service_menu_id
                 //,'PostCategories.display_flg'                => 1
                 ,'PostCategories.deleted IS'                 => NULL
            ])
            ->order(['orderby' => 'ASC']);

        if( count( $where ) > 0 ) $query->where( $where );

        return $query->all();
    }


    public function getOrder( $service_menu_id=null )
    {
        $res = $this->find()
            ->where([
                'contractant_service_menu_id' => $service_menu_id
                ,'deleted IS'    => null
            ]);

        return $res->count();
    }


}
