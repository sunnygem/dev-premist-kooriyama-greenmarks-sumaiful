<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class LabelInformationUserTypesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('LabelInformations');
    }

    public function getEditDataByLabelInformationId( $contractant_id, $id )
    {
         return $res = $this->find('list', [
                 'valueField' => 'user_type'
             ])
             ->where([
                 'contractant_id'        => $contractant_id
                 ,'label_information_id' => $id

             ])
             ->toList();
    }

}
