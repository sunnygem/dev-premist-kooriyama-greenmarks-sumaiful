<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PresentationBuildingsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Presentations');
        $this->belongsTo('Buildings');
    }

    public function getEditDataByInformationId( $contractant_id, $id )
    {
         return $res = $this->find('list', [
                 'valueField' => 'building_id'
             ])
             ->where([
                 'contractant_id'  => $contractant_id
                 ,'presentation_id' => $id

             ])
             ->toList();
    }

}
