<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class UserDeviceTokensTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('FrontUsers');
    }

    public function validation( $data=[] )
    {
        $err=[];
        return $err;
    }

    // paginator
    //public function findSearch( \Cake\ORM\Query $query, array $options )
    //{
    //     return $query
    //         //->select( [
    //         //    'id'
    //         //] )
    //         ->where( [
    //         ]);
    //}

    public function saveData( $data, $parent_id=null )
    {
        //重複をしていなければ登録
        $query = $this->find()->where( $data );
        if( $query->count() === 0 && $data['device_token'] !== '' )
        {
            parent::saveData( $data );
        }
        else if ( $data['device_token'] === '' )
        {
            return;
        }
        else
        {
            $res = $query->toArray();
            $tmp = [
                'id' => $res[0]['id']
                ,'modified' => date( 'Y-m-d H:i:s' )
            ];
            parent::saveData( $tmp );
        }
    }

    public function deleteDeviceToken( $contractant_id, $device_token )
    {
        $data = [
            'contractant_id' => $contractant_id
            ,'device_token' => $device_token
        ];
        $this->deleteAll( $data );
    }
    
}
