<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class AdminUsersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Users');
    }

    public function validation( $data=[] )
    {
        $err=[];

        if( isset( $data['name'] ) && strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        if( isset( $data['email'] ) && strlen( $data['email'] ) === 0 )
        {
            $err['email'] = '入力してください';
        }
        elseif( isset( $data['email'] ) && $this->check_email( $data['email'] ) === false )
        {
            $err['email'] = 'メールの書式を確認してください';
        }

        if( isset( $data['authority'] ) && strlen( $data['authority'] ) === 0 )
        {
            $err['authority'] = '選択してください';
        }
        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'AdminUsers.deleted IS' => null
             ]);
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function checkExitInitUser( $login_id )
    {
        $res = $this->find()
            ->where([
                'login_id' => $login_id
            ])
            ->count();

        return ( $res > 0 ) ? true : false;
    }

    // 管理画面
    public function deleteDataByUserId( $id )
    {
        $this->query()
            ->update()
            ->set(['deleted' => date('Y-m-d H:i:s')])
            ->where(['user_id' => $id ])
            ->execute();
    }

    public function getDataByRemindkey( $key, $contractant_id )
    {
        return $this->find()
            ->where([
                'AdminUsers.remind_key'      => $key
                ,'AdminUsers.contractant_id' => $contractant_id
                //,'AdminUsers.init_flg'       => 0
                ,'AdminUsers.deleted IS'     => null
            ])
            ->contain('Users')
            ->first();
    }

    public function getAdminUserByEmail( $email, $contractant_id )
    {
        return $this->find()
        ->where([
            'AdminUsers.deleted IS'      => null
            ,'AdminUsers.contractant_id' => $contractant_id
            ,'Users.email'               => $email
            ,'Users.deleted IS'          => null
        ])
        ->contain(['Users'])
        ->first();
 
    }

}
