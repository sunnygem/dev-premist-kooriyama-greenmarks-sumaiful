<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ImageSharingsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        // 画像
        $this->hasMany( 'RelImagesharingFiles', [
            'conditions' => [
                'RelImagesharingFiles.deleted IS' => null
            ]
        ]);
        $this->hasMany('ImageSharingBuildings');
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }

        if( isset( $data['contractant_id'] ) && $data['contractant_id'] === '' )
        {
            $err['contractant_id'] = '選択してください';
        }

        if( isset( $data['release_date'] ) && mb_strlen( $data['release_date'] ) === 0 )
        {
            $err['release_date'] = '入力してください';
        }

        if( isset( $data['files']['image'] ) && count( $data['files']['image'] ) > 0 )
        {
            foreach( $data['files']['image'] as $key => $val )
            {
                if( $val['size'] > 0 && $val['error'] === UPLOAD_ERR_INI_SIZE )
                {
                    $err['files']['image'][$key]  = 'ファイルサイズが大きすぎます';
                }
                elseif ( isset( $val ) && $val['size'] > 0 && $val['error'] > 0 && $this->check_image( $val['image'] ) === false )
                {
                    $err['files']['image'][$key]  = '画像ファイル( png, jpg, gif )以外は受け付けません';
                }
            }
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'ImageSharings.deleted IS' => null
            ])
            ->contain([
                'RelImagesharingFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
            ])
            ->order([
                'ImageSharings.id' => 'ASC'
            ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'ImageSharings.id'          => $id
                ,'ImageSharings.deleted IS' => null
            ])
            ->contain([
                 'RelImagesharingFiles.Files' => [
                    'conditions' => [
                        'Files.deleted IS' => null
                    ]
                ]
            ])
            ->first();
    }

    public function getAlbumData( $params=[] )
    {
        if( isset( $params['contractant_id'] ) )
        {
            $res = $this->find()
                ->select([
                    'ImageSharings.id'
                    ,'ImageSharings.title'
                    ,'ImageSharings.release_date'
                ])
                ->where([
                    'ImageSharings.contractant_id'   => $params['contractant_id']
                    ,'ImageSharings.display_flg'     => 1
                    ,'ImageSharings.release_date <=' => date( 'Y-m-d' )
                    ,'OR' => [
                        'ImageSharings.close_date IS' => NULL
                        ,'ImageSharings.close_date >' => date( 'Y-m-d' ) . ' 23:59:59'
                    ]
                ])
                ->contain([
                    // 画像
                    'RelImagesharingFiles' => [
                        'fields' => [ 'image_sharing_id', 'file_id' ]
                        ,'sort' => [ 'RelImagesharingFiles.orderby' => 'ASC' ]
                        ,'conditions' => [
                            'Files.display_flg' => 1
                        ]
                    ]
                    ,'RelImagesharingFiles.Files' => [
                        'fields' => [ 'name', 'file_path', 'mime_type' ]

                    ]
                ]);

            if( isset( $params['building_id'] ) )
            {
                $res->leftJoin(
                    ['ImageSharingBuildings' => 'image_sharing_buildings'],
                    [ 'ImageSharingBuildings.image_sharing_id = ImageSharings.id' ]
                )
                ->where([
                    // 全公開もしくはユーザーのbuilding_idに合致するもの
                    'OR' => [
                        'ImageSharings.all_building_flg'     => 1
                        ,'ImageSharingBuildings.building_id' => $params['building_id']
                    ]
                ]);
            }

            if( isset( $params['id'] ) )
            {
                return $res->where(['ImageSharings.id' => $params['id']])
                    ->first();
            }
            else
            {
                if( isset( $params['limit'] ) && preg_match( '/^[0-9]+$/', $params['limit'] ) === 1 )
                {
                    $res->limit( $params['limit'] );
                }

                // ページング
                if( isset( $params['page'] ) && $params['page'] > 0 )
                {
                    $res->page( $params['page'] );
                }

                return $res->order([ 'ImageSharings.modified' => 'DESC' ])
                    ->all();
            }
        }
        else
        {
            return [];
        }

    }

    //表示フラグ:1 通知フラグ:0 で公開日を過ぎているもの
    public function getUnannouncedData( $contractant_id=null )
    {
         return $this->find()
             ->where([
                 'contractant_id'   => $contractant_id
                 ,'display_flg'     => 1
                 ,'notified_flg'    => 0
                 ,'release_date <=' => date( 'Y-m-d H:i:s' )
                 ,'deleted IS'      => null
             ])
             ->contain([ 'ImageSharingBuildings' ])
             ->all();
             //->count();
    }

}
