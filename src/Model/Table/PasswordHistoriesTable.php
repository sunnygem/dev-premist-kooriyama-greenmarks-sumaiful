<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

use Cake\Auth\DefaultPasswordHasher;

class PasswordHistoriesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Users', []);

    }

    public function validation( $data=[] )
    {
        $err = [];
        return $err;
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function checkProhibitionNum( $user_id, $password, $security=[] )
    {
        $flg = true;
        $res= $this->find()
            ->where([
                'user_id'      => $user_id
            ])
            ->order([ 'id' => 'desc' ])
            ->limit( $security['prohibition_num'] )
            ->all();
        if ( $res->count() !== 0 )
        {
            $hasher = new DefaultPasswordHasher;
            foreach( $res as $val )
            {
                if ( $hasher->check( $password, $val['password'] ) )
                {
                    $flg = false;
                }
            }
            return $flg;
        }
        return $flg;
    }


}
