<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ChatGroupsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->hasMany('ChatGroupMembers');
    }
    public function getByContractantId( $contractant_id )
    {
        $res = $this->find()
            ->where( [
                'contractant_id' => $contractant_id
                ,'deleted IS'    => null
            ] );
        return ( $res->isEmpty() === false ) ? $res->first() : null;
    }
}

