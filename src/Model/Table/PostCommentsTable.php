<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PostCommentsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Posts');
        $this->belongsTo('Users');

        $this->HasMany('PostCommentLikes', [
            'foreignKey' => 'comment_id'
        ]);

        $this->HasMany('ChildPostComments', [
            'className'   => 'PostComments'
            ,'foreignKey' => 'parent_comment_id'
            ,'conditions' => [
                'parent_comment_id IS NOT' => null
            ]
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    public function api_validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    public function getComments( $conditions=[], $limit=null, $page=0 )
    {
        if( isset( $conditions['contractant_id'] ) )
        {
            $q = $this->find()
               ->select([
                   'id',
                   'user_id',
                   'comment',
                   'created'
               ])
               ->where([
                   'PostComments.contractant_id'        => $conditions['contractant_id']
                   ,'PostComments.parent_comment_id IS' => null
                   ,'PostComments.deleted IS'           => null
                   ,'FrontUsers.id IS NOT'              => null
               ])
               ->contain([
                   'Users' => [
                       'fields' => [ 'id' ]
                   ]
                   ,'Users.FrontUsers' => [
                       'fields' => [  'nickname', 'thumbnail_path', 'leave_flg' ]
                   ]
                   // いいね
                   ,'PostCommentLikes' => [
                       'fields' => [ 'comment_id', 'user_id' ]
                       ,'conditions' => [ 'FrontUsers.id IS NOT' => null ]
                   ]
                   ,'PostCommentLikes.Users.FrontUsers' => [
                       'fields' => [ 'Users.id', 'FrontUsers.id', 'FrontUsers.nickname', 'FrontUsers.leave_flg', 'FrontUsers.thumbnail_path' ]
                   ]
                   // コメント返信
                   ,'ChildPostComments' => [
                       'fields' => [ 'id', 'user_id', 'parent_comment_id', 'comment', 'created' ]
                       ,'conditions' => [
                           'ChildPostComments.deleted IS' => null
                           ,'FrontUsers.id IS NOT'        => null
                       ]
                       ,'sort' => ['ChildPostComments.created' => 'ASC']
                   ]
                   ,'ChildPostComments.Users' => [
                       'fields' => [ 'Users.id' ]
                   ]
                   ,'ChildPostComments.Users.FrontUsers' => [
                       'fields' => [ 'thumbnail_path', 'leave_flg', 'nickname' ]
                   ]
                   // コメントにいいね
                   ,'ChildPostComments.PostCommentLikes' => [
                       'fields' => [ 'PostCommentLikes.comment_id', 'PostCommentLikes.user_id' ]
                   ]
                   ,'ChildPostComments.PostCommentLikes.Users.FrontUsers' => [
                       'fields' => [ 'Users.id', 'FrontUsers.id', 'FrontUsers.nickname', 'FrontUsers.leave_flg', 'FrontUsers.thumbnail_path' ]
                   ]
               ])
               ->order(['PostComments.created' => 'ASC']) ;

            if( isset( $conditions['post_id'] ) )
            {
                $q->where([
                    'PostComments.post_id' => $conditions['post_id']
                ]);
            }
           if( $limit !== null ) $q->limit( $limit );
           if( $page > 0 )       $q->page( $page );

            return $q->all();
        }
        else
        {
            return false;
        }

    }


}
