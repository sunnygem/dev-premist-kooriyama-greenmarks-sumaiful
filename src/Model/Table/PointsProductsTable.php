<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PointsProductsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
    }

    public function validation( $data=[] )
    {
        $err = [];
        
        if( isset( $data['name'] ) && mb_strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }
        
        if( isset( $data['use_points'] ) && mb_strlen( $data['use_points'] ) === 0 )
        {
            $err['use_points'] = '入力してください';
        }

        return $err;
    }

    // paginator 汎用
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'PointsProducts.deleted IS' => null
            ])
            ->contain([
            ])
            ->order([
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }
    
    // paginator フロント
    public function findFront( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->select([
                'id',
                'name',
                'name_en',
                'icon_url_path',
                'use_points',
            ])
            ->where( [
                'PointsProducts.deleted IS' => null
                ,'PointsProducts.display_flg'   => 1
            ])
            ->order([
            ]);

        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );

    }
    
    public function getData( $contractant_id )
    {
        return $this->find()
            ->select([
                'name'
                ,'name_en'
                ,'use_points'
                ,'icon_url_path'
            ])
            ->where([
                'PointsProducts.contractant_id' => 4
                ,'PointsProducts.deleted IS' => null
                ,'PointsProducts.display_flg'   => 1
            ])
            ->toArray();
    }
    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'PointsProducts.id'          => $id
                ,'PointsProducts.deleted IS' => null
            ])
            ->contain([])
            ->first();
    }
}
