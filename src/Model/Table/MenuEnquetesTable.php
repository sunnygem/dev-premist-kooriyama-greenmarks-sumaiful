<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MenuEnquetesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('ContractantServiceMenus');

        $this->hasMany('EnqueteQuestions', [
            'foreignKey' => 'enquete_id'
        ]);
        $this->hasMany('EnqueteAnswers', [
            'foreignKey' => 'enquete_id'
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['title'] ) && mb_strlen( $data['title'] ) === 0 )
        {
            $err['title'] = '入力してください';
        }

        if( isset( $data['release_date'] ) && mb_strlen( $data['release_date'] ) === 0 )
        {
            $err['release_date'] = '入力してください';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query->where( [
                 'MenuEnquetes.deleted IS' => null
             ])
             ->order([
                 'MenuEnquetes.release_date' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id = null )
    {
        // アソシエーションごと登録
        if( isset( $data['enquete_questions'] ) )
        {
            $entitiy = $this->newEntity( $data, [
                'associated' => ['EnqueteQuestions']
            ]);
            return $this->save( $entitiy );
        }
        else
        {
            return parent::saveData( $data );
        }
    }

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    public function getDetailById( $id=null )
    {
         return $this->find()
         ->where([
             'MenuEnquetes.id' => $id
             ,'MenuEnquetes.contractant_id' => $this->_session->read('ContractantData.id')
             ,'MenuEnquetes.deleted IS'     => null
         ])
         ->contain([
             'EnqueteQuestions' => [
                 'conditions' => [
                     'EnqueteQuestions.deleted IS' => null
                 ]
                 ,'sort' => [ 'EnqueteQuestions.orderby' => 'ASC' ]
             ]
         ])
         ->first();
    }

    public function getDataByContractantSeriviceMenuId( $id=null, $display_flg=false )
    {
        $res = $this->find()
        ->where([
            'contractant_service_menu_id' => $id
            ,'deleted IS' => null
        ]);

        if( $display_flg === true ) $res->where(['display_flg' => 1]);
        return $res->all();
    }

    public function getDataByContractantId( $contractant_id=null, $display_flg=false )
    {
        $res = $this->find()
        ->where([
            'contractant_id' => $contractant_id
            ,'MenuEnquetes.release_date <=' => date('Y-m-d H:i:s')
            ,'OR' => [
                ['MenuEnquetes.close_date IS'  => null ]
                ,['MenuEnquetes.close_date >=' => date('Y-m-d H:i:s')]
            ]
            ,'deleted IS'    => null
        ])
        ->order(['release_date' => 'DESC']);

        if( $display_flg === true ) $res->where(['display_flg' => 1]);
        return $res->all();
    }

}
