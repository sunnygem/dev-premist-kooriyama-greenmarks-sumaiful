<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PostJoinsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Posts');
        $this->belongsTo('Users');
    }

    public function validation( $data=[] )
    {
        $err = [];


        return $err;
    }

    public function saveData( $data, $parent_id=null )
    {
        $res = self::checkDataExist( $data );
 
        // データがあれば削除
        if( $res > 0 )
        {
            $cond = [
                'contractant_id' => $data['contractant_id']
                ,'user_id' => $data['user_id']
                ,'post_id' => $data['post_id']
            ];
            return $this->deleteAll( $cond );
        }
        // データが無ければ登録
        else
        {
            parent::saveData( $data );
            return 'true';
        }
    }

    public function checkDataExist( $data )
    {
         if( isset( $data['contractant_id'] ) && isset( $data['user_id'] ) && isset( $data['post_id'] ) )
         {
             return $this->find()
                 ->where([
                     'contractant_id' => $data['contractant_id']
                     ,'user_id'       => $data['user_id']
                     ,'post_id'       => $data['post_id']
                 ]) 
                 ->count();
         }
         return false;

    }
}
