<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FaqsQuestionsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->hasMany( 'Faqs', [
        ]);

    }

    public function validation( $data=[] )
    {
        $err=[];

        if ( isset( $data['faq_category_id'] ) && $data['faq_category_id'] === '' )
        {
           $err['faq_category_id'] = '選択してください';
        }

        return $err;
    }

    // paginator 汎用
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'FaqsQuestions.deleted IS' => null
            ])
            ->contain([
            ])
            ->order([
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    // paginator フロント
    public function findFront( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->select([
                'id',
                'faq_category_id',
                'question',
                'question_en',
            ])
            ->where( [
                'FaqsQuestions.deleted IS' => null
                ,'FaqsQuestions.display_flg'   => 1

            ])
            ->order([
                'FaqsQuestions.created' => 'DESC'
            ]);

        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }
    
    public function getFaqsQuestionsData( $contractant_id=null )
    {
        $res = $this->find()
            ->select([
                'id', 'faq_category_id', 'question','question'
            ])
            ->where([
                'contractant_id' => $contractant_id
                ,'FaqsQuestions.display_flg'   => 1,
            ])
            ->order(['id' => 'ASC'])
            ->toArray();

        return $res;
    }

    public function getFaqsQuestionsDataFront( $contractant_id=null )
    {
        $res = $this->find()
            ->select([
                'FaqsQuestions.id',
                'FaqsQuestions.faq_category_id',
                'FaqsQuestions.question', 
                'FaqsQuestions.question_en',
                //'faqs.faqs_question_id',
                'faqs.answer',
                'faqs.answer_en',
                'faqs.icon_url_path',
            ])
            ->where([
                'FaqsQuestions.contractant_id' => 4
                ,'FaqsQuestions.display_flg'   => 1,
            ])
            ->join([
                'table' => 'faqs',
                'type' => 'inner',
                'conditions' => 'FaqsQuestions.id = faqs.faqs_question_id'
            ])
            ->toArray();

        return $res;
    }
    
    public function getFaqsQuestionsDataFrontId( $contractant_id=null, $category_id)
    {
        $res = $this->find()
            ->select([
                'FaqsQuestions.id',
                'FaqsQuestions.question', 
                'FaqsQuestions.question_en',
                'faqs.answer',
                'faqs.answer_en',
                'faqs.icon_url_path',
            ])
            ->where([
                'FaqsQuestions.contractant_id' => $contractant_id
                ,'FaqsQuestions.display_flg'   => 1
                ,'FaqsQuestions.faq_category_id'   => $category_id
            ])
            ->join([
                'table' => 'faqs',
                'type' => 'inner',
                'conditions' => 'FaqsQuestions.id = faqs.faqs_question_id'
            ])
            ->toArray();

        return $res;
    }
}
