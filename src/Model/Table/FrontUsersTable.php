<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;

use Cake\Core\Configure;

// エンドユーザ管理Model
class FrontUsersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Users');
        $this->belongsTo('Buildings');
        $this->belongsTo('EncryptionParameters');
        $this->hasOne('UserDeviceTokens');
    }

    public function validation( $data=[] )
    {
        $err=[];

        if( isset( $data['name'] ) && strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        if( isset( $data['nickname'] ) && strlen( $data['nickname'] ) === 0 )
        {
            $err['nickname'] = '入力してください';
        }
        elseif( isset( $data['nickname'] ) && $this->check_str_length( $data['nickname'], 30 ) )
        {
            $err['nickname'] = '30文字以内で入力してください';
        }

        if ( isset( $data['biography'] ) && strlen( $data['biography'] ) === 0 )
        {
            $err['biography'] = '入力してください';
        }
        elseif ( isset( $data['biography'] ) && $this->check_str_length( $data['biography'], 150 ) )
        {
            $err['biography'] = '150文字以内で入力してください';
        }

        if ( isset( $data['birth_place'] ) && strlen( $data['birth_place'] ) === 0 )
        {
            $err['birth_place'] = '入力してください';
        }
        elseif( isset( $data['birth_place'] ) && $this->check_str_length( $data['birth_place'], 30 ) )
        {
            $err['birth_place'] = '30文字以内で入力してください';
        }

        if ( isset( $data['university_undergraduate'] ) && strlen( $data['university_undergraduate'] ) === 0 )
        {
            $err['university_undergraduate'] = '入力してください';
        }
        elseif ( isset( $data['university_undergraduate'] ) && $this->check_str_length( $data['university_undergraduate'], 30 ) )
        {
            $err['university_undergraduate'] = '30文字以内で入力してください';
        }

        if ( isset( $data['hobby'] ) && strlen( $data['hobby'] ) === 0 )
        {
            $err['hobby'] = '入力してください';
        }
        elseif( isset( $data['hobby'] ) && $this->check_str_length( $data['hobby'], 50 ) )
        {
            $err['hobby'] = '50文字以内で入力してください';
        }

        return $err;
    }

    // ポイント消込時
    public function validationApply(Validator $validator)
    {
        $validator
            ->notEmpty('apply_point', '入力してください')
            ->add('apply_point', 'invalid', [
                'rule' =>  function($val, $context){
                     if( preg_match("/^[0-9]+$/", $val ) === 0 )
                     {
                         return '入力値が不正です';
                     }

                     return true;
                },
            ])
            ->add('point', 'invalid', [
                'rule' =>  function($val, $context){
                     if( $val < 0 )
                     {
                         return '入力値が不正です';
                     }
                     return true;
                },
            ])
            ;
        return $validator;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'FrontUsers.deleted IS' => null
             ]);
    }
    public function getAllData( $contractant_id )
    {
        return $this->find()
            ->where([
                'auth_flg'         => 1
                ,'contractant_id'  => $contractant_id
                ,'common_user_flg' => 0
                ,'deleted IS'      => NULL
            ])
            ->all();
    }

    public function getDataByUserIdAndContractantId( $user_id=null, $contractant_id=null )
    {
        return $this->find()
            ->where([
                'user_id'         => $user_id
                ,'contractant_id' => $contractant_id
                ,'deleted IS'     => NULL
            ])
            ->contain('EncryptionParameters')
            ->first();
    }

    public function getDataForChat( $id )
    {
        return $this->find()
            ->select( [
                'nickname'
                ,'thumbnail_path'
            ] )
            ->where( [
                'id' => $id
            ] )
            ->first();

    }

    public function checkExitInitUser( $login_id )
    {
        $res = $this->find()
            ->where([
                'login_id' => $login_id
            ])
            ->count();

        return ( $res > 0 ) ? true : false;
    }

    public function getUnautholizedBuildingRoomID( $contractant_id=null, $cond=[] )
    {
        $res = $this->find()
            ->where([
               'contractant_id' => $contractant_id
                ,'OR' => [
                    'auth_flg'     => 0
                    ,'reapply_flg' => 1
                ]
                ,'deleted IS' => null
            ])
            ->where( $cond )
            ->all();

        $arr = [];
        foreach( $res as $val )
        {
            $arr[] = $val->building_room_number_id;
        }
        return $arr;
    }

    public function deleteDataByUserId( $id )
    {
        $this->query()
            ->update()
            ->set(['deleted' => date('Y-m-d H:i:s')])
            ->where(['user_id' => $id ])
            ->execute();
    }

    public function checkDuplicateFamilyAuthKey( $key )
    {
        $res = $this->find()
            ->where([
                'family_auth_key' => $key
                ,'deleted IS'     => null
            ])
            ->all();

        return ( $res->count() > 0 ) ? true : false;
    }
    public function updateAccessToken( $access_token, $new_access_token )
    {
        $this->updateAll(
            [
                'access_token' => $new_access_token
                ,'modified'    => date( 'Y-m-d H:i:s' )
            ]
            ,[
                'access_token' => $access_token
            ]
        );
    }

    // 通知設定をチェック
    public function checkConfigPushNotification( $user_id=null, $contractant_id=null, $target=null )
    {
        $target_col = 'push_' . $target . '_flg';
        $res = $this->find()
            ->select( $target_col )
            ->where([
                'user_id'         => $user_id
                ,'contractant_id' => $contractant_id
            ])
            ->first();
        return $res->{$target_col};
    }


    // push用のユーザー一覧の取得
    public function getPushData( $contractant_id=null, $type='' )
    {
        $query =  $this->find()
            ->where([
                'FrontUsers.contractant_id'        => $contractant_id
                ,'FrontUsers.device_token IS NOT'  => null
                //,'FrontUsers.common_user_flg'      => 0
                ,'FrontUsers.deleted IS'           => null
            ])
            ->contain([
                'UserDeviceTokens' => [
                    'fields' => [ 'UserDeviceTokens.os', 'UserDeviceTokens.device_token' ]
                ]
            ]);
        if( $type === 'information' )
        {
            $query->where(['FrontUsers.push_information_flg' => 1]);
        }
        elseif( $type === 'image_sharing' )
        {
            $query->where(['FrontUsers.push_image_sharing_flg' => 1]);
        }
        else
        {
            return [];
        }
        return $query->all();
    }
    public function getPushDataByBuildingId( $arr_building_id=[], $type='' )
    {
        $query =  $this->find()
            ->where([
                'FrontUsers.device_token IS NOT' => null
                //,'FrontUsers.common_user_flg'    => 0
                ,'FrontUsers.deleted IS'         => null
                ,'FrontUsers.building_id IN'     => $arr_building_id
            ])
            ->contain([
                'UserDeviceTokens' => [
                    'fields' => [ 'UserDeviceTokens.os', 'UserDeviceTokens.device_token' ]
                ]
            ]);
        if( $type === 'information' )
        {
            $query->where(['FrontUsers.push_information_flg' => 1]);
        }
        elseif( $type === 'image_sharing' )
        {
            $query->where(['FrontUsers.push_image_sharing_flg' => 1]);
        }
        else
        {
            return [];
        }
        return $query->all();
    }

    public function getDataByIdAndUesrId( $id, $user_id )
    {
         return $this->find()
         ->where([
             'id' => $id
             ,'user_id' => $user_id
             ,'contractant_id' => $this->_session->read('ContractantData.id')
             ,'deleted IS'     => null
         ])
         ->first();
    }

    public function getDataByUserId( $user_id )
    {
        return $this->find()
        ->where([
            'user_id' => $user_id
            ,'deleted IS' => null
        ])
        ->first();
    }

    public function checkExistValidUser( $user_id, $front_user_id )
    {
        return $this->find()
            ->where([
                'FrontUsers.user_id'   => $user_id
                ,'FrontUsers.id'       => $front_user_id
                ,'FrontUsers.auth_flg' => 1
                ,'FrontUsers.deleted IS' => null
                ,'Users.deleted IS' => null
                ,'Users.lock_flg'   => 0
                ,'OR' => [
                    'Users.expire_date IS' => null
                    ,'Users.expire_date >' => date('Y-m-d H:i:s')
                ]
            ])
            ->contain(['Users'])
            ->first();

    }

    public function getDataById( $id, $contractant_id )
    {
        $res = $this->find()
            ->where([
                'FrontUsers.id' => $id
                ,'FrontUsers.contractant_id' => $contractant_id
                ,'FrontUsers.deleted IS'     => null
            ])
            ->contain('EncryptionParameters')
            ->first();
        return ( $res ) ? $res : false;
    }

    public function getAdminFrontUser( $contractant_id=null, $cond=[] )
    {
        $query = $this->find()
        ->where([
            'FrontUsers.contractant_id' => $contractant_id
            ,'FrontUsers.admin_flg'     => 1
            ,'FrontUsers.room_number'   => '999'
            ,'FrontUsers.deleted IS'    => null
            ,'Users.deleted IS'         => null
        ])
        ->order(['FrontUsers.created' => 'DESC'])
        ->contain([ 'Users', 'EncryptionParameters' ]);

        if( count( $cond ) > 0 )
        {
            $query->where( $cond );
        }

        return $query->all();
    }

    // 累計ポイントを取得
    public function getTotalPoint( $id )
    {
        $res = $this->find()
            ->select(['point'])
            ->where([
                'id' => $id
                ,'deleted IS' => null
            ])
            ->first();
        return ( $res ) ? $res->point : 0;
    }
    // ポイント付与用の日付情報を取得
    public function getDateForPoint( $id )
    {
        $res = $this->find()
            ->select([
                'last_access_date',
                'last_post_date',
                'last_comment_date',
                'last_like_date',
            ])
            ->where([
                'id' => $id
                ,'deleted IS' => null
            ])
            ->first();
        return ( $res ) ? $res->toArray() : null;
    }

    // アクション別ににポイントを付与
    public function setActionPoint( $id, $date_col )
    {
        $mt_point = Configure::read('mt_point');
        $current_point = self::getTotalPoint( $id );
        switch( $date_col )
        {
        case 'last_access_date':
            $increment = $mt_point['access_point'];
            break;
        case 'last_post_date':
            $increment = $mt_point['post_point'];
            break;
        case 'last_comment_date':
            $increment = $mt_point['comment_point'];
            break;
        case 'last_like_date':
            $increment = $mt_point['like_point'];
            break;
        default:
            $increment = false;
        }

        if( $increment )
        {
            $data = [
                'id'      => $id,
                'point'   => $current_point + $increment,
                $date_col => date('Y-m-d H:i:s')
            ];
            $entity = $this->newEntity( $data );
            $res = $this->save( $entity );
            return $res->{$date_col};
        }
    }
    
    // キャンペーン期間中の20pt付与
    public function setCampaignPoint( $id, $campain_point )
    {
        $current_point = self::getTotalPoint( $id );
        
        if( $current_point < 20 )
        {
            $data = [
                'id'      => $id,
                'point'   => $current_point + $campain_point
            ];
            
            $entity = $this->newEntity( $data );
            $res = $this->save( $entity );
            return $res;
        }
    }
}
