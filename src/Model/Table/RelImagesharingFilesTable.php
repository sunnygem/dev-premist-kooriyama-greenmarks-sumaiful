<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class RelImagesharingFilesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Files');
        $this->belongsTo('ImageSharings');
    }

    public function validation( $data=[] )
    {
        $err = [];
        return $err;
    }

    public function getDataByImageSharingId( $image_sharing_id )
    {
         return $this->find()
             ->where([
                 'RelImagesharingFiles.image_sharing_id' => $image_sharing_id
                 ,'RelImagesharingFiles.deleted IS' => null
             ])
             ->contain([
                 'Files'
             ])
             ->order(['RelImagesharingFiles.orderby' => 'ASC'])
             ->all();
    }

    public function getOrderby( $image_sharing_id )
    {
        return $this->find()
            ->where([
                'RelImagesharingFiles.image_sharing_id' => $image_sharing_id
                ,'RelImagesharingFiles.deleted IS' => null
            ])
            ->count();
    }


    // paginator
    //public function findSearch( \Cake\ORM\Query $query, array $options )
    //{
    //     $query
    //         ->where( [
    //             'deleted IS' => null
    //         ])
    //         ->order([
    //             'created' => 'DESC'
    //         ]);
    //    if(isset($options['sort'])) $query->order( $options['sort'] );
    //    return $query;
    //}

}
