<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MenuLinktypesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('ContractantServiceMenus');

        $this->belongsToMany('Files', [
            'joinTable' => 'rel_menu_linktype_files'
            ,'conditions' => [
                'Files.mime_type LIKE' => 'image/%'
            ]
            ,'order' => [
                'Files.modified' => 'DESC'
            ]
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    //public function saveData( $data, $id = null )
    //{
    //    return parent::saveData( $data );
    //}

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    public function setEditFlgByContractantServiceMenuId( $contractant_service_menu_id=null )
    {
        return $this->updateAll(
            [ 'edit_flg' => 1 ]
            ,[
                'contractant_service_menu_id' => $contractant_service_menu_id
                ,'deleted IS'                 => null
            ]
        );
    }
    public function setDeletedOnEditFlg( $contractant_service_menu_id=null )
    {
        $res = $this->find()
           ->select( 'id' )
           ->where([
               'contractant_service_menu_id' => $contractant_service_menu_id
               ,'edit_flg'                   => 1
               ,'deleted IS'                 => null
           ])
           ->all();
        $arr_id = array_column( $res->toArray(), 'id' );

        if( count( $arr_id ) )
        {
            $this->updateAll(
                ['deleted' => date('Y-m-d H:i:s')]
                ,[ 'id IN ' => $arr_id ]
            );
        }

        return $arr_id;
    }

    public function getDetailById( $id=null )
    {
         return $this->find()
         ->where([
             'deleted IS' => null
         ])
         ->first();
    }

    public function getDetailByContractantSeriviceMenuId( $id=null )
    {
         return $this->find()
         ->where([
             'contractant_service_menu_id' => $id
             ,'deleted IS' => null
         ])
         ->contain([ 'Files' ])
         ->all();
    }
}
