<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/* 物件管理クラス */
class BuildingsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        //契約者情報
        $this->belongsTo('Contractants', [
            'conditions' => [
                'Contractants.deleted IS' => null
            ]
        ]);

        $this->hasMany('FrontUsers', []);
        $this->hasMany('BuildingRoomNumbers', [
            'conditions' => [
                'BuildingRoomNumbers.deleted IS' => null
            ]
        ]);
        $this->hasMany('PresentationBuildings');

    }

    public function validation( $data=[] )
    {
        $err = [];

        $id = ( isset( $data['id'] ) ) ?  $data['id'] : null;

        return $err;
    }
    public function validationDefault(Validator $validator)
    {
        $validator->notEmpty('name', '物件名を入力してください')
            ->notEmpty('name_en', '物件名(英語)を入力してください');

        return $validator;
    }
 
    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             ->where( [
                 'Buildings.deleted IS' => null
             ])
             ->contain([
                 'BuildingRoomNumbers' 
                 ,'BuildingRoomNumbers' 
                 ,'BuildingRoomNumbers.FrontUsers' 
                 ,'BuildingRoomNumbers.FrontUsers.EncryptionParameters' 
                 ,'BuildingRoomNumbers.FrontUsers.Users' 
             ]);
    }

    public function saveData( $data, $id = null )
    {
        //return parent::saveData( $data );
        if( isset( $data['type'] ) )
        {

            switch( $data['type'] )
            {
            case 'system':
                $entitiy = $this->newEntity( $data, [
                    'associated' => ['SystemUsers']
                ]);
                return $this->save( $entitiy );

                break;
            case 'admin':
                $entitiy = $this->newEntity( $data, [
                    'associated' => ['AdminUsers']
                ]);
                return $this->save( $entitiy );

                break;
            case 'front':
                // アソシエーションごと登録
                if( isset( $data['front_user'] ) )
                {
                    $entitiy = $this->newEntity( $data, [
                        'associated' => ['FrontUsers']
                    ]);
                    return $this->save( $entitiy );
                }
                else
                {
                    return parent::saveData( $data );
                }
            }
        }
        else
        {
            return parent::saveData( $data );
        }

    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getData( $contractant_id=null, $id=null ) 
    {
        $q = $this->find()
            ->where([
                'contractant_id' => $contractant_id
                ,'deleted IS'    => null
            ]);
            

        if( $id === null )
        {
            return $q->order(['id' => 'ASC']) 
                ->all();
        }
        else
        {
            return $q->where(['id' => $id])
                ->first();
        }

    }

    public function getDataById( $id=null ) 
    {
        $res = $this->findById( $id );
        return ( $res->isEmpty() === false ) ? $res->first() : false;
    }

    // id : nameの配列を返す
    // bool $all_flg 選択に0 => 'ALL'を追加する
    public function getOptions( $contractant_id=null, $all_flg=false )
    {
        $options = $this->find('list')
            ->where([
                'contractant_id' => $contractant_id
                ,'display_flg'   => 1
                ,'deleted IS'    => null
            ])
            ->order(['id' => 'ASC'])
            ->toArray();

        if( $all_flg === true )
        {
            $options = $options + [ '0' => 'ALL' ];
        }

        return $options;
    }

    // api取得用に画像パスの入った配列を返す
    public function getOptionsForApi( $contractant_id, $all_flg=false, $domain=null )
    {
        $query = $this->find()
            ->select([
                'id',
                'name',
                'icon_path',
            ])
            ->where([
                'contractant_id' => $contractant_id
                ,'display_flg'   => 1
                ,'deleted IS'    => null
            ])
            ->order(['id' => 'ASC']);

        if( $domain !== null )
        {
            $query->select([
                'full_icon_path' => "CONCAT( 'https://" . $domain . DS . "',  icon_path )"
            ]);
        }

        $options = $query->toArray();

        if( $all_flg === true )
        {
            $options[] = [ 'id' => '0', 'name' => 'ALL' ];
        }

        return $options;
    }

    // 物件ごとに未承認のユーザーの人数を取得する
    public function getUnapprovedUserPerBuilding( $contractant_id, $building_id=null )
    {
        $res = $this->find()
            ->select(['id', 'name'])
            ->where([
                'Buildings.contractant_id' => $contractant_id
                ,'Buildings.display_flg'   => 1    // 表示中
                ,'Buildings.deleted IS'    => null //削除されていない
            ])
            ->contain([
                // building_idを持つfront_userをくっつける
                'FrontUsers' => [
                    'fields' => [ 'building_id' ] //カウントをする場合の記述
                    ,'conditions' => [
                        'OR' => [
                            'FrontUsers.auth_flg'      => 0 // 承認されていない
                            ,'FrontUsers.reapply_flg'  => 1 // もしくは、再申請中
                        ]
                        ,'FrontUsers.reject_comment IS' => null // 却下コメントが空=却下されていない
                        ,'FrontUsers.deleted IS' => null // 削除されていない
                    ]
                ]
            ]);
        if( $building_id !== null && preg_match( '/^[\d]+$/', $building_id ) )
        {
            $res->where([
                'Buildings.id' => $building_id
            ]);
        }
        return $res->all();
    }

    // 物件ごとの報告件数を取得
    public function getNonCompatibleCount( $contractant_id, $building_id=null )
    {
        $res = $this->find()
            ->select([
               'Buildings.id' 
               ,'Buildings.name' 
              // ,'count'  => 'COUNT(  )'
            ])
            ->where([
                'Buildings.contractant_id'  => $contractant_id
                ,'Buildings.display_flg'    => 1
                ,'Buildings.deleted IS'     => null
            ])
            ->contain([
                'PresentationBuildings' => function ($q) {
                    return $q
                        ->select([
                            'PresentationBuildings.building_id'
                            ,'count' => 'COUNT( PresentationBuildings.id )'
                        ])
                        ->contain(['Presentations'])
                        ->where([
                            'Presentations.checked IS'    => null
                            ,'Presentations.deleted IS'    => null
                        ])
                        ->distinct('PresentationBuildings.building_id');
                }
            ]);
        if( $building_id !== null && preg_match( '/^[\d]+$/', $building_id ) )
        {
            $res->where([
                'Buildings.id' => $building_id
            ]);
        }
        return $res->all();
    }

}
