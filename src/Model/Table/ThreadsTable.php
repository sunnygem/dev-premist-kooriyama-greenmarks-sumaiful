<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ThreadsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Users', [
            'foreignKey' => 'created_user_id'
        ]);
        $this->belongsTo('ContractantServiceMenus');
        $this->belongsTo('ChatGroups');
    }
    public function getData( $contractant_id, $chat_group_id, $contractant_service_menu_id )
    {
        $res = $this->find()
            ->where( [
                'Threads.contractant_id'               => $contractant_id
                ,'Threads.chat_group_id'               => $chat_group_id
                ,'Threads.contractant_service_menu_id' => $contractant_service_menu_id
                ,'Threads.deleted IS'                  => null
            ] )
            ->contain( [
                'Users'
                ,'Users.FrontUsers' => [
                    'fields' => [
                        'id', 'thumbnail_path'
                    ]
                ]
                ,'ContractantServiceMenus'
            ] );
        return ( $res->isEmpty() === false ) ? $res->all() : null;
    }

    public function getIdFromChatGroupId( $contractant_id, $chat_group_id, $contractant_service_menu_id, $user_id, $partner_user_id )
    {
        $res = $this->find()
            ->where( [
                'contractant_id'               => $contractant_id
                ,'chat_group_id'               => $chat_group_id
                ,'contractant_service_menu_id' => $contractant_service_menu_id
                ,'OR' => [
                    [ 'created_user_id' => $user_id ]
                    ,[ 'created_user_id' => $partner_user_id ]
                ]
            ] );
        return ( $res->isEmpty() === false ) ? $res->first()->id : null;
    }

    public function updateWriteUpdateDate( $id )
    {
        $data = [
            'id' => $id
            ,'write_update_date' => date( 'Y-m-d H:i:s' )
        ];
        parent::saveData( $data );
    }

    public function getDataByTitle( $user_id, $front_user_id, $target_user_id, $target_front_user_id )
    {
        return $this->find()
            ->where([
                'OR' => [
                    ['title' => $user_id . '-' . $front_user_id . 'x' . $target_user_id . '-' . $target_front_user_id ]
                   ,['title' => $target_user_id . '-' . $target_front_user_id . 'x' . $user_id . '-' . $front_user_id ]
                ]
                ,'deleted IS' => null
            ])
            ->order(['created' => 'DESC'])
            ->first();
    }
}

