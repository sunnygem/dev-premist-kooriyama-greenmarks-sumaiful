<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class RelInformationFilesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Files');
        $this->belongsTo('Informations');
        $this->belongsTo('LabelInformations');
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'deleted IS' => null
             ])
             ->order([
                 'created' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

}
