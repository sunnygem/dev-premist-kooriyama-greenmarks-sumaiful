<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class EncryptionParametersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
    }
    public function getDataById( $id )
    {
        $res = $this->find()
            ->where( [
                'id' => $id
                ,'expiration_date >=' => date( 'Y-m-d H:i:s' )
            ] );
        return ( $res->count() === 1 ) ? $res->first() : false;
    }
    public function getEncryptionParameters( $iv, $salt, $flg=false )
    {
        $data = [
            'iv'               => $iv
            ,'salt'            => $salt
            ,'expiration_date' => date( 'Y-m-d H:i:s', time() + ENCRYPTION_PARAMETER_EXPIRATION_DATE )
        ];
        $id = self::saveData( $data );
        if ( $flg === true )
        {
            $data['id'] = $id;
        }
        return $data;
    }
    public function saveData( $data, $parent_id=null )
    {
        return parent::saveData( $data );
    }
}
