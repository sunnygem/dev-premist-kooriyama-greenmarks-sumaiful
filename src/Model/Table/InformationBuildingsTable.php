<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class InformationBuildingsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Informations');
        $this->belongsTo('Buildings');
    }

    public function getEditDataByInformationId( $contractant_id, $id )
    {
         return $res = $this->find('list', [
                 'valueField' => 'building_id'
             ])
             ->where([
                 'contractant_id'  => $contractant_id
                 ,'information_id' => $id

             ])
             ->toList();
    }

}
