<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MenuContactTelsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('ContractantServiceMenus');
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    //public function saveData( $data, $id = null )
    //{
    //    return parent::saveData( $data );
    //}

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    public function getDetailById( $id=null )
    {
         return $this->find()
         ->where([
             'deleted IS' => null
         ])
         ->first();
    }
    public function getDetailByContractantSeriviceMenuId( $id=null )
    {
         return $this->find()
         ->where([
             'contractant_service_menu_id' => $id
             ,'deleted IS' => null
         ])
         ->first();
    }

}
