<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ChatMemberlistsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->hasOne( 'SelfFrontUser', [
                'className' => 'FrontUsers'
                ,'foreignKey' => 'id'
                ,'bindingKey' => 'self_front_user_id'
        ] );
        $this->hasOne( 'PartnerFrontUser', [
                'className' => 'FrontUsers'
                ,'foreignKey' => 'id'
                ,'bindingKey' => 'partner_front_user_id'
        ] );
    }

    public function createList( $contractant_id, $user_id, $front_user_id, $front_users )
    {
        if ( $front_users->isEmpty() === false )
        {
            $front_users = $front_users->toArray();
            foreach( $front_users as $key => $val )
            {
                // 自分の場合とそうでない場合
                if ( (int)$val->user_id === (int)$user_id && (int)$val->id === (int)$front_user_id )
                {
                    foreach( $front_users as $key2 => $val2 )
                    {
                        if ( (int)$val2->user_id !== (int)$user_id && (int)$val2->id !== (int)$front_user_id )
                        {
                            $data = [
                                'contractant_id'         => $contractant_id
                                ,'self_user_id'          => $val2->user_id
                                ,'self_front_user_id'    => $val2->id     
                                ,'partner_user_id'       => $user_id
                                ,'partner_front_user_id' => $front_user_id
                            ];
                            if ( self::check_empty( $data ) )
                            {
                                parent::saveData( $data );
                            }
                        }
                    }
                }
                else
                {
                    $data = [
                        'contractant_id'         => $contractant_id
                        ,'self_user_id'          => $user_id
                        ,'self_front_user_id'    => $front_user_id
                        ,'partner_user_id'       => $val->user_id
                        ,'partner_front_user_id' => $val->id
                    ];
                    if ( self::check_empty( $data ) )
                    {
                        parent::saveData( $data );
                    }
                }
            }
        }
    }
    public function check_empty( $data )
    {
        $res = $this->find()
            ->where( [
                'contractant_id'         => $data['contractant_id']
                ,'self_user_id'          => $data['self_user_id']
                ,'self_front_user_id'    => $data['self_front_user_id']
                ,'partner_user_id'       => $data['partner_user_id']
                ,'partner_front_user_id' => $data['partner_front_user_id']
                ,'deleted IS'            => null
            ] );

        return $res->isEmpty();
    }

    public function saveUnreadFlg( $contractant_id, $self_user_id, $self_front_user_id, $partner_user_id, $partner_front_user_id, $unread_flg=1 )
    {
        $this->updateAll(
            [
                'unread_flg' => $unread_flg
                ,'modified'  => date( 'Y-m-d H:i:s' )
            ]
            ,[
                'contractant_id'         => $contractant_id
                ,'self_user_id'          => $self_user_id
                ,'self_front_user_id'    => $self_front_user_id
                ,'partner_user_id'       => $partner_user_id
                ,'partner_front_user_id' => $partner_front_user_id
                ,'deleted IS'            => null
            ]
        );
    }

    public function getList( $where, $limit=null, $page=null )
    {
        $query = $this->find()
            ->where( [
                'PartnerFrontUser.id IS NOT'           => null
                ,'PartnerFrontUser.nickname IS NOT'    => 0
                ,'PartnerFrontUser.common_user_flg'    => 0
                ,'PartnerFrontUser.auth_flg'           => 1
                ,'PartnerFrontUser.reject_comment IS'  => null
                ,'PartnerFrontUser.building_id IS NOT' => null
                ,'PartnerFrontUser.deleted IS'         => null
                ,'PartnerFrontUser.admin_flg IS'       => 0
            ] )
            ->where( $where )
            ->order( [ 'ChatMemberlists.modified' => 'DESC', 'ChatMemberlists.id' ] )
            ->contain( [
                'SelfFrontUser'
                ,'PartnerFrontUser'
                ,'PartnerFrontUser.Buildings' => [
                    'fields' => [ 'id', 'name' ]
                ]
            ] );
        if ( $limit !== null && $page !== null )
        {
            $query->limit( $limit )
            ->page( $page );
        }
        if ( $query->isEmpty() === false )
        {
            $data = [];
            $res = $query->all();
            foreach( $res as $key => $val )
            {
                $tmp = [
                    'id'          => $val->partner_front_user->user_id
                    ,'front_user' => [
                        'id'               => $val->partner_front_user->id
                        ,'nickname'        => $val->partner_front_user->nickname
                        ,'thumbnail_path'  => $val->partner_front_user->thumbnail_path
                        ,'leave_flg'       => $val->partner_front_user->leave_flg
                        ,'biography'       => $val->partner_front_user->biography
                        ,'university_name' => $val->partner_front_user->university_name
                        ,'undergraduate'   => $val->partner_front_user->undergraduate
                        ,'club_name'       => $val->partner_front_user->club_name
                        ,'hobby'           => $val->partner_front_user->hobby
                        ,'building_name'   => $val->partner_front_user->building->name
                    ]
                ];
                if ( $val->unread_flg === 1 ) $tmp['unread_flg'] = 1;
                $data[] = $tmp;
            }

            return $data;
        }
        return $query->all();
    }

    public function check_unread( $contractant_id, $user_id, $front_user_id )
    {
        $res = $this->find()
            ->where( [
                //'self_user_id'        => $user_id
                //,'self_front_user_id' => $front_user_id
                'partner_user_id'        => $user_id
                ,'partner_front_user_id' => $front_user_id
                ,'unread_flg'         => 1
                ,'deleted IS'         => null
            ] )->all();
        return ( $res->isEmpty() !== false ) ? true : false;
    }
}

