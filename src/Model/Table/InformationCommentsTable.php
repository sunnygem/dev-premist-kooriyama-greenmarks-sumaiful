<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class InformationCommentsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Informations');
        $this->belongsTo('Users');
        $this->HasMany('InformationCommentLikes', [
            'foreignKey' => 'comment_id'
        ]);
        $this->HasMany('ChildInformationComments', [
            'className'   => 'InformationComments'
            ,'foreignKey' => 'parent_comment_id'
            ,'conditions' => [
                'ChildInformationComments.parent_comment_id IS NOT' => null
                ,'ChildInformationComments.deleted IS'              => null
            ]
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    public function api_validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    public function getComments( $conditions=[], $limit=null, $page=0 )
    {
        if( isset( $conditions['contractant_id'] ) )
        {
            $q = $this->find()
               ->select([
                   'id',
                   'user_id',
                   'comment',
                   'created'
               ])
               ->where([
                   'InformationComments.contractant_id'        => $conditions['contractant_id']
                   ,'InformationComments.parent_comment_id IS' => null
                   ,'InformationComments.deleted IS'           => null
                   ,'FrontUsers.id IS NOT'                     => null
               ])
               ->contain([
                   'Users' => [
                       'fields' => [ 'id' ]
                   ]
                   ,'Users.FrontUsers' => [
                       'fields' => [ 'nickname', 'thumbnail_path', 'leave_flg' ]
                   ]
                   // コメントにいいね
                   ,'InformationCommentLikes.Users.FrontUsers' => [
                       'fields' => [ 'InformationCommentLikes.comment_id', 'Users.id', 'FrontUsers.id', 'FrontUsers.nickname', 'FrontUsers.leave_flg', 'FrontUsers.thumbnail_path' ]
                   ]
                   ,'ChildInformationComments' => [
                       'fields' => [ 'id', 'user_id', 'parent_comment_id', 'comment', 'created' ]
                       ,'conditions' => [
                           'ChildInformationComments.deleted IS' => null
                       ]
                       ,'sort' => ['ChildInformationComments.created' => 'ASC']
                   ]
                   ,'ChildInformationComments.Users.FrontUsers' => [
                       'fields' => [ 'Users.id', 'FrontUsers.nickname', 'FrontUsers.thumbnail_path', 'FrontUsers.leave_flg' ]
                   ]
                   // コメントにいいね
                   ,'ChildInformationComments.InformationCommentLikes.Users.FrontUsers' => [
                       'fields' => [ 'InformationCommentLikes.comment_id', 'Users.id', 'FrontUsers.id', 'FrontUsers.nickname', 'FrontUsers.leave_flg', 'FrontUsers.thumbnail_path' ]
                   ]
               ])
               ->order(['InformationComments.created' => 'ASC']) ;

            if( isset( $conditions['information_id'] ) )
            {
                $q->where([
                    'InformationComments.information_id' => $conditions['information_id']
                ]);
            }
           if( $limit !== null ) $q->limit( $limit );
           if( $page > 0 )       $q->page( $page );

            return $q->all();
        }
        else
        {
            return false;
        }

    }
}
