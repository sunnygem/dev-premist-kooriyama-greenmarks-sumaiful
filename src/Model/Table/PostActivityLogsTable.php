<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PostActivityLogsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Posts');
        $this->belongsTo('Users');
        $this->belongsTo('TargetUsers', [
            'className' => 'users'
            ,'foreignKey'  => 'target_user_id'
        ]);
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'PostActivityLogs.deleted IS' => null
            ]);
            //->contain([
            //    'PostActivityLogs.Files' => [
            //        'conditions' => [
            //            'Files.deleted IS' => null
            //        ]
            //    ]
            //    ,'Users.FrontUsers'
            //])
            //->order([
            //    '.id' => 'ASC'
            //]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;

    }


//    public function getActivity( $where=[] )
//    {
//        $query = $this->find()
//            ->where([ 'Posts.deleted IS' => NULL ])
//            ->select([
//                ,'Posts.building_id'
//                ,'Posts.content'
//                ,'Posts.event_date'
//                ,'Posts.created'
//                ,'PostCategories.id'
//                ,'PostCategories.label'
//                ,'Users.id'
//                ,'Users.login_id'
//                ,'FrontUsers.name'
//                ,'FrontUsers.room_number'
//                ,'FrontUsers.thumbnail_path'
//                ,'FrontUsers.device_token'
//            ])
//            // todo 建物
//            // todo コメント
//            // todo いいね
//            // todo 参加
//            ->contain([
//                'PostCategories'
//                ,'Users.FrontUsers'
//            ])
//            ->order(['Posts.created' => 'DESC']);
//        if( count( $where ) > 0 ) $query->where( $where );
//
//        return $query->all();
//    }
//
     // 自分へのアクティビティ(自分を除く)
     public function getDataByTargetUserId( $where=[], $limit=null, $page=0 )
     {
         if( isset( $where['contractant_id'] ) )
         {
             $q = $this->find()
                 ->select([
                     'PostActivityLogs.id'
                     ,'PostActivityLogs.activity_type'
                     ,'PostActivityLogs.post_id'
                     ,'PostActivityLogs.user_id'
                     ,'PostActivityLogs.created'
                     ,'Users.id'
                     ,'Users.nickname'
                     ,'FrontUsers.id'
                     ,'FrontUsers.nickname'
                     ,'FrontUsers.thumbnail_path'
                     ,'FrontUsers.leave_flg'
                     ,'Posts.id'
                     ,'Posts.post_category_id'
                     ,'PostCategories.id'
                     ,'PostCategories.label'
                 ])
                 ->where([
                     'PostActivityLogs.contractant_id'  => $where['contractant_id']
                     ,'PostActivityLogs.target_user_id' => $where['target_user_id']
                     ,'PostActivityLogs.user_id !='     => $where['target_user_id']
                     ,'PostActivityLogs.deleted IS'     => null
                     ,'FrontUsers.id IS NOT'            => null
                 ])
                 ->contain([
                     'Users.FrontUsers'
                     ,'Posts.PostCategories'
                 ])
                 ->order(['PostActivityLogs.created' => 'DESC' ]);

             if( $limit !== null ) $q->limit( $limit );
             if( $page > 0 )       $q->page( $page );

             return $q->all();
         }
         else
         {
             return [];
         }
     }

     // 未読件数を返す
     public function checkUnread( $target_user_id=null )
     {
         $q = $this->find()
             ->where([
                 'PostActivityLogs.target_user_id' => $target_user_id
                 ,'PostActivityLogs.read_flg'      => 0
                 ,'PostActivityLogs.deleted IS'    => null
             ]);

         return $q->count();
     }

     // 既読フラグ立てる
     public function FlagOnByUserId( $target_user_id=null )
     {
         $data = [ 'read_flg' => 1 ];
         $cond = [
             'target_user_id' => $target_user_id
             ,'read_flg'      => 0
         ];
         return $this->updateAll( $data, $cond );
     }

     public function getPushData( $id=null )
     {
         return $this->find()
            ->where([
                'PostActivityLogs.id'          => $id
                ,'PostActivityLogs.deleted IS' => null
            ])
            ->contain([
                'Users' => [
                    'fields' => [ 'id' ]
                ]
                ,'Users.FrontUsers' => [
                    'fields' => [ 'user_id', 'nickname' ]
                ]
                ,'TargetUsers' => [
                    'fields' => [ 'id' ]
                ]
                ,'TargetUsers.FrontUsers' => [
                    'fields' => [ 'user_id' ]
                ]
                ,'TargetUsers.FrontUsers.UserDeviceTokens' => [
                    'fields' => [ 'front_user_id', 'os', 'device_token' ]
                ]
            ])
            ->all();
     }

}
