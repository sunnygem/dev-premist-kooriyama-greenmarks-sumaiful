<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class ServiceMenusTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsToMany( 'Contractants', [
            'joinTable' => 'contractant_service_menus'
            ,'through' => 'ContractantServiceMenus'
        ]);

    }

    public function validation( $data=[] )
    {
        $err=[];

        if( isset( $data['name'] ) && strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'deleted IS' => null
             ])
             ->order( ['orderby ASC']);
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    // 新規の並び順を取得
    public function getOrder()
    {
        $res = $this->find()
            ->where([
                'deleted IS'     => null
            ])
            ->order([ 'orderby DESC' ])
            ->first();
        return ( $res ) ? $res->orderby + 1 : 1;
    }

    public function getOptions()
    {
        return $this->find('list')
            ->where([
                'deleted IS' => null
            ])
            ->order(['orderby ASC'])
            ->toArray();
    }
 
}
