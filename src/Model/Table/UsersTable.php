<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class UsersTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        //システムユーザー
        $this->hasOne('SystemUsers', [
            'foreignKey' => 'user_id'
            ,'conditions' => [
                'SystemUsers.deleted IS' => null
            ]
        ]);
        //契約ユーザー
        $this->hasOne('AdminUsers', [
            'foreignKey' => 'user_id'
            ,'conditions' => [
                'AdminUsers.deleted IS' => null
            ]
        ]);
        //エンドユーザー
        $this->hasOne('FrontUsers', [
            'foreignKey' => 'user_id'
            ,'conditions' => [
                'FrontUsers.deleted IS' => null
            ]
        ]);
        //契約者情報
        $this->belongsTo('Contractants', [
            'conditions' => [
                'Contractants.deleted IS' => null
            ]
        ]);

        $this->hasMany( 'LuggageStrages', [
            'conditions' => [
                'LuggageStrages.deleted IS' => null
            ]
        ]);

    }

    public function system_validation( $data=[] )
    {
        $err = [];
        $id = ( isset( $data['id'] ) ) ?  $data['id'] : null;

        if( isset( $data['login_id'] ) && mb_strlen( $data['login_id'] ) === 0 )
        {
            $err['login_id'] = '入力してください';
        }
        //else if( isset( $data['login_id'] ) && $this->check_duplication( $data, 'login_id', true ) )
        //{
        //    $err['login_id'] = '既に使用されているIDです。使用できません。';
        //}

        if( isset( $data['login_id'] ) && mb_strlen( $data['login_id'] ) === 0 )
        {
            $err['login_id'] = '入力してください';
        }
        else if( isset( $data['login_id'] ) && $this->check_duplication( $data, 'login_id', true ) )
        {
            $err['login_id'] = '既に使用されているIDです。使用できません。';
        }


        return $err;
    }

    // 管理画面j登録用
    public function admin_validation( $data=[] )
    {
        $err = [];

        $id = ( isset( $data['id'] ) ) ?  $data['id'] : null;

        //if( isset( $data['login_id'] ) && mb_strlen( $data['login_id'] ) === 0 )
        //{
        //    $err['login_id'] = '入力してください';
        //}
        //if( isset( $data['login_id'] ) && $this->check_duplication( $data, 'login_id', true ) )
        //{
        //    $err['login_id'] = '既に使用されているIDです。使用できません。';
        //}

        if( isset( $data['email'] ) && mb_strlen( $data['email'] ) === 0 )
        {
            $err['email'] = '入力してください';
        }
        elseif( isset( $data['email'] ) && $this->check_email( $data['email'] ) === false )
        {
            $err['email'] = 'メールの書式を確認してください';
        }
        // admin_userを持っていない
        else if( isset( $data['email'] ) && self::checkAdminDuplication( $data ) )
        {
            $err['email'] = '既に使用されているアドレスです。使用できません。';
        }

        if( isset( $data['password'] ) && mb_strlen( $data['password'] ) === 0 )
        {
            $err['password'] = '入力してください';
        }
        else if( isset( $data['password_confirm'] ) && $data['password'] !== $data['password_confirm'] )
        {
            $err['password_confirm'] = 'パスワードが一致しません';
        }

        return $err;
    }

    // admin_usersの重複チェック
    public function checkAdminDuplication( $data=[] )
    {
        $res = $this->find()
            ->where([
                'Users.email'           => $data['email']
                ,'Users.contractant_id' => $this->_session->read('ContractantData.id')
                ,'Users.deleted IS'     => null
                ,'AdminUsers.id IS NOT' => null
            ])
            ->contain([
                'AdminUsers'
            ]);
        if( isset( $data['id'] ) )
        {
            $res->where(['Users.id !=' => $data['id']]);
        }
        return $res->first();
    }


    // admin 初回ログイン
    public function init_validation( $data=[], $security=[] )
    {
        $err=[];
        if( isset( $data['login_id'] ) && strlen( $data['login_id'] ) === 0 )
        {
            $err['login_id'] = '入力してください';
        }
        elseif( isset( $data['login_id'] ) && $this->getUninitializedUserBy( $data['login_id'] ) === null )
        {
            $err['login_id'] = '該当のユーザーが存在しません';
        }

        if( isset( $data['pre_password'] ) && strlen( $data['pre_password'] ) === 0 )
        {
        }

        if( isset( $data['password'] ) && strlen( $data['password'] ) === 0 )
        {
            $err['password'] = '入力してください';
        }
        elseif( isset( $data['password'] ) && $security['password_length'] !== null && $this->check_str_length( $data['password'], $security['password_length'] ) === false )
        {
            $err['password'] = $security['password_length'] . '文字以上で入力してください';
        }
        elseif( isset( $data['password'] ) && $security['include_number_flg'] === 1 && $this->check_include_number( $data['password'] ) === false )
        {
            $err['password'] = 'パスワードは数字を混合して入力してください';
        }
        elseif( isset( $data['password'] ) && $security['include_symbol_flg'] === 1 && $this->check_symbol_number( $data['password'] ) === false )
        {
            $err['password'] = 'パスワードは記号(!-/:-@[-`{-~)を混合して入力してください';
        }

        if( isset( $data['password_confirm'] ) && strlen( $data['password_confirm'] ) === 0 )
        {
            $err['password_confirm'] = '入力してください';
        }
        elseif( isset( $data['password_confirm'] ) && $data['password'] !== $data['password_confirm'] )
        {
            $err['password_confirm'] = 'パスワードが合致していません';
        }

        return $err;
    }

    // admin ユーザーデータ変更
    public function reset_validation( $data=[], $security=[] )
    {
        $err=[];

        if( isset( $data['password'] ) && strlen( $data['password'] ) === 0 )
        {
            $err['password'] = '入力してください';
        }
        elseif( isset( $data['password'] ) && $security['password_length'] !== null && $this->check_str_length( $data['password'], $security['password_length'] ) === false )
        {
            $err['password'] = $security['password_length'] . '文字以上で入力してください';
        }
        elseif( isset( $data['password'] ) && $security['include_number_flg'] === 1 && $this->check_include_number( $data['password'] ) === false )
        {
            $err['password'] = 'パスワードは数字を混合して入力してください';
        }
        elseif( isset( $data['password'] ) && $security['include_symbol_flg'] === 1 && $this->check_symbol_number( $data['password'] ) === false )
        {
            $err['password'] = 'パスワードは記号(!-/:-@[-`{-~)を混合して入力してください';
        }

        if( isset( $data['password_confirm'] ) && strlen( $data['password_confirm'] ) === 0 )
        {
            $err['password_confirm'] = '入力してください';
        }
        elseif( isset( $data['password_confirm'] ) && $data['password'] !== $data['password_confirm'] )
        {
            $err['password_confirm'] = 'パスワードが合致していません';
        }

        return $err;
    }

    // ログイン承認
    public function login_approve_validation( $data=[] )
    {
        $error = [];
        if( isset( $data['login_id'] ) && $data['login_id'] === '' )
        {
            $error['login_id'] = '入力してください';
        }


        if( isset( $data['auth_user_id'] ) && $data['auth_user_id'] === '' )
        {
            $error['auth_user_id'] = '選択してください';
        }
        else if( isset( $data['auth_user_id'] ) && $this->checkSelfIdSelect( $data['auth_user_id'], $data['login_id'] ) )
        {
            $error['auth_user_id'] = '自身を承認者として選択することはできません。';
        }

        if( isset( $data['login_approval_date'] ) && $data['login_approval_date'] !== ''  && $this->check_date( $data['login_approval_date'], '/' ) === false )
        {
            $error['login_approval_date'] = '日付の有効性が確認できませんでした';
        }

        return $error;
    }

    // 共通ユーザー
    public function common_user_validation( $data=[] )
    {
        $error = [];
        if( isset( $data['front_user']['building_id'] ) && $data['front_user']['building_id'] === '' )
        {
            $error['building_id'] = '選択してください';
        }

        if( isset( $data['front_user']['type'] ) && $data['front_user']['type'] === '' )
        {
            $error['type'] = '選択してください';
        }

        if( isset( $data['front_user']['nickname'] ) && $data['front_user']['nickname'] === '' )
        {
            $error['nickname'] = '入力してください';
        }

        if( isset( $data['login_id'] ) && $data['login_id'] === '' )
        {
            $error['login_id'] = '入力してください';
        }
        // 重複チェック
        else if( self::checkCommonUserDuplication( $data ) )
        {
            $error['login_id'] = '既に存在するログインIDです';
        }

        if( isset( $data['password'] ) && mb_strlen( $data['password'] ) === 0 )
        {
            $error['password'] = '入力してください';
        }
        else if( isset( $data['password_confirm'] ) && $data['password'] !== $data['password_confirm'] )
        {
            $error['password_confirm'] = 'パスワードが一致しません';
        }

        if( isset( $data['expire_date'] ) && $data['expire_date'] !== ''  && $this->check_date( $data['expire_date'], '/' ) === false )
        {
            $error['expire_date'] = '日付の有効性が確認できませんでした';
        }

        return $error;
    }
    // 共通ユーザーの重複チェック
    private function checkCommonUserDuplication( $data=[] )
    {
        $where = [
            'Users.login_id'         => $data['login_id']
            ,'Users.contractant_id'  => $data['contractant_id']
            ,'Users.deleted IS '     => null
            ,'FrontUsers.common_user_flg' => 1
            ,'FrontUsers.deleted IS' => null
        ];
        if( isset( $data['id'] ) ) $where['Users.id !='] = $data['id'];
        $res = $this->find()
            ->where($where)
            ->contain(['FrontUsers'])
            ->isEmpty();
        return ( $res === false ) ? true : false;

    }


    // SystemのAuth
    public function findSystemauth( \Cake\ORM\Query $query, array $options )
    {
          $query
             ->where( [
                 'Users.deleted IS'       => null
                 ,'Users.password IS NOT' => null
                 ,'Users.type'            => 'system'
                 ,'Users.contractant_id'  => 0
             ])
             ->contain([
                 'SystemUsers' => [
                     'conditions' => [
                         'SystemUsers.deleted IS' => null
                     ]
                 ]
             ]);

         return $query;
    }

    // AdminのAuth
    public function findAdminauth( \Cake\ORM\Query $query, array $options )
    {
          $query
             ->where( [
                 'Users.deleted IS'       => null
                 ,'Users.password IS NOT' => null
                 //,'Users.type'            => 'admin'
                 ,'AdminUsers.id IS NOT'  => null
                 ,'AdminUsers.valid_flg'  => 1
                 ,'AdminUsers.deleted IS' => null
                 ,'AdminUsers.contractant_id' => $options['contractant_id']
             ])
             ->contain(['AdminUsers']);

         return $query;
    }
    // FrontのAuth
    public function findFrontauth( \Cake\ORM\Query $query, array $options )
    {
          $query
             ->where( [
                 'Users.deleted IS'       => null
                 ,'Users.password IS NOT' => null
                 ,'FrontUsers.id IS NOT'  => null
                 ,'FrontUsers.auth_flg'   => 1
                 ,'FrontUsers.deleted IS' => null
                 ,'FrontUsers.contractant_id' => $options['contractant_id']
             ])
             ->contain([ 'FrontUsers' ])
             ->order([ 'Users.valid_date' => 'DESC' ]); // 有効な最新のユーザーを返す

         return $query;
    }


    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'Users.deleted IS' => null
             ]);
    }

    public function saveData( $data, $id = null )
    {
        // アソシエーションごと登録
        if( isset( $data['system_user'] ) )
        {
            $entitiy = $this->newEntity( $data, [
                'associated' => ['SystemUsers']
            ]);
            return $this->save( $entitiy );

        }
        elseif( isset( $data['admin_user'] ) )
        {
            $entitiy = $this->newEntity( $data, [
                'associated' => ['AdminUsers']
            ]);
            return $this->save( $entitiy );
        }
        elseif( isset( $data['front_user'] ) )
        {
            $entitiy = $this->newEntity( $data, [
                'associated' => ['FrontUsers']
            ]);
            return $this->save( $entitiy );
        }
        else
        {
            return parent::saveData( $data );
        }

    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    // フロントユーザーの申請
    public function registFrontUser( $data )
    {
        // アソシエーションでFrontUserも登録します
        $entitiy = $this->newEntity( $data, [
            'associated' => ['FrontUsers']
        ]);
        return $this->save( $entitiy );
    }

    public function loginFrontUser( $where=[] )
    {
        return $this->find()
            ->where($where)
            ->where([
                //'Users.type' => 'front'
                'Users.lock_flg' => 0
                ,'Users.deleted IS' => null
                ,'FrontUsers.auth_flg' => 1
            ])
            ->contain([
                'FrontUsers' => []
                ,'FrontUsers.Buildings' => [
                    'fields' => [ 'Buildings.name', 'Buildings.icon_path' ]
                ]
            ])
            ->order([ 'Users.valid_date' => 'DESC' ]) // 有効な最新のユーザーを返す
            ->first();

    }

    public function getSystemUser( $id )
    {
        return $this->find()
            ->where([
                'Users.id'          => $id
                ,'Users.type'       => 'system'
                ,'Users.deleted IS' => null
            ])
            ->contain(['SystemUsers'])
            ->first();
    }

    public function getAdminUser( $id )
    {
        return $this->find()
            ->where([
                'Users.id'          => $id
                //,'Users.type'       => 'admin'
                ,'Users.deleted IS' => null
                ,'AdminUsers.id IS NOT' => null
            ])
            ->contain(['AdminUsers'])
            ->first();
    }

    public function checkExistInitAdminUser( $login_id )
    {
        $res = $this->find()
            ->where([
                'Users.login_id' => $login_id
                //,'Users.type' => 'admin'
                ,'Users.deleted IS' => null
                ,'AdminUsers.valid_flg' => 1
            ])
            ->contain(['AdminUsers'])
            ->first();
        return ( $res !== null ) ? true : false ;
    }

    public function checkAdminExistsByEmail( $email )
    {
        $res = $this->find()
            ->where([
                'Users.email' => $email
                ,'Users.deleted IS' => null
            ])
            ->contain(['AdminUsers'])
            ->first();
        if( $res === null )
        {
            return false; // 新規作成
        }
        elseif( $res->admin_user === null )
        {
            return $res->id; // admin_userのみ作成
        }
        else
        {
            return true; // 存在してるのでNG
        }

    }

    // admin_userを持っていてfront_uerを持っていないユーザー
    public function checkExistAdminAndNoFrontByEmail( $email, $contractant_id )
    {
        return $res = $this->find()
            ->where([
                'Users.email'           => $email
                ,'Users.contractant_id' => $contractant_id
                ,'Users.deleted IS'  => null
                ,'AdminUsers.id IS NOT' => null
                ,'OR' => [
                    'FrontUsers.id IS'  => null // 存在しない
                    ,'FrontUsers.deleted IS NOT'  => null // 削除されている
                ]
            ])
            ->contain(['AdminUsers', 'FrontUsers'])
            ->first();
    }
    // front_userを持っていてadmin_uerを持っていないユーザー
    public function checkExistFrontAndNoAdminByEmail( $email, $contractant_id )
    {
        $res = $this->find()
            ->where([
                'Users.email'        => $email
                ,'Users.email'       => $contractant_id
                ,'Users.deleted IS'  => null
                ,'FrontUsers.id IS NOT' => null
                ,'OR' => [
                    'AdminUsers.id IS'  => null // 存在しない
                    ,'AdminUsers.deleted IS NOT'  => null // 削除されている
                ]
            ])
            ->contain(['AdminUsers', 'FrontUsers']);
        // ヒットするユーザが複数ある場合はNG
        if(  $res->count() > 1 )
        {
            return null;
        }
        // 該当一件もしくはnullを返す
        else
        {
            return  $res ->first();
        }
    }
    public function getFrontUser( $id=null, $conditions=[], $limit=null, $page=null, $admin=false )
    {
        $q = $this->find()
            ->where([
                //'Users.type'        => 'front'
                'FrontUsers.id IS NOT' => 'front'
                ,'Users.deleted IS'    => null
            ])
            ->contain([
                'FrontUsers'
                ,'FrontUsers.Buildings' => [
                    'fields' => [ 'Buildings.name', 'Buildings.icon_path' ]
                ]
            ]);

        if( is_array( $conditions ) && count( $conditions ) > 0 )
        {
            $q->where($conditions);
        }

        if( $admin === false )
        {
            $q->select([
                'Users.id'
                ,'Users.login_id'
                ,'Users.email'
                ,'Users.nickname'
                ,'Users.created'
                ,'FrontUsers.id'
                ,'FrontUsers.type'
                ,'FrontUsers.leave_flg'
                ,'FrontUsers.common_user_flg'
                ,'FrontUsers.nickname'
                ,'FrontUsers.building_id'
                ,'FrontUsers.biography'
                ,'FrontUsers.birth_place'
                ,'FrontUsers.room_number'
                ,'FrontUsers.thumbnail_path'
                ,'FrontUsers.university_undergraduate'
                ,'FrontUsers.university_name'
                ,'FrontUsers.undergraduate'
                ,'FrontUsers.club_name'
                ,'FrontUsers.hobby'
                ,'FrontUsers.push_like_flg'
                ,'FrontUsers.push_comment_flg'
                ,'FrontUsers.push_join_flg'
                ,'FrontUsers.push_baggage_flg'
                ,'FrontUsers.push_message_flg'
                ,'FrontUsers.push_information_flg'
                ,'FrontUsers.push_image_sharing_flg'
            ]);
        }

        if( $id !== null && preg_match('/^[0-9]+$/', $id ) === 1 )
        {
            $q
                ->where([ 'Users.id' => $id ])
                ->contain(['Contractants']);
            //if( $auth === true ) $q->where([ 'FrontUsers.auth_flg' => 1 ]);
            return $q->first();
        }
        elseif( $id === 'all' )
        {
            $q->contain(['FrontUsers.EncryptionParameters']);
            if ( $limit !== null && $page !== null )
            {
                $q->limit( $limit )
                ->page( $page );
            }
            return $q->all();
        }
        else
        {
            return [];
        }
    }
    public function getFrontUserByFrontUserId( $front_user_id )
    {
        $res = $this->find()
            ->select([
                'Users.id'
                ,'Users.login_id'
                ,'Users.email'
                ,'Users.nickname'
                ,'Users.created'
                ,'FrontUsers.id'
                ,'FrontUsers.type'
                ,'FrontUsers.leave_flg'
                ,'FrontUsers.common_user_flg'
                ,'FrontUsers.nickname'
                ,'FrontUsers.building_id'
                ,'FrontUsers.biography'
                ,'FrontUsers.birth_place'
                ,'FrontUsers.room_number'
                ,'FrontUsers.thumbnail_path'
                ,'FrontUsers.university_undergraduate'
                ,'FrontUsers.university_name'
                ,'FrontUsers.undergraduate'
                ,'FrontUsers.club_name'
                ,'FrontUsers.hobby'
                ,'FrontUsers.push_like_flg'
                ,'FrontUsers.push_comment_flg'
                ,'FrontUsers.push_join_flg'
                ,'FrontUsers.push_baggage_flg'
                ,'FrontUsers.push_message_flg'
                ,'FrontUsers.push_information_flg'
                ,'FrontUsers.push_image_sharing_flg'
            ])
            ->where([
                'FrontUsers.id' => $front_user_id
                ,'Users.deleted IS' => null
            ])
            ->contain(['FrontUsers'])
            ->all();
        return ( $res->count() === 1 ) ? $res->first() : false;
    }
    public function getFrontUserByAccessToken( $access_token )
    {
        $res = $this->find()
            ->select([
                'Users.id'
                ,'Users.login_id'
                ,'Users.email'
                ,'Users.nickname'
                ,'Users.created'
                ,'FrontUsers.id'
                ,'FrontUsers.type'
                ,'FrontUsers.leave_flg'
                ,'FrontUsers.common_user_flg'
                ,'FrontUsers.nickname'
                ,'FrontUsers.building_id'
                ,'FrontUsers.biography'
                ,'FrontUsers.birth_place'
                ,'FrontUsers.room_number'
                ,'FrontUsers.thumbnail_path'
                ,'FrontUsers.university_undergraduate'
                ,'FrontUsers.university_name'
                ,'FrontUsers.undergraduate'
                ,'FrontUsers.club_name'
                ,'FrontUsers.hobby'
                ,'FrontUsers.push_like_flg'
                ,'FrontUsers.push_comment_flg'
                ,'FrontUsers.push_join_flg'
                ,'FrontUsers.push_baggage_flg'
                ,'FrontUsers.push_message_flg'
                ,'FrontUsers.push_information_flg'
                ,'FrontUsers.push_image_sharing_flg'
            ])
            ->where([
                'access_token' => $access_token
                ,'Users.deleted IS' => null
            ])
            ->contain(['FrontUsers'])
            ->all();
        return ( $res->count() === 1 ) ? $res->first() : false;
    }
    public function getFrontUserOptions( $conditions=[] )
    {
        $res = self::getFrontUser( 'all', $conditions );

        $arr = [];
        foreach( $res as $key => $val )
        {
            $arr[$val->id] = ( $val->front_user->name ) ? $val->front_user->name : '名無し';
        }
        return $arr;
    }

    public function getFrontUserByFamilyAuthKey( $family_auth_key )
    {
        return $this->find()
            ->where([
                'FrontUsers.family_auth_key' => $family_auth_key
                ,'Users.type'       => 'front'
                ,'Users.deleted IS' => null
            ])
            ->contain(['FrontUsers'])
            ->first();
    }
    public function getFrontUserByTmpKey( $tmp_key )
    {
        return $this->find()
            ->where([
                'FrontUsers.tmp_key' => $tmp_key
                //,'Users.type'       => 'front'
                ,'Users.deleted IS' => null
                //,'Users.login_id'   => $login_id
                //,'Users.contractant_id' => $this->_session->read('ContractantData.id')
                //,'Users.password IS'    => null
                //,'Users.type'           => 'admin'
                //,'Users.deleted IS'     => null
            ])
            ->contain([
                'AdminUsers' => [
                    'conditions' => [
                        'AdminUsers.contractant_id' => $this->_session->read('ContractantData.id')
                        ,'AdminUsers.valid_flg'     => 1
                    ]
                ]
                ,'FrontUsers'
            ])
            ->first();
    }
    public function getFrontUserIncludeName( $id, $conditions=[] )
    {
        $query = $this->find()
            ->select([
                'Users.id'
                ,'Users.login_id'
                ,'Users.email'
                ,'Users.nickname'
                ,'Users.created'
                ,'FrontUsers.id'
                ,'FrontUsers.name'
                ,'FrontUsers.nickname'
                ,'FrontUsers.building_id'
                ,'FrontUsers.biography'
                ,'FrontUsers.birth_place'
                ,'FrontUsers.room_number'
                ,'FrontUsers.thumbnail_path'
                ,'FrontUsers.university_undergraduate'
                ,'FrontUsers.university_name'
                ,'FrontUsers.undergraduate'
                ,'FrontUsers.club_name'
                ,'FrontUsers.hobby'
                ,'FrontUsers.encryption_parameter_id'
                ,'FrontUsers.push_like_flg'
                ,'FrontUsers.push_comment_flg'
                ,'FrontUsers.push_join_flg'
                ,'FrontUsers.push_baggage_flg'
                ,'FrontUsers.push_message_flg'
                ,'FrontUsers.push_information_flg'
                ,'FrontUsers.push_image_sharing_flg'
                ,'EncryptionParameters.iv'
                ,'EncryptionParameters.salt'
            ])
            ->where([
                'Users.id'          => $id
                ,'Users.deleted IS' => null
                ,'FrontUsers.id IS NOT' => null
            ])
            ->contain( [ 'FrontUsers', 'FrontUsers.EncryptionParameters', 'Contractants' ] );
        if( is_array( $conditions ) && count( $conditions ) > 0 )
        {
            $query->where($conditions);
        }
        return $query->first();
    }

    // パスワード再設定用
    public function getDataPrePassword( $login_id, $contractant_id )
    {
         $res =  $this->find()
             ->where([
                 'Users.login_id'        => $login_id
                 ,'Users.contractant_id' => $contractant_id
                 //,'Users.type'           => 'admin'
                 ,'Users.deleted IS'     => null
                 ,'AdminUsers.id IS NOT' => null
            ])
            ->contain('AdminUsers');

         return $res->first();
    }

    // 選択したユーザが自身であるか
    public function checkSelfIdSelect( $id=null, $login_id=null )
    {
         $res = $this->getAdminUser( $id );
         return ( $res->login_id === $login_id ) ? true : false;
    }

    // cookie_keyからシステムユーザーの取得
    public function getSystemUserFromCookieKey( $cookie_key )
    {
        return $this->find('Systemauth')
            ->where([
                'SystemUsers.cookie_key' => $cookie_key
            ])
            ->first();
    }
}
