<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PasswordSecuritiesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Contractants', []);
    }

    public function validation( $data=[] )
    {
        $err = [];
        if( isset( $data['password_length'] ) && strlen( $data['password_length'] ) > 0 && $this->checkNumeric( $data['password_length'] ) === false )
        {
            $err['password_length'] = '数字で値を入力してください';
        }
        else if( isset( $data['password_length'] ) && strlen( $data['password_length'] ) > 0 && $data['password_length'] > 16 )
        {
            $err['password_length'] = '16以下で入力してください';
        }

        if( isset( $data['valid_term'] ) && strlen( $data['valid_term'] ) > 0 && $this->checkNumeric( $data['valid_term'] ) === false )
        {
            $err['valid_term'] = '数字で値を入力してください';
        }

        if( isset( $data['prohibition_num'] ) && strlen( $data['prohibition_num'] ) > 0 && $this->checkNumeric( $data['prohibition_num'] ) === false )
        {
            $err['prohibition_num'] = '数字で値を入力してください';
        }

        if( isset( $data['lock_num'] ) && strlen( $data['lock_num'] ) > 0 && $this->checkNumeric( $data['lock_num'] ) === false )
        {
            $err['lock_num'] = '数字で値を入力してください';
        }

        if( isset( $data['release_hour'] ) && strlen( $data['release_hour'] ) > 0 && $this->checkNumeric( $data['release_hour'] ) === false )
        {
            $err['release_hour'] = '数字で値を入力してください';
        }

        if( isset( $data['login_expires'] ) && strlen( $data['login_expires'] ) > 0 && $this->checkNumeric( $data['login_expires'] ) === false )
        {
            $err['login_expires'] = '数字で値を入力してください';
        }




        return $err;
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function check( $remote_address=null, $contractant_id=null )
    {
        $flg = false;
        if( $remote_address !== null && $contractant_id !== null )
        {
            $res = $this->find()
                ->where([
                    'contractant_id' => $contractant_id
                    ,'deleted IS'    => null
                ])
                ->all();
            if( $res->count() > 0 )
            {
                foreach( $res as $val )
                {
                    $ip_address = $val->ip_address;
                    // 255.255.255.0/24という形式ではない場合
                    if ( strpos( $ip_address, '/' ) === false )
                    {
                        if ( $remote_address === $ip_address )
                        {
                            $flg = true;
                        }
                    }
                    else
                    {
                        list( $permit_ip, $mask ) = explode( '/', $ip_address );
                        $accept_long = ip2long( $permit_ip )      >> ( 32 - $mask );
                        $remote_long = ip2long( $remote_address ) >> ( 32 - $mask );
                        if ( $accept_long === $remote_long )
                        {
                            $flg = true;
                        }
                    }
                }
                
            }
            else
            {
                // 登録がなければ制限はしない
                $flg = true;

            }
        }
        return $flg;
    }

    public function getFirstData( $contractant_id=null )
    {
        return $this->find()
        ->where([
            'contractant_id' => $contractant_id
        ])
        ->order(['id' => 'DESC'])
        ->first();
    }

    public function truncateByContractantId( $contractant_id=null  )
    {
        if( $contractant_id !== null )
        {
            $this->updateAll(
                ['deleted' => date( 'Y-m-d H:i:s' )]
                ,[
                    'contractant_id' => $contractant_id
                    ,'deleted IS'    => null
                ]
            );
        }
    }


}
