<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

/**
 *
 *   契約者側のメニュー設定を管理
 *
 **/
class ContractantServiceMenusTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo('Contractants');
        $this->belongsTo('ServiceMenus');

        $this->hasOne('MenuContactTels');
        $this->belongsTo('AppScreens', [
            'conditions' => [
                'AppScreens.deleted IS' => null
            ]
        ]);

        $this->hasMany('MenuApplications' );
        $this->hasMany('MenuLinktypes' );
        $this->hasMany('MenuManuals' );
        $this->hasMany('MenuEnquetes' );
    }

    public function validation( $data=[] )
    {
        $err = [];

        // 電話
        if( isset( $data['menu_detail']['user_type'] ) && $data['menu_detail']['user_type'] === '' )
        {
            $err['menu_detail']['user_type'] = '選択してください。';
        }
        if( isset( $data['menu_detail']['tel'] ) && $data['menu_detail']['tel'] === '' )
        {
            $err['menu_detail']['tel'] = '入力してください。';
        }


        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         $query
             ->where( [
                 'deleted IS' => null
             ])
             ->order([
                 'created' => 'DESC'
             ]);
        if(isset($options['sort'])) $query->order( $options['sort'] );
        return $query;
    }

    //public function saveData( $data, $id = null )
    //{
    //    return parent::saveData( $data );
    //}

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    public function getDetailById( $id=null )
    {
         return $this->find()
         ->where([
             'ContractantServiceMenus.id'          => $id
             ,'ContractantServiceMenus.valid_flg'  => 1
             ,'ContractantServiceMenus.deleted IS' => null
         ])
         ->contain([
             'ServiceMenus'
         ])
         ->first();
    }

    public function getDataByContractantId( $contractant_id=null, $admin_flg=false )
    {
         $query = $this->find()
         ->where([
             'ContractantServiceMenus.contractant_id' => $contractant_id
             ,'ContractantServiceMenus.deleted IS'    => null
         ])
         ->contain([
             'ServiceMenus'
             ,'AppScreens'
         ])
         ->order([
             'ContractantServiceMenus.app_screen_id' => 'ASC'
             ,'ContractantServiceMenus.orderby' => 'ASC'
         ]);

         if( $admin_flg === false ) $query->where(['ContractantServiceMenus.valid_flg' => 1]);

         return $query->all();
    }

    // 有効なサービスメニューのIDを返す
    public function getServiceMenuIdsByContractId( $contractant_id=null )
    {
        $res = self::getDataByContractantId( $contractant_id )->toArray();
        $arr = array_column( $res, 'service_menu_id' );
        return $arr;
    }

    public function setEditFlgByContractantId( $contractant_id=null )
    {
        if( $contractant_id != null )
        {
             $this->updateAll(
                 [
                     'edit_flg' => 1
                 ]
                 ,[
                     'contractant_id' => $contractant_id
                     ,'deleted IS' => null
                 ]
             );
        }
    }

    public function checkExistAndBreakEditFlg( $condition=[] )
    {
        $res = $this->find()
            ->where( $condition )
            ->first();
        // 既存レコードがあればedit_flgを折る
        if( $res )
        {
            $this->saveData([
                'id' => $res->id
                ,'edit_flg' => 0
            ]);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getDataByAppFooterId( $contractant_id, $app_screen_id=null, $user_type=null, $contain=true )
    {
        $query = $this->find()
            ->order(['ContractantServiceMenus.orderby' => 'ASC'])
            ->where([
                'ContractantServiceMenus.contractant_id' => $contractant_id
                ,'ContractantServiceMenus.app_screen_id' => $app_screen_id
                ,'ContractantServiceMenus.valid_flg'     => 1
                ,'ContractantServiceMenus.deleted IS' => null
                // 問い合わせの仕様変更により止むを得ず
                ,'OR' => [
                    [
                        'ContractantServiceMenus.service_menu_id' => 1
                        ,'MenuContactTels.user_type'              => $user_type
                    ]
                    ,'ContractantServiceMenus.service_menu_id !=' => 1
                ]
            ]);

        if( $contain === true )
        {
            $query
                ->contain([
                    'MenuContactTels' => [
                        'fields' => ['tel', 'user_type', 'property_url_label', 'property_url', 'attend_reserve_url_label', 'attend_reserve_url', 'comment' ]
                        ,'conditions' => [
                            'MenuContactTels.deleted IS' => null
                            ,'MenuContactTels.user_type' => $user_type
                        ] 
                    ]
                    ,'MenuApplications' => [
                        'conditions' => [
                            'MenuApplications.deleted IS' => null
                        ]
                    ]
                    //,'MenuApplications.Files' => [ ]
                    ,'MenuLinktypes' => [
                        'conditions' => [
                            'MenuLinktypes.deleted IS' => null
                        ]
                    ]
                    ,'MenuManuals' => [
                        'conditions' => [
                            'MenuManuals.deleted IS' => null
                        ]
                    ]
                    ,'MenuManuals.Files' => [
                        'conditions' => [
                            'Files.contractant_id' => $contractant_id
                            ,'Files.deleted IS' => null
                        ]
                    ]
                    //,'MenuEnquetes' => [
                    //    'conditions' => [
                    //        'MenuEnquetes.display_flg' => 1
                    //        ,'MenuEnquetes.deleted IS' => null
                    //    ]
                    //    ,'sort' => ['MenuEnquetes.id' => 'ASC']
                    //]
                ]);
        }

        return $query
            ->all();
        
    }

    public function checkValidPostType( $contractant_id=null )
    {
        return $this->find()
            ->where([
                 'contractant_id'   => 4
                 ,'service_menu_id' => 12 // ポストタイプ
                 ,'valid_flg'       => 1
                 ,'deleted IS'      => NULL
            ])
            ->first();
    }
}
