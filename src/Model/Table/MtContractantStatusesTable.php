<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MtContractantStatusesTable extends AppTable
{
    //public function initialize( array $config )
    //{
    //}

    public function getList()
    {
        return $this->find('list')
            ->order( 'orderby ASC' )
            ->toArray();
    }

}
