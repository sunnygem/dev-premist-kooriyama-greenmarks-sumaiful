<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class EquipmentsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Buildings');
    }

    public function validation( $data=[] )
    {
        $err = [];

        if( isset( $data['building_id'] ) && $data['building_id'] === '' )
        {
            $err['building_id'] = '選択してください';
        }

        if( isset( $data['category_id'] ) && $data['category_id'] === '' )
        {
            $err['category_id'] = '選択してください';
        }

        if( isset( $data['name'] ) && mb_strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        return $err;
    }

    // 貸出し
    public function validationLending(Validator $validator)
    {
        $validator
            ->notEmpty('expected_return_date', '入力してください')
            ->add('expected_return_date', 'invalid', [
                 'rule' => function ( $val ){
                     if( strtotime( $val ) < time() )
                     {
                         return '過去の日時は設定できません';
                     }
                     return true;
                 }
            ]);

            return $validator;
    }

    // paginator 汎用
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'Equipments.deleted IS' => null
            ])
            ->contain([
            ])
            ->order([
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    // paginator フロント
    public function findFront( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->select([
                'id',
                'name',
                'name_en',
                'icon_url_path',
                'in_use_flg',
                'expected_return_date',
                'category_id',
            ])
            ->where( [
                'Equipments.deleted IS' => null
                ,'Equipments.display_flg'   => 1

            ])
            ->order([
                'Equipments.created' => 'DESC'
            ]);

        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getEditData( $id=null )
    {
        return $this->find()
            ->where([
                'id'          => $id
                ,'deleted IS' => null
            ])
            ->contain([])
            ->first();
    }

}
