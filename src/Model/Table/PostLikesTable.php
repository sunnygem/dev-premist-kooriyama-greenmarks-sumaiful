<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class PostLikesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Posts');
        $this->belongsTo('Users');
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }
    
    public function saveData( $data, $parent_id=null )
    {
        $res = self::checkDataExist( $data );
 
        // データがあれば削除
        if( $res > 0 )
        {
            $cond = [
                'contractant_id' => $data['contractant_id']
                ,'user_id' => $data['user_id']
                ,'post_id' => $data['post_id']
            ];
            return $this->deleteAll( $cond );
        }
        // データが無ければ登録
        else
        {
            parent::saveData( $data );
            return 'true';
        }
    }

    public function checkDataExist( $data )
    {
         if( isset( $data['contractant_id'] ) && isset( $data['user_id'] ) && isset( $data['post_id'] ) )
         {
             return $this->find()
                 ->where([
                     'contractant_id' => $data['contractant_id']
                     ,'user_id'       => $data['user_id']
                     ,'post_id'       => $data['post_id']
                 ]) 
                 ->count();
         }
         return false;
    }


    public function getMessageMemberListByUsersById( $contractant_id=null, $post_id=null, $limit=null, $page=null )
    {
        $q = $this->find()
            ->select([
                'id'        => 'Users.id'
                ,'login_id' => 'Users.login_id'
                ,'email'    => 'Users.email'
                ,'created'  => 'Users.created'
                ,'front_user__id' => 'FrontUsers.id'
                ,'front_user__nickname' => 'FrontUsers.nickname'
                ,'front_user__building_id' => 'FrontUsers.building_id'
                ,'front_user__biography' => 'FrontUsers.biography'
                ,'front_user__room_number' => 'FrontUsers.room_number'
                ,'front_user__thumbnail_path' => 'FrontUsers.thumbnail_path'
                ,'front_user__university_name' => 'FrontUsers.university_name'
                ,'front_user__undergraduate' => 'FrontUsers.undergraduate'
                ,'front_user__club_name' => 'FrontUsers.club_name'
                ,'front_user__hobby' => 'FrontUsers.hobby'
                ,'front_user__push_like_flg' => 'FrontUsers.push_like_flg'
                ,'front_user__push_comment_flg' => 'FrontUsers.push_comment_flg'
                ,'front_user__push_join_flg' => 'FrontUsers.push_join_flg'
                ,'front_user__push_baggage_flg' => 'FrontUsers.push_baggage_flg'
                ,'front_user__push_message_flg' => 'FrontUsers.push_message_flg'
                ,'front_user__push_information_flg' => 'FrontUsers.push_information_flg'
                ,'front_user__push_image_sharing_flg' => 'FrontUsers.push_image_sharing_flg'
            ])
            ->where([
                'PostLikes.contractant_id' => $contractant_id
                ,'PostLikes.post_id'       => $post_id
                ,'PostLikes.deleted IS'    => null
            ])
            ->contain([
                'Users.FrontUsers'
            ])
            ->order([
                'PostLikes.created' => 'ASC'
            ]);
        if( $limit !== null ) $q->limit($limit);
        if( $page  !== null ) $q->page($page);
        return $q->all();
    }
}
