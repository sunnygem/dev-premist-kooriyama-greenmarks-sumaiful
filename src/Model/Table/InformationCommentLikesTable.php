<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class InformationCommentLikesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );

        $this->belongsTo('Informations');
        $this->belongsTo('InformationComments', [
            'foreignKey' => 'comment_id'
        ]);
        $this->belongsTo('Users');
    }

    public function validation( $data=[] )
    {
        $err = [];

        return $err;
    }

    public function saveData( $data, $parent_id=null )
    {
        $res = self::checkDataExist( $data );
 
        // データがあれば削除
        if( $res > 0 )
        {
            $cond = [
                'contractant_id' => $data['contractant_id']
                ,'user_id'       => $data['user_id']
                ,'information_id' => $data['information_id']
                ,'comment_id'    => $data['comment_id']
            ];
            return $this->deleteAll( $cond );
        }
        // データが無ければ登録
        else
        {
            return parent::saveData( $data );
        }
    }

    public function checkDataExist( $data )
    {
         if( isset( $data['contractant_id'] ) && isset( $data['user_id'] ) && isset( $data['information_id'] ) && isset( $data['comment_id'] ) )
         {
             return $this->find()
                 ->where([
                     'contractant_id'  => $data['contractant_id']
                     ,'user_id'        => $data['user_id']
                     ,'information_id' => $data['information_id']
                     ,'comment_id'     => $data['comment_id']
                 ]) 
                 ->count();
         }
         return false;

    }
}
