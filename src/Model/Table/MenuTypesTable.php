<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MenuTypesTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
    }

    public function validation( $data=[] )
    {
        $err=[];

        if( isset( $data['login_id'] ) && strlen( $data['login_id'] ) === 0 )
        {
            $err['login_id'] = '入力してください';
        }
        elseif( isset( $data['login_id'] ) && $this->check_duplication( $data, 'login_id' ) )
        {
            $err['login_id'] = '既に登録されているログインIDは使用できません';
        }

        if( isset( $data['name'] ) && strlen( $data['name'] ) === 0 )
        {
            $err['name'] = '入力してください';
        }

        if( isset( $data['authority'] ) && strlen( $data['authority'] ) === 0 )
        {
            $err['authority'] = '選択してください';
        }

        return $err;
    }

    // paginator
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
         return $query
             //->select( [
             //    'id'
             //] )
             ->where( [
                 'deleted IS' => null
             ])
             ->order([
                 'orderby' => 'ASC'
             ]);
    }

    public function saveData( $data, $id = null )
    {
        return parent::saveData( $data );
    }

    //public function deleteData( $id )
    //{
    //    parent::deleteData( $id );
    //}

    // 新規の並び順を取得
    public function getOrder()
    {
        $res = $this->find()
            ->where([
                'deleted IS'     => null
            ])
            ->order([ 'orderby DESC' ])
            ->first();
        return ( $res ) ? $res->orderby + 1 : 1;
    }

    public function getOptions()
    {
        return $this->find('list')
            ->where([
                'deleted IS' => null
            ])
            ->order(['orderby ASC'])
            ->toArray();
    }
 
}
