<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;

class MtFormTypesTable extends AppTable
{
    //public function initialize( array $config )
    //{
    //}

    public function getList()
    {
        return $this->find('list', [
                'keyField'    => 'name'
                ,'valueField' => 'type'
            ])
            ->order( 'id ASC' )
            ->toArray();
    }

}
