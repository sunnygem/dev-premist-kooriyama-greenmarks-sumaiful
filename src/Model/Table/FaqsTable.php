<?php

namespace App\Model\Table;

use App\Model\Table\AppTable;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FaqsTable extends AppTable
{
    public function initialize( array $config )
    {
        parent::initialize( $config );
        $this->belongsTo( 'FaqsQuestions', [
        ]);

    }

    public function validation( $data=[] )
    {
        $err=[];

        if ( isset( $data['faq_category_id'] ) && $data['faq_category_id'] === '' )
        {
           $err['faq_category_id'] = '選択してください';
        }

        return $err;
    }

    // paginator 汎用
    public function findSearch( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->where( [
                'Faqs.deleted IS' => null
            ])
            ->contain([
            ])
            ->order([
            ]);
        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    // paginator フロント
    public function findFront( \Cake\ORM\Query $query, array $options )
    {
        $query
            ->select([
                'id',
                'faqs_question_id',
                'answer',
                'answer_en',
                'icon_url_path',
            ])
            ->where( [
                'Faqs.deleted IS' => null
                ,'Faqs.display_flg'   => 1

            ])
            
            ->contain([
                'FaqsQuestions' => [
                    'fields' => [
                        'question' => 'FaqsQuestions.faq_category_id',
                        'question' => 'FaqsQuestions.question',
                        'question' => 'FaqsQuestions.question_en'

                    ]
                ]
            ])
            ->order([
                'Faqs.created' => 'DESC'
            ]);

        if( isset($options['sort']) ) $query->order( $options['sort'] );
        return $query;
    }

    public function saveData( $data, $id=null )
    {
        return parent::saveData( $data );
    }

    public function deleteData( $id )
    {
        parent::deleteData( $id );
    }

    public function getFaqsData( $contractant_id=null )
    {
        $res = $this->find()
            ->select([
                'id', 'faqs_question_id','answer', 'answer_en', 'icon_url_path'
            ])
            ->where([
                'contractant_id' => $contractant_id
                ,'Faqs.display_flg'   => 1,
            ])
            ->order(['id' => 'ASC'])
            ->toArray();
        return $res;
    }
    
    public function getCountFaqsQuestionsId($contractant_id=null, $faqs_question_id)
    {
        $res = $this->find()
            ->select([
                'count' => 'count(faqs_question_id)'
            ])
            ->where([
                'Faqs.faqs_question_id' => $faqs_question_id
                ,'Faqs.contractant_id' => $contractant_id
                ,'Faqs.display_flg'   => 1
            ])
            ->group([
                'faqs_question_id'
            ])
            ->first();
        return $res;
    }

}
