<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class LineOpenChat extends Entity
{
    protected $_virtual = ['is_new'];
    // newかどうか
    protected function _getIsNew()
    {
        if( $this->isNew() === false )
        {
            return ( $this->_properties['release_date'] !== null && $this->_properties['release_date']->toUnixString() >= strtotime('-14 day', time() ) ) ? 1 : 0;
        }
    }

}
