<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Information extends Entity
{
    protected $_virtual = ['is_new'];

    // API仕様書1.3.0以降の対応
    public function _getType( $type )
    {
        if( isset( $this->_properties['event_flg'] ) && $this->_properties['event_flg'] === 1 )
        {
            return 'event';
        }
        else if( $type === null || $type === '0' )
        {
            return 'general';
        }
        else
        {
            return $type;
        }
    }

    // newかどうか
    protected function _getIsNew()
    {
        if( $this->isNew() === false )
        {
            return ( $this->_properties['release_date'] !== null && $this->_properties['release_date']->toUnixString() >= strtotime('-14 day', time() ) ) ? 1 : 0;
        }
    }

}
