<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Core\Configure;

class Equipment extends Entity
{
    public    $_mt_equipment_category;

    protected $_virtual = ['category_name'];

    // newかどうか
    protected function _getCategoryName()
    {
        $this->_mt_equipment_category =  Configure::read('mt_equipment_category');
        if( $this->isNew() === false )
        {
            return ( $this->_properties['category_id'] !== null  ) ? $this->_mt_equipment_category[$this->_properties['category_id']] : null;
        }
    }

}
