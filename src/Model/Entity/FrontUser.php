<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

use Cake\Core\Configure;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\CommonComponent;

class FrontUser extends Entity
{
    protected function _getName( $name )
    {
//var_dump($name);
//var_dump($this->_properties);

        $this->Common = new CommonComponent( new ComponentRegistry() );
        if ( isset( $this->_properties['encryption_parameter'] ) )
        {
            $iv   = $this->_properties['encryption_parameter']->iv;
            $salt = $this->_properties['encryption_parameter']->salt;
            return $this->Common->decrypt( $name, Configure::read( 'Security.encryptKey' ), base64_decode( $iv ), base64_decode( $salt ) ); 
        }
        return $name;

    }

}

