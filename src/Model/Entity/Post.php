<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Log\Log;

class Post extends Entity
{
    //protected $_virtual = [ '' ]

    protected function _getPublishedBuildingId( $published_building_id )
    {
       if( $published_building_id === null )
       {
           return 0;
       }
       return $published_building_id;
    }

}
