<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class SystemUser extends Entity
{
    protected function _setPassword( $password )
    {
        if ( strlen( $password ) > 0) {
            return ( new DefaultPasswordHasher )->hash( $password );
        }
    }
}
