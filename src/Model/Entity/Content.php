<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Content extends Entity
{
    protected $_virtual = ['html_content'];

    // API用にHTMLに加User工する
    protected function _getHtmlContent()
    {
        $html = '<!doctype html><html>'
                  . '<head>'
                      . '<meta charset="UTF-8">'
                      . '<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">'
                      . '<style>img{width:100%; height:auto}</style>'
                  . '</head>'
                  . '<body>' . $this->_properties['content'] . '</body>'
              . '</html>';

        return $html;
            ////return $content;
    }
}
