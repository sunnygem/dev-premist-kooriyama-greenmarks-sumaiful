<?php
namespace App\Model\Behavior;
use Cake\ORM\Behavior;

class CommonValidationBehavior extends Behavior
{
    public function initialize(array $config)
    {
        // 何らかの初期化処理
    }

    public function check_file( $data )
    {
        //エラーチェック
        switch ( $data['error'] )
        {
        //case UPLOAD_ERR_OK: // OK
        //    break;
        case UPLOAD_ERR_NO_FILE:   // 未選択
            return  'ファイルが選択されていません';
        case UPLOAD_ERR_INI_SIZE:  // php.ini定義の最大サイズ超過
            return  'ファイルサイズが大きすぎます';
        //default:
        //    return 'その他のエラーが発生しました';
        }
    }

    public function check_image( $data )
    {
        $res = mime_content_type( $data['tmp_name'] );
        return  in_array( $res, [ 'image/png', 'image/jpeg', 'image/jpeg' ], true );
    }

    public function check_pdf( $data )
    {
        $res = mime_content_type( $data['tmp_name'] );
        return ( $res === 'application/pdf' ) ? true : false;
    }

    // セッションが有効の場合のみ
    public function check_duplication( $data, $key=null, $contractant=true )
    {
        $res = $this->_table->find()
            ->where([
                $key => $data[$key]
                ,'deleted IS' => null
            ]);

        // 契約IDを見る場合は条件を追加
        if( $contractant )
        {
            $res->where(['contractant_id' => $this->_table->_session->read('ContractantData.id') ]);
        }
        //更新=idがある場合は自分を除く
        if( isset( $data['id'] ) )
        {
            $res->where(['id !=' => $data['id']]);
        }

        return ( $res->count() > 0 ) ? true : false;
    }

    // 半角英数ハイフンアンダースコア
    public function checkAlphabet( $data )
    {
        return ( preg_match( "/^[a-zA-Z0-9_-]+$/", $data ) ) ? true : false;
    }
    public function checkIpAddress( $data )
    {
        return ( preg_match( "/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/", $data ) ) ? true : false;
    }

    public function checkNumeric( $num )
    {
        return ( preg_match( "/^[0-9]+$/", $num ) ) ? true : false;
    }

    public function check_str_length( $str, $len )
    {
        return ( mb_strlen( $str ) > $len ) ? true : false;
    }

    public function check_include_number( $str )
    {
        return ( preg_match( '/\d/', $str ) === 1 ) ? true : false;
    }

    public function check_symbol_number( $str )
    {
        return ( preg_match( '/[\!-\/:-@\[-`\{-~]/', $str ) === 1 ) ? true : false;
    }

    public function check_date( $date, $delimiter='/' )
    {
        $tmp = explode( $delimiter, $date );
        return checkdate( $tmp[1], $tmp[2], $tmp[0] );

    }

    // メールアドレス形式チェック
    public function check_email( $email )
    {
        return (bool) preg_match( '/^[!-\-\/-~][!-~]*@([!-\-\/-~]+([\.][!-\-\/-~]+)+)$/', $email );
    }
    
}
