<?php
namespace App\Chat;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use Cake\ORM\TableRegistry;
use App\Service\CommonService;

class Chat implements MessageComponentInterface {
    protected $clients;

    //private $_unique_parameter; // PORTで分けて対応しようとしたけど、webserver側のport対応も必要みたい(supervisor？)なので、いったん使わないでparameter対応する

    public function __construct( $unique_parameter ) {
        $this->clients = new \SplObjectStorage;

        require dirname(__DIR__) . '/../webroot/index.php';

        //$this->_unique_parameter = $unique_parameter;
        $this->Contractants     = TableRegistry::getTableLocator()->get( 'Contractants' );
        $this->ChatMessages     = TableRegistry::getTableLocator()->get( 'ChatMessages' );
        $this->Users            = TableRegistry::getTableLocator()->get( 'Users' );
        $this->FrontUsers       = TableRegistry::getTableLocator()->get( 'FrontUsers' );
        //$this->ChatAlreadyReads = TableRegistry::getTableLocator()->get( 'ChatAlreadyReads' );
        $this->ChatGroupMembers = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->ChatUnreads      = TableRegistry::getTableLocator()->get( 'ChatUnreads' );
        $this->Threads          = TableRegistry::getTableLocator()->get( 'Threads' );
        $this->ChatMemberlists  = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg ) {

        $query = $from->httpRequest->getUri()->getQuery();
        list( $unique_parameter, $chat_group_id, $contractant_service_menu_id, $thread_id, $user_id, $front_user_id ) = $this->checkQuery( $query );
        // $unique_parameterがIDでも文字列パラメータでも大丈夫なように対応
        if ( preg_match( '/^[\d]+$/', $unique_parameter ) === 1 )
        {
            $contractant_id = $unique_parameter;
            $res = $this->Contractants->findById( $contractant_id )->first();
            $unique_parameter = $res->unique_parameter;
        }
        else
        {
            $res = $this->Contractants->getDataByUniqueParameter( $unique_parameter );
            $contractant_id = $res->id;
        }

//var_dump($from->httpRequest->getUri());
//var_dump($from->httpRequest->getUri()->getQuery());
var_dump($msg);
//var_dump(strlen($msg));

        $msg   = json_decode( $msg, true );
        $image = ( isset( $msg['image'] ) ) ? $msg['image'] : '';
        $audio = ( isset( $msg['audio'] ) ) ? $msg['audio'] : '';
        $video = ( isset( $msg['video'] ) ) ? $msg['video'] : '';
        $ext   = ( isset( $msg['ext'] ) )   ? $msg['ext'] : '';

        $file_path = '';
        if ( strlen( $image ) > 0 || strlen( $audio ) > 0 || strlen( $video ) > 0 )
        {
            // ファイル処理
            $file_path = $this->fileProcess( $thread_id, $image, $audio, $video, $ext );
        }

        $data = [
            'contractant_id'               => $contractant_id
            ,'chat_group_id'               => $chat_group_id
            ,'contractant_service_menu_id' => $contractant_service_menu_id
            ,'thread_id'                   => $thread_id
            ,'user_id'                     => $user_id
            ,'front_user_id'               => $front_user_id
            ,'message'                     => $msg['message']
            ,'file_path'                   => $file_path
        ];
var_dump($data);
        $this->ChatMessages->saveData( $data );
        $this->Threads->updateWriteUpdateDate( $thread_id );
        //$this->ChatAlreadyReads->saveChatAlreadyReadData( $contractant_id, $thread_id, $user_id, $front_user_id );
        // チャットグループからメンバーを取得
        $chat_members = $this->ChatGroupMembers->getChatGroupMembers( $contractant_id, $chat_group_id, $user_id, $front_user_id );
        // 未読登録
        foreach( $chat_members as $key => $val )
        {
            //$this->ChatAlreadyReads->saveChatAlreadyReadData( $contractant_id, $thread_id, $val->user_id, $val->front_user_id, false );
            $this->ChatUnreads->saveChatUnreadData( $contractant_id, $chat_group_id, $thread_id, $val->user_id, $val->front_user_id );
            $this->Users->saveData( [ 'id' => $val->user_id, 'unread_date' => date( 'Y-m-d H:i:s' ) ] );
            //$this->ChatMemberlists->saveUnreadFlg( $contractant_id, $user_id, $front_user_id, $val->user_id, $val->front_user_id, 1 );
            $this->ChatMemberlists->saveUnreadFlg( $contractant_id, $val->user_id, $val->front_user_id, $user_id, $front_user_id, 1 );
            // 自身のものを更新する
            $this->ChatMemberlists->saveUnreadFlg( $contractant_id, $user_id, $front_user_id, $val->user_id, $val->front_user_id, 0 );
        }
        // フロントユーザーからnicknameとthumbnail_pathを取得
        $data['front_user'] = $this->FrontUsers->getDataForChat( $front_user_id );

        // flgがconsultantの場合、画像のパスを取得する 2018.5.21 名前も取得する
        //list( $image_path, $consultant_name ) = $this->getImagePath( $login_id );

        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s ' . "\n" , $from->resourceId, json_encode( $msg ), $numRecv, $numRecv == 1 ? '' : 's' );

        // リンクメッセージを置換
        $CommonService = new CommonService();
        $data['message'] = $CommonService->filterUrlStr( $data['message'] );
        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $client_query = $client->httpRequest->getUri()->getQuery();
                list( $client_contractant_id, $client_chat_group_id, $client_contractant_service_menu_id, $client_thread_id, $client_user_id, $client_front_user_id ) = $this->checkQuery( $client_query );

                if ( $thread_id === $client_thread_id )
                {
                    // 既読処理
                    //$this->ChatAlreadyReads->saveChatAlreadyReadData( $client_contractant_id, $client_thread_id, $client_user_id, $client_front_user_id );
                    $this->ChatUnreads->deleteChatUnreadData( $client_contractant_id, $client_chat_group_id, $client_thread_id, $client_user_id, $client_front_user_id );
                    $this->ChatMemberlists->saveUnreadFlg( $client_contractant_id, $user_id, $front_user_id, $client_user_id, $client_front_user_id, 0 );
                    $json_data = json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT );
                    $client->send( $json_data );
                    //// The sender is not the receiver, send to each client connected
                    //if ( $user_id === $client_user_id )
                    //{
                    //    $client->send( '%%SAME_CLIENT%%::'.$msg);
                    //}
                    //else
                    //{
                    //    // 既読処理
                    //    //$this->chatAlreadyReadSave( $chat_id, $client_login_id );
                    //    $client->send( $msg );
                    //}
                }
            }
        }
        // 未読ユーザーにpush通知
        exec( SHELL_COMMAND . ' push ' . $unique_parameter . ' unread ' . $contractant_id . ' ' . $chat_group_id . ' ' . $thread_id );
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    private function checkQuery( $query )
    {
        $tmp = explode( '&', $query );
        foreach( $tmp as $key => $val )
        {
            $tmp2 = explode( '=', $val );
            switch( $tmp2[0] )
            {
            case 'contractant_id'              : $contractant_id              = $tmp2[1]; break;
            case 'chat_group_id'               : $chat_group_id               = $tmp2[1]; break;
            case 'contractant_service_menu_id' : $contractant_service_menu_id = $tmp2[1]; break;
            case 'thread_id'                   : $thread_id                   = $tmp2[1]; break;
            case 'user_id'                     : $user_id                     = $tmp2[1]; break;
            case 'front_user_id'               : $front_user_id               = $tmp2[1]; break;
            }
        }
        return [ $contractant_id, $chat_group_id, $contractant_service_menu_id, $thread_id, $user_id, $front_user_id ];
    }

    private function chatAlreadyReadSave( $chat_id, $login_id )
    {
        $params = [ 
            'chat_id'    => $chat_id
            ,'login_id'  => $login_id
        ];
        exec( CAKE_APP_DIR . 'bin/cake chat_save chat_already_read_save \'' . json_encode( $params, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) . '\'' );
    }

    private function getImagePath( $login_id )
    {
        exec( CAKE_APP_DIR . 'bin/cake get_image_path \'' . json_encode( $login_id, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ) . '\'', $ret, $res );
        return ( $res === 0 ) ? explode( "\t", $ret[0] ) : '';
    }

    // ファイル処理
    private function fileProcess( $thread_id, $image='', $audio='', $video='', $ext='' )
    {
        if ( $image !== '' )
        {
            $file_path = $this->fileSave( $image, 'image', $thread_id, $ext );
        }
        if ( $audio !== '' )
        {
            $file_path = $this->fileSave( $audio, 'audio', $thread_id, $ext );
        }
        if ( $video !== '' )
        {
            $file_path = $this->fileSave( $video, 'video', $thread_id, $ext );
        }
        return $file_path;
        //if ( preg_match( '/^data:image/', $msg ) )
        //{
        //    $file_path = $this->fileSave( $msg, 'image', $thread_id );
        //    $msg = '<img src="//' . DOMAIN . $file_path . '" class="thumb slb">';
        //}
        //else if ( preg_match( '/^data:audio/', $msg ) )
        //{
        //    $file_path = $this->fileSave( $msg, 'audio', $thread_id );
        //    $msg = '<audio src="//' . DOMAIN . $file_path . '" controls>';
        //}
        //else if ( preg_match( '/video.*\.mp4/', $msg ) )
        //{
        //    $file_path = $this->fileSave( $msg, 'video', $thread_id );
        //    $msg = '<video playsinline preload="none" controls muted><source src="//' . DOMAIN . $file_path . '"></video>';
        //}
        //else
        //{
        //    if ( $this->is_json( $msg ) )
        //    {
        //        $msgArr = json_decode( $msg );
        //        $msg = $msgArr->msg;
        //    }
        //    else
        //    {
        //        $msg = trim( $msg, '"' );
        //    }
        //}
        //return $msg;
    }

    // ファイル保存
    private function fileSave( $file, $case='image', $thread_id, $ext )
    {
        //$tmp  = explode( ',', $msg );
        //$file = trim( $tmp[1] );
        //$ext  = self::getExtenstion( $msg );
        switch( $case )
        {
        case 'image':
            $dir          = CHAT_IMAGE_DIR  . $thread_id;
            $relative_dir = CHAT_IMAGE_PATH . $thread_id;
            break;
        case 'audio':
            $dir          = CHAT_AUDIO_DIR  . $thread_id;
            $relative_dir = CHAT_AUDIO_PATH . $thread_id;
            break;
        case 'video':
            $dir          = CHAT_VIDEO_DIR  . $thread_id;
            $relative_dir = CHAT_VIDEO_PATH . $thread_id;
            break;
        default:
            return;
            break;
        }
        //if ( $case === 'video' )
        //{
        //    $tmp_file_name = preg_replace( '/\/\/' . DOMAIN . '\//', '', $msg );
        //    $file_name     = preg_replace( '/video\/tmp\//', '', $tmp_file_name );
        //    if ( file_exists( $dir ) === false ) mkdir( $dir, 0777, true );
        //    exec( 'cp ' . CAKE_APP_DIR . 'webroot/' . $tmp_file_name . ' ' . $dir . DS . $file_name, $res, $ret );
        //    return $relative_dir . DS . $file_name;
        //}
        //else
        //{
            if ( $ext !== '' ) $ext .= '.' . $ext;
            $file_name = uniqid( time() . '_' ) . $ext;
            if ( file_exists( $dir ) === false ) mkdir( $dir, 0777, true );
            file_put_contents( $dir. DS . $file_name, base64_decode( $file ) );
            return $relative_dir . DS . $file_name;
        //}
    }

    //private function getExtenstion( $msg )
    //{
    //    preg_match( '/audio\/(.*);/', $msg, $match );
    //    return ( isset( $match[1] ) && strlen( $match[1] ) > 0 ) ? '.' . $match[1] : '';
    //}

    // jsonかどうか
    private function is_json( $string )
    {
       return is_string( $string ) && is_array( json_decode( $string, true ) ) && ( json_last_error() === JSON_ERROR_NONE ) ? true : false;
    }

}
