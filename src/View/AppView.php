<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View;

use Cake\View\View;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link https://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadHelper( 'Common' );

        // タイトルセット
        $prefix = $this->request->getParam('prefix');
        if( $prefix !== null )
        {
            $title = '';
            if (!empty($this->viewVars['title_for_layout'])) {
                $title = $this->viewVars['title_for_layout']  . ' | ';
            }
            if( $prefix === 'system' )
            {
                $title .= SYSTEM_SITE_NAME;
            }
            elseif( $prefix === 'admin' )
            {
                $title .= ADMIN_SITE_NAME;
            }
            $this->assign( 'title', $title );
        }

        // Formヘルパーの拡張
        $this->Form->setTemplates([
            'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}}>{{text}}</label>',
            'radioWrapper' => '<span class="radio">{{label}}</span>',
            //'checkboxWrapper' => '<span class="checkbox">{{label}}</span>',
            'error' => '<p class="error">{{content}}</p>'
        ]);

    }
}
