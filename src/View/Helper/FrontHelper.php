<?php
namespace App\View\Helper;

use Cake\View\Helper;
use App\Service\CommonService;

class FrontHelper extends Helper
{
    public $CommonService;
    public function set_user_image( $user=[] )
    {
        // todo leave_flg
        if( isset( $user['front_user'] ) && isset( $user['front_user']['thumbnail_path'] ) &&  $user['front_user']['thumbnail_path'] )
        {
            return DS . $user['front_user']['thumbnail_path'];
        }
        else
        {
            return '/img/no_profile_image.jpg';
        }
    }

    public function set_activity_time( $time=null )
    {
        // 現時点との差分
        $diff_time = time() - strtotime( $time );
        // 60秒以内
        if( $diff_time < 60 )
        {
            echo noh( __('{0}秒前', $diff_time ) );
        }
        // 一時間以内
        elseif( $diff_time < 3600 )
        {
            echo __('{0}分前', floor( $diff_time / 60 ) );
        }
        // 24時間以内
        elseif( $diff_time < 86400 )
        {
            echo noh( __('{0}時間前', floor( $diff_time / 60 / 60 ) ) );
        }
        else
        {
            echo noh( date( __( 'Y/m/d H:i' ), strtotime( $time ) ) );
        }
    }

    public function set_category_class( $post=null )
    {
       $category = '';
       if( $post !== null )
       {
           switch( $post->post_category_id )
           {
           case 1:
               $category = 'cat_tweet';
               break;
           case 2:
               $category = 'cat_invite';
               break;
           case 3:
               $category = 'cat_ask';
               break;
           case 4:
               $category = 'cat_event';
               break;
           }
       }
       return $category;
    }

    public function check_my_activity( $user_id=null, $data=[] )
    {
        $flg = false;
        foreach( $data as $val )
        {
            if( $user_id === $val['user_id'] )
            {
                $flg = true;
            }
        }
        return $flg;
    }

    // urlがあったら<a>で囲って出力
    public function filterUrlStr( $str=null )
    {
        $this->CommonService = new CommonService();
        return $this->CommonService->filterUrlStr( $str );
    }

} 
