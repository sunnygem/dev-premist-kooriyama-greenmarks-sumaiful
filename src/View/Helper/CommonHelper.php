<?php
namespace App\View\Helper;

use Cake\View\Helper;

class CommonHelper extends Helper
{
     public function convert_binary_to_base64( $data )
     {
         return 'data:' . $data['mime_type'] . ';base64,' . base64_encode( $data['data'] );
     }

     public function get_stream( $resource=null )
     {
         return stream_get_contents( $resource );
     }

     public function convert_object_to_base64( $obj=null )
     {
         return 'data:' . $obj->mime_type . ';base64,' . base64_encode( $this->get_stream( $obj->data ) );
     }
}
