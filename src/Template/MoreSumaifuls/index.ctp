<div class="moresumaiful">
    <h2 class="contents_logo">MORE SUMAIFUL</h2>
    <div class="article_list">
        <ul>
        <?php foreach( $contents as $val ): ?>
            <li>
                <a href="<?php echo noh( $this->Url->build(['action' => 'article', $val->id ]) ); ?>">
                    <div class="info_img" style="<?php if( count( $val->files ) > 0 ) echo noh( 'background-image: url( ' . DS . $val->files[0]->file_path . ');' ); ?>">
                    </div>
                    <div class="info_txt">
                        <p class="date"><?php echo noh( $val->release_date->format('Y.m.d') ); ?></p>
                        <p class="title"> <?php echo noh( $val->title ); ?></p>
                    </div>
                </a>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>
</div>

