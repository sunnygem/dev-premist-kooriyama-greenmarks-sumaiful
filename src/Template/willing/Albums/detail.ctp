<?php $this->assign('body_id','album') ?>
<?php $this->assign('title',$image_sharings->title) ?>
<section class="posts">
    <div class="bt_box">
        <a class="bt_back" href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>">Album</a>
    </div>
    <div class="bg">
        <div class="inner">
            <div class="album_inner">
                <div class="overview clearfix">
                    <div class="clearfix">
                        <div class="ttl">
                            <?php echo $image_sharings->title ; ?>
                        </div>
                        <div class="date"><?php echo noh( $image_sharings->release_date->format( __( 'Y/m/d' ) ) ); ?></div>
                        <div class="post_menu_wrap">
                            <span
                            class="bt_other"
                            data-target-id="<?php echo $image_sharings->rel_imagesharing_files[0]->file_id; ?>"
                            data-target-type="file"
                            data-login-user-id="<?php echo $login_user['id']; ?>"
                            data-login-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                            ><?php echo __('その他'); ?></span>
                        </div>
                    </div>
                </div>
                <?php if( $image_sharings->rel_imagesharing_files ): ?>
                    <div id="img_area">
                        <img src="<?php echo DS . noh( $image_sharings->rel_imagesharing_files[0]->file->file_path ); ?>" alt="">
                    </div>
                    <div class="thumb">
                        <ul>
                            <?php foreach( $image_sharings->rel_imagesharing_files as $key => $val ): ?>
                                <li class="<?php if( $key === 0) echo 'active'; ?>">
                                    <div class="thumb_img" style="background-image:url(<?php echo DS . noh( $val->file->file_path ); ?>);" data-target-id="<?php echo $val->file_id; ?>"></div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

