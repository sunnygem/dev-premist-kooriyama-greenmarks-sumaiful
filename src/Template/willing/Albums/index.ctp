<?php $this->assign('body_id','album') ?>
<?php $this->assign('title',__('アルバム')) ?>
<section class="posts">
    <div class="bt_box">
        <a class="bt_back" href="<?php echo noh( $this->Url->build(['controller' => 'Tops', 'action' => 'index']) ); ?>">Top</a>
    </div>
    <div class="bg">
        <div class="inner clearfix">
            <?php foreach( $image_sharings as $val ): ?>
            <article>
                <a class="bt_more" href="<?php echo noh( $this->Url->build(['action' => 'detail', $val->id]) ); ?>">
                    <div class="article_header clearfix">
                        <div class="title"><?php echo h( $val->title ); ?></div>
                        <div class="num"><?php echo noh( count( $val->rel_imagesharing_files ) ); ?></div>
                    </div>
                </a>
                <a class="bt_more" href="<?php echo noh( $this->Url->build(['action' => 'detail', $val->id]) ); ?>">
                    <ul class="list clearfix">
                        <?php $i = 0; ?>
                        <?php foreach( $val->rel_imagesharing_files as $val2 ): ?>
                            <li>
                                <div class="thumb_img" style="background-image:url(<?php echo DS . $val2->file->file_path ; ?>);"></div>
                            </li>
                            <?php $i++; ?>
                            <?php if( $i > 5 ) break; ?>
                        <?php endforeach; ?>
                    </ul>
                </a>
                <?php // <a class="bt_other" href="#">その他</a> ?>
            </article>
            <?php endforeach; ?>
            <?php /*
            <article>
                <div class="article_header clearfix">
                    <div class="title">新年あいさつ会</div>
                    <div class="num">20</div>
                </div>
                <a class="bt_more" href="#">
                    <ul class="list clearfix">
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                    </ul>
                </a>
                <a class="bt_other" href="#">その他</a>
            </article>
            <article>
                <div class="article_header clearfix">
                    <div class="title">新年あいさつ会</div>
                    <div class="num">20</div>
                </div>
                <a class="bt_more" href="#">
                    <ul class="list clearfix">
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                    </ul>
                </a>
                <a class="bt_other" href="#">その他</a>
            </article>
            <article>
                <div class="article_header clearfix">
                    <div class="title">新年あいさつ会</div>
                    <div class="num">20</div>
                </div>
                <a class="bt_more" href="#">
                    <ul class="list clearfix">
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                    </ul>
                </a>
                <a class="bt_other" href="#">その他</a>
            </article>
            <article>
                <div class="article_header clearfix">
                    <div class="title">新年あいさつ会</div>
                    <div class="num">20</div>
                </div>
                <a class="bt_more" href="#">
                    <ul class="list clearfix">
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                        <li><img src="/img/willing/img_album.jpg" alt=""></li>
                    </ul>
                </a>
                <a class="bt_other" href="#">その他</a>
            </article>
            */ ?>
        </div>
    </div>
</section>
