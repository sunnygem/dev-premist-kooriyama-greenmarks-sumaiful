<?php $this->assign('body_id', 'equipment') ?>
<?php $this->assign('title', __('備品')) ?>
<section class=" equipment">
    <ul class="tab">
        <li class="category_all <?php if( $this->request->getQuery('category') === null ) echo noh( 'active' );  ?>"><a href= "<?php echo $this->Url->build(['action' => 'index']); ?>">ALL</a></li>
        <?php foreach( $mt_equipment_category as $key => $val ): ?>
            <li class="category_<?php echo noh( $key ); ?> <?php if( (int)$this->request->getQuery('category') === $key ) echo noh( 'active' );  ?>">
                <a href="<?php echo $this->Url->build([ '?' => [ 'category' => $key ] ]); ?>"><?php echo $val; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>

    <div class="list">
        <?php foreach( $data as $val ): ?>
            <article class="">
                <div class="preview">
                    <?php if( $val->icon_url_path !== null  ) echo $this->Html->image( DS . $val->icon_url_path, [] ); ?>
                </div>
                <div class="content">
                    <?php if( $val->in_use_flg == 1 ): ?><span class="in_use">使用中</span><?php endif; ?>
                    <div class="name">
                        <?php if( $lang === 'en' && $val->name_en ): ?>
                            <?php echo h( $val->name_en ); ?>
                        <?php else: ?>
                            <?php echo h( $val->name ); ?>
                        <?php endif; ?>
                    </div>
                    <?php if( $val->expected_return_date !== null ): ?>
                        <div class="date">返却予定日時：<?php echo noh( $val->expected_return_date->format( __( 'Y/m/d H:i' ) ) ); ?></div>
                    <?php endif; ?>
                </div>
            </article>
        <?php endforeach; ?>
    </div>

</section>
<div class="pager">
    <ul>
        <?php echo noh( $this->Paginator->prev( '' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '' ) ); ?>
    </ul>
</div>
