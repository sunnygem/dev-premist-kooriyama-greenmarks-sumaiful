<?php $this->assign('body_id','post') ?>
<?php $this->assign('title',__('投稿') ) ?>
<?php echo noh( $this->Form->create(null, ['type' => 'file']) ); ?>
    <div class="tab clearfix">
        <a class="tweet active" href="javascript:void(0)" onclick="return false;" data-category-id="1" data-placeholder="<?php echo __('本文'."\n".'例：ゆったりできるカフェ見つけた！') ?>"><?php echo __('つぶやく') ?></a>
        <a class="invite"       href="javascript:void(0)" onclick="return false;" data-category-id="2" data-placeholder="<?php echo __('本文'."\n".'例：今から食堂で集まって英会話教室やりませんか？') ?>"><?php echo __('おさそい') ?></a>
        <a class="ask"          href="javascript:void(0)" onclick="return false;" data-category-id="3" data-placeholder="<?php echo __('本文'."\n".'例：おすすめの病院を教えてください。') ?>"><?php echo __('質問する') ?></a>
        <a class="event"        href="javascript:void(0)" onclick="return false;" data-category-id="4" data-placeholder="<?php echo __('本文'."\n".'例：お花見交流会を企画します！みんなでわいわい春を楽しみましょう！') ?>"><?php echo __('イベント') ?></a>
    </div>
    <div class="tab_inner">
        <?php echo noh( $this->Form->hidden( 'post_category_id' ) ); ?>
        <section class="tweet">
            <?php echo noh( $this->Form->textarea('content', ['placeholder' => __('本文'."\n".'例：ゆったりできるカフェ見つけた！') ] ) ); ?>
            <div class="file">
                <div class="preview_target img">
                <span class="img_remove" id="img_remove"></span>
                </div>
                <div class="bt_box">
                    <label>
                        <span class="bt_upload"><?php echo __('写真をアップロード') ?></span>
                        <?php echo noh( $this->Form->file( 'files.posts', ['class' => 'posts'] )); ?>
                    </label>
                </div>
            </div>

            <div class="send_button_wrap">
                <div class="wrap_disclosure_range">
                    公開範囲
                    <?php echo $this->Form->select('post_buildings.building_id[]', $mt_buildings, ['class' => 'disclosure_range', 'default' => 0 ]); ?>
                </div>

                <?php echo noh( $this->Form->button( __('送信'), ['class' => 'submit', 'type' => 'button' ] ) ); ?>
            </div>
        </section>
    </div>
<?php echo noh( $this->Form->end() ); ?>
