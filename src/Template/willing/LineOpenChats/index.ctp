<?php $this->assign('body_id','line_open_chat') ?>
<?php $this->assign('title',__('LINE オープンチャット')) ?>
<section class="posts line_open_chat">
    <ul class="tab">
        <li><a href= "<?php echo $this->Url->build(['action' => 'index']); ?>">ALL</a></li>
        <?php foreach( $categories as $key => $val ): ?>
            <li><a href= "<?php echo $this->Url->build([ '?' => [ 'category' => $val->id ] ]); ?>" class="<?php if( (int)$this->request->getQuery('category') === $val->id ) echo noh( 'active' ); ?>">
                <i class="icon" style="<?php if( $val->icon_url_path !== null ) echo noh( 'background-image: url(' . DS . $val->icon_url_path . ')' ); ?>"></i>
                <?php if( $lang === 'en' && $val->name_en ): ?>
                    <?php echo $val->name_en; ?>
                <?php else: ?>
                    <?php echo $val->name; ?>
                <?php endif; ?>
            </a></li>
        <?php endforeach; ?>
    </ul>

    <hr>

    <div>
    <?php foreach( $data as $val ): ?>
        <article class="">
            <a href="<?php echo noh( $val->url ); ?>" target="_blank">
                <div class="preview" style="<?php if( $val->icon_url_path !== null  ) echo noh( 'background-image: url(' . DS . $val->icon_url_path . ')' ); ?>"></div>
                <div class="name">
                    <?php if( $val->is_new === 1 ) echo noh( '<i class="is_new"></i>' ); ?>
                    <?php if( $lang === 'en' && $val->title_en ): ?>
                        <?php echo h( mb_strimwidth( $val->title_en, 0, 54, '…' )  ); ?>
                    <?php else: ?>
                        <?php echo h( mb_strimwidth( $val->title, 0, 54, '…' )  ); ?>
                    <?php endif; ?>
                </div>
                <?php /* div class="name"><?php echo h( mb_strimwidth( $val->caption, 0, 60, '…' )  ); ?></div> */ ?>
            </a>
        </article>
    <?php endforeach; ?>
    </div>

</section>
<div class="pager">
    <ul>
        <?php echo noh( $this->Paginator->prev( '' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '' ) ); ?>
    </ul>
</div>
