<?php $this->assign('body_id','login') ?>
<div class="main">
    <div class="form">
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->hidden( 'contractant_id', ['value' => $this->request->session()->read('ContractantData.id') ] ) ); ?>
            <?php echo noh( $this->Form->text( 'login_id',     ['placeholder' => 'Log in ID', 'class' => 'id' ] ) ); ?>
            <?php echo noh( $this->Form->password( 'password', ['placeholder' => 'Password',  'class' => 'password' ] ) ); ?>
            <div class="bt_box">
                <a class="bt_forgot" href="<?php echo noh( $this->Url->build(['controller' => 'Applications', 'action' => 'index']) ); ?>"><?php echo __('forget password') ?></a>
            </div>
            <div class="lang_box">
                <a class="bt_jp <?php if( $lang === 'ja' ): ?>active<?php endif; ?>" href="<?php echo noh( $this->Url->build(['action' => 'setLocale', 'ja']) ); ?>">JP</a>
                <a class="bt_en <?php if( $lang === 'en' ): ?>active<?php endif; ?>" href="<?php echo noh( $this->Url->build(['action' => 'setLocale', 'en']) ); ?>">EN</a>
            </div>
            <?php echo noh( $this->Flash->render() ); ?>
            <?php echo noh( $this->Form->button( __('log in'), [ 'class' => 'submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
