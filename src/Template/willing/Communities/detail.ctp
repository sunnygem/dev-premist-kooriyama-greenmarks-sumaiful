<?php $this->assign('body_id','community') ?>
<?php $this->assign('title',__('コミュニティ')) ?>
<script>
    var json = {};
</script>
<section class="posts">
    <article class="<?php echo h( $this->Front->set_category_class( $posts ) ); ?>">
        <div class="inner">
            <div class="article_header clearfix">
                <a class="author" href="<?php echo noh( $this->Url->build(['controller' => 'Mypages', 'action' => 'index', $posts->user->id] ) ); ?>">
                    <div class="photo_wrap">
                        <div class="photo" style="background-image:url(<?php echo $this->Front->set_user_image( $posts->user ) ?>);"></div>
                    </div>
                    <div class="building">
                        <?php if( $posts->user->front_user->building->icon_path ): ?>
                            <?php echo noh( '<img src="/' . $posts->user->front_user->building->icon_path . '" alt="' . $posts->user->front_user->building->name . '" class="icon">' ); ?>
                        <?php else: ?>
                            <?php echo h( $posts->user->front_user->building->name ); ?>
                        <?php endif; ?>
                    </div>
                    <div class="name <?php if( $posts->user->front_user->leave_flg === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $posts->user->front_user->nickname ); ?></div>
                    <div class="date"><?php echo noh( $posts->created->format( __( 'Y/m/d' ) ) . ' at ' . $posts->created->format( 'g:i A' ) ); ?></div>
                </a>
                <div class="post_menu_wrap">
                    <span
                    class="bt_other"
                    data-target-id="<?php echo $posts->id; ?>"
                    data-target-type="post"
                    data-user-id="<?php echo $posts->user->id; ?>"
                    data-front-user-id="<?php echo $posts->user->front_user->id; ?>"
                    data-login-user-id="<?php echo $login_user['id']; ?>"
                    data-login-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                    ><?php echo __('メニュー'); ?></span>
                </div><!--./post_menu_wrap-->
            </div>
            <div class="content">
                <?php echo nl2br( $this->Front->filterUrlStr( h( $posts->content ) ) ); ?>
                <?php if( count( $posts->rel_post_files ) > 0 ): ?>
                    <?php echo noh( $this->Html->image( DS . $posts->rel_post_files[0]->file->file_path, ['alt' => ''] ) ); ?>
                <?php endif; ?>
            </div>
            <div class="article_footer">
                <div class="bt_box clearfix">
                    <?php if( $posts->post_category_id === 4 ): ?>
                        <a href="javascript: void(0);"
                        class="bt_event join fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $posts->post_joins ) ) echo 'active'; ?>"
                        data-action="join"
                        data-controller="communities"
                        data-user-id="<?php echo $login_user['id']; ?>"
                        data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                        data-id="<?php echo $posts->id; ?>"
                        ><?php echo __('参加') ?></a>
                    <?php else: ?>
                        <a href="javascript: void(0);"
                        class="bt_favorite fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $posts->post_likes ) ) echo 'active'; ?>"
                        data-action="like"
                        data-controller="communities"
                        data-user-id="<?php echo $login_user['id']; ?>"
                        data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                        data-id="<?php echo $posts->id; ?>"
                        ><?php echo __('いいね') ?></a>
                    <?php endif; ?>
                    <a class="bt_comment" href="<?php echo noh($this->Url->build(['action' => 'comment', $posts->id]) ); ?>"><?php echo __('コメント') ?></a>
                    <?php if( count( $posts->post_comments ) > 0 ): ?>
                        <span class="comment_count"><?php echo __( '{0}コメント', count( $posts->post_comments ) ); ?></span>
                    <?php endif; ?>

                    <?php if( $posts->post_category_id === 4 ): ?>
                        <div class="favorite event" data-target-id="<?php echo $posts->id; ?>">
                            <span class="count" data-count="<?Php echo count( $posts->post_joins ); ?>">
                            <?Php echo __x( '参加の数', '{0}人', count( $posts->post_joins ) ) ?></span>
                            <script>json[<?php echo $posts->id; ?>] = <?php echo json_encode($posts->post_joins, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ); ?>;</script>
                            <?php /*
                            <?php if( count( $posts->post_joins ) === 0 ): ?>
                                <?php echo __('0人') ?>
                            <?php elseif( count( $posts->post_joins ) > 0 ): ?>
                                <?php echo noh( $posts->post_joins[0]->user->front_user->nickname ); ?><?php if( count( $posts->post_joins ) > 1 ): ?>、<?php echo __('他') ?><?php echo noh( count( $posts->post_joins ) - 1 ); ?><?php echo __('人') ?><?php endif; ?>
                            <?php endif; ?>
                            */ ?>
                        </div>
                    <?php else: ?>
                        <div class="favorite" data-target-id="<?php echo $posts->id; ?>">
                            <span class="count" data-count="<?Php echo count( $posts->post_likes ); ?>">
                            <?Php echo __x( 'いいねの数', '{0}人', count( $posts->post_likes ) ) ?></span>
                            <script>json[<?php echo $posts->id; ?>] = <?php echo json_encode($posts->post_likes, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ); ?>;</script>
                            <?php /*
                            <?php if( count( $posts->post_likes ) === 0 ): ?>
                                <?php echo __('0人') ?>
                            <?php elseif( count( $posts->post_likes ) > 0 ): ?>
                                <?php echo noh( $posts->post_likes[0]->user->front_user->nickname ); ?><?php if( count( $posts->post_likes ) > 1 ): ?>、<?php echo __('他') ?><?php echo noh( count( $posts->post_likes ) - 1 ); ?><?php echo __('人') ?><?php endif; ?>
                            <?php endif; ?>
                            */ ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="comment">
                    <?php if( count( $posts->post_comments ) > 0 ): ?>
                        <?php $comments = array_reverse( $posts->post_comments ); ?>
                        <div class="comment_preview">
                            <p>
                                <span class="name"><?php echo h( $comments[0]->user->front_user->nickname ); ?></span><?php echo mb_strimwidth( h( $comments[0]->comment ), 0, 60, '...' ); ?>
                                <a class="more" href="<?php echo $this->Url->build(['action' => 'comment', $posts->id]); ?>"><?php echo __('コメントをすべて表示'); ?></a>
                            </p>
                        </div>
                    <?php endif; ?>
                    <div class="photo_wrap">
                        <div class="photo" style="background-image:url(<?php echo DS . $login_user['front_user']['thumbnail_path']; ?>);"></div>
                    </div>
                    <input type="text" placeholder="Comment..." readonly data-controller="communities" data-id="<?php echo $posts->id; ?>">
                </div>
            </div>
        </div>
    </article>

</section>

