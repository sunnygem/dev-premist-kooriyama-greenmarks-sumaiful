<?php $this->assign('body_id','community') ?>
<?php $this->assign('title',__('コミュニティ')) ?>
<script>
    var json = {};
</script>
<div class="tab clearfix">
    <a class="trig tweet <?php if( $this->request->getQuery('post_category_id') === '1' ): ?>active<?php endif; ?>" href="javascript:void(0)" onclick="return false;" data-category-id="1">つぶやく</a>
    <a class="trig invite <?php if( $this->request->getQuery('post_category_id') === '2' ): ?>active<?php endif; ?>" href="javascript:void(0)" onclick="return false;" data-category-id="2">おさそい</a>
    <a class="trig ask <?php if( $this->request->getQuery('post_category_id') === '3' ): ?>active<?php endif; ?>" href="javascript:void(0)" onclick="return false;" data-category-id="3">質問する</a>
    <a class="trig event <?php if( $this->request->getQuery('post_category_id') === '4' ): ?>active<?php endif; ?>" href="javascript:void(0)" onclick="return false;" data-category-id="4">イベント</a>
</div>
<section class="posts <?php // autoPager ?>" data-action="/communities/paging" data-element="post_list">
    <?php foreach( $posts as $val ): ?>

    <article class="<?php echo h( $this->Front->set_category_class( $val ) ); ?>" data-label="<?php echo noh( $val->post_category->label ); ?>">
        <div class="inner">
            <div class="article_header clearfix">
                <a class="author" href="<?php echo noh( $this->Url->build(['controller' => 'Mypages', 'action' => 'index', $val->user->id] ) ); ?>">
                    <div class="photo_wrap">
                        <div class="photo" style="background-image:url(<?php echo $this->Front->set_user_image( $val->user ) ?>);"></div>
                    </div>
                    <div class="building">
                        <?php if( $val->user->front_user->building->icon_path ): ?>
                            <?php echo noh( '<img src="/' . $val->user->front_user->building->icon_path . '" alt="' . $val->user->front_user->building->name . '" class="icon">' ); ?>
                        <?php else: ?>
                            <?php echo h( $val->user->front_user->building->name ); ?>
                        <?php endif; ?>
                    </div>
                    <div class="name <?php if( $val->user->front_user->leave_flg === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $val->user->front_user->nickname ); ?></div>
                    <div class="date"><?php echo noh( $val->created->format( __( 'Y/m/d' ) ) . ' at ' . $val->created->format( __( 'g:i A' ) ) ); ?></div>
                </a>
                <div class="post_menu_wrap">
                    <span
                        class="bt_other"
                        data-target-id="<?php echo $val->id; ?>"
                        data-target-type="post"
                        data-user-id="<?php echo $val->user->id; ?>"
                        data-front-user-id="<?php echo $val->user->front_user->id; ?>"
                        data-login-user-id="<?php echo $login_user['id']; ?>"
                        data-login-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                    ><?php echo __('メニュー'); ?></span>
                </div><!--./post_menu_wrap-->
            </div>
            <div class="content">
                <?php echo nl2br( $this->Front->filterUrlStr( h( $val->content ) ) ); ?>
                <?php if( count( $val->rel_post_files ) > 0 ): ?>
                    <?php echo noh( $this->Html->image( DS . $val->rel_post_files[0]->file->file_path, ['alt' => ''] ) ); ?>
                <?php endif; ?>
            </div>
            <div class="article_footer">
                <div class="bt_box clearfix">
                    <?php if( $val->post_category_id === 4 ): ?>
                        <a href="javascript: void(0);"
                        class="bt_event join fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $val->post_joins ) ) echo 'active'; ?>"
                        data-action="join"
                        data-controller="communities"
                        data-user-id="<?php echo $login_user['id']; ?>"
                        data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                        data-id="<?php echo $val->id; ?>"
                        ><?php echo __('参加') ?></a>
                    <?php else: ?>
                        <a href="javascript: void(0);"
                        class="bt_favorite fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $val->post_likes ) ) echo 'active'; ?>"
                        data-action="like"
                        data-controller="communities"
                        data-user-id="<?php echo $login_user['id']; ?>"
                        data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                        data-id="<?php echo $val->id; ?>"
                        ><?php echo __('いいね') ?></a>
                    <?php endif; ?>
                    <a class="bt_comment" href="<?php echo noh($this->Url->build(['action' => 'comment', $val->id]) ); ?>"><?php echo __('コメント') ?></a>
                    <?php if( count( $val->post_comments ) > 0 ): ?>
                        <span class="comment_count"><?php echo __( '{0}コメント', count( $val->post_comments ) ); ?></span>
                    <?php endif; ?>

                    <?php if( $val->post_category_id === 4 ): ?>
                        <div class="favorite event" data-target-id="<?php echo $val->id; ?>">
                            <span class="count" data-count="<?Php echo count( $val->post_joins ); ?>">
                            <?Php echo __x( '参加の数', '{0}人', count( $val->post_joins ) ) ?></span>
                            <script>json[<?php echo $val->id; ?>] = <?php echo json_encode($val->post_joins, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ); ?>;</script>
                        <?php /*
                            <?php if( count( $val->post_joins ) === 0 ): ?>
                                <?php echo __('人') ?>
                            <?php elseif( count( $val->post_joins ) > 0 ): ?>
                                <?php echo noh( $val->post_joins[0]->user->front_user->nickname ); ?><?php if( count( $val->post_joins ) > 1 ): ?>、<?php echo __('他') ?><?php echo noh( count( $val->post_joins ) - 1 ); ?><?php echo __('人') ?><?php endif; ?>
                            <?php endif; ?>

                        */ ?>
                        </div>
                    <?php else: ?>
                        <div class="favorite" data-target-id="<?php echo $val->id; ?>">
                            <span class="count" data-count="<?Php echo count( $val->post_likes ); ?>">
                            <?Php echo __x( 'いいねの数', '{0}人',count( $val->post_likes ) ) ?></span>
                            <script>json[<?php echo $val->id; ?>] = <?php echo json_encode($val->post_likes, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ); ?>;</script>
                        <?php /*
                            <?php if( count( $val->post_likes ) === 0 ): ?>
                                <?php echo __('0人') ?>
                            <?php elseif( count( $val->post_joins ) > 0 ): ?>
                            <?php elseif( count( $val->post_likes ) > 0 ): ?>
                                <?php echo noh( $val->post_likes[0]->user->front_user->nickname ); ?><?php if( count( $val->post_likes ) > 1 ): ?>、<?php echo __('他') ?><?php echo noh( count( $val->post_likes ) - 1 ); ?><?php echo __('人') ?><?php endif; ?>
                            <?php endif; ?>
                        */ ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="comment">
                    <?php if( count( $val->post_comments ) > 0 ): ?>
                        <div class="comment_preview">
                            <p>
                                <span class="name"><?php echo h( $val->post_comments[0]->user->front_user->nickname ); ?></span><?php echo mb_strimwidth( h( $val->post_comments[0]->comment ), 0, 60, '...' ); ?>
                                <a class="more" href="<?php echo $this->Url->build(['action' => 'comment', $val->id]); ?>"><?php echo __('コメントをすべて表示'); ?></a>
                            </p>
                        </div>
                    <?php endif; ?>
                    <div class="photo_wrap">
                        <div class="photo" style="background-image:url(<?php echo noh( DS . $login_user['front_user']['thumbnail_path'] ); ?>);"></div>
                    </div>
                    <input type="text" placeholder="Comment..." readonly data-controller="communities" data-id="<?php echo $val->id; ?>">
                </div>
            </div>
        </div>
    </article>
    <?php endforeach; ?>

</section>
<div class="pager">
    <ul>
        <?php echo noh( $this->Paginator->prev( '' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '' ) ); ?>
    </ul>
</div>

