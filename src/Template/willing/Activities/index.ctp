<?php $this->assign('body_id','like') ?>
<?php $this->assign( 'title','アクティビティ') ?>
<section class="likes">
    <ul class="list">
        <?php if( $data->isEmpty() ): ?>
            <p><?php echo __('まだデータがありません') ?></p>
        <?php else: ?>
            <?php foreach( $data as $val ): ?>
            <?php if( $val->post  ): ?>
            <li class="single <?php echo noh( $this->Front->set_category_class( $val->post ) ); ?>">
                <div class="clearfix">
                    <div class="left clearfix">
                        <a href="<?php echo noh( $this->Url->build(['controller' => 'Mypages', 'action' => 'index', $val->user_id ]) ); ?>">
                            <div class="photo_wrap">
                                <div class="photo" style="background-image:url( <?php echo $this->Front->set_user_image( $val->user ); ?>" alt=""></div>
                            </div>
                            <div class="line">
                                <div class="name <?php if( $val->user->front_user->leave_flg === 1 ): ?>leaved<?php endif; ?>"><?php echo noh( ( $val->user !== null ) ? $val->user->front_user->nickname  : __('退会したユーザー') ); ?></div>
                                <span class="txt"><?php echo __('さんが') ?></span>
                            </div>
                            <div class="info">
                                <?php
                                    switch( $val->activity_type )
                                    {
                                    case 'like':
                                        echo '<div class="txt">'. __('いいねしました。') .'</div>';
                                        break;
                                    case 'join':
                                        echo '<div class="txt">'. __('イベントに参加しました。') .'</div>';
                                        break;
                                    case 'comment':
                                        echo '<div class="txt">'. __('コメントしました。') .'</div>';
                                        break;
                                    case 'comment_like':
                                        echo '<div class="txt">'. __('コメントにいいねしました。') .'</div>';
                                        break;
                                    }
                                ?>
                                <div class="date"><?php echo noh( $this->Front->set_activity_time( $val->created ) ); ?></div>
                            </div>
                        </a>
                    </div>
                    <div class="right">
                        <a href="<?php echo noh( $this->Url->build(['controller' => 'Communities', 'action' => 'detail', $val->post->id ]) ); ?>">
                        </a>
                    </div>
                </div>
            </li>
            <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>

    <div class="pager">
        <ul>
            <?php echo noh( $this->Paginator->prev( '' ) ); ?>
            <?php echo noh( $this->Paginator->numbers() ); ?>
            <?php echo noh( $this->Paginator->next( '' ) ); ?>
        </ul>
    </div>
</section>
