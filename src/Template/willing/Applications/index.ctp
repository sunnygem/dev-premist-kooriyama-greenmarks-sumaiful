<?php echo noh( $this->Form->create( $users, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off', 'id' => 'application_form' ] ) ); ?>
<div class="form_logo">
    <?php echo noh( $this->Html->image( 'img_logo.png' ) ); ?>
</div>
<?php echo nl2br( noh( $this->Flash->render() ) ); ?>
<div class="form_wrap application">
    <div class="form_content">
        <div class="form">
            <table>
                <tr>
                    <th>Full Name<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->input( 'front_user.name', [ 'label' => false, 'placeholder' => 'フルネーム/ Full name' ] ) ); ?></td>
                </tr>
                <tr>
                    <th>Email<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->email( 'email', [ 'label' => false, 'placeholder' => 'メールアドレス/ E-mail' ] ) ); ?></td>
                </tr>
                <tr>
                    <th>Building Name<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->select( 'front_user.building_id', $buildings, [ 'label' => false, 'placeholder' => 'Building Name' ] ) ); ?><span></span></td>
                </tr>
                <tr>
                    <th>Room No.<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->text( 'front_user.room_number', [ 'label' => false, 'placeholder' => '部屋番号/ Room number' ] ) ); ?></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="text-align:center;">
                            <div><a href="<?php echo noh( $this->Url->build(['controller' => 'Terms', 'action' => 'index']) ); ?>" style="text-decoration: underline;" target="_blank">利用規約 / Term of use</a></div>
                            <?php /*
                            <div class="term_of_use">
                                <?php echo noh( $term_of_use->content ); ?>
                            </div>
                            <div class="term_of_use">
                                <?php echo noh( $term_of_use->content_en ); ?>
                            </div>
                            */ ?>
                            <p style="display: inline-block;margin-bottom:40px;">
                                <?php echo noh( $this->Form->checkbox( 'front_user.agree_flg', [ 'label' => false, 'id' => 'agree' ] ) ); ?>
                                <label for="agree" class="fs13">同意します。/ I Agree.<span class="require">*</span></label>
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align:middle;">再申請<br>Reapply</th>
                    <td>
                        <?php echo noh( $this->Form->checkbox( 'front_user.reapply_flg', [ 'label' => false, 'id' => 'reapply' ] ) ); ?>
                        <label for="reapply" class="fs13">
                            メールアドレス・パスワードを忘れた方はこちらにチェックを入れて再申請して下さい。<br>
                            If you forgot your email or password, please check this box and apply again.
                        </label>
                    </td>
                </tr>
            </table>
        </div>

        <div class="text_c">
            <?php echo noh( $this->Form->button( 'OK', [ 'label' => false, 'name' => 'send', 'class' => 'btn submit active', 'id' => 'ok' ] ) ); ?>
        </div>
    </div>
</div>

<?php echo noh( $this->Form->end() ); ?>
<p style="text-align:center;margin:20px;"><a style="text-decoration:underline;" href="/applications/help" target="_blank">メールが届かない場合はこちら</a></p>
<div class="loader_wrap hidden">
    <div class="loader hidden"></div>
</div>
<script>
$(function(){
    $('button[name="send"]').on('click',function(){
        $(this).attr('disabled',true);
        $('.loader_wrap')[0].classList.remove('hidden');
        $('.loader')[0].classList.remove('hidden');
        $('#application_form').submit();
    });
});
</script>
