<style>
h2 {
    font-size:18px;
    font-weight:bold;
    margin-bottom:20px;
}
h3 {
    font-size:16px;
    font-weight:bold;
    margin-bottom:20px;
}
h4 {
    font-size:14px;
    font-weight:bold;
    margin-bottom:10px;
}
ul {
    margin-bottom:30px;
    margin-left:10px;
}
.dot li {
    list-style-type: disc;
    margin-left:30px;
}
.num li {
    list-style-type: decimal;
    margin-left:30px;
}
p {
    margin-bottom:10px;
    margin-left:10px;
    font-weight:bold;
};
</style>
<div class="form_wrap">
    <div class="form_content">
        <h2>メールが受信できない場合</h2>
        
        <p>※こちらは電子契約サービスのクラウドサインからのメールの受信についての説明です。</p>
        
        
        <p>メールが受信できない要因は主に3つ考えられます。</p>
        
        <ul class="num">
            <li>送信されたメールアドレスが間違っている</li>
            <li>メールの設定により受信が拒否されている</li>
            <li>なんらかの不具合によりサーバーでメールが止められている</li>
        </ul>
        
        <p>まず、送信されたメールアドレスが間違っていないか送信者に確認していただき、間違っていない場合はメールの受信設定を確認してください。</p>
        
        <p>キャリアによって複数の受信設定がありますので全て確認してみてください。
        
        <h3>docomoの場合</h3>
        <ul class="dot">
            <li><a href="https://www.nttdocomo.co.jp/info/spam_mail/spmode/domain/index.html">受信設定</a>　<?php echo $email; ?> の受信許可設定を行なってください。</li>
            <li><a href="https://www.nttdocomo.co.jp/info/spam_mail/spmode/easy_setup/index.html">かんたん設定</a>　かんたん設定がされていないか確認してください。</li>
            <li><a href="https://www.nttdocomo.co.jp/info/spam_mail/spmode/url/">特定URL付メール拒否設定</a>　「拒否しない」となっているか確認してください。</li>
            <li><a href="https://www.nttdocomo.co.jp/info/spam_mail/spmode/mail_setup/">パソコンなどのメール受信設定</a>　「受信する」となっているか確認してください。</li>
            <li><a href="https://www.nttdocomo.co.jp/info/spam_mail/spmode/mail_setup/">なりすましメールの拒否設定</a>　「拒否しない」となっているか確認してください。</li>
        </ul>
           
           
        <h3>auの場合</h3>
        <p>迷惑メール設定の一括解除は<a href="https://www.au.com/support/faq/view.k20000002304/">こちら</a></p>
        
        <ul class="dot">
            <li><a href="https://www.au.com/support/service/mobile/trouble/mail/email/filter/detail/domain/">受信設定</a>　<?php echo $email; ?> の受信許可設定を行なってください。</li>
            <li><a href="https://www.au.com/support/faq/view.k1391721617/">オススメ設定</a>　「設定しない」となっているか確認してください。</li>
            <li><a href="https://www.au.com/support/service/mobile/trouble/mail/email/filter/detail/antiurl/">URLリンク／HTMLメール規制設定</a>　どちらも「規制しない」となっているか確認してください。</li>
            <li><a href="https://www.au.com/support/service/mobile/trouble/mail/email/filter/detail/forgery/">なりすましメールの拒否設定</a>　「規制しない」となっているか確認してください。</li>
            <li><a href="https://www.au.com/support/faq/view.k13121323727/">アドレス帳受信設定</a>　「設定しない」となっているか確認してください。</li>
        </ul>
        
        
        <h3>SoftBankの場合</h3>
        <ul class="dot">
            <li><a href="https://www.softbank.jp/mobile/support/mail/antispam/email-i/spammailfilter/">迷惑メールフィルタ設定</a>　「利用しない」となっているか確認してください。</li>
            <li><a href="https://www.softbank.jp/mobile/support/mail/antispam/email-i/white/">受信設定</a>　<?php echo $email; ?>の受信許可設定を行なってください。</li>
            <li><a href="https://www.softbank.jp/mobile/support/mail/antispam/mms/antiurl/">URLリンク付きメール拒否設定</a>　「利用しない」となっているか確認してください。</li>
            <li><a href="https://www.softbank.jp/mobile/support/mail/antispam/mms/whiteblack/">ケータイ／PHSからのみ許可設定</a>　「利用しない」となっているか確認してください。</li>
        </ul>
        
        <p>ご利用のキャリアのメールソフトによっては、メールが迷惑メールへ分類されている可能性がありますので、 <?php echo $email; ?>   からメールが届いていないか検索してみてください。</p>
        
        <p>それでもメールが見つからない場合は受信拒否設定がされている可能性があります。以下の手順にて迷惑メールフォルダおよび設定をご確認ください。</p>
        
        <h3>Yahoo!メールの場合</h3>
        <h4>【ドメインの受信設定】</h4>
        
        <ul class="num">
            <li>Yahoo!メールにログインし、画面の［設定・利用規約］→［メール設定］をクリック</li>
            <li>［フィルターと受信通知］をクリック</li>
            <li>Fromへ「gmail.com」を含む条件を設定</li>
            <li>［保存］ボタンをクリック</li>
        </ul>

        <h4>【迷惑メールフォルダの確認と解除方法】</h4>
        
        <ul class="num">
            <li>［迷惑メール］フォルダをクリック</li>
            <li>一覧からクラウドサインのメールを確認</li>
            <li>メールが確認できた場合、メールにチェックしてタブの下にある［迷惑メールでない］ボタンをクリック</li>
            <li>メールは［受信箱］フォルダに移動されているか確認</li>
        </ul>
        
        <h3>Gmailの場合</h3>
        <h4>【ドメインの受信設定】</h4>
        
        <ul class="num">
            <li>Gmailにログインし、画面の歯車のアイコンから［設定］をクリック</li>
            <li>次に［フィルタとブロック中のアドレス］→［新しいフィルタを作成］をクリック</li>
            <li>Fromへ「gmail.com」と入力して［この検索条件でフィルタを作成］をクリック</li>
            <li>［迷惑メールにしない］にチェックを入れて［フィルタを作成］をクリック</li>
        </ul>

        <h4>［迷惑メールフォルダの確認と解除方法］</h4>
        <ul class="num">
            <li>Gmailにログインし［迷惑メール］をクリック</li>
            <li>迷惑メールフォルダからクラウドサインのメールをクリックして開く、または、チェックボックスにチェック</li>
            <li>ページ上部にある「迷惑メールではない」をクリック</li>
        </ul>
        
        <h3>Outlook.comの場合</h3>
        <h4>【迷惑メールフォルダの確認と解除方法】</h4>
        
        <ul class="num">
            <li>［迷惑メール］フォルダをクリック</li>
            <li>一覧からクラウドサインのメールを確認</li>
            <li>メールが確認できた場合、リストから該当のメール上で右クリックし［迷惑メールではないメールとしてマーク］をクリック</li>
            <li>［受信トレイ］に移動</li>
        </ul>
        
        <h3>Microsoft Outlook の場合</h3>
        <h4>【ドメインの受信設定】</h4>
        
        <ul class="num">
            <li>Microsoft Outlook を起動し、［ホーム］→［削除］→［迷惑メール］→［迷惑メールオプション］をクリック</li>
            <li>［信頼できる差出人のリスト］タブをクリック</li>
            <li>［追加］ボタンをクリックし、「gmail.com」と入力して設定</li>
        </ul>

        <h4>【迷惑メールフォルダの確認と解除方法】</h4>
        <ul class="num">
            <li>Microsoft Outlook を起動し、［迷惑メール］フォルダをクリック</li>
            <li>リストからクラウドサインのメールを確認</li>
            <li>メールが確認できた場合、受信一覧からクリックし、［ホーム］→［削除］→［迷惑メール］→［迷惑メールではない］をクリック</li>
            <li>［迷惑メールではないメールとしてマーク］メッセージで「～からの電子メールを常に信頼する」チェックボックスにチェックして「OK」ボタンをクリック</li>
            <li>［受信トレイ］に移動</li>
        </ul>
        
        <h3>iCloud の場合</h3>
        <h4>【迷惑メールフォルダの確認と解除方法】</h4>
        
        <ul class="num">
            <li>iCloudメールを開き、［迷惑メール］をクリック</li>
            <li>クラウドサインのメールを確認</li>
            <li>メールが確認できた場合、メールを選択して画面上部にある［迷惑メールではない］ボタンをクリック</li>
            <li>クラウドサインメールが［受信トレイ］に移動したか確認</li>
        </ul>
    </div>
</div>
