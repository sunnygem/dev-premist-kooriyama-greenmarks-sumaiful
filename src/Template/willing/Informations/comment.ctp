<?php $this->assign('body_id','comment') ?>
<?php $this->assign('title',__('コメント')) ?>

<section class="comments">
    <ul class="list list1">
        <?php if( $comment->isEmpty() ): ?>
            <p><?php echo __('このインフォーメーションには、まだコメントがありません。') ?></p>
        <?php else: ?>
            <?php foreach( $comment as $val ): ?>
            <li>
                <div class="inner clearfix">
                <div class="photo_wrap">
                    <div class="photo" style="background-image:url(<?php echo noh( $this->Front->set_user_image( $val->user ) );?>);"></div>
                </div>

                <div class="line">
                    <div class="name <?php if( $val->user->front_user->leave_flg === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $val->user->front_user->nickname ); ?></div>
                    <div class="title"><?php echo nl2br( h( $val->comment ) ); ?></div>
                </div>
                <div class="info">
                <div class="date"><?php $this->Front->set_activity_time( $val->created ); ?></div>
                    <div class="like">
                        <span class="count" data-count="<?php echo noh( count( $val->information_comment_likes ) ); ?>">
                        <?php echo noh( __('「いいね！」{0}件',count( $val->information_comment_likes ) ) ) ?></span>
                    </div>
                    <a class="bt_reply" href="javascript:void(0);" data-comment-id="<?php echo $val->id; ?>"><?php echo __('返信する') ?></a></div>
                    <a href="javascript:void(0);"
                    class="bt_like fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $val->information_comment_likes ) ) echo 'active'; ?>"
                    data-action="comment_like"
                    data-controller="informations"
                    data-user-id="<?php echo $login_user['id']; ?>"
                    data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                    data-id="<?php echo $information_id; ?>"
                    data-comment-id="<?php echo $val->id; ?>"
                    ><?php echo __('いいね') ?></a>
                </div>
                <ul class="list list2">
                    <?php foreach( $val->child_information_comments as $val2 ): ?>
                        <li>
                            <div class="inner clearfix">
                                <div class="photo_wrap">
                                    <div class="photo" style="background-image:url(<?php echo noh( $this->Front->set_user_image( $val2->user ) );?>);"></div>
                                </div>
                                <div class="line">
                                    <div class="name <?php if( $val2->user->front_user->leave_flg === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $val2->user->front_user->nickname ); ?></div>
                                    <?php /* <div class="target">@<?php echo h( $val->user->front_user->nickname ); ?></div> */ ?>
                                </div>
                                <div class="line line2">
                                    <div class="title"><?php echo nl2br( h( $val2->comment ) ); ?></div>
                                </div>
                                <div class="info">
                                    <div class="date"><?php $this->Front->set_activity_time( $val2->created ); ?></div>
                                    <div class="like">
                                        <span class="count" data-count="<?php echo noh( count( $val2->information_comment_likes ) ); ?>">
                                        <?php echo noh( __('「いいね！」{0}件',count( $val2->information_comment_likes ) ) ) ?></span>
                                    </div>
                                    <?php // <a class="bt_reply" href="#">返信する</a> ?>
                                </div>
                                <a href="javascript:void(0);"
                                class="bt_like fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $val2->information_comment_likes ) ) echo 'active'; ?>"
                                data-action="comment_like"
                                data-controller="informations"
                                data-user-id="<?php echo $login_user['id']; ?>"
                                data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                                data-id="<?php echo $information_id; ?>"
                                data-comment-id="<?php echo $val2->id; ?>"
                                ><?php echo __('いいね') ?></a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                
                </ul>
            </li>
            <?php endforeach; ?>
        <?php endif; ?>

        <li class="post">
            <?php echo $this->Form->create(null, ['name' => 'form_comment']); ?>
            <?php echo $this->Form->hidden( 'information_id',    ['value' => $information_id ]); ?>
            <?php echo $this->Form->hidden( 'parent_comment_id'); ?>
            <div class="inner clearfix">
            <div class="photo" style="background-image: url(<?php echo noh( $this->Front->set_user_image( $login_user ) ); ?>);"></div>
                <div class="input_box">
                    <?php
                        $disabled = ( isset( $this->request->data['comment'] ) && mb_strlen( $this->request->data['comment'] ) > 0  ) ? false : true;
                    ?>
                    <?php echo $this->Form->textarea( 'comment', ['placeholder' => __( '{0}としてコメントを追加...', $login_user['front_user']['nickname'] ) ]  ); ?>
                    <?php echo $this->Form->button( __('送信'), ['name' => 'complete', 'disabled' => $disabled ] ); ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </li>

    </ul>
</section>
