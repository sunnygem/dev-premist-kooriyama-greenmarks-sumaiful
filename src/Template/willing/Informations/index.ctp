<?php $this->assign('body_id','information') ?>
<?php $this->assign('title',__('インフォメーション')) ?>
<section class="posts information">
    <ul class="tab">
        <li class="type_general <?php if( $this->request->getQuery('type') === 'general' )        echo noh( 'active' );  ?>"><a href= "<?php echo $this->Url->build(['?' => ['type' => 'general']]); ?>">お知らせ</a></li>
        <li class="type_event   <?php if( $this->request->getQuery('type') === 'event' )          echo noh( 'active' );  ?>"><a href= "<?php echo $this->Url->build(['?' => ['type' => 'event' ]]); ?>">イベント</a></li>
        <li class="type_chat    <?php if( $this->request->getQuery('type') === 'line_open_chat' ) echo noh( 'active' );  ?>"><a href= "<?php echo $this->Url->build(['?' => ['type' => 'line_open_chat']]); ?>">チャット</a></li>
    </ul>

    <?php foreach( $data as $val ): ?>
        <article class="<?php echo noh( $val->type ); ?>">
            <a href="<?php echo noh( $this->Url->build(['controller' => 'Informations', 'action' => 'detail', $val->id ]) ); ?>">
                <div class="inner">
                    <div class="article_header clearfix">
                        <?php if( $val->all_building_flg === 0 && count( $val->information_buildings ) === 1 ): ?>
                            <div class="icon">
                                <?php echo noh( $this->Html->image( '/' . $val->information_buildings[0]->building->icon_path, ['alt' => $val->information_buildings[0]->building->name]) ); ?>
                            </div>
                        <?php endif; ?>
                        <div class="author">
                            <div class="photo_wrap">
                                <div class="photo" style="background-image:url(/img/willing/icons/icon_info_willing.png);"></div>
                            </div>
                            <div class="name"><?php echo h( mb_strimwidth( $val['title'], 0, 20, '…' )  ); ?></div>
                            <div class="date"><?php echo noh( $val->release_date->format( __( 'Y/m/d' ) ) ); ?></div>
                        </div>
                        <?php if( $val->rel_information_file !== null ): ?><div class="attachment">添付ファイル</div><?php endif; ?>
                    </div>
                    <div class="content"><?php echo h( mb_strimwidth( $val['content'], 0, 150, '…' )  ); ?></div>
                </div>
            </a>
        </article>
    <?php endforeach; ?>
    <?php /*
    <article class="cat_tweet">
        <a href="/informations/detail">
                <div class="inner">
                    <div class="article_header clearfix">
                        <div class="author">
                        <div class="photo"><img src="/img/willing/img_user.png" alt=""></div>
                        <div class="name">Yuri Yoshida</div>
                        <div class="date">Thursday at 2:04 PM</div>
                    </div>
                </div>
                <div class="content">
                ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
                </div>
            </div>
        </a>
    </article>
    <article class="cat_invite">
        <a href="/informations/detail">
            <div class="inner">
                <div class="article_header clearfix">
                    <div class="author">
                        <div class="photo"><img src="/img/willing/img_user.png" alt=""></div>
                        <div class="name">Yuri Yoshida</div>
                        <div class="date">Thursday at 2:04 PM</div>
                    </div>
                    <div class="attachment">添付ファイル</div>
                </div>
                <div class="content">
                ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
                </div>
            </div>
        </a>
    </article>
    <article class="cat_ask">
        <a href="/informations/detail">
            <div class="inner">
                <div class="article_header clearfix">
                    <div class="author">
                        <div class="photo"><img src="/img/willing/img_user.png" alt=""></div>
                        <div class="name">Yuri Yoshida</div>
                        <div class="date">Thursday at 2:04 PM</div>
                    </div>
                </div>
                <div class="content">
                ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
                </div>
            </div>
        </a>
    </article>
    <article class="cat_event">
        <a href="/informations/detail">
            <div class="inner">
                <div class="article_header clearfix">
                    <div class="author">
                        <div class="photo"><img src="/img/willing/img_user.png" alt=""></div>
                        <div class="name">Yuri Yoshida</div>
                        <div class="date">Thursday at 2:04 PM</div>
                    </div>
                    <div class="attachment">添付ファイル</div>
                </div>
                <div class="content">
                ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
                </div>
            </div>
        </a>
    </article>
    */ ?>
</section>
<div class="pager">
    <ul>
        <?php echo noh( $this->Paginator->prev( '' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '' ) ); ?>
    </ul>
</div>
