<?php $this->assign('body_id','information_detail') ?>
<?php $this->assign('title', mb_strimwidth( $data['title'], 0, 40, '…') ) ?>
<script>
    var json = {};
</script>
<section class="post">
    <div class="bt_box">
        <a class="bt_back" href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>">Information</a>
    </div>
    <?php $category = ( $data->event_flg === 1 ) ? 'cat_event' : 'cat_info'; ?>

    <article class="<?php echo noh( $category ); ?> <?php echo noh( $data->type ); ?>">
        <div class="inner">
            <div class="article_header clearfix">
                <?php if( $data->all_building_flg === 0 && count( $data->information_buildings ) === 1 ): ?>
                    <div class="icon">
                        <?php echo noh( $this->Html->image( '/' . $data->information_buildings[0]->building->icon_path, ['alt' => $data->information_buildings[0]->building->name]) ); ?>
                    </div>

                <?php endif; ?>

                <a class="author">
                    <div class="photo_wrap">
                        <div class="photo" style="background-image:url(/img/willing/icons/icon_info_willing.png);"></div>
                    </div>
                    <div class="name"><?php echo h( $data['title'] ); ?></div>
                    <div class="date"><?php echo noh( $data->release_date->format( __('Y/m/d') ) ); ?></div>
                </a>
                <?php /*
                <span
                class="bt_other"
                data-target-id="<?php echo $data->id; ?>"
                data-login-user-id="<?php echo $login_user['id']; ?>"
                ><?php echo __('メニュー'); ?></span>
                */ ?>
            </div>
            <div class="content">
                <?php echo nl2br( $this->Front->filterUrlStr( h( $data['content'] ) ) ); ?>
                <?php if( $data->rel_information_file !== null ): ?>
                    <?php if ( $data->rel_information_file->file->mime_type === 'application/pdf' ): ?>
                        <p><a style="text-decoration:underline;font-size:86%;" href="<?php echo noh( DS . $data->rel_information_file->file->file_path ); ?>" target="_blank">PDFを開く</a></p>
                        <embed src="<?php echo noh( DS . $data->rel_information_file->file->file_path ); ?>" type="application/pdf" style="width:100%;height:500px;">
                        <p><a style="text-decoration:underline;font-size:86%;" href="<?php echo noh( DS . $data->rel_information_file->file->file_path ); ?>" target="_blank">PDFを開く</a></p>
                    <?php else: ?>
                        <?php echo noh( $this->Html->image( DS . $data->rel_information_file->file->file_path, ['alt' => ''] ) ); ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <?php if( $login_user['front_user']['common_user_flg'] === 0 ): ?>
                <div class="article_footer">
                    <div class="bt_box clearfix">
                        <?php if( $data->event_flg === 1 ): ?>
                            <a href="javascript: void(0);"
                            class="bt_join join fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $data->information_joins ) ) echo 'active'; ?>"
                            data-action="join"
                            data-controller="informations"
                            data-user-id="<?php echo $login_user['id']; ?>"
                            data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                            data-id="<?php echo $data->id; ?>"
                            ><?php echo __('参加') ?></a>
                        <?php else: ?>
                            <a class="bt_favorite fnSetActivity <?php if( $this->Front->check_my_activity( $login_user['id'], $data->information_likes ) ) echo 'active'; ?>"
                            href="javascript: void(0);"
                            data-action="like"
                            data-controller="informations"
                            data-user-id="<?php echo $login_user['id']; ?>"
                            data-front-user-id="<?php echo $login_user['front_user']['id']; ?>"
                            data-id="<?php echo $data->id; ?>"
                            ><?php echo __('いいね') ?></a>
                        <?php endif; ?>

                        <a class="bt_comment" href="<?php echo noh( $this->Url->build(['action' => 'comment', $data->id]) ); ?>"><?php echo __('コメント') ?></a>
                        <?php if( count( $data->information_comments ) > 0 ): ?>
                            <span class="comment_count"><?php echo __( '{0}コメント', count( $data->information_comments ) ); ?></span>
                        <?php endif; ?>

                        <?php if( $data->event_flg === 1 ): ?>
                            <div class="favorite event" data-target-id="<?php echo $data->id; ?>">
                                <span class="count" data-count="<?Php echo count( $data->information_joins ); ?>">
                                <?Php echo __x('参加の数', '{0}人',count( $data->information_joins ) ) ?></span>
                                <script>json[<?php echo $data->id; ?>] = <?php echo json_encode($data->information_joins, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ); ?>;</script>
                                <?php /*
                                <?php if( count( $data->information_joins ) === 0 ): ?>
                                    <?php echo __('0人') ?>
                                <?php elseif( count( $data->information_joins ) > 0 ): ?>
                                    <?php echo noh( $data->information_joins[0]->user->front_user->nickname ); ?><?php if( count( $data->information_joins ) > 1 ): ?>、<?php echo __('他') ?><?php echo noh( count( $data->information_joins ) - 1 ); ?><?php echo __('人') ?><?php endif; ?>
                                <?php endif; ?>
                                */ ?>
                            </div>
                        <?php else: ?>
                            <div class="favorite" data-target-id="<?php echo $data->id; ?>">
                                <span class="count" data-count="<?Php echo count( $data->information_likes ); ?>">
                                <?Php echo __x('いいねの数', '{0}人',count( $data->information_likes ) ); ?></span>
                                <script>json[<?php echo $data->id; ?>] = <?php echo json_encode($data->information_likes, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ); ?>;</script>
                            <?php /*
                            <?php if( count( $data->information_likes ) === 0 ): ?>
                            <?php elseif( count( $data->information_likes ) > 0 ): ?>
                                <?php echo noh( $data->information_likes[0]->user->front_user->nickname ); ?><?php if( count( $data->information_likes ) > 1 ): ?>、<?php echo __('他') ?><?php echo noh( count( $data->information_likes ) - 1 ); ?><?php echo __('人') ?><?php endif; ?>
                            <?php endif; ?>
                            */ ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="comment">
                        <?php if( count( $data->information_comments ) > 0 ): ?>
                            <?php $comments = array_reverse( $data->information_comments ); ?>
                            <div class="comment_preview">
                                <p>
                                    <span class="name"><?php if ( isset( $comments[0]->user ) ) echo h( $comments[0]->user->front_user->nickname ); ?></span><?php echo mb_strimwidth( h( $comments[0]->comment ), 0, 60, '...' ); ?>
                                    <a class="more" href="<?php echo $this->Url->build(['action' => 'comment', $data->id]); ?>"><?php echo __('コメントをすべて表示'); ?></a>
                                </p>
                            </div>
                        <?php endif; ?>
                        <div class="photo_wrap">
                            <div class="photo" style="background-image:url(<?php echo DS . $login_user['front_user']['thumbnail_path']; ?>);"></div>
                        </div>
                        <input type="text" placeholder="Comment..." readonly data-controller="informations" data-id="<?php echo $data->id; ?>">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </article>
</section>
