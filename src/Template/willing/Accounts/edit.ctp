<?php $this->assign('body_id', 'profile') ?>
<div class="content_header clearfix">
    <div class="title"><?php echo __('プロフィールを編集') ?></div>
    <?php /* <a class="bt_back" href="#">キャンセル</a> */ ?>
</div><!--/.content_header -->
<div class="form">
    <?php echo noh( $this->Form->create( $user, [ 'type' => 'file']) ); ?>
        <?php echo noh( $this->Form->hidden('id') ); ?>
        <?php echo noh( $this->Form->hidden('front_user.id') ); ?>
        <div class="photo">
            <label class="file">
                <div class="photo_wrap">
                    <div class="user_photo preview_target" style="background-image:url(<?php echo noh( $this->Front->set_user_image( $user ) ); ?>);"></div>
                    <span class="plus"><?php echo noh( $this->Form->file('files.front_users', []) ); ?></span>
                </div>
            </label>
            <?php if( $user->front_user->thumbnail_path ): ?>
                <?php echo noh( $this->Form->hidden('front_user.thumbnail_path' ) ); ?>
            <?php endif; ?>
        </div>
        <table>
            <tr>
                <th><?php echo __('ユーザーネーム') ?><span class="required">【必須】</span></th>
                <td>
                    <?php echo noh( $this->Form->text( 'front_user.nickname', ['placeholder'=>__('TARO')] ) ); ?>
                    <?php if( isset( $error['nickname'] ) ) echo noh( '<span class="error">' . $error['nickname'] . '</span>' ); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('自己紹介') ?>
                <span class="required">【必須】</span></th>
                <td>
                    <?php echo noh( $this->Form->textarea('front_user.biography', ['placeholder' => __('〇〇階に住んでる××です！高校時代はサッカー部で++チームが好きです。')] ) ); ?>
                    <?php if( isset( $error['biography'] ) ) echo noh( '<span class="error">' . $error['biography'] . '</span>' ); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('学校名(学部)') ?>
                <span class="required">【必須】</span></th>
                <td>
                    <?php echo noh( $this->Form->text('front_user.university_undergraduate', ['placeholder' => __( '〇〇大学××学部')] ) ); ?>
                    <?php if( isset( $error['university_undergraduate'] ) ) echo noh( '<span class="error">' . $error['university_undergraduate'] . '</span>' ); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __('出身地') ?>
                <span class="required">【必須】</span></th>
                <td>
                    <?php echo noh( $this->Form->text('front_user.birth_place', ['placeholder' => __('東京')] ) ); ?>
                    <?php if( isset( $error['birth_place'] ) ) echo noh( '<span class="error">' . $error['birth_place'] . '</span>' ); ?>
                </td>
            </tr>

            <tr>
                <th><?php echo __('趣味') ?>
                <span class="required">【必須】</span></th>
                <td>
                    <?php echo noh( $this->Form->text('front_user.hobby', ['placeholder' => __('サッカー、旅行')] ) ); ?>
                    <?php if( isset( $error['hobby'] ) ) echo noh( '<span class="error">' . $error['hobby'] . '</span>' ); ?>
                </td>
            </tr>
        </table>
        <?php echo noh( $this->Form->button( __('完了'), ['name' => 'complete', 'class' => 'submit']) ); ?>
    <?php echo noh( $this->Form->end() ); ?>
</div><!--/.form-->
