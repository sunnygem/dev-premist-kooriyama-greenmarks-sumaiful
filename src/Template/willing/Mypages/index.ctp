<?php $this->assign('body_id', 'mypage') ?>
<?php $this->assign('title', 'mypage') ?>
<div class="userdata clearfix">
<div class="photo_wrap">
    <div class="photo" style="background-image:url(<?php echo noh( $this->Front->set_user_image( $mypage_user ) ); ?>);"></div>
</div>
<div class="name <?php if( $mypage_user['front_user']['leave_flg'] === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $mypage_user['front_user']['nickname'] ); ?></div>
    <div class="profile">
        <div class="">
        <?php if( $mypage_user['id'] !== $login_user['id'] ): ?>
            <p class="building">
                <?php if( $mypage_user['front_user']['building']['icon_path'] ): ?>
                    <?php echo noh( '<img src="/' . $mypage_user['front_user']['building']['icon_path'] . '" alt="' . $mypage_user['front_user']['building']['name'] . '" class="icon">' ); ?>
                <?php else: ?>
                    <?php echo h( $mypage_user['front_user']['building']['name'] ); ?>
                <?php endif; ?>
            </p>
        <?php endif; ?>
        <?php if( $login_user['id'] !== $mypage_user['id'] ): ?>
            <a href="<?php echo noh( $this->Url->build(['controller' => 'Messages', 'action' => 'start_message', $login_user['id'], $login_user['front_user']['id'], $mypage_user['id'], $mypage_user['front_user']['id'] ] ) ); ?>" class="send_message"><span class="icon"></span><?php echo noh( __('メッセージを送る') ); ?></a>
        <?php endif; ?>
        </div>
        <?php if( $mypage_user['front_user']['university_undergraduate'] ): ?>
            <p class="university"><?php echo h( $mypage_user['front_user']['university_undergraduate'] ); ?></p>
        <?php elseif( $mypage_user['front_user']['university_name'] || $mypage_user['front_user']['undergraduate'] ): ?>
            <p class="university"><?php echo h( $mypage_user['front_user']['university_name'] . '　' . $mypage_user['front_user']['undergraduate'] ); ?></p>
        <?php endif; ?>
        <?php if( $mypage_user['front_user']['biography'] ): ?>
            <p><?php echo h( $mypage_user['front_user']['biography'] ); ?></p>
        <?php endif; ?>
        <?php if( $mypage_user['front_user']['birth_place'] ): ?>
            <p><?php echo __('出身地'); ?>：<?php echo h( $mypage_user['front_user']['birth_place'] ); ?></p>
        <?php endif; ?>
        <?php if( $mypage_user['front_user']['hobby'] ): ?>
            <p><?php echo __('趣味'); ?>：<?php echo h( $mypage_user['front_user']['hobby'] ); ?></p>
        <?php endif; ?>
    </div>
</div>
<section class="posts information">
    <?php foreach( $posts as $val ): ?>
    <?php
        $category = '';
        switch( $val->post_category_id )
        {
        case 1:
            $category = 'cat_tweet';
            break;
        case 2:
            $category = 'cat_invite';
            break;
        case 3:
            $category = 'cat_ask';
            break;
        case 4:
            $category = 'cat_event';
            break;
        }
    ?>

    <article class="<?php echo noh( $category ); ?>">
        <a href="<?php echo $this->Url->build([ 'controller' => 'Communities', 'action' => 'detail', $val->id] ); ?>">
            <div class="inner">
                <div class="article_header clearfix">
                    <div class="author">
                        <div class="date"><?php echo noh( $val->created->format( __('Y/m/d') ) . ' at ' . $val->created->format('g:i A') ); ?></div>
                    </div>
                    <?php if( count( $val->rel_post_files ) > 0 ): ?><div class="attachment">添付ファイル</div><?php endif; ?>
                </div>
                <div class="content"><?php echo nl2br( h( $val->content ) ); ?></div>
            </div>
        </a>
    </article>
    <?php endforeach; ?>

</section>
<div class="pager">
    <ul>
        <?php echo noh( $this->Paginator->prev( '' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '' ) ); ?>
    </ul>
</div>
