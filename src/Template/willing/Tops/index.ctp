<?php $this->assign('body_id', 'index') ?>
<?php $this->assign('title', 'top') ?>

<?php if( $login_user['front_user']['common_user_flg'] === 1 ): ?>
    <section class="common_user">
        <?php
            $note_path = ( $lang === 'ja' ) ? 'common_top_jp.png' : 'common_top_en.png';
            
            echo noh( $this->Html->image( $note_path ) );
        ?>
    </section>
<?php endif; ?>

<section class="information">
    <h2><a href="<?php echo noh( $this->Url->build(['controller' => 'Informations', 'action' => 'index']) ); ?>">Information</a></h2>
    <div class="inner">
        <ul>
            <?php foreach( $informations as $val ): ?>
            <li class="<?php echo noh( $val->type ); ?>">
                <a class="post" href="<?php echo noh( $this->Url->build(['controller' => 'Informations', 'action' => 'detail', $val->id ]) ); ?>">
                    <div class="inner">
                        <div class="date"><?php echo noh( $val->release_date->format( __('Y/m/d') ) ); ?></div>
                        <div class="title"><?php echo h( mb_strimwidth( $val['title'], 0, 20, '…' )  ); ?></div>
                        <div class="excerpt"><?php echo h( mb_strimwidth( $val['content'], 0, 50, '…' )  ); ?></div>
                    </div>
                </a>

                <div class="author list_footer clearfix">
                    <?php if( $val->all_building_flg === 0 && count( $val->information_buildings ) === 1 ): ?>
                        <div class="icon">
                            <?php echo noh( $this->Html->image( '/' . $val->information_buildings[0]->building->icon_path, ['alt' => $val->information_buildings[0]->building->name]) ); ?>
                        </div>
                    <?php else: ?>
                        <div class="photo_wrap">
                            <div class="photo" style="background-image:url(/img/willing/icons/icon_info_willing.png);"></div>
                        </div>
                        <div class="name">CREVIA WILL</div>
                    <?php endif; ?>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

<section class="album">
    <h2><a href="/albums">Album</a></h2>
    <div class="inner">
        <ul class="list1">
            <?php foreach( $image_sharings as $val ): ?>
            <li>
                <a class="post" href="<?php echo noh( $this->Url->build(['controller' => 'Albums', 'action' => 'detail', $val->id ]) ); ?>">
                    <div class="inner">
                        <div class="clearfix">
                            <div class="title"><?php echo noh( $val->title ); ?></div>
                            <div class="date"><?php echo noh( $val->release_date->format( __('Y/m/d') ) ); ?></div>
                        </div>
                        <ul class="list2 clearfix">
                            <?php $i = 0; ?>
                            <?php foreach( $val->rel_imagesharing_files as $val2 ): ?>
                                <li><div class="thumb_img" style="background-image:url(<?php echo  DS . $val2->file->file_path; ?>);"></div></li>
                                <?php $i++; ?>
                                <?php if( $i > 4 ) break; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

<?php if ( $start === true ): ?>
<div id="start">
    <div>
        <?php echo $this->Html->image('img_logo.png',[ 'alt' => 'willing' ]); ?>
    </div>
</div>
<?php endif; ?>
