<?php $this->assign('body_id','message') ?>
<?php $this->assign('title',__('メッセージ') ) ?>

<style>
[v-cloak] {
  display: none;
  }
</style>
<section class="posts">
    <ul class="list" id="member_list" v-cloak>
        <li v-for="val in result">
            <a class="clearfix" :href="'/messages/detail/' + val.chat_group_id + '/' + val.thread_id + '/' + val.id + '/' + val.front_user.id">
                <div class="photo_wrap">
                    <div class="photo" :style='{backgroundImage: "url(" + val.front_user.thumbnail_path + ")", }'></div>
                </div>
                <div class="line">
                    <div class="name" v-bind:class="{leaved:val.front_user.leave_flg}">{{ val.front_user.nickname }}</div>
                    <div class="unread" v-if="val.unread_flg">●</div>
                </div>
            </a>
        </li>
    </ul>
</section>
<?php echo $this->Html->script( 'jquery' ); ?>
<?php echo $this->Html->script( 'message' ); ?>
