<?php $this->assign('body_id','message_detail') ?>
<?php $this->assign('title',__('メッセージ') ) ?>
<?php

    echo noh( $this->Form->hidden( 'domain',                      [ 'value' => APP_DOMAIN,                   'id' => 'domain' ] ) );
    echo noh( $this->Form->hidden( 'wss',                         [ 'value' => WSS,                          'id' => 'wss' ] ) );
    echo noh( $this->Form->hidden( 'contractant_id',              [ 'value' => $contractant_id,              'id' => 'contractant_id' ] ) );
    echo noh( $this->Form->hidden( 'chat_group_id',               [ 'value' => $chat_group_id,               'id' => 'chat_group_id' ] ) );
    echo noh( $this->Form->hidden( 'contractant_service_menu_id', [ 'value' => $contractant_service_menu_id, 'id' => 'contractant_service_menu_id' ] ) );
    echo noh( $this->Form->hidden( 'thread_id',                   [ 'value' => $thread_id,                   'id' => 'thread_id' ] ) );
    echo noh( $this->Form->hidden( 'user_id',                     [ 'value' => $user_id,                     'id' => 'user_id' ] ) );
    echo noh( $this->Form->hidden( 'front_user_id',               [ 'value' => $front_user_id,               'id' => 'front_user_id' ] ) );
    echo noh( $this->Form->hidden( 'number',                      [ 'value' => 1,                            'id' => 'number' ] ) ); // 初期は1ページ目
    echo noh( $this->Form->hidden( 'limit',                       [ 'value' => CHAT_MESSAGE_LIMIT,           'id' => 'limit' ] ) );
?>
<div id="chat_detail_head" class="clearfix">
    <a class="bt_back" href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>"></a>
    <a class="profile clearfix " href="<?php echo noh( $this->Url->build(['controller' => 'Mypages', 'action' => 'index', $front_user_data->user_id ]) ); ?>">
        <div class="photo_wrap">
            <div class="photo" style="background-image:url(/<?php echo noh( $front_user_data->thumbnail_path ); ?>);"></div>
        </div>
        <div class="line">
            <div class="name <?php if( $front_user_data->leave_flg === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $front_user_data->nickname ); ?></div>
        </div>
    </a>
</div>
<section class="posts" id="chat_posts">
<div class="chat_wrap" id="chat_wrap">
    <?php if ( $data !== null && count( $data ) > 0 ): ?>
        <?php foreach( $data as $key => $val ): ?>
            <div class="time"><?php echo h( $val->created->format( __( 'n月j日H:i') ) ); ?></div>
            <div class="<?php echo ( (int)$val->front_user_id !== (int)$front_user_id ) ? 'left' : 'right'; ?>">
                <?php if ( (int)$val->front_user_id !== (int)$front_user_id ): ?>
                    <div class="photo_wrap">
                        <div class="photo" style="background-image:url(/<?php echo $val->user->front_user->thumbnail_path; ?>)"></div>
                    </div>
                <?php endif; ?>
                <?php if ( strlen( $val->file_path ) > 0 ): ?>
                    <p class="file">
                        <img src="<?php echo h( $val->file_path ); ?>">
                    </p>
                <?php else: ?>
                    <p><?php echo nl2br( $this->Front->filterUrlStr( h( $val->message ) ) ); ?></p>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <chat-list v-for="list in lists"
        v-bind:own="list.own"
        v-bind:img="list.img"
        v-bind:photo="list.photo"
        v-bind:message="list.message"
        v-bind:date="list.date">
    </chat-list>
</div>
</section>
<div class="box_wrap">
    <div class="box_inner clearfix">
        <div class="input_box">
            <textarea id="message" placeholder="<?php echo __('メッセージを入力') ?>"></textarea>
        </div>
        <input type="file" name="files[]" id="files" multiple>
        <label for="files" class="bt_camera"></label>
        <p class="notes"><?php echo __('送信はCtrl+Enter') ?></p>
    </div>
</div>

<?php echo $this->Html->script( 'jquery' ); ?>
<?php echo $this->Html->script( 'chat' ); ?>
<script>
document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>
