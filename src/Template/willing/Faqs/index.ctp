<?php $this->assign('title', __('Q&A') ) ?>
<div class="form_logo">
    <?php echo noh( $this->Html->image( 'img_logo.png' ) ); ?>
</div>
<div class="form_wrap application">
    <div class="form_content" style="lihe-heiht: 1.2rem;">
    <h1 class="title_faq">Q&A</h1>
        <?php foreach ( $data as $key => $val ): ?>
        <h2 style="margin-bottom: 2rem;" class="faq_category_name"><?php echo ($val['category_name']); ?></h2>
        
            <?php foreach ( $val['contents'] as $key1  => $val1 ): ?>
            <h3 class="question"><b><?php echo ($val['contents'][0]['question']); ?></b></h3>
            <ul class="answers">
                <?php foreach ( $val1['answers'] as $key2 => $val2 ): ?>
                <?php if(isset( $val2['icon_url_path'] )): ?>
                <li><?php echo noh( $this->Html->image( DS . $val2['icon_url_path'] , ['style' => 'width:30px; height:30px;'])); ?></li>
                <?php endif; ?>
                <?php if(isset( $val2['answer'] )): ?>
                <li class=answer""><?php echo noh( $val2['answer'] ); ?></li>
                <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
</div>

