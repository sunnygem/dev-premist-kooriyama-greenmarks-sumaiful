<?php echo noh( $this->element( 'Admin/config_tab' ) ); ?>
<section class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
        <div class="right"> 
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table style="table-layout: fixed;">
            <tr>
                <th>利用規約</th>
                <td>
                    <div style="white-space:pre-wrap"><?php echo noh( ( $data->content ) ? $data->content : '登録がありません' ); ?></div>
                </td>
            </tr>

            <tr>
                <th>利用規約(英語)</th>
                <td>
                    <div style="white-space:pre-wrap"><?php echo noh( ( $data->content_en ) ? $data->content_en : '登録がありません' ); ?></div>
                </td>
            </tr>
            <tr>
                <th>利用規約URL</th>
                <td>
                    <div class=""><?php echo noh( ( $data->url ) ? $data->url : '--' ); ?></div>
                </td>
            </tr>
            <tr>
                <th>利用規約URL(英語)</th>
                <td>
                    <?php echo noh( ( $data->url_en ) ? $data->url_en : '--' ); ?>
                </td>
            </tr>
        </table>
    </div>
</section>
