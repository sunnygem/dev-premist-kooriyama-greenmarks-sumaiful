<div class="content">
    <div class="clearfix contents_title">
        <h2 class="left">
            コンテンツ<?php if( isset( $contents->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $contents['display_flg'] === '1' ) ? '表示する' : '表示しない' ); ?></td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td>
                    <?php echo h( $contents['title'] ); ?>
                    <?php if( isset( $error['title'] ) ) echo noh( '<p class="error">' . $error['title'] . '</p>' ); ?>
                </td>
            </tr>
            <tr>
                <th>サムネイル</th>
                <td>
                    <?php
                        if( isset( $contents['uploaded']['image']['file_path'] ) )
                        {
                            echo  noh( '<img src="' . DS . $contents['uploaded']['image']['file_path'] . '">' );
                        }
                        else
                        {
                            echo '--';
                        }
                    ?>
                    <?php if( isset( $error['image_file'] ) ) echo noh( '<p class="error">' . $error['release_file'] . '</p>' ); ?>
                </td>
            </tr>

            <tr>
                <th>公開日</th>
                <td>
                    <?php echo h( $contents['release_date'] ); ?>
                    <?php if( isset( $error['release_date'] ) ) echo noh( '<p class="error">' . $error['release_date'] . '</p>' ); ?>
                </td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td>
                    <?php echo h( $contents['close_date'] ); ?>
                    <?php if( isset( $error['close_date'] ) ) echo noh( '<p class="error">' . $error['close_date'] . '</p>' ); ?>
                </td>
            </tr>

            <tr>
                <th>本文</th>
                <td>
                    <?php echo noh( ( strlen( $contents['content'] ) > 0 ) ?  $contents['content'] : '-- ' ); ?>
                    <?php if( isset( $error['content'] ) ) echo noh( '<p class="error">' . $error['content'] . '</p>' ); ?>
                </td>
            </tr>


        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
