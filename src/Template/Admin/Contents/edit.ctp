<div class="content">
    <div class="clearfix contents_title">
        <h2 class="left">
            コンテンツ<?php if( isset( $contents->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $contents, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $contents->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $contents->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'タイトルを入力' ] ) ); ?>
                        <?php if( isset( $error['title'] ) ) echo '<p class="error">' . $error['title'] . '</p>'; ?>
                    </td>
                </tr>

                <tr>
                    <th>サムネイル</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $contents['uploaded']['image'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $contents['uploaded']['image']['file_path'] . '">' ); ?>
                               <?php if( isset( $contents['uploaded']['id'] ) ) echo $this->Form->hidden( 'uploaded[image][id]', [ 'value' => $contents['uploaded']['image']['id'] ] ); ?>
                               <?php echo $this->Form->hidden( 'uploaded[image][file_path]', [ 'value' => $contents['uploaded']['image']['file_path'] ] ); //確認画面用 ?>
                               <?php echo $this->Form->hidden( 'uploaded[image][name]',      [ 'value' => $contents['uploaded']['image']['name'] ] ); //確認画面用 ?>

                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <span class="file_name"><?php if( isset(( $contents['uploaded']['image'] ) ) ) echo h( $contents['uploaded']['image']['name'] ); ?></span>
                        <?php echo noh( $this->Form->file( 'files[image]', [ 'id' => 'image_file', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker'] ) ); ?>
                        <?php if( isset( $error['release_date'] ) ) echo '<p class="error">' . $error['release_date'] . '</p>'; ?>
                    </td>
                </tr>

                <tr>
                    <th>公開終了日</th>
                    <td><?php echo noh( $this->Form->text( 'close_date',  [ 'placeholder' => '公開終了日を設定', 'class' => 'datepicker']) ); ?></td>
                </tr>

                <tr>
                    <th>本文</th>
                    <td><?php echo noh( $this->Form->textarea( 'content',  [ 'class' => 'ck_editor' ]) ); ?></td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
