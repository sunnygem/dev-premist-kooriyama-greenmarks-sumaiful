<div class="">
    <div class="clearfix contents_title">
        <h2 class="left">コンテンツ管理</h2>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>サムネイル</th>
                <th>タイトル</th>
                <th>本文</th>
                <th>公開</th>
                <th>公開日</th>
                <th>公開終了日</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class="title"><?php echo h( $val->id ); ?></td>
                    <td class="title"><?php echo noh( ( count( $val->files ) > 0 ) ? '<img src="' . DS . $val->files[0]->file_path . '">' : 'no image' ); ?></td>
                    <td class="title"><?php echo h( $val->title ); ?></td>
                    <td class="title"><?php echo h( mb_strimwidth( strip_tags( $val->content ), 0, 100, '...' ) ); ?></td>
                    <td class="title"><?php echo noh( ( $val->display_flg === 1 ) ? '公開' : '非公開' ); ?></td>
                    <td class="title"><?php echo h( $val->release_date->format('Y/m/d') ); ?></td>
                    <td class="title"><?php echo ( $val->close_date ) ? h( $val->close_date->format('Y/m/d') ) : '--'; ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <?php if( in_array( $login_user['admin_user']['authority'], [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
