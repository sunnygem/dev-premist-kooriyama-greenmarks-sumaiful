<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>サムネイル</th>
                <th>内容</th>
                <th>投稿者</th>
                <th>建物</th>
                <th>表示フラグ</th>
                <th>作成日時</th>
                <?php /*
                <th>&nbsp;</th>
                */ ?>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class=""><?php echo h( $val->id ); ?></td>
                    <td class="">
                        <?php if ( count( $val->rel_post_files ) > 0 ): ?>
                            <?php if ( isset( $val->rel_post_files[0] ) && $val->rel_post_files[0]->file->mime_type === 'application/pdf' ): ?>
                                <embed src="<?php echo noh( DS . $val->rel_post_files[0]->file->file_path ); ?>" type="application/pdf">
                            <?php else: ?>
                                <?php echo '<img src="' .  DS .$val->rel_post_files[0]->file->file_path . '">'; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo 'no image'; ?>
                        <?php endif; ?>
                    </td>
                    <td class=""><?php echo h( mb_strimwidth( $val->content, 0, 50, '...' ) ); ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build(['controller' => 'Residences', 'action' => 'detail', $val->user->id]) ); ?>">
                            <?php echo h( $val->user->front_user->nickname ); ?>
                        </a>
                    </td>
                    <td class=""><?php echo noh( $mt_buildings[$val->building_id] ); ?></td>
                    <td class=""><?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?></td>
                    <td class=""><?php echo ( $val->created ) ? h( $val->created->format('Y/m/d H:i:s') ) : '--'; ?></td>
                    <?php /*
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'detail',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 詳細</a>
                    </td>
                    +*/ ?>

                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
