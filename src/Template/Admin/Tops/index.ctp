<div class="dashboard">
    <div class="clearfix contents_title">
        <h2><?php echo h( $app_name ); ?> 管理画面ホーム</h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php if( $this->request->Session()->read('ContractantData.unique_parameter') !== 'sumaiful' ) : ?>
    <div class="list_table">
        <h3>未承認のユーザー件数</h3>
        <table>
            <tr>
                <th>建物名</th>
                <th>件数</th>
                <th></th>
            </tr>

            <?php foreach( $unapproved_user_count as $key => $val ): ?>
                <?php /*if( count( $val->front_users ) === 0  ): ?>
                <?php else: ?>
                <?php endif;*/ ?>

                <tr>
                    <td><?php echo $val->name; ?></td>
                    <td>
                        <?php if( count( $val->front_users ) > 0 ): ?>
                            <?php echo number_format( count( $val->front_users ) ); ?>件
                        <?php else: ?>
                            <p>データはありません</p>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?php echo $this->Url->build(['controller' => 'Residences', 'action' => 'index', '?' => ['buildings' => $val->id, 'unautholized' => 1, 'room_number' => '' ] ]); ?>" class="btn">詳細</a>
                    </td>
                </tr>
            <?php endforeach; ?> 
        </table>

    </div>

    <div class="list_table">
        <h3>報告件数</h3>
        <table>
            <tr>
                <th>建物名</th>
                <th>件数</th>
                <th></th>
            </tr>
            <?php foreach( $no_compatible_presentation_count as $key => $val ): ?>
            <tr>
                <td><?php echo h( $val->name ); ?></td>
                <td>
                    <?php if( $val->presentation_buildings ): ?>
                        <?php echo number_format( $val->presentation_buildings[0]->count ); ?>件
                    <?php else: ?>
                        データはありません
                    <?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo $this->Url->build(['controller' => 'Presentations', 'action' => 'index' ]); ?>" class="btn">詳細</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>

    </div>

    <?php endif; ?>

</div><!--/.dashboard -->
