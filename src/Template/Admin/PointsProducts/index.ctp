<div class="equipment">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>名前</th>
                <th>必要ポイント</th>
                <th>画像</th>
                <th>表示<th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr class="">
                    <td class=""><?php echo h( $val->id ); ?></td>
                    <td class="">
                        <?php echo h( $val->name ); ?>
                        <?php if( $val->name ) echo noh( '<br>' .  $val->name_en ); ?>
                    </td>
                    <td class=""><?php echo noh( $val->use_points ); ?></td>
                    <td class="preview">
                        <?php
                            if( $val->icon_url_path !== null )
                            {
                                 echo $this->Html->image( DS . $val->icon_url_path );
                            }
                            else
                            {
                                 echo 'no_image';
                            }
                        ?>
                    </td>
                    <td class=""><?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?></td>
                    <td class="">
                        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                            <a data-action="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete jsConfirm" tabIndex="-1"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
