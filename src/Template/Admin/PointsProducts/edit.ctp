<div class="equipment">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> <?php if( isset( $points_products->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $points_products, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $points_products->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $points_products->id ] ) ); ?>
        <div class="form_table">
           <table>
                <tr>
                    <th><span class="required">*</span>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 1 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <th><span class="required">*</span>商品名</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => '名前を入力' ] ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>商品名(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name_en', [ 'label' => false, 'placeholder' => '名前(英語)を入力' ] ) ); ?>
                        <?php if( isset( $error['name_en'] ) ) echo noh( '<p class="error">' . $error['name_en'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $points_products['icon_url_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $points_products['icon_url_path'] . '">' ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files[icon_url_path]', [ 'id' => 'image_file', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>
                </tr>
                
                <tr>
                    <th><span class="required">*</span>必要ポイント</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'use_points', [ 'label' => false ] ) ); ?>
                        <?php if( isset( $error['use_points'] ) ) echo noh( '<p class="error">' . $error['use_points'] . '</p>' ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
