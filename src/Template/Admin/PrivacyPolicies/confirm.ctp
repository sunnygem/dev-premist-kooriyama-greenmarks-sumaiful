<section class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $data['id'] ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>プライバシーポリシー</th>
                <td>
                    <pre><?php echo $data['content']; ?></pre>
                </td>
            </tr>
            <tr>
                <th>プライバシーポリシー(英語)</th>
                <td>
                    <pre><?php echo $data['content_en']; ?></pre>
                </td>
            </tr>

            <tr>
                <th>プライバシーポリシーURL</th>
                <td>
                    <?php echo noh( ( $data['url'] ) ? $data['url'] : '登録がありません' ); ?>
                </td>
            </tr>
            <tr>
                <th>プライバシーポリシーURL(英語)</th>
                <td>
                    <?php echo noh( ( $data['url_en'] ) ? $data['url_en'] : '登録がありません' ); ?>
                </td>
            </tr>

        </table>
    </div>

    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</section>
