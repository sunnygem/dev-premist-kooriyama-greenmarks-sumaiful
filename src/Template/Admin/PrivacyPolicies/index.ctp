<?php echo noh( $this->element( 'Admin/config_tab' ) ); ?>
<section class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
        <div class="right"> 
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table style="table-layout: fixed;">
            <tr>
                <th>プライバシーポリシー</th>
                <td>
                    <pre> <?php echo noh( ( $data->content ) ? $data->content : '登録がありません' ); ?> </pre>
                </td>
            </tr>
            <tr>
                <th>プライバシーポリシー(英語)</th>
                <td>
                    <pre> <?php echo noh( ( $data->content_en ) ? $data->content_en : '登録がありません' ); ?> </pre>
                </td>
            </tr>
            <tr>
                <th>プライバシーポリシーURL</th>
                <td>
                    <?php echo noh( ( $data->url ) ? $data->url : '登録がありません' ); ?>
                </td>
            </tr>
            <tr>
                <th>プライバシーポリシーURL(英語)</th>
                <td>
                    <?php echo noh( ( $data->url_en ) ? $data->url_en : '登録がありません' ); ?>
                </td>
            </tr>
        </table>

    </div>
</section>
