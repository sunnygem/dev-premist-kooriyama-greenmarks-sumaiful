<div class="contractant">
    <section>
        <?php echo noh( $this->element( 'Admin/config_tab') ); ?>
        <div class="clearfix contents_title">
            <h2 class="left"> 契約者情報 </h2>
            <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
            <div class="right"> 
                <a href="<?php echo noh( $this->Url->build( ['action' => 'contractantEdit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                <a href="<?php echo noh( $this->Url->build( ['action' => 'favicon' ]) ); ?>" class="btn"><i class="fas fa-wrench"></i> ファビコン登録</a>
            </div>
            <?php endif; ?>
        </div>

        <?php echo noh( $this->Flash->render() ); ?>

        <div class="form_table">
            <table>
                <tr>
                    <th>ドメイン</th>
                    <td><?php echo h( $contracts->domain ); ?></td>
                </tr>
                <tr>
                    <th>契約者名</th>
                    <td><?php echo noh( $contracts->name ); ?></td>
                </tr>
                <tr>
                    <th>ステータス</th>
                    <td><?php echo noh( $mt_contractant_status[$contracts->status_id] ); ?></td>
                </tr>
                <tr>
                    <th>契約締結日</th>
                    <td><?php echo noh( $contracts->contract_date->format( 'Y/m/d' ) ); ?></td>
                </tr>
                <tr>
                    <th>サービス開始日</th>
                    <td><?php echo noh( $contracts->service_start_date->format( 'Y/m/d' ) ); ?></td>
                </tr>
                <tr>
                    <th>サービス終了日</th>
                    <td><?php echo noh( $contracts->service_end_date->format( 'Y/m/d' ) ); ?></td>
                </tr>
                <tr>
                    <th>ロゴ画像</th>
                    <td>
                       <?php if( $contracts->file_path ): ?>
                           <img src="<?php echo noh( DS . $contracts->file_path ); ?>">
                       <?php else: ?>
                           登録してください
                       <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th>テーマカラー</th>
                    <td>
                        <?php echo h( $contracts['theme_color'] ); ?>
                        <div style="display: inline-block; width: 2em; height: 1em; background: <?php echo h( $contracts['theme_color'] ); ?>"></div>
                    </td>
                </tr>


            </table>
        </div>
    </section>

    <section>
        <div class="clearfix contents_title">
            <h2 class="left"> 管理物件一覧 </h2>
        </div>
            <div class="list_table">
            <table>
                <tr>
                    <th>ID</th>
                    <th>表示フラグ</th>
                    <th>名前</th>
                    <th>住所</th>
                </tr>

                <?php foreach( $buildings as $key => $val ): ?>
                    <tr>
                        <td class="title"><?php echo h( $val->id); ?></td>
                        <td class=""><?php echo noh( ( $val->display_flg ) ? '表示': '非表示' ); ?></td>
                        <td class=""><?php echo h( $val->name); ?></td>
                        <td class="">
                            〒<?php echo h( $val->zip ); ?><br>
                            <?php echo h( $val->address ); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>

    </section>
</div>
