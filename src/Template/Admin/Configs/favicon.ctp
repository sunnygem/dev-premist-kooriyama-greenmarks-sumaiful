<section class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( null, [ 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <div class="form_table">
            <table>

                <tr>
                    <th>ファビコン</th>
                    <td class="file">
                        <label for="image_file" class="btn">ファビコンを選択</label>
                        <?php echo noh( $this->Form->file( 'file', [ 'id' => 'image_file', 'label' => false ] ) ); ?>
                        <?php if( isset( $error['file'] ) ) echo noh( '<p class="error">' . $error['file'] . '</p>' ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</section>
