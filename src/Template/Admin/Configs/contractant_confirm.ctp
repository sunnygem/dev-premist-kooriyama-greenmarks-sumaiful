<section class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?>
        </h2>
    </div>

    <?php echo noh( $this->Form->create( null, [ 'novalidate' => true, 'autocomplete' => 'off' ] ) ); ?>
        <div class="form_table">
            <table>

                <tr>
                    <th>ロゴ画像</th>
                    <td class="file">
                        <?php
                            if( isset( $contractants['files']['contractants']['file_path'] ) ) // 登録されたサムネを表示 
                            {
                                echo noh( '<img src="' . DS . $contractants['files']['contractants']['file_path'] ) . '">';
                            }
                            elseif( isset( $contractants['file_path'] ) ) // 登録されたサムネを表示 
                            {
                                echo noh( '<img src="' . DS . $contractants['file_path'] ) . '">';
                            }
                            else
                            {
                                echo noh( '--' );
                            }
                        ?>
                    </td>
                </tr>

                <tr>
                    <th>テーマカラー</th>
                    <td>
                        <?php echo h( $contractants['theme_color'] ); ?>
                        <div style="display: inline-block; width: 2em; height: 1em; background: <?php echo h( $contractants['theme_color'] ); ?>"></div>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <a href="<?php echo noh( $this->Url->build(['action' => 'contractantEdit']) ); ?>" class="btn back">戻る</a>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</section>
