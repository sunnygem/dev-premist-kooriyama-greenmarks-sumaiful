<section class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( $contractants, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php echo noh( $this->Form->hidden( 'id', [ 'value' => $contractants->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>名前</th>
                    <td>
                        <?php echo noh( $contractants['name'] ); ?>
                    </td>
                </tr>

                <tr>
                    <th>ロゴ画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $contractants['file_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh( '<img src="' . DS . $contractants['file_path'] ) . '">'; ?>
                               <?php //echo $this->Form->hidden( 'file_path', [ 'value' => $contractants['file_path'] ] ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files[contractants]', [ 'id' => 'image_file', 'label' => false ] ) ); ?>
                    </td>
                </tr>

                <tr>
                    <th>テーマカラー</th>
                    <td>
                        <?php echo noh( $this->Form->color( 'theme_color',  [ 'default' => 'null' ]) ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</section>
