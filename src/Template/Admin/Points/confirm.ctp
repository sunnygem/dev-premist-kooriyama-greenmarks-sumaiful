<div class="point">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo noh( $title_for_layout ); ?> 消込確認
        </h2>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>名前</th>
                <td><?php echo h( ( $front_user['name'] ) ); ?></td>
            </tr>
            <tr>
                <th>今回の使用ポイント</th>
                <td><?php echo noh( number_format( $front_user['apply_point'] ) ); ?>pt.</td>
            </tr>
            <tr>
                <th>残りポイント</th>
                <td><?php echo noh( number_format( $front_user['point'] ) ); ?>pt.</td>
            </tr>
 
        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'apply', $front_user->id] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
