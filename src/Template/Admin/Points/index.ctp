<div class="user_point">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">

        <?php echo noh( $this->Form->create( null, [ 'type' => 'get', 'autocomplete' => 'off' ] ) ); ?>
        <table>
            <tr>
                <th style="width: 15%;">建物</th>
                <td style="width: 30%;">
                    <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
                        <?php echo noh( $this->Form->select('search[building_id]', $mt_buildings, [ 'empty' => '建物を選択', 'default' => $this->request->getQuery('search.building_id'), 'class' => 'autoSubmit' ] ) ); ?>
                    <?php else: ?>
                        <?php echo h( $mt_buildings[$login_user['admin_user']['building_id']] ); ?>
                    <?php endif; ?>
                </td>
                <th style="width: 15%;">部屋番号</th>
                <td style="width: 30%;"> <?php echo noh( $this->Form->text('search[room_number]', [ 'placeholder' => '部屋番号を入力', 'default' => $this->request->getQuery('search.room_number' ), 'class' => 'autoSubmit' ] ) ); ?></td>
                <td style="width: 10%;" class="text_c" rowspan="2"> <?php echo noh( $this->Form->button( '検索', [ 'class' => 'btn', 'style' => 'margin: 0;' ] ) ); ?> </td>
            </tr>
        </table>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>名前</th>
                <th>ポイント</th>
                <th>建物</th>
                <th>部屋番号</th>
                <th>退去</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class=""><?php echo noh( $val->id ); ?></td>
                    <td class="">
                        <?php echo h( $val->name ); ?>
                        <?php if ( $val->user->nickname ) echo noh( '<br>' . $val->user->nickname ); ?>
                    </td>
                    <td class=""><?php echo noh( number_format( $val->point ) ); ?>pt.</td>
                    <td class=""><?php echo h( $val->building->name ); ?></td>
                    <td class=""><?php echo h( $val->room_number ); ?></td>
                    <td class=""><?php echo noh( ( $val->leave_flg ) ? '●' : '-' ); ?></td>
                    <td class="">
                        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'apply',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 消込</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
