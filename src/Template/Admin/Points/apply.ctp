<div class="point">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> ポイント消込
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <div class="form_table">
            <table>
                <tr>
                    <th>名前</th>
                    <td>
                        <?php echo noh( $data->name ); ?>
                    </td>
                </tr>

                <tr>
                    <th>所持ポイント</th>
                    <td>
                        <?php echo noh( number_format( $data->point ) ); ?>pt.
                    </td>
                </tr>

                <tr>
                    <th>使用ポイント</th>
                    <td>
                        <?php echo noh( $this->Form->number( 'apply_point', [ 'label' => false, 'step' => '1', 'min' => 0, 'value' => 0 ] ) ); ?>
                        <?php if( $this->Form->isFieldError('apply_point') ) echo noh( $this->Form->error('apply_point') ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
