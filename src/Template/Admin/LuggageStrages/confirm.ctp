<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?> 確認
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>受け取り人</th>
                <td><?php echo noh( $user_options[$data['user_id']] ); ?></td>
            </tr>

            <tr>
                <th>荷物の特徴など</th>
                <td><?php echo h( $data['luggage'] ); ?></td>
            </tr>
 
        </table>
    </div>
    <div class="text_c">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
