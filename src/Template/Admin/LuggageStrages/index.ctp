<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>建物</th>
                <th>受け取り人</th>
                <th>荷物の特徴</th>
                <th>登録日時</th>
                <th>受け取り</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class=""><?php echo h( $val->id ); ?></td>
                    <td class=""><?php echo h( $val->user->front_user->building->name ); ?></td>
                    <td class=""><?php echo h( $val->user->front_user->name ); ?></td>
                    <td class=""><?php echo h( $val->luggage ); ?></td>
                    <td class=""><?php echo h( $val->created->format('Y/m/d H:i') ); ?></td>
                    <td class=""><?php echo noh( ( $val->receive_flg === 1 ) ? '受取済み' : '未' ); ?></td>
                    <td class="btn_area">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>

                        <?php if( $val->event_flg === 1 ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 参加者の確認</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
