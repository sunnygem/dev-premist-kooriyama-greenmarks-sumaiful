<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?> <?php if( isset( $data->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $data->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>受け取り人</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'user_id', $user_options, [ 'empty' => '選択してください' ]) ); ?>
                        <?php if( isset( $error['user_id'] ) ) echo noh( '<p class="error">' . $error['user_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>荷物</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'luggage', [ 'label' => false, 'placeholder' => '荷物の特徴などを入力' ] ) ); ?>
                        <?php if( isset( $error['luggage'] ) ) echo noh( '<p class="error">' . $error['luggage'] . '</p>' ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
