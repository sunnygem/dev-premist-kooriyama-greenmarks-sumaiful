<div class="equipment">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> 貸出し
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $equipment, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <div class="form_table">
           <table>
                <tr>
                    <th>カテゴリ</th>
                    <td> <?php echo noh( $equipment->category_name ); ?> </td>
                </tr>

                <tr>
                    <th><span class="required">*</span>名前</th>
                    <td> <?php echo noh( $equipment->name ); ?> </td>
                </tr>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $equipment['icon_url_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $equipment['icon_url_path'] . '">' ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                    </td>

                </tr>
            </table>
        </div>

        <div class="form_table">
            <table>
                <tr>
                    <th>返却予定日時</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'expected_return_date',  [ 'placeholder' => '返却予定日時を設定', 'class' => 'datetimepicker']) ); ?>
                        <?php if( $this->Form->isErrorField('expected_return_date') ) echo noh( $this->Form->error( 'expected_return_date' ) ); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'regist', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
