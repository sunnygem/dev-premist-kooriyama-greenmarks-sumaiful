<div class="equipment">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> <?php if( isset( $equipment->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $equipment, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $equipment->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $equipment->id ] ) ); ?>
        <div class="form_table">
           <table>
                <tr>
                    <th><span class="required">*</span>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 1 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th><span class="required">*</span>建物</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'building_id', $mt_buildings, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['building_id'] ) ) echo noh( '<p class="error">' . $error['building_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th><span class="required">*</span>カテゴリ</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'category_id', $mt_equipment_category, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['category_id'] ) ) echo noh( '<p class="error">' . $error['category_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th><span class="required">*</span>名前</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => '名前を入力' ] ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>名前(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name_en', [ 'label' => false, 'placeholder' => '名前(英語)を入力' ] ) ); ?>
                        <?php if( isset( $error['name_en'] ) ) echo noh( '<p class="error">' . $error['name_en'] . '</p>' ); ?>
                    </td>
                </tr>

                <?php /*
                <tr>
                    <th>キャプション</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'caption',  [ 'placeholder' => 'テキストを入力']) ); ?>
                        <?php if( isset( $error['caption'] ) ) echo noh( '<p class="error">' . $error['caption'] . '</p>' ); ?>
                    </td>
                </tr>
                */ ?>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $equipment['icon_url_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $equipment['icon_url_path'] . '">' ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files[icon_url_path]', [ 'id' => 'image_file', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>

                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
