<div class="equipment">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo  noh( $title_for_layout ); ?> <?php if( isset( $equipment->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $equipment['display_flg'] === '1' ) ? '表示する' : '表示しない' ); ?></td>
            </tr>

            <tr>
                <th>建物</th>
                <td><?php echo h( $mt_buildings[$equipment['building_id']] ); ?></td>
            </tr>

            <tr>
                <th>カテゴリ</th>
                <td><?php echo h( $mt_equipment_category[$equipment['category_id']] ); ?></td>
            </tr>

            <tr>
                <th>名前</th>
                <td><?php echo h( $equipment['name'] ); ?></td>
            </tr>

            <tr>
                <th>名前(英語)</th>
                <td><?php echo h( $equipment['name_en'] ); ?></td>
            </tr>

            <tr>
                <th>画像</th>
                <td class="file">
                    <div class="preview">
                        <?php if( isset( $equipment['files']['icon_url_path']['file_path'] ) ): // 登録されたサムネを表示 ?>
                           <?php echo noh( '<img src="/' . $equipment['files']['icon_url_path']['file_path'] . '">'); ?>
                        <?php else: ?>
                            no image
                        <?php endif; ?>
                    </div><!--/.preview -->
                </td>
            </tr>

        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
