<div class="equipment">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">

        <?php echo noh( $this->Form->create( null, [ 'type' => 'get', 'autocomplete' => 'off' ] ) ); ?>
        <table>
            <tr>
                <th style="width: 15%;">建物</th>
                <td style="width: 30%;">
                    <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
                        <?php echo noh( $this->Form->select('search.building_id', $mt_buildings, [ 'empty' => '建物を選択', 'default' => $this->request->getQuery('search.buildings_id' ), 'class' => '' ] ) ); ?>
                    <?php else: ?>
                        <?php echo h( $mt_buildings[$login_user['admin_user']['building_id']] ); ?>
                    <?php endif; ?>
                </td>
                <th style="width: 15%;">カテゴリ</th>
                <td style="width: 30%;">
                    <?php echo noh( $this->Form->select('search.category_id', $mt_equipment_category, [ 'empty' => 'カテゴリを選択', 'default' => $this->request->getQuery('search.category_id'), 'class' => '' ] ) ); ?>
                </td>
                <td style="width: 10%;" class="text_c" rowspan="2"> <?php echo noh( $this->Form->button( '検索', [ 'class' => 'btn', 'style' => 'margin: 0;' ] ) ); ?> </td>
            </tr>
            <tr>
                <th style="width: 15%;">備品名</th>
                <td style="width: 30%;"> <?php echo noh( $this->Form->text('search[name]', [ 'placeholder' => '備品名を検索', 'default' => $this->request->getQuery('search.name'), 'class' => '' ] ) ); ?></td>

                <td style="width: 30%;" colspan="2">
                    <div class="checkbox">
                        <?php echo noh( $this->Form->checkbox( 'search.in_use_flg', ['hiddenField' => false, 'id' => 'in_use_flg', 'default' => $this->request->getQuery('search.in_use_flg' ), 'class' => '' ] ) ); ?>
                        <label for="in_use_flg">使用中のみ</label>
                        <?php echo noh( $this->Form->checkbox( 'search.display_flg', ['hiddenField' => false, 'id' => 'display_flg', 'default' => $this->request->getQuery('search.display_flg' ), 'class' => '' ] ) ); ?>
                        <label for="display_flg">表示中のみ</label>
                    </div>
                </td>
            </tr>
        </table>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>名前</th>
                <th>画像</th>
                <th>カテゴリ</th>
                <th>建物</th>
                <th>使用中</th>
                <th>表示フラグ</th>
                <th>返却予定日時/使用終了日時</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr class="<?php if( $val->display_flg === 0 ) echo noh('undisplay'); ?>">
                    <td class=""><?php echo h( $val->id ); ?></td>
                    <td class="">
                        <?php echo h( $val->name ); ?>
                        <?php if( $val->name ) echo noh( '<br>' .  $val->name_en ); ?>
                    </td>
                    <td class="preview">
                        <?php
                            if( $val->icon_url_path !== null )
                            {
                                 echo $this->Html->image( DS . $val->icon_url_path );
                            }
                            else
                            {
                                 echo 'no_image';
                            }
                        ?>
                    </td>
                    <td class=""><?php echo noh( $val->category_name ); ?></td>
                    <td class=""><?php echo noh( $val->building_name ); ?></td>
                    <td class=""><?php echo noh( ( $val->in_use_flg  === 1 ) ? '使用中' : '--' ); ?></td>
                    <td class=""><?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?></td>
                    <td class=""><?php echo ( $val->expected_return_date )  ? h( $val->expected_return_date->format('Y/m/d H:i') ) : '--'; ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'lend',   $val->id ]) ); ?>" class="btn"><i class="fas fa-drum"></i> 貸出し</a>
                        <?php if( $val->in_use_flg === 1 ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'returnOf',   $val->id ]) ); ?>" class="btn"><i class="fas fa-undo"></i> 返却</a>
                        <?php else: ?>
                            <button type="button" class="btn off" disabled="1"><i class="fas fa-undo"></i> 返却</button>
                        <?php endif; ?>
                    </td>
                    <td class="">
                        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                            <a data-action="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete jsConfirm" tabIndex="-1"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
