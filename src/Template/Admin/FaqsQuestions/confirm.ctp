<section class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $data['id'] ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>カテゴリー</th>
                <td><?php echo h( $faq['faq_category_id'] ); ?></td>
            </tr>
            <tr>
                <th>Qustion</th>
                <td><?php echo h( $faq['question'] ); ?></td>
            </tr>
            <tr>
                <th>Qustion(英語)</th>
                <td><?php echo h( $faq['question_en'] ); ?></td>
            </tr>
        </table>
    </div>

    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</section>
