<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout); ?></h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $faq, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data['id'] ) ) echo noh( $this->Form->hidden( 'id', [] ) ); ?>
        <div class="form_table">
           <table>
                <tr>
                    <th><span class="required">*</span>カテゴリ</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'faq_category_id', $mt_faq_category, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['faq_category_id'] ) ) echo noh( '<p class="error">' . $error['faq_category_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>Question</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'question', [ 'label' => false ] ) ); ?>
                        <?php if( isset( $error['question'] ) ) echo noh( '<p class="error">' . $error['question'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>Question(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'question_en', [ 'label' => false ] ) ); ?>
                        <?php if( isset( $error['question_en'] ) ) echo noh( '<p class="error">' . $error['question_en'] . '</p>' ); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text_c brn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
