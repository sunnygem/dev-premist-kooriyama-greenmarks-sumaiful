<div class="">

    <?php echo noh( $this->element( 'Admin/config_tab') ); ?>

    <div class="clearfix contents_title">
        <h2 class="left"><?php echo h( $title_for_layout ); ?></h2>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <?php echo noh( $this->Form->create( null, [ 'autocomplete' => 'off' ] ) ); ?>
        <div class="list_table">
            <table>
                <tr>
                    <th>IPアドレス</th>
                    <th>メモ</th>
                </tr>
                <?php for( $i=0;  $i < 10; $i++ ): ?>
                    <tr>
                        <td class="title">
                            <?php echo noh( $this->Form->text( 'ip_limits[' . $i . '][ip_address]', [ 'placeholder' => 'xxx.xxx.xxx.xxx', 'default' => ( isset( $ip_limits[$i] ) ) ? $ip_limits[$i]['ip_address'] : null ] ) ); ?>
                            <?php if( isset( $error[$i]['ip_address'] ) ) echo noh( '<p class="error">' . $error[$i]['ip_address'] . '</p>' ); ?>
                        </td>
                        <td>
                            <?php echo noh( $this->Form->text( 'ip_limits[' . $i . '][contents]',   [ 'placeholder' => 'メモを入力', 'default' => ( isset( $ip_limits[$i] ) ) ? $ip_limits[$i]['contents'] : null ] ) ); ?>
                            <?php if( isset( $error[$i]['contents'] ) ) echo noh( '<p class="error">' . $error[$i]['contents'] . '</p>' ); ?>
                        </td>
                    </tr>
                <?php endfor; ?>
            </table>
        </div>
        <div class="btn_area text_c">
            <?php echo noh( $this->Form->button( '登録', [ 'class' => 'btn submit', 'name' => 'complete', 'tabIndex' => '-1' ]) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>

</div>
