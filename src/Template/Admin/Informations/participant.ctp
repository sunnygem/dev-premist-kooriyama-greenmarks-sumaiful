<section class="information">

    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo noh( $title_for_layout ); ?> </h2>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( [ 'action' => 'index' ] ) ); ?>" class="btn btn_back">戻る</a>
        </div>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>イベント名</th>
                <td><?php echo h( $data->title ); ?></td>
            </tr>

            <tr>
                <th>開催日</th>
                <td><?php echo noh( $data->event_date->format( 'Y/m/d' ) );  ?></td>
            </tr>

            <tr>
                <th>内容</th>
                <td>
                    <pre><?php echo h( $data['content'] ); ?></pre>
                </td>
            </tr>

            <tr>
                <th>画像</th>
                <td>
                    <?php if( $data->rel_information_file ): ?>
                        <?php if ( isset( $data->rel_information_file->file ) && $data->rel_information_file->file->mime_type === 'application/pdf' ): ?>
                            <embed src="<?php echo noh( DS . $data->rel_information_file->file->file_path ); ?>" type="application/pdf">
                        <?php else: ?>
                            <img src="/<?php echo h( $data->rel_information_file->file->file_path ); ?>">
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>

        </table>
    </div>

    <div class="clearfix contents_title">
        <h2 class="left">参加者</h2>
    </div>

    <div class="list_table">
        <table>
            <tr>
                <th>画像</th>
                <th>名前</th>
                <th>ニックネーム</th>
                <th>建物</th>
                <th>部屋番号</th>
            </tr>
            <?php foreach( $data->information_joins as $key => $val ): ?>
                <tr>
                    <td>
                        <?php if( $val->user->front_user->thumbnail_path ): ?>
                            <img src="/<?php echo noh( $val->user->front_user->thumbnail_path ); ?>" >
                        <?php else: ?>
                            no image
                        <?php endif; ?>
                    </td>
                    <td><?php echo h( $val->user->front_user->name ); ?> </td>
                    <td><?php echo h( $val->user->front_user->nickname ); ?> </td>
                    <td><?php echo h( $val->user->front_user->building->name ); ?> </td>
                    <td><?php echo h( $val->user->front_user->room_number ); ?> </td>
                </tr>
            <?php endforeach;?>

        <table>
    </div>

</section>
