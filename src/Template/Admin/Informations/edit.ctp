<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> <?php if( isset( $informations->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $informations, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $informations->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $informations->id ] ) ); ?>
        <?php if( $login_user['admin_user']['building_id'] !== 0 ): ?>
            <?php echo noh( $this->Form->hidden( 'all_building_flg', [ 'value' => 0 ] ) ); ?>
            <?php if( isset( $informations->id ) ): ?>
                <?php foreach( $informations->buildings as $val ): ?>
                    <?php echo noh( $this->Form->hidden( 'buildings[]', [ 'value' => $val ] ) ); ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?php echo noh( $this->Form->hidden( 'buildings[]', [ 'value' => $login_user['admin_user']['building_id'] ] ) ); ?>
            <?php endif; ?>
        <?php endif; ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'タイトルを入力' ] ) ); ?>
                        <?php if( isset( $error['title'] ) ) echo noh( '<p class="error">' . $error['title'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>タイプ</th>
                    <td>
                        <?php echo noh( $this->Form->radio( 'type', $mt_information_type, [ 'default' => 0 ] ) ); ?>
                        <?php if( isset( $error['type'] ) ) echo noh( '<p class="error">' . $error['type'] . '</p>' ); ?>
                    </td>
                </tr>
                <?php /*
                <tr>
                    <th>
                        イベントフラグ
                    </th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'event_flg',  [ 'id' => 'event_flg', 'default' => 0 ]) ); ?>
                            <label for="event_flg">イベント</label>
                        </div>
                        <p class="note">イベントとして表示する際にチェックしてください</p>
                    </td>
                </tr>
                */ ?>

                <tr>
                    <th>開催日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'event_date',  [ 'default' => null, 'class' => 'datepicker', 'placeholder' => 'イベント開催日を設定'] ) ); ?>
                        <?php if( isset( $error['event_date'] ) ) echo noh( '<p class="error">' . $error['event_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $informations['uploaded']['image'] ) ): // 登録されたサムネを表示 ?>
                                <?php if ( isset( $informations->rel_information_file->file ) && $informations->rel_information_file->file->mime_type === 'application/pdf' ): ?>
                                    <embed src="<?php echo noh( DS . $informations->rel_information_file->file->file_path ); ?>" type="application/pdf">
                                <?php else: ?>
                                    <?php echo noh( '<img src="' . DS . $informations['uploaded']['image']['file_path'] ) . '">'; ?>
                                <?php endif; ?>
                                <?php if( isset( $informations['uploaded']['image']['id'] ) ) echo $this->Form->hidden( 'uploaded[image][id]', [ 'value' => $informations['uploaded']['image']['id'] ] ); ?>
                                <?php echo $this->Form->hidden( 'uploaded[image][file_path]', [ 'value' => $informations['uploaded']['image']['file_path'] ] ); ?>
                                <?php echo $this->Form->hidden( 'uploaded[image][name]',      [ 'value' => $informations['uploaded']['image']['name'] ] ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <span class="file_name"><?php if( isset( $informations['uploaded']['image'] ) ) echo h( $informations['uploaded']['image']['name'] ); ?></span>
                        <?php echo noh( $this->Form->file( 'files[image]', [ 'id' => 'image_file', 'label' => false ] ) ); ?>
                    </td>
                </tr>

                <tr>
                    <th>本文</th>
                    <td>
                        <?php echo noh( $this->Form->textarea( 'content',  [ 'placeholder' => '本文を入力']) ); ?>
                        <?php if( isset( $error['content'] ) ) echo noh( '<p class="error">' . $error['content'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker'] ) ); ?>
                        <?php if( isset( $error['release_date'] ) ) echo noh( '<p class="error">' . $error['release_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開終了日</th>
                    <td><?php echo noh( $this->Form->text( 'close_date',  [ 'placeholder' => '公開終了日を設定', 'class' => 'datepicker']) ); ?></td>
                </tr>

                <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
                <tr>
                    <th>建物の公開範囲</th>
                    <td>
                        <div class="inline" data-exclusive-check="all_building_flg">
                            <div class="checkbox">
                                <?php echo noh( $this->Form->checkbox('all_building_flg', [ 'id' => 'all_building_flg' ] ) ); ?> 
                                <label for="all_building_flg">全て</label>
                            </div>
                            <?php echo noh( $this->Form->select( 'buildings', $mt_buildings, [ 'multiple' => 'checkbox' ] ) ); ?>
                        </div>
                        <?php if( isset( $error['buildings'] ) ) echo noh( '<p class="error">' . $error['buildings'] . '</p>' ); ?>
                    </td>
                </tr>
                <?php endif; ?>

                <tr>
                    <th>退去者への公開</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'reject_leave_readable_flg',  [ 'id' => 'reject_leave_readable_flg', 'default' => 0 ]) ); ?>
                            <label for="reject_leave_readable_flg">公開しない</label>
                        </div>
                    </td>
                </tr>


            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
