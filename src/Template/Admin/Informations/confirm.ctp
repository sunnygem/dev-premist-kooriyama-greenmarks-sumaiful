<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> <?php if( isset( $informations->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $informations['display_flg'] === '1' ) ? '表示する' : '表示しない' ); ?></td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td><?php echo h( $informations['title'] ); ?></td>
            </tr>
            <tr>
                <th>画像</th>
                <td>
                    <?php
                        if( isset( $informations['uploaded']['image']['file_path'] ) )
                        {
                            if ( $informations['uploaded']['image']['type'] === 'application/pdf' )
                            {
                                echo  noh(  '<embed src="' . DS . $informations['uploaded']['image']['file_path'] . '" type="application/pdf">' );
                            }
                            else
                            {
                                echo  noh(  '<img src="' . DS . $informations['uploaded']['image']['file_path'] . '">' );
                            }
                        }
                        else
                        {
                            echo noh( '--' );
                        }
                    ?>
                </td>
            </tr>

            <tr>
                <th>タイプ</th>
                <td><?php echo h( $mt_information_type[$informations['type']] ); ?></td>
            </tr>

            <tr>
                <th>本文</th>
                <td><?php echo h( $informations['content'] ); ?></td>
            </tr>
 
            <tr>
                <th>公開日</th>
                <td><?php echo h( $informations['release_date'] ); ?></td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td><?php echo noh($informations['close_date']) ? h( $informations['close_date'] ) : '設定なし'; ?></td>
            </tr>
            <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
            <tr>
                <th>建物の公開範囲</th>
                <td>
                    <?php if( $informations['buildings'] === '' ): ?>
                        全て
                    <?php else: ?>
                        <?php foreach( $informations['buildings'] as $val): ?>
                            <p><?php echo noh( $mt_buildings[$val] ); ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <th>退去者への公開</th>
                <td><?php echo noh( ( $informations['reject_leave_readable_flg'] === '1' ) ? '公開しない' : '公開' ); ?></td>
            </tr>


        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
