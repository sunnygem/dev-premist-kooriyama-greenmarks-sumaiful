<section class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout); ?></h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data['id'] ) ) echo noh( $this->Form->hidden( 'id', [] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>Q&A</th>
                    <td><?php echo noh( $this->Form->textarea( 'content',  [ 'class' => 'ckeditor' ]) ); ?></td>
                </tr>
                <tr>
                    <th>Q&A(英語)</th>
                    <td><?php echo noh( $this->Form->textarea( 'content_en',  [ 'class' => 'ckeditor' ]) ); ?></td>
                </tr>
            </table>

        </div>
        <div class="text_c brn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</section>
