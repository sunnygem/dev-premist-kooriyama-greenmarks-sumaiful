<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $contractant_service_menus['id'] ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <div class="form_table">
        <table>
            <tr>
                <th>機能名</th>
                <td><?php echo h( $contractant_service_menus['service_menu_id'] ); ?></td>
            </tr>

            <tr>
                <th>ラベル</th>
                <td><?php echo h( $contractant_service_menus['label'] ); ?></td>
            </tr>
            <tr>
                <th>ラベル(英語)</th>
                <td><?php echo h( $contractant_service_menus['label_en'] ); ?></td>
            </tr>
             <tr>
                <th>アイコン画像</th>
                <td class="file">
                    <div class="preview">
                        <?php if( isset( $contractant_service_menus['files']['service_menu_icon_path']['file_path'] ) ): // 登録されたサムネを表示 ?>
                           <?php echo noh( '<img src="/' . $contractant_service_menus['files']['service_menu_icon_path']['file_path'] . '">'); ?>
                        <?php else: ?>
                            no image
                        <?php endif; ?>
                    </div><!--/.preview -->
                </td>
            </tr>
            <tr>
                <th>画面割り当て</th>
                <td>
                    <?php if(  $contractant_service_menus['app_screen_id'] ): ?>
                        <?php echo h(  $screen_list[ $contractant_service_menus['app_screen_id']] ); ?>
                    <?php else: ?>
                        選択なし
                    <?php endif; ?>
                </td>
            </tr>

            <?php if( $contractant_service_menus['service_menu_id']  === '6' ): ?>
            <tr>
                <th>メモ</th>
                <td>
                    <?php echo nl2br( $contractant_service_menus['memo'] ); ?>
                </td>
            </tr>
            <?php endif; ?>

            <?php
                switch( $contractant_service_menus['service_menu_id'] ):
                case 1: // 問合せメール 
            ?>
                <tr>
                    <th>居住者タイプ</th>
                    <td>
                        <?php echo noh( $contractant_service_menus['menu_detail']['user_type'] ); ?>
                    </td>
                </tr>
                <tr>
                    <th>電話番号</th>
                    <td>
                        <?php echo noh( $contractant_service_menus['menu_detail']['tel'] ); ?>
                    </td>
                </tr>
                <tr>
                    <th>物件URL</th>
                    <td>
                        ラベル：<?php echo noh( $contractant_service_menus['menu_detail']['property_url_label'] ); ?><br>
                        URL：<?php echo noh( $contractant_service_menus['menu_detail']['property_url' ] ); ?>
                    </td>
                </tr>
                <tr>
                    <th>来場予約用URL</th>
                    <td>
                        ラベル：<?php echo noh( $contractant_service_menus['menu_detail']['attend_reserve_url_label' ] ); ?><br>
                        URL：<?php echo noh( $contractant_service_menus['menu_detail']['attend_reserve_url' ] ); ?>
                    </td>
                </tr>
                <tr>
                    <th>注釈</th>
                    <td>
                        <?php echo noh( $contractant_service_menus['menu_detail']['comment'] ); ?>
                    </td>
                </tr>
                <?php break; ?>
            <?php case 2: ?>
                <tr>
                    <th>問合せメール</th>
                    <td>
                        <?php echo noh( $menu_detail['email'] ); ?>
                    </td>
                </tr>
                <?php break; ?>
            <?php case 3: ?>
                <tr>
                    <th>フォーム送信先メールアドレス</th>
                    <td>
                        <?php echo noh( $menu_detail['email'] ); ?>
                    </td>
                </tr>
                <?php break; ?>
            <?php case 4: // リンクグループ ?>
                <tr>
                    <td colspan="2">
                    
                        <?php foreach( $contractant_service_menus['menu_detail'] as $key => $val ): ?>
                        <table>
                            <tr>
                                <th>名前</th>
                                <td>
                                    <?php echo noh( $val['name'] ); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>リンクURL</th>
                                <td>
                                    <?php echo noh( $val['link_url'] ); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>アイコン画像</th>
                                <td>
                                    <?php 
                                    if( isset( $contractant_service_menus['uploaded'][$key] ) )
                                    {
                                        echo  noh( $this->Html->image( 'upload/' . $contractant_service_menus['uploaded'][$key]['file_path'] ) );
                                    }
                                    else
                                    {
                                        echo noh( '--' );
                                    }
                                    ?>
                                </td>
                            </tr>

                        </table>
                        <?php endforeach; ?>
                    </td>
                </tr>

                <?php break; ?>
            <?php case 6: ?>
                <tr>
                    <td colspan="2">
                    
                        <?php foreach( $contractant_service_menus['menu_detail'] as $key => $val ): ?>
                        <table>
                            <tr>
                                <th>アプリ名</th>
                                <td>
                                    <?php echo noh( $val['name'] ); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>スキーマURL(iOS)</th>
                                <td><?php echo noh( $val['scheme_ios'] ); ?></td>
                            </tr>
                            <tr>
                                <th>APPストアURL(iOS)</th>
                                <td><?php echo noh( $val['app_id_ios'] ); ?></td>
                            </tr>
                            <tr>
                                <th>google playストアURL(Android)</th>
                                <td><?php echo noh( $val['store_url_android'] ); ?></td>
                            </tr>
                            <?php /*
                            <tr>
                                <th>アイコン画像</th>
                                <td>
                                    <?php 
                                    if( isset( $contractant_service_menus['uploaded'][$key] ) )
                                    {
                                        echo  noh( '<img src="' . DS . $contractant_service_menus['uploaded'][$key]['file_path'] . '">' );
                                    }
                                    else
                                    {
                                        echo noh( '--' );
                                    }
                                    ?>
                                </td>
                            </tr>
                            */ ?>

                        </table>
                        <?php endforeach; ?>
                    </td>
                </tr>

                <?php break; ?>
            <?php case 7: ?>
                <tr>
                    <td colspan="2">
                    
                        <?php foreach( $contractant_service_menus['menu_detail'] as $key => $val ): ?>
                        <table>
                            <tr>
                                <th>機器名</th>
                                <td>
                                    <?php echo noh( $val['equipment_name'] ); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>カテゴリー</th>
                                <td>
                                    <?php echo noh( ( $val['category_id'] ) ? $manual_categories[$val['category_id']] : 'なし' ); ?>
                                </td>
                            </tr>
                            <tr>
                                <th>PDF</th>
                                <td>
                                    <?php 
                                    if( isset( $contractant_service_menus['uploaded'][$key] ) )
                                    {
                                        echo noh( '<a href="' . DS  . $contractant_service_menus['uploaded'][$key]['file_path'] . '" target="_blank"><span class="far fa-file-pdf"></span></a> '. $contractant_service_menus['uploaded'][$key]['name'] );
                                    }
                                    else
                                    {
                                        echo noh( '--' );
                                    }
                                    ?>
                                </td>
                            </tr>

                        </table>
                        <?php endforeach; ?>
                    </td>
                </tr>

                <?php break; ?>

            <?php case 11: // リンクタイプ ?>
                <tr>
                    <td colspan="2">
                    
                        <?php foreach( $contractant_service_menus['menu_detail'] as $key => $val ): ?>

                        <table>
                            <tr>
                            <tr>
                                <th>リンクURL</th>
                                <td>
                                    <?php echo noh( $val['link_url'] ); ?>
                                </td>
                            </tr>

                        </table>

                        <?php endforeach; ?>
                    </td>
                </tr>

                <?php break; ?>

            <?php default: ?>
            <?php endswitch; ?>
        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit', $contractant_service_menus['id'] ] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
