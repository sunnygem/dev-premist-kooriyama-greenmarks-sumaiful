<div class="service_menu">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo $title_for_layout; ?> </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="list_table">
        <table>
            <tr>
                <th>ラベル名</th>
                <th>機能名</th>
                <th>画面割り当て</th>
                <th>概要</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $val ): ?>
                <?php if( $val->id !== 39 ): //sumaifulのモアスマイフルは表示しない ?>
                <tr>
                    <td><?php echo noh( ( $val->label ) ? $val->label : '--' ); ?></td>
                    <td><?php echo h( $val->service_menu->name ); ?></td>
                    <td>
                        <?php if( $val->app_screen_id ): ?>
                            <?php echo h( $val->app_screen->label ); ?>
                        <?php else: ?>
                            --
                        <?php endif; ?>
                    </td>
                    <td><?php echo h( $val->service_menu->description ); ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit', $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                    </td>
                </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
    </div>

</div>
