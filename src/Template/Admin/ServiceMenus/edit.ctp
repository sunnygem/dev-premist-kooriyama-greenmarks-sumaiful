<div class="service_menu">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo( $title_for_layout ); ?> </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $contractant_service_menus, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php  echo noh( $this->Form->hidden( 'id', [ 'value' => $contractant_service_menus->id ] ) ); ?>
        <?php  echo noh( $this->Form->hidden( 'service_menu_id', [ 'value' => $contractant_service_menus->sevice_menu_id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>機能名</th>
                    <td>
                        <?php echo noh( $contractant_service_menus['service_menu']['name'] ); ?>
                    </td>
                </tr>
                <tr>
                    <th>ラベル</th>
                    <td><?php echo noh( $this->Form->text( 'label', [ 'label' => false, 'placeholder' => 'ラベルを入力' ] ) ); ?></td>
                </tr>
                <tr>
                    <th>ラベル(英語)</th>
                    <td><?php echo noh( $this->Form->text( 'label_en', [ 'label' => false, 'placeholder' => 'ラベル(英語)を入力' ] ) ); ?></td>
                </tr>
                <tr>
                    <th>アイコン画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $contractant_service_menus['service_menu_icon_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh( '<img src="/' . $contractant_service_menus['service_menu_icon_path'] . '">'); ?>
                               <?php //echo noh( $this->Form->hidden( 'service_menu_icon_path', [] ) ); //確認画面用 ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="service_menu_icon_path" class="btn select_file">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files.service_menu_icon_path', [ 'id' => 'service_menu_icon_path', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>
                </tr>
                <tr>
                    <th>画面割り当て</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'app_screen_id', $screen_list, [ 'empty' => '選択してください' ] ) ); ?>
                    </td>
                </tr>

                <?php if( $contractant_service_menus->service_menu_id === 6 ): ?>
                <tr>
                    <th>メモ</th>
                    <td>
                        <?php echo noh( $this->Form->textarea( 'memo', [ 'palaceholder' => '商標などを入力してください' ] ) ); ?>
                    </td>
                </tr>
                <?php endif; ?>

                <?php
                    switch( $contractant_service_menus->service_menu_id ):
                    case 1: // 問合せメール 
                ?>
                    <tr>
                        <th>居住者タイプ</th>
                        <td>
                            <div class="radio inline">
                                <?php echo noh( $this->Form->radio( 'menu_detail.user_type', $mt_residence_type ) ); ?>
                            </div>
                            <?php if( isset( $error['menu_detail']['user_type'] ) ) echo noh( '<p class="error">' . $error['menu_detail']['user_type'] . '</p>' ); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>電話番号</th>
                        <td>
                            <?php echo noh( $this->Form->text( 'menu_detail.tel', [ 'label' => false, 'placeholder' => '問合せ電話番号を入力' ] ) ); ?>
                            <?php  if( isset( $contractant_service_menus['menu_detail']['id'] ) ) echo noh( $this->Form->hidden( 'menu_detail.id', [ 'value' => $contractant_service_menus['menu_detail']['id'] ] ) ); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>物件URL</th>
                        <td>
                            ラベル：<?php echo noh( $this->Form->text( 'menu_detail.property_url_label', [ 'label' => false, 'placeholder' => 'ラベルを変更する場合に入力' ] ) ); ?>
                            <?php if( isset( $error['menu_detail']['property_url_label'] ) ) echo noh( '<p class="error">' . $error['menu_detail']['property_url_label'] . '</p>' ); ?>
                            URL：<?php echo noh( $this->Form->text( 'menu_detail.property_url', [ 'label' => false, 'placeholder' => '物件URLを入力' ] ) ); ?>
                            <?php if( isset( $error['menu_detail']['property_url'] ) ) echo noh( '<p class="error">' . $error['menu_detail']['property_url'] . '</p>' ); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>来場予約用URL</th>
                        <td>
                            ラベル：<?php echo noh( $this->Form->text( 'menu_detail.attend_reserve_url_label', [ 'label' => false, 'placeholder' => 'ラベルを変更する場合に入力' ] ) ); ?>
                            <?php if( isset( $error['menu_detail']['attend_reserve_url_label'] ) ) echo noh( '<p class="error">' . $error['menu_detail']['attend_reserve_url_label'] . '</p>' ); ?>
                            URL：<?php echo noh( $this->Form->text( 'menu_detail.attend_reserve_url', [ 'label' => false, 'placeholder' => '来場予約用URLを入力' ] ) ); ?>
                            <?php if( isset( $error['menu_detail']['attend_reserve_url'] ) ) echo noh( '<p class="error">' . $error['menu_detail']['attend_reserve_url'] . '</p>' ); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>注釈</th>
                        <td>
                            <?php echo noh( $this->Form->text( 'menu_detail.comment', [ 'label' => false, 'placeholder' => '自由テキストを入力' ] ) ); ?>
                            <?php if( isset( $error['menu_detail']['comment'] ) ) echo noh( '<p class="error">' . $error['menu_detail']['comment'] . '</p>' ); ?>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 2: // 問合せメール ?>
                    <tr>
                        <th>問合せメールアドレス</th>
                        <td>
                            <?php echo noh( $this->Form->text( 'menu_detail.email', [ 'label' => false, 'placeholder' => '問合せアドレスを入力' ] ) ); ?>
                            <?php  if( isset( $contractant_service_menus['menu_detail']['id'] ) ) echo noh( $this->Form->hidden( 'menu_detail.id', [ 'value' => $contractant_service_menus['menu_detail']['id'] ] ) ); ?>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 3: // 問合せフォーム ?>
                    <tr>
                        <th>フォーム送信先メールアドレス</th>
                        <td>
                            <?php echo noh( $this->Form->text( 'menu_detail.email', [ 'label' => false, 'placeholder' => '問合せ先アドレスを入力' ] ) ); ?>
                            <?php  if( isset( $contractant_service_menus['menu_detail']['id'] ) ) echo noh( $this->Form->hidden( 'menu_detail.id', [ 'value' => $contractant_service_menus['menu_detail']['id'] ] ) ); ?>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 4: // リンク集タイプ ?>
                    <tr>
                        <th colspan="2"> 登録リンク </th>
                    </tr>

                    <tr>
                        <td colspan="2" class="inner_table_wrap">
                            <?php if( count( $contractant_service_menus->menu_detail ) > 0  ): ?>
                                <?php foreach( $contractant_service_menus->menu_detail as $key => $val ): ?>
                                    <table class="inner_table" id="index[<?php echo noh( $key ); ?>]">
                                        <tr>
                                            <th class="function" rowspan="3">
                                                <i class="fas fa-times-circle delete_table" data-index="<?php echo noh( $key ); ?>"></i>
                                            </th>
                                            <th>名前</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][name]', [ 'label' => false, 'placeholder' => 'アプリの名前を入力', 'default' => $val['name'] ] ) ); ?>
                                                <?php  if( isset( $val['id'] ) ) echo noh( $this->Form->hidden( 'menu_detail[' . $key . '][id]', [ 'value' => $val['id'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>リンクURL</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][link_url]', [ 'label' => false, 'placeholder' => 'リンクURLを入力', 'default' => $val['link_url'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>アイコン画像</th>
                                            <td class="file">
                                                <div class="preview">
                                                    <?php if( isset( $contractant_service_menus['uploaded'][$key] ) ): // 登録されたサムネを表示 ?>
                                                       <?php echo noh( '<a href="' . $contractant_service_menus['uploaded'][$key]['file_path'] . '">'); ?>
                                                       <?php if( isset( $contractant_service_menus['uploaded'][$key]['id'] ) ) echo $this->Form->hidden( 'uploaded[' . $key . '][id]',        [ 'value' => $contractant_service_menus['uploaded'][$key]['id'] ] ); ?>
                                                       <?php echo $this->Form->hidden( 'uploaded[' . $key . '][file_path]', [ 'value' => $contractant_service_menus['uploaded'][$key]['file_path'] ] ); //確認画面用 ?>
                                                       <?php echo $this->Form->hidden( 'uploaded[' . $key . '][name]',      [ 'value' => $contractant_service_menus['uploaded'][$key]['name'] ] ); //確認画面用 ?>
                                                    <?php endif; ?>
                                                </div><!--/.preview -->
                                                <label for="file[<?php echo h( $key ) ; ?>]" class="btn select_file">画像を選択する</label>
                                                <span class="file_name"><?php if ( isset( $contractant_service_menus['uploaded'][$key] ) ) echo h( $contractant_service_menus['uploaded'][$key]['name'] ); ?></span>
                                                <?php echo noh( $this->Form->file( 'menu_detail[' . $key . '][files][image]', [ 'id' => 'file[' . $key . ']', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                                            </td>
                                        </tr>
                                    </table>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <table class="inner_table" id="index[0]">
                                    <tr>
                                        <th class="function" rowspan="3">
                                            <i class="fas fa-times-circle delete_table" data-index="0"></i>
                                        </th>
                                        <th>名前</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][name]', [ 'label' => false, 'placeholder' => 'アプリの名前を入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>リンクURL</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][link_url]', [ 'label' => false, 'placeholder' => 'リンクURLを入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>アイコン画像</th>
                                        <td class="file">
                                            <div class="preview"> </div><!--/.preview -->
                                            <label for="file[0]" class="btn select_file">画像を選択する</label>
                                            <span class="file_name"><?php //echo h( $contractant_service_menus->images[0]['name'] ); ?></span>
                                            <?php echo noh( $this->Form->file( 'menu_detail[0][files][image]', [ 'id' => 'file[0]', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                                        </td>
                                    </tr>
                                </table>
                            <?php endif;  ?>
                            <div class="text_r" style="margin-top: 10px;">
                                <button type="button" class="btn add_table" tabIndex="-1">リンクを追加</button>
                            </div>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 5: // ブログタイプ ?>
                    <?php break; ?>
                <?php case 6: // アプリ ?>
                    <tr>
                        <th colspan="2"> 登録アプリ </th>
                    </tr>

                    <tr>
                        <td colspan="2" class="inner_table_wrap">
                            <?php if( count( $contractant_service_menus->menu_detail ) > 0  ): ?>
                                <?php foreach( $contractant_service_menus->menu_detail as $key => $val ): ?>
                                    <table class="inner_table" id="index[<?php echo noh( $key ); ?>]">
                                        <tr>
                                            <th class="function" rowspan="4">
                                                <i class="fas fa-times-circle delete_table" data-index="<?php echo noh( $key ); ?>"></i>
                                            </th>
                                            <th>アプリ名</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][name]', [ 'label' => false, 'placeholder' => 'アプリの名前を入力', 'default' => $val['name'] ] ) ); ?>
                                                <?php  if( isset( $val['id'] ) ) echo noh( $this->Form->hidden( 'menu_detail[' . $key . '][id]', [ 'value' => $val['id'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>スキーマURL(iOS)</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][scheme_ios]', [ 'label' => false, 'placeholder' => 'アプリのスキーマを入力', 'default' => $val['scheme_ios'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>アプリID(iOS)</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][app_id_ios]', [ 'label' => false, 'placeholder' => 'app ストアのアプリIDを入力', 'default' => $val['app_id_ios'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>google playストアURL(Android)</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][store_url_android]', [ 'label' => false, 'placeholder' => 'google play ストアのurlを入力', 'default' => $val['store_url_android'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <?php /*
                                        <tr>
                                            <th>アイコン画像</th>
                                            <td class="file">
                                                <div class="preview">
                                                    <?php if( isset( $contractant_service_menus['uploaded'][$key] ) ): // 登録されたサムネを表示 ?>
                                                       <?php echo noh( '<img src="' . DS . $contractant_service_menus['uploaded'][$key]['file_path'] . '">'); ?>
                                                       <?php if( isset( $contractant_service_menus['uploaded'][$key]['id'] ) ) echo $this->Form->hidden( 'uploaded[' . $key . '][id]',        [ 'value' => $contractant_service_menus['uploaded'][$key]['id'] ] ); ?>
                                                       <?php echo $this->Form->hidden( 'uploaded[' . $key . '][file_path]', [ 'value' => $contractant_service_menus['uploaded'][$key]['file_path'] ] ); //確認画面用 ?>
                                                       <?php echo $this->Form->hidden( 'uploaded[' . $key . '][name]',      [ 'value' => $contractant_service_menus['uploaded'][$key]['name'] ] ); //確認画面用 ?>
                                                    <?php endif; ?>
                                                </div><!--/.preview -->
                                                <label for="file[<?php echo h( $key ) ; ?>]" class="btn select_file">画像を選択する</label>
                                                <span class="file_name"><?php if( isset( $contractant_service_menus['uploaded'][$key] ) ) echo h( $contractant_service_menus['uploaded'][$key]['name'] ); ?></span>
                                                <?php echo noh( $this->Form->file( 'menu_detail[' . $key . '][files][image]', [ 'id' => 'file[' . $key . ']', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                                            </td>
                                        </tr>
                                        */ ?>
                                    </table>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <table class="inner_table" id="index[0]">
                                    <tr>
                                        <th class="function" rowspan="4">
                                            <i class="fas fa-times-circle delete_table" data-index="0"></i>
                                        </th>
                                        <th>アプリ名</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][name]', [ 'label' => false, 'placeholder' => 'アプリの名前を入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>スキーマURL(iOS)</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][scheme_ios]', [ 'label' => false, 'placeholder' => 'アプリのスキーマを入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>アプリID(iOS)</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][app_id_ios]', [ 'label' => false, 'placeholder' => 'app ストアのアプリIDを入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>スキーマURL(Android)</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][scheme_android]', [ 'label' => false, 'placeholder' => 'アプリのスキーマを入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <?php /*
                                    <tr>
                                        <th>アイコン画像</th>
                                        <td class="file">
                                            <div class="preview"> </div><!--/.preview -->
                                            <label for="file[0]" class="btn select_file">画像を選択する</label>
                                            <span class="file_name"><?php //echo h( $contractant_service_menus->images[0]['name'] ); ?></span>
                                            <?php echo noh( $this->Form->file( 'menu_detail[0][files][image]', [ 'id' => 'file[0]', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                                        </td>
                                    </tr>
                                    */ ?>
                                </table>
                            <?php endif;  ?>
                            <div class="text_r" style="margin-top: 10px;">
                                <button type="button" class="btn add_table" tabIndex="-1">アプリを追加</button>
                            </div>
                        </td>
                    </tr>
                    <?php break; ?>

                <?php case 7: // マニュアル ?>
                    <tr>
                        <td colspan="2">
                            <div class="text_l" style="margin-top: 10px;">
                                <a href="<?php echo noh( $this->Url->build([ 'controller' => 'MenuCategories', 'action' => 'index', $contractant_service_menus['id'] ]) ); ?>" class="btn" tabIndex="-1">マニュアルのカテゴリを登録</a>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2"> 登録マニュアル </th>
                    </tr>

                    <tr>
                        <td colspan="2" class="inner_table_wrap">
                            <?php if( count( $contractant_service_menus->menu_detail ) > 0  ): ?>
                                <?php foreach( $contractant_service_menus->menu_detail as $key => $val ): ?>
                                    <table class="inner_table" id="index[<?php echo noh( $key ); ?>]">
                                        <tr>
                                            <th class="function" rowspan="3">
                                                <i class="fas fa-times-circle delete_table" data-index="<?php echo noh( $key ); ?>"></i>
                                            </th>
                                            <th>機器名</th>
                                            <td>
                                                <?php echo noh( $this->Form->text( 'menu_detail[' . $key . '][equipment_name]', [ 'label' => false, 'placeholder' => 'マニュアルの機器名を入力', 'default' => $val['equipment_name'] ] ) ); ?>
                                                <?php  if( isset( $val['id'] ) ) echo noh( $this->Form->hidden( 'menu_detail[' . $key . '][id]', [ 'value' => $val['id'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>カテゴリー</th>
                                            <td>
                                                <?php echo noh( $this->Form->select( 'menu_detail[' . $key . '][category_id]', $manual_categories, [ 'label' => false, 'empty' => '選択してください', 'default' => $val['category_id'] ] ) ); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>マニュアル(PDF)</th>
                                            <td class="file">
                                                <?php if( isset( $contractant_service_menus['uploaded'][$key] ) ): // 登録されたサムネを表示 ?>
                                                   <?php if( isset( $contractant_service_menus['uploaded'][$key]['id'] ) ) echo $this->Form->hidden( 'uploaded[' . $key . '][id]',        [ 'value' => $contractant_service_menus['uploaded'][$key]['id'] ] ); ?>
                                                   <?php echo $this->Form->hidden( 'uploaded[' . $key . '][file_path]', [ 'value' => $contractant_service_menus['uploaded'][$key]['file_path'] ] ); //確認画面用 ?>
                                                   <?php echo $this->Form->hidden( 'uploaded[' . $key . '][name]',      [ 'value' => $contractant_service_menus['uploaded'][$key]['name'] ] ); //確認画面用 ?>
                                                <?php endif; ?>
                                                <label for="file[<?php echo h( $key ) ; ?>]" class="btn select_file">PDFを選択する</label>
                                                <span class="file_name"><?php if( isset( $contractant_service_menus['uploaded'][$key] ) ) echo noh( '<a href="' . DS . $contractant_service_menus['uploaded'][$key]['file_path'] . '" target="_blank"><span class="far fa-file-pdf"></span>' . $contractant_service_menus['uploaded'][$key]['name'] . '</a>'); ?></span>
                                                <?php echo noh( $this->Form->file( 'menu_detail[' . $key . '][files][pdf]', [ 'id' => 'file[' . $key . ']', 'label' => false, 'accept' => 'application/pdf' ] ) ); ?>
                                            </td>
                                        </tr>
                                    </table>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <table class="inner_table" id="index[0]">
                                    <tr>
                                        <th class="function" rowspan="3">
                                            <i class="fas fa-times-circle delete_table" data-index="0"></i>
                                        </th>
                                        <th>機器名</th>
                                        <td>
                                            <?php echo noh( $this->Form->text( 'menu_detail[0][equipment_name]', [ 'label' => false, 'placeholder' => 'マニュアルの機器名を入力' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>マニュアル(PDF)</th>
                                        <td class="file">
                                            <label for="file[0]" class="btn select_file">PDFを選択する</label>
                                            <span class="file_name"><?php //echo h( $contractant_service_menus->images[0]['name'] ); ?></span>
                                            <?php echo noh( $this->Form->file( 'menu_detail[0][files][pdf]', [ 'id' => 'file[0]', 'label' => false, 'accept' => 'application/pdf' ] ) ); ?>
                                        </td>
                                    </tr>
                                </table>
                            <?php endif;  ?>
                            <div class="text_r" style="margin-top: 10px;">
                                <button type="button" class="btn add_table" tabIndex="-1">マニュアルを追加</button>
                            </div>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 9: // アンケート ?>
                    <?Php /*
                    <tr>
                        <th colspan="2"> アンケート一覧 </th>
                    </tr>
                    */ ?>

                    <tr>
                        <td colspan="2" class="inner_table_wrap">
                            <?php /*
                            <table class="inner_table">
                                <tr>
                                    <th>管理ID</th>
                                    <th>アンケートタイトル</th>
                                    <th>公開日</th>
                                    <th>公開終了日</th>
                                    <th>表示フラグ</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php if( count( $contractant_service_menus->menu_detail ) > 0  ): ?>
                                    <?php foreach( $contractant_service_menus->menu_detail as $val ): ?>
                                        <tr>
                                            <td> <?php echo noh( $val->id ); ?> </td>
                                            <td> <?php echo h( $val->title ); ?> </td>
                                            <td> <?php echo h( $val->release_date ); ?> </td>
                                            <td> <?php if( $val->close_date) echo h( $val->close_date ); ?> </td>
                                            <td> <?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?> </td>
                                            <td>
                                                <a href="<?php echo noh( $this->Url->build( [ 'controller' => 'Enquetes', 'action' => 'edit',   $contractant_service_menus->id, $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                                                <a href="<?php echo noh( $this->Url->build( [ 'controller' => 'Enquetes', 'action' => 'delete', $contractant_service_menus->id, $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                            
                                <?php endif;  ?>
                            </table>
                            */ ?>
                            <div class="text_c" style="margin-top: 10px;">
                                <a href="<?php echo noh( $this->Url->build([ 'controller' => 'Enquetes', 'action' => 'index', $contractant_service_menus['id'] ]) ); ?>" class="btn" tabIndex="-1">アンケート管理へ</a>
                            </div>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 10: // チャット ?>

                    <?php break; ?>
                <?php case 11: // リンクタイプ ?>

                    <tr>
                        <th colspan="2"> 登録リンク </th>
                    </tr>

                    <tr>
                        <td colspan="2" class="inner_table_wrap">
                            <table class="inner_table" id="index[0]">
                                <tr>
                                    <th>リンクURL</th>
                                    <td>
                                        <?php echo noh( $this->Form->text( 'menu_detail[0][link_url]', [ 'label' => false, 'placeholder' => 'リンクURLを入力', 'default' => ( isset( $contractant_service_menus['menu_detail'][0] ) ) ? $contractant_service_menus['menu_detail'][0]->link_url : null ] ) ); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php break; ?>
                <?php case 11: // リンクタイプ ?>
                    <?php break; ?>
                <?php case 12: // postタイプ ?>
                    <tr>
                        <td colspan="2">
                            <div class="text_l" style="margin-top: 10px;">
                                <a href="<?php echo noh( $this->Url->build([ 'controller' => 'PostTypes', 'action' => 'category', $contractant_service_menus['id'] ]) ); ?>" class="btn" tabIndex="-1">ポストカテゴリを登録</a>
                            </div>
                        </td>
                    </tr>
                <?php default: ?>
                <?php endswitch; ?>
            </table>
        </div>
        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
