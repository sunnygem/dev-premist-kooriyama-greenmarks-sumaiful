<div class="service_menu">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo( $title_for_layout ); ?> </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="sub_title clearfix">
        <h2 class="left">アンケート 作成確認</h2>
    </div>

    <div class="form_table">
        <table>
            <tr>
                <th>タイトル</th>
                <td><?php echo noh( $menu_enquete['title'] ); ?></td>
            </tr>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $menu_enquete['display_flg'] === '1' ) ? '表示' : '非表示' ); ?></td>
            </tr>

            <tr>
                <th>公開日</th>
                <td><?php echo noh( $menu_enquete[ 'release_date'] ); ?></td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td><?php echo noh( $menu_enquete[ 'close_date' ] ); ?></td>
            </tr>
        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'editEnquete', $service_menu_id ] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>

        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
