<div class="posts">
    <div class="clearfix contents_title">
        <h2 class="left">カテゴリー管理</h2>
        <div class="right"> </div>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="form_table">
        <?php echo noh( $this->Form->create( $data, ['novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off'] ) ); ?>
            <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id' ) ); ?>
            <?php echo noh( $this->Form->hidden( 'contractant_service_menu_id', [ 'value' => $service_menu_id ] ) ); ?>
            <?php if( $this->Form->isFieldError( 'contractant_service_menu_id' ) ) echo noh( $this->Form->error( 'contractant_service_menu_id' ) ); ?>
            <table>
                <tr>
                    <th colspan="2"><?php echo h( $service_menu->label ); ?>のカテゴリー設定</th>
                </tr>
                <tr>
                    <th>カテゴリ名</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', ['placeholder' => 'カテゴリ名を入力'] ) ); ?>
                        <?php if( $this->Form->isFieldError( 'name' ) ) echo noh( $this->Form->error( 'name' ) ); ?>
                    </td>
                </tr>
                <tr>
                    <th>アイコン画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $data['icon_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh( '<img src="/' . $data['icon_path'] . '">'); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="icon_path" class="btn select_file">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files.icon_path', [ 'id' => 'icon_path', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>
                </tr>
                <tr>
                    <th>表示</th>
                    <td>
                        <?php echo noh( $this->Form->radio( 'display_flg', [ '1' => '表示', '0' => '非表示'], [ 'default' => '1' ]  ) ); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="text_c">
                        <?php echo noh( $this->Form->button( '登録',  [ 'name' => 'complete', 'class' => 'btn', 'tabIndex' => '-1' ] ) ); ?>
                    </td>
                </tr>

            </table>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>カテゴリ名</th>
                <th>アイコン</th>
                <th>表示</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $category as $key => $val ): ?>
                <tr>
                    <td class="title"><?php echo h( $val->id ); ?></td>
                    <td class=""><?php echo h( $val->name ); ?></td>
                    <td class="text_c preview"><?php echo noh( ( $val->icon_path ) ? '<img src="/'. $val->icon_path.'">' : 'なし' ); ?></td>
                    <td class=""><?php echo noh( ( $val->display_flg ) ? '表示' : '非表示' ); ?></td>
                    <td class="">
                        <?php if( $val->id === $data->id ): ?>
                            <a class="btn " disabled="disabled"><i class="fa fa-edit"></i> 編集中</a>
                        <?php else: ?>
                            <a href="<?php echo noh( $this->Url->build( [ $val->contractant_service_menu_id, $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <?php endif; ?>

                        <a href="<?php echo noh( $this->Url->build( ['action' => 'categoryDelete', $val->contractant_service_menu_id, $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>

    <div class="text_c btn_area">
        <a href="<?php echo noh( $this->Url->build([ 'controller' => 'ServiceMenus', 'action' => 'edit', $service_menu_id] ) ); ?>" class="btn btn_back">戻る</a>
    </div>
</div>
