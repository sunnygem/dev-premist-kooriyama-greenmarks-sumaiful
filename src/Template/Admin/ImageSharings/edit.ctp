<div class="image_sharing">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $data->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $data->id ] ) ); ?>
        <?php if( $login_user['admin_user']['building_id'] !== 0 ): ?>
            <?php echo noh( $this->Form->hidden( 'all_building_flg', [ 'value' => 0 ] ) ); ?>
            <?php if( isset( $data->id ) ): ?>
                <?php foreach( $data->buildings as $val ): ?>
                    <?php echo noh( $this->Form->hidden( 'buildings[]', [ 'value' => $val ] ) ); ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?php echo noh( $this->Form->hidden( 'buildings[]', [ 'value' => $login_user['admin_user']['building_id'] ] ) ); ?>
            <?php endif; ?>
        <?php endif; ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'タイトルを入力' ] ) ); ?>
                        <?php if( isset( $error['title'] ) ) echo noh( '<p class="error">' . $error['title'] . '</p>' ); ?>
                    </td>
                </tr>


                <tr>
                    <th>公開日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker'] ) ); ?>
                        <?php if( isset( $error['release_date'] ) ) echo noh( '<p class="error">' . $error['release_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開終了日</th>
                    <td><?php echo noh( $this->Form->text( 'close_date',  [ 'placeholder' => '公開終了日を設定', 'class' => 'datepicker']) ); ?></td>
                </tr>

                <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
                <tr>
                    <th>公開範囲</th>
                    <td>
                        <div class="inline" data-exclusive-check="all_building_flg">
                            <div class="checkbox">
                                <?php echo noh( $this->Form->checkbox('all_building_flg', [ 'id' => 'all_building_flg' ] ) ); ?> 
                                <label for="all_building_flg">全て</label>
                            </div>
                            <?php echo noh( $this->Form->select( 'buildings', $mt_buildings, [ 'multiple' => 'checkbox' ] ) ); ?>
                        </div>
                        <?php if( isset( $error['buildings'] ) ) echo noh( '<p class="error">' . $error['buildings'] . '</p>' ); ?>
                    </td>
                </tr>
                <?php endif; ?>

            </table>
        </div>
        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
