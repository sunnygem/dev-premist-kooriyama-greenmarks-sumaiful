<div class="image_sharing">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo noh( $title_for_layout ); ?>
        </h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> アルバム新規作成</a>
        </div>
        <?php endif; ?>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>タイトル</th>
                <th>公開対象物件</th>
                <th>表示</th>
                <th>公開日</th>
                <th>公開終了日</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class="title"><?php echo h( $val->id ); ?></td>
                    <td class="title"><?php echo h( $val->title ); ?></td>
                    <td class="">
                        <?php if( $val->all_building_flg === 1 ): ?>
                            全建物
                        <?php else: ?>
                            <?php foreach( $val->image_sharing_buildings as $val2 ): ?>
                                <?php echo h( $val2->building->name ); ?><br>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                    <td class="title"><?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?></td>
                    <td class="title"><?php echo h( $val->release_date->format('Y/m/d') ); ?></td>
                    <td class="title"><?php echo ( $val->close_date ) ? h( $val->close_date->format('Y/m/d') ) : '--'; ?></td>
                    <td class="">
                        <?php if( in_array( $login_user['admin_user']['authority'], [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',      $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> アルバムの編集</a>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'imageEdit', $val->id ]) ); ?>" class="btn"><i class="fa fa-images"></i> 画像登録</a>
                            <a data-action="<?php echo noh( $this->Url->build( ['action' => 'deleteAlbum', $val->id ]) ); ?>" class="btn btn_delete jsConfirm" tabIndex="-1"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
