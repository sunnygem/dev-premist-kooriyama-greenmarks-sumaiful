<div class="image_sharing">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $data->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $data['display_flg'] === '1' ) ? '表示する' : '表示しない' ); ?></td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td><?php echo h( $data['title'] ); ?></td>
            </tr>

            <tr>
                <th>公開日</th>
                <td><?php echo h( $data['release_date'] ); ?></td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td><?php echo noh( ( $data['close_date'] ) ? $data['close_date'] : '設定なし' ); ?></td>
            </tr>
            <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
            <tr>
                <th>公開範囲物件</th>
                <td>
                    <?php if( $data['buildings'] === '' ): ?>
                        全公開
                    <?php else: ?>
                        <?php foreach( $data['buildings'] as $val): ?>
                            <p><?php echo noh( $mt_buildings[$val] ); ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div class="text_c">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
