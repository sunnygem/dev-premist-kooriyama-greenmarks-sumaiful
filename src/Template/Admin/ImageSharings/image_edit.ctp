<?php echo noh( $this->Html->script( 'picture', [ 'block' => true ] ) ); ?>
<div class="image_sharing">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo noh( $title_for_layout ); ?><?php if( isset( $data->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( null, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'image_sharing_id', [ 'value' => $data->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo noh( $data->title ); ?>
                    </td>
                </tr>

                <tr>
                    <th>画像
                    </th>
                    <td class="file">
                        <ul id="image_area"> </ul>
                        <label for="image_file" class="btn">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files[image][]', [ 'id' => 'image_file', 'label' => false, 'multiple' => true ] ) ); ?>
                    </td>
                </tr>


            </table>
        </div>

        <p>登録画像</p>
        <div class="dl_table">
            <dl class="">
                <!-- <dt class="">順番</dt> -->
                <dt class="">状態</dt>
                <dt class="">画像</dt>
                <dt class="">操作</dt>
            </dl>
            <?php foreach ( $images as $key => $val ): ?>
                <dl class="">
                    <!--
                    <dd 
                        <?php echo h( $val->orderby ); ?>
                    </dd>
                    -->
                    <dd class="">
                        <?php echo ( $val->file->display_flg === 1) ? '表示中': '非表示'; ?>
                    </dd>
                    <dd class="image">
                        <img src="<?php echo h( DS . $val->file->file_path ); ?>" alt="<?php echo h( $val->file->name) ; ?>" class="list_image">
                    </dd>
                    <dd class="">
                        <a href="javascript: void(0);" class="btn btn_delete jsConfirm" tabIndex="-1" data-action="<?php echo noh( $this->Url->build( ['action' => 'imageDelete', $val->id  ]) ); ?>"><i class="fa fa-times"></i> 削除</a>
                    </dd>
                </dl>
            <?php endforeach; ?>
        </div>
        <div class="text_c btn_area">
            <?php echo noh( $this->Html->link( '戻る', ['action' => 'index'], [ 'label' => false, 'name' => 'complete', 'class' => 'btn btn_back' ] ) ); ?>
            <?php echo noh( $this->Form->button( '画像の登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        </div>

    <?php echo noh( $this->Form->end() ); ?>
</div>
