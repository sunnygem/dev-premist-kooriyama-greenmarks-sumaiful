<div class="line_open_chat">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo  noh( $title_for_layout ); ?> <?php if( isset( $line_open_chat->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $line_open_chat['display_flg'] === '1' ) ? '表示する' : '表示しない' ); ?></td>
            </tr>

            <tr>
                <th>タイトル</th>
                <td><?php echo h( $line_open_chat['title'] ); ?></td>
            </tr>
            <tr>
                <th>タイトル(英語)</th>
                <td><?php echo h( $line_open_chat['title_en'] ); ?></td>
            </tr>


            <?php /*
            <tr>
                <th>キャプション</th>
                <td><?php echo h( $line_open_chat['caption'] ); ?></td>
            </tr>
            */ ?>

            <tr>
                <th>URL</th>
                <td><?php echo h( $line_open_chat['url'] ); ?></td>
            </tr>

            <tr>
                <th>アイコン画像</th>
                <td class="file">
                    <div class="preview">
                        <?php if( isset( $line_open_chat['files']['icon_url_path']['file_path'] ) ): // 登録されたサムネを表示 ?>
                           <?php echo noh( '<img src="/' . $line_open_chat['files']['icon_url_path']['file_path'] . '">'); ?>
                        <?php else: ?>
                            no image
                        <?php endif; ?>
                    </div><!--/.preview -->
                </td>
            </tr>

            <tr>
                <th>カテゴリ</th>
                <td><?php echo h( $categories[$line_open_chat['line_open_chat_category_id']] ); ?></td>
            </tr>
 
            <tr>
                <th>おすすめフラグ</th>
                <td><?php echo noh( ( $line_open_chat['pickup_flg'] === '1' ) ? '有効にする' : '有効にしない' ); ?></td>
            </tr>
            <tr>
                <th>公開日</th>
                <td><?php echo h( $line_open_chat['release_date'] ); ?></td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td><?php echo noh($line_open_chat['close_date']) ? h( $line_open_chat['close_date'] ) : '設定なし'; ?></td>
            </tr>

        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
