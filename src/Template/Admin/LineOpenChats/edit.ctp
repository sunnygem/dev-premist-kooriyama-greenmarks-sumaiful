<div class="line_open_chat">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> <?php if( isset( $line_open_chat->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $line_open_chat, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $line_open_chat->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $line_open_chat->id ] ) ); ?>
        <div class="form_table">
           <table>
                <tr>
                    <th><span class="required">*</span>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 1 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th><span class="required">*</span>タイトル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'タイトルを入力' ] ) ); ?>
                        <?php if( isset( $error['title'] ) ) echo noh( '<p class="error">' . $error['title'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>タイトル(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title_en', [ 'label' => false, 'placeholder' => 'タイトル(英語)を入力' ] ) ); ?>
                        <?php if( isset( $error['title_en'] ) ) echo noh( '<p class="error">' . $error['title_en'] . '</p>' ); ?>
                    </td>
                </tr>

                <?php /*
                <tr>
                    <th>キャプション</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'caption',  [ 'placeholder' => 'テキストを入力']) ); ?>
                        <?php if( isset( $error['caption'] ) ) echo noh( '<p class="error">' . $error['caption'] . '</p>' ); ?>
                    </td>
                </tr>
                */ ?>

                <tr>
                    <th><span class="required">*</span>URL</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'url',  [ 'placeholder' => 'URLを入力']) ); ?>
                        <?php if( isset( $error['url'] ) ) echo noh( '<p class="error">' . $error['url'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $line_open_chat['icon_url_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $line_open_chat['icon_url_path'] . '">' ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <span class="file_name"><?php if( isset(( $contents['files']['icon_url_path'] ) ) ) echo h( $contents['files']['icon_url_path']['name'] ); ?></span>
                        <?php echo noh( $this->Form->file( 'files[icon_url_path]', [ 'id' => 'image_file', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>

                </tr>

                <tr>
                    <th><span class="required">*</span>カテゴリ</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'line_open_chat_category_id', $categories, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['line_open_chat_category_id'] ) ) echo noh( '<p class="error">' . $error['line_open_chat_category_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>おすすめフラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'pickup_flg',  [ 'id' => 'pickup_flg', 'default' => 0 ]) ); ?>
                            <label for="pickup_flg">有効にする</label>
                        </div>
                        <p>※チェックを入れるとタグの表示がおすすめに代わります。</p>
                    </td>
                </tr>


                <tr>
                    <th><span class="required">*</span>公開日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker', 'placeholder' => '公開開始日を設定'] ) ); ?>
                        <?php if( isset( $error['release_date'] ) ) echo noh( '<p class="error">' . $error['release_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開終了日</th>
                    <td><?php echo noh( $this->Form->text( 'close_date',  [ 'placeholder' => '公開終了日を設定', 'class' => 'datepicker']) ); ?></td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
