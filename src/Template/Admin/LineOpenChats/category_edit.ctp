<div class="line_open_chat">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= noh( $title_for_layout ); ?> <?php if( isset( $line_open_chat_category->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $line_open_chat_category, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $line_open_chat_category->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $line_open_chat_category->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 1 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>カテゴリ名</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => 'カテゴリ名を入力' ] ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>カテゴリ名(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name_en', [ 'label' => false, 'placeholder' => 'カテゴリ名(英語)を入力' ] ) ); ?>
                        <?php if( isset( $error['name_en'] ) ) echo noh( '<p class="error">' . $error['name_en'] . '</p>' ); ?>
                    </td>
                </tr>


                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $line_open_chat_category['icon_url_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $line_open_chat_category['icon_url_path'] . '">' ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <span class="file_name"><?php if( isset(( $contents['files']['icon_url_path'] ) ) ) echo h( $contents['files']['icon_url_path']['name'] ); ?></span>
                        <?php echo noh( $this->Form->file( 'files[icon_url_path]', [ 'id' => 'image_file', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>

                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
