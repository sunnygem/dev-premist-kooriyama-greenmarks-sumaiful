<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'categoryEdit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <table>
            <tr>
                <?php // <th>並び順</th> ?>
                <th>カテゴリ名</th>
                <th>表示フラグ</th>
                <th>登録数</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <?php /* <td class=""><?php echo number_format( $val->orderby ); ?></td> 2021.01.18 仕様にないので、追加要望が出たら対応する */ ?>
                    <td class="">
                        <?php echo h( $val->name ); ?>
                        <?php if( $val->name_en ) echo noh( '<br>' . $val->name_en ); ?>
                    </td>
                    <td class=""><?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?></td>
                    <td class=""><?php echo number_format( $val->total_registered ); ?>件</td>
                    <td class="">
                        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'categoryEdit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                            <a data-action="<?php echo noh( $this->Url->build( ['action' => 'categoryDelete', $val->id ]) ); ?>" class="btn btn_delete jsConfirm" tabIndex="-1"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

</div>
