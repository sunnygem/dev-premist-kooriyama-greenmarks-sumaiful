<div class="login_box">
    <div class="text_c logo">
        <h1><img src="/<?php echo noh( $this->request->Session()->read('ContractantData.file_path') ); ?>" alt="<?php echo h( $app_name ) . ' 管理画面' ; ?>"></h1>
        <h2 style="font-size:1.2rem; font-weight:bold;"><?php echo h( $app_name ); ?><br>管理画面</h2>
    </div>
    <?php echo $this->Form->create( null, [ 'class' => 'form-signin' ] ); ?>
        <label for="login_id">ログインID</label>
        <?php echo $this->Form->input( 'login_id', [ 'type' => 'text', 'label' => false ] ); ?>

        <label for="password">パスワード</label>
        <?php echo $this->Form->input( 'password', [ 'type' => 'password', 'label' => false ] ); ?>

        <?php echo $this->Flash->render(); ?>

        <div class="checkbox">
            <?php echo $this->Form->checkbox('auto_login', ['id' => 'auto_login', 'default' => '' ] );?>
            <label for="auto_login">ログイン状態を保持する</label>
        </div>

        <?php echo $this->Form->submit( 'ログイン', [ 'class' => 'btn btn-large btn-success' ] ); ?>
        <?php  if ( ENV === 'dev' || ENV === 'test' ): ?>
            <p> <?php echo h( $this->request->Session()->read( 'ContractantData.unique_parameter' ) ); ?>_admin / admin1234 </p>
        <?php endif;  ?>
    <?php echo $this->Form->end(); ?>
    <?php /*
    <p><?php echo $this->Html->link('ログイン期間承認', ['action' => 'requestApproval']); ?></p>
    */ ?>
    <p><?php echo $this->Html->link('パスワードを忘れた方はこちら', ['action' => 'passwordRemind']); ?></p>
</div><!--/ login_box -->
