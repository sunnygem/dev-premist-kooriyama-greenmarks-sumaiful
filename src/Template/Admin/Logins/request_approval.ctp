<div class="login_box">
    <div class="text_c">
        <?php echo $this->Html->image( 'system/logo.png', [ 'alt' => $app_name . ' 管理画面' ] ); ?>
    </div>
    <h2 class="login_title"><?php echo noh( $title_for_layout ); ?></h2>
    <?php echo $this->Form->create( null, [ 'class' => 'form-signin' ] ); ?>
        <label>ログインID</label>
        <?php echo $this->Form->text( 'login_id', [ 'label' => false ] ); ?>
        <?php if( isset( $error['login_id'] ) ) echo noh( '<p class="error">' . $error['login_id'] . '</p>' ); ?>
        <label>承認者</label>
        <?php echo $this->Form->select( 'auth_user_id', $auth_users, [ 'label' => false, 'empty' => '選択してください' ] ); ?>
        <?php if( isset( $error['auth_user_id'] ) ) echo noh( '<p class="error">' . $error['auth_user_id'] . '</p>' ); ?>
        <label>期間</label>
        <?php echo $this->Form->input( 'login_approval_date', [ 'label' => false, 'class' => 'datepicker' ] ); ?>
        <?php if( isset( $error['login_approval_date'] ) ) echo noh( '<p class="error">' . $error['login_approval_date'] . '</p>' ); ?>

        <?php echo $this->Flash->render(); ?>

        <?php echo $this->Form->submit( '承認リクエスト', [ 'name' => 'approve', 'class' => 'btn btn-large btn-success' ] ); ?>
    <?php echo $this->Form->end(); ?>
</div><!--/.login_box -->
