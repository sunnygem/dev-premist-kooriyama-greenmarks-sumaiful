<div class="login_box">
    <div class="text_c">
        <img src="/<?php echo noh( $this->request->Session()->read('ContractantData.file_path') ); ?>" alt="<?php echo h( $app_name ) . ' 管理画面' ; ?>">
    </div>
    <h2 class="login_title"><?php echo h( $title_for_layout ); ?></h2>
    <p>登録しているEメールアドレス入力してください。<p>
    <?php echo $this->Form->create( null, [ 'class' => 'form-signin' ] ); ?>
        <label>Eメール</label>
        <?php echo $this->Form->text( 'email', [ 'label' => false ] ); ?>

        <?php echo $this->Flash->render(); ?>

        <?php echo $this->Form->submit( '送信', [ 'class' => 'btn btn-large btn-success' ] ); ?>
    <?php echo $this->Form->end(); ?>
</div><!--/.login_box -->
