<div class="login_box">
    <div class="text_c">
        <?php echo $this->Html->image( 'system/logo.png', [ 'alt' => $app_name . ' 管理画面' ] ); ?>
    </div>
    <h2 class="login_title"><?php echo h( $title_for_layout ); ?></h2>
    <?php echo $this->Form->create( null, [ 'class' => 'form-signin' ] ); ?>
        <label>ログインID</label>
        <?php echo $this->Form->text( 'login_id', [ 'label' => false ] ); ?>
        <?php if( isset( $error['login_id'] ) ) echo noh( '<p class="error">' . $error['login_id'] . '</p>' ); ?>
        <label>現在のパスワード</label>
        <?php echo $this->Form->input( 'pre_password', ['type' => 'password', 'label' => false ] ); ?>
        <?php if( isset( $error['pre_password'] ) ) echo noh( '<p class="error">' . $error['pre_password'] . '</p>' ); ?>

        <label>新しいパスワード</label>
        <?php echo $this->Form->input( 'password', [ 'type' => 'password', 'label' => false ] ); ?>
        <?php if( isset( $error['password'] ) ) echo noh( '<p class="error">' . $error['password'] . '</p>' ); ?>
        <label>新しいパスワード(確認用)</label>
        <?php echo $this->Form->input( 'password_confirm', [ 'type' => 'password', 'label' => false ] ); ?>
        <?php if( isset( $error['password_confirm'] ) ) echo noh( '<p class="error">' . $error['password_confirm'] . '</p>' ); ?>

        <?php echo $this->Flash->render(); ?>

        <?php echo $this->Form->submit( '再設定', [ 'class' => 'btn btn-large btn-success' ] ); ?>
    <?php echo $this->Form->end(); ?>
</div><!--/.login_box -->
