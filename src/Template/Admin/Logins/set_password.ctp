<div class="login_box">
    <div class="text_c">
        <img src="/<?php echo noh( $this->request->Session()->read('ContractantData.file_path') ); ?>" alt="<?php echo h( $app_name ) . ' 管理画面' ; ?>">
    </div>
    <h2 class="login_title"><?php echo h( $title_for_layout ); ?></h2>
    <?php echo $this->Form->create( null, [ 'class' => 'form-signin' ] ); ?>
        <label>パスワード</label>
        <?php echo $this->Form->input( 'password', [ 'type' => 'password', 'label' => false ] ); ?>
        <label>パスワード(確認用)</label>
        <?php echo $this->Form->input( 'password_confirm', [ 'type' => 'password', 'label' => false ] ); ?>

        <?php echo $this->Flash->render(); ?>

        <?php echo $this->Form->submit( '設定', [ 'class' => 'btn btn-large btn-success' ] ); ?>
    <?php echo $this->Form->end(); ?>
</div><!--/.login_box -->
