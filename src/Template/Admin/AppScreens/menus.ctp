<div class="app_screen">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo h( $title_for_layout ); ?> </h2>
    </div>

    <div class="sub_title clearfix">
        <h2 class="left">メニュー並び替え-<?php echo h( $footer->label ); ?></h2>
        <div class="right"> 
        </div>
    </div>

    <?php echo noh( $this->Form->create( null ) ); ?>
        <div class="menu_wrap">
            <div class="list_table">
                <p class="note">・ドラッグして並び替えてください</p>
                <table class="selected">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>メニュー</th>
                        </tr> 
                    </thead>
                    <tbody class="sort">
                    <?php foreach( $selected_service_menus as $val ): ?>
                        <tr>
                            <td>
                                <i class="fas fa-bars handle"></i>
                                <?php echo noh( $this->Form->hidden( 'id', ['value' => $val->id ] ) ); ?>
                            </td>
                            <td> <?php echo h( $val->label ); ?> </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button('登録', [ 'class' => 'btn submit order_confirm' ]) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>

</div>
