<div class="app_screen">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo h( $title_for_layout ); ?> </h2>
        <div class="right"> 
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <div class="list_table">
        <table>
            <tr>
                <th>ラベル名</th>
                <th>割りあてメニュー</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $screens as $val ): ?>
                <tr>
                    <td><?php echo h( $val->label ); ?></td>
                    <td>
                        <?php if( $val->contractant_service_menus ): ?>
                            <ul>
                            <?php foreach( $val->contractant_service_menus as $val2 ): ?>
                                <li><?php echo h( $val2->label ); ?></li>
                            <?php endforeach; ?>
                            </ul>
                        <?php else: ?>
                            なし
                        <?php endif; ?>
                    </td>
                    <td class="btn_area">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',  $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> ラベル変更</a>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'menus', $val->id ]) ); ?>" class="btn"><i class="fa fa-list"></i> メニュー並び替え</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

</div>
