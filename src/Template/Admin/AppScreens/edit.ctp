<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            画面管理<?php if( isset( $data->id ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $data->id ] ) ); ?>
        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'orderby', [ 'value' => $data->orderby ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>ラベル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'label', [ 'label' => false, 'placeholder' => 'ラベル' ] ) ); ?>
                        <?php if( isset( $error['label'] ) ) echo noh( '<p class="error">' . $error['label'] . '</p>' ); ?>
                    </td>
                </tr>


            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
