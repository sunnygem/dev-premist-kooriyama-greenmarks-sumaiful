<section class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $data['id'] ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>Question</th>
                    <td><?php echo h( $faq['faqs_question_id'] ); ?></td>
            </tr> 
            <tr>
                <th>Answer</th>
                <td><?php echo h( $faq['answer'] ); ?></td>
            </tr>
            <tr>
                <th>Answer(英語)</th>
                <td><?php echo h( $faq['answer_en'] ); ?></td>
            </tr>
            
            <tr>
                <th>画像</th>
                <td class="file">
                    <div class="preview">
                        <?php if( isset( $faq['files']['icon_url_path']['file_path'] ) ): // 登録されたサムネを表示 ?>
                           <?php echo noh( '<img src="/' . $faq['files']['icon_url_path']['file_path'] . '">'); ?>
                        <?php else: ?>
                            no image
                        <?php endif; ?>
                    </div><!--/.preview -->
                </td>
            </tr>

        </table>
    </div>

    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</section>
