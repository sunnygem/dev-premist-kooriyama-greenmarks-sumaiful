<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout); ?></h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $faq, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data['id'] ) ) echo noh( $this->Form->hidden( 'id', [] ) ); ?>
        <div class="form_table">
           <table>
                <tr>
                    <th><span class="required">*</span>カテゴリ</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'faq_category_id', $mt_faq_category, [ 'class' => 'mt_fag_category', 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['faq_category_id'] ) ) echo noh( '<p class="error">' . $error['faq_category_id'] . '</p>' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>Question</th>
                    <td>
                        <select name="faqs_question_id" class="questions">
                            <option value="">選択してください</option>
                            <?php foreach ( $category as $key => $val ): ?>
                            <option value="<?php echo noh ( $val->id ); ?>" data-val="<?php echo noh ( $val->faq_category_id );?>"><?php echo noh( $val->question ); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr> 
                <tr>
                    <th>Answer</th>
                    <td>
                        <?php echo noh( $this->Form->textarea( 'answer', [ 'label' => false ] ) ); ?>
                        <?php if( isset( $error['answer'] ) ) echo noh( '<p class="error">' . $error['answer'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>Answer(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->textarea( 'answer_en', [ 'label' => false ] ) ); ?>
                        <?php if( isset( $error['answer_en'] ) ) echo noh( '<p class="error">' . $error['answer_en'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $faq['icon_url_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh(  '<img src="' . DS . $faq['icon_url_path'] . '">' ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files[icon_url_path]', [ 'id' => 'image_file', 'label' => false, 'accept' => 'image/*' ] ) ); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text_c brn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
<script>
$(function() {
    var questions = $('.questions');
    var category_questions = questions.html();
    $('.mt_fag_category').change(function() {
        var val1 = $(this).val();
        questions.html(category_questions).find('option').each(function() {
            var val2 = $(this).data('val');
            if (val1 != val2)
            {
                $(this).remove();
            }
        });
        
        if ($(this).val() === '')
        {
            questions.attr('disabled', 'disabled');
        }
        else
        {
            questions.removeAttr('disabled');
        }
    });
});
</script>
