<div class="fag">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo noh( $title_for_layout ); ?></h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
        <div class="right"> 
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>
    

    <div class="list_table">
        <table style="">
            <tr>
                <th>category</th>
                <th>Question</th>
                <th>Question(英語)</th>
                <th>Answer</th>
                <th>Answer(英語)</th>
                <th></th>
                <th></th>
            </tr>
            <?php if( empty ( $data ) === false ): ?>
            <?php foreach( $data as $key => $val ): ?>
            <tr>
                <td>
                    <?php foreach ( $mt_faq_category as $key1 => $val1 )
                    {
                       if (  $val->faq_category_id === (int)$key1 )
                       { 
                            echo ( $val1 );
                       }
                    }?>
                </td>
                <td><?php echo ( $val->question ); ?></td>
                <td><?php echo ( $val->question_en ); ?></td>
                <td>
                    <?php echo ( $val->answer ); ?>
                </td>
                <td><?php echo ( $val->answer_en ); ?></td>
                <td class="preview">
                <?php
                    if( $val->icon_url_path !== null )
                    {
                         echo $this->Html->image( DS . $val->icon_url_path , ['style' => 'width:30px; height:30px;']);
                    }
                    else
                    {
                         echo 'no_image';
                    }
                ?>
                </td>
                <td>
                    <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <a data-action="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete jsConfirm" tabIndex="-1"><i class="fa fa-times"></i> 削除</a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php endif;  ?>
        </table>
    </div>
</div>
