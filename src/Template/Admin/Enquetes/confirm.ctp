<div class="service_menu">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo( $title_for_layout ); ?> </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="sub_title clearfix">
        <h2 class="left">アンケート 作成確認</h2>
    </div>

    <div class="form_table">
        <table>
            <tr>
                <th>タイトル</th>
                <td><?php echo noh( $menu_enquete['title'] ); ?></td>
            </tr>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $menu_enquete['display_flg'] === '1' ) ? '表示' : '非表示' ); ?></td>
            </tr>

            <tr>
                <th>公開日</th>
                <td><?php echo noh( $menu_enquete[ 'release_date'] ); ?></td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td><?php echo noh( $menu_enquete[ 'close_date' ] ); ?></td>
            </tr>

            <tr>
                <th colspan="2"> 設問 </th>
            </tr>
            <tr>
                <td colspan="2" class="inner_table_wrap">
                    <?php if( count( $menu_enquete['enquete_questions'] ) > 0  ): ?>
                        <?php foreach( $menu_enquete['enquete_questions'] as $key => $val ): ?>
                            <table class="inner_table" id="index[<?php echo noh( $key ); ?>]">
                                <tr>
                                    <th>設問</th>
                                    <td><?php echo h( $val['question'] ); ?></td>
                                </tr>
                                <tr>
                                    <th>フォームタイプ</th>
                                    <td><?php echo noh( $mt_form_types[$val['type']] ); ?></td>
                                </tr>
                                <tr>
                                    <th>オプション</th>
                                    <td>
                                        <div class="item">プレースホルダー：<?php echo h( $val['placeholder'] ); ?></div>
                                        <div class="item">項目：<?php echo h( $val['item'] ); ?></div>
                                        <div class="item">複数選択：<?php echo noh( ( $val['multiple_flg'] === '1' ) ? '有効' : '無効' ); ?></div>
                                    </td>
                                </tr>
 
                            </table>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </td>
            </tr>

            

        </table>
    </div>
    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit' ] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>

        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
