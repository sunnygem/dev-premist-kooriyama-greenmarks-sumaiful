<div class="service_menu">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo( $title_for_layout ); ?> </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="sub_title clearfix">
        <h2 class="left">アンケート 作成</h2>
    </div>

    <?php echo noh( $this->Form->create( $menu_enquete, [ 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $menu_enquete->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $menu_enquete->id ] ) ); ?>
        <?php //echo noh( $this->Form->hidden( 'contractant_service_menu_id', [ 'value' => $menu_enquete->contractant_service_menu_id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'アンケートタイトルを入力' ] ) ); ?>
                        <?php if( isset( $error['title'] ) ) echo noh( '<p class="error">' .  $error['title'] . '</p>' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                        <?php if( isset( $error['display_flg'] ) ) echo noh( '<p class="error">' .  $error['display_flg'] . '</p>' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>公開日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_date', [ 'placeholder' => '公開日を入力', 'default' => date( 'Y/m/d' ), 'class' => 'datepicker' ] ) ); ?>
                        <?php if( isset( $error['release_date'] ) ) echo noh( '<p class="error">' .  $error['release_date'] . '</p>' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>公開終了日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'close_date',   [ 'placeholder' => '公開終了日を入力', 'class' => 'datepicker'  ] ) ); ?>
                        <?php if( isset( $error['close_date'] ) ) echo noh( '<p class="error">' .  $error['release_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th colspan="2"> 設問 </th>
                </tr>
                <tr>
                    <td colspan="2" class="inner_table_wrap">
                        <?php if( count( $menu_enquete->enquete_questions ) > 0  ): ?>
                            <?php foreach( $menu_enquete->enquete_questions as $key => $val ): ?>
                                <table class="inner_table" id="index[<?php echo noh( $key ); ?>]">
                                    <tr>
                                        <th class="function" rowspan="3">
                                            <i class="fas fa-times-circle delete_table" data-index="<?php echo noh( $key ); ?>"></i>
                                        </th>
                                        <th>設問内容</th>
                                        <td>
                                            <?php echo noh( $this->Form->textarea( 'enquete_questions.' . $key . '.question', [ 'label' => false, 'placeholder' => '設問内容を入力' ] ) ); ?>
                                            <?php  if( isset( $val['id'] ) ) echo noh( $this->Form->hidden( 'enquete_questions.' . $key . '.id' ) ); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>フォームタイプ</th>
                                        <td>
                                            <?php echo noh( $this->Form->select( 'enquete_questions.' . $key . '.type', $mt_form_types, [ 'label' => false, 'empty' => 'フォームタイプを選択' ] ) ); ?>
                                        </td>
                                    </tr>
                                    <tr class="form_option">
                                        <th>オプション</th>
                                        <td>
                                            <div class="flex item item_option">
                                                <div>選択項目</div>
                                                <div><?php echo noh( $this->Form->text( 'enquete_questions.' . $key . '.item', [ 'label' => false, 'placeholder' => '|(パイプ)で区切って入力' ] ) ); ?></div>
                                                <div class="checkbox  set_multiple">
                                                    <?php echo noh( $this->Form->hidden( 'enquete_questions.' . $key . '.multiple_flg', [ 'label' => false, 'value' => 0, 'class' => 'clone'  ] ) ); ?>
                                                    <?php echo noh( $this->Form->checkbox( 'enquete_questions.' . $key . '.multiple_flg', [ 'label' => false, 'id' => 'multiple['. $key . ']', 'hiddenField' => false ] ) ); ?>
                                                    <label for="multiple[<?php echo $key; ?>]">複数選択</label>
                                                </div>
                                            </div>
                                            <div class="flex item placeholder">
                                                <div>プレースホルダ―</div>
                                                <div><?php echo noh( $this->Form->text( 'enquete_questions.' . $key . '.placeholder', [ 'label' => false ] ) ); ?></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <table class="inner_table" id="index[0]">
                                <tr>
                                    <th class="function" rowspan="3">
                                        <i class="fas fa-times-circle delete_table" data-index="0"></i>
                                    </th>
                                    <th>設問内容</th>
                                    <td>
                                        <?php echo noh( $this->Form->textarea( 'enquete_questions.0.question', [ 'label' => false, 'placeholder' => '設問内容を入力' ] ) ); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>フォームタイプ</th>
                                    <td>
                                        <?php echo noh( $this->Form->select( 'enquete_questions.0.type', $mt_form_types, [ 'label' => false, 'empty' => 'フォームタイプを選択' ] ) ); ?>
                                    </td>
                                </tr>
                                <tr class="form_option">
                                    <th>オプション</th>
                                    <td>
                                        <div class="flex item item_option">
                                            <div>選択項目</div>
                                            <div><?php echo noh( $this->Form->text( 'enquete_questions.0.item', [ 'label' => false, 'placeholder' => '|(パイプ)で区切って入力' ] ) ); ?></div>
                                            <div class="checkbox set_multiple">
                                                <?php echo noh( $this->Form->hidden( 'enquete_questions.0.multiple_flg', [ 'label' => false, 'value' => 0, 'class' => 'clone'  ] ) ); ?>
                                                <?php echo noh( $this->Form->checkbox( 'enquete_questions.0.multiple_flg', [ 'label' => false, 'id' => 'multiple[0]', 'hiddenField' => false ] ) ); ?>
                                                <label for="multiple[0]">複数選択</label>
                                            </div>
                                        </div>
                                        <div class="flex item placeholder">
                                            <div>プレースホルダ―</div>
                                            <div><?php echo noh( $this->Form->text( 'enquete_questions.0.placeholder', [ 'label' => false ] ) ); ?></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        <?php endif;  ?>
                        <div class="text_r" style="margin-top: 10px;">
                            <button type="button" class="btn add_table" tabIndex="-1">設問を追加</button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
