<div class="enquete">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo( $title_for_layout ); ?></h2>
        <div class="right"> </div>
    </div>

    <div class="sub_title clearfix">
        <h2 class="left"><?php echo $enquete->title; ?></h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table class="inner_table">
            <tr>
                <th rowspan="2">回答日時</th>
                <th rowspan="2">IPアドレス</th>
                <th colspan="<?php echo count( $enquete->enquete_questions ); ?>" style="text-align: center; ">設問</th>
                <th rowspan="2">&nbsp;</th>
            </tr>
            <tr>
                <?php foreach( $enquete->enquete_questions as $val): ?>
                    <th><?php echo h( $val->question ); ?></th>
                <?php endforeach; ?>
            </tr>
            <?php if( $answers->isEmpty() === false  ): ?>
            <?php foreach( $answers as $val ): ?>
                <tr>
                    <td> <?php echo h( $val->created ); ?> </td>
                    <td> <?php echo h( $val->ip_address ); ?> </td>
                    <?php foreach( $enquete->enquete_questions as $val2 ): ?>
                        <td>
                            <?php foreach( $val->enquete_answer_items as $val3 ): ?>
                                <?php if( $val3->enquete_question_id === $val2->id ): ?>
                                    <?php echo ( $val3->answer ) ? h( $val3->answer ) : '--'; ?><br>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    <?php endforeach; ?>
                    <td>
                    </td>
                </tr>

            <?php endforeach; ?>
            <?php endif;  ?>
        </table>
    </div>

    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div></div><!-- /.enquete -->
