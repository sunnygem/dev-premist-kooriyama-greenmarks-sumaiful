<div class="enquete">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo( $title_for_layout ); ?> </h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( [ 'action' => 'edit', $service_menu_id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table class="inner_table">
            <tr>
                <th>管理ID</th>
                <th>アンケートタイトル</th>
                <th>状態</th>
                <th>公開日/公開終了日</th>
                <th>表示フラグ</th>
                <th>回答</th>
                <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
                <th>&nbsp;</th>
                <?php endif; ?>
            </tr>
            <?php if( count( $enquetes ) > 0  ): ?>
            <?php foreach( $enquetes as $val ): ?>
                <tr>
                    <td> <?php echo noh( $val->id ); ?> </td>
                    <td> <?php echo h( $val->title ); ?> </td>
                    <td>
                        <?php if( $val->close_date !== null && strtotime( $val->close_date ) <= time() ): ?>
                            終了
                        <?php elseif( strtotime( $val->release_date ) >= time() ): ?>
                            公開前
                        <?php else: ?>
                            公開中
                        <?php endif; ?>

                    </td>
                    <td> <?php echo h( $val->release_date ); ?><br>  <?php if( $val->close_date) echo h( $val->close_date ); ?> </td>
                    <td> <?php echo noh( ( $val->display_flg === 1 ) ? '表示' : '非表示' ); ?> </td>
                    <td>
                        <?php echo noh( $val->answer_count ); ?>件
                        <a href="<?php echo noh( $this->Url->build([ 'action' => 'answer', $val->id ]) ); ?>" class="btn"><i class="fa fa-list-ul"></i> 回答を見る</a>
                    </td>
                    <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
                    <td>
                        <a href="<?php echo noh( $this->Url->build( [ 'action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <a href="<?php echo noh( $this->Url->build( [ 'action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete jsConfirm"><i class="fa fa-times"></i> 削除</a>
                    </td>
                    <?php endif; ?>
                </tr>

            <?php endforeach; ?>
            <?php endif;  ?>
        </table>
    </div>

    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div></div><!-- /.enquete -->
