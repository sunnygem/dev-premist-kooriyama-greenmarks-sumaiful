<div class="front_user">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo noh( $title_for_layout ); ?> 入居者情報 </h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
            <div class="right"><a href="javascript: void(0)" class="btn jsConfirm" data-action="<?php echo noh( $this->Url->build(['action' => 'delete', $users->front_user->id ]) ); ?>" tabIndex="-1" ><i class="fa fa-times"></i>ユーザー 削除</a></div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>ユーザーID</th>
                <td><?php echo h( $users['front_user']['id'] ); ?></td>
            </tr>

            <tr>
                <th>名前</th>
                <td><?php echo h( $users['front_user']['name'] ); ?></td>
            </tr>
            <?php if ( isset( $users['front_user']['type'] ) ): ?>
            <tr>
                <th>タイプ</th>
                <td>
                <?php echo h( $mt_customer_type[$users['front_user']['type']] ); ?>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <th>住居</th>
                <td>
                    <?php echo h( $mt_buildings[$users['front_user']['building_id']] ); ?><br>
                    <?php echo h( $users['front_user']['room_number'] ); ?>
                </td>
            </tr>

            <tr>
                <th>ログインID</th>
                <td><?php echo h( $users['login_id'] ); ?></td>
            </tr>

            <tr>
                <th>ニックネーム</th>
                <td><?php echo noh( ( $users['front_user']['nickname'] ) ? $users['front_user']['nickname'] : '-' ); ?></td>
            </tr>

            <tr>
                <th>状態</th>
                <td>
                    <?php if( $users->lock_flg === 1 ): ?>
                    凍結中
                    <?php elseif( $users->front_user->leave_flg === 1 ): ?>
                    退去済み
                    <?php elseif( $users->front_user->reject_comment ): ?>
                    却下
                    <div class="memo">
                        <?php echo h( $users->front_user->reject_comment ); ?>
                    </div>
                    <?php elseif( $users->front_user->reapply_flg === 1 ): ?>
                    再申請
                    <?php elseif( $users->front_user->auth_flg === 1 ): ?>
                    承認済み
                    <?php if( $users['valid_date'] ) echo noh( '<br>承認日' . $users['valid_date']->format( 'Y年m月d日' ) ); ?>
                    <?php else: ?>
                    未承認
                    <?php endif; ?>
                </td>
            </tr>

            <tr>
                <th>プロフィール画像</th>
                <td>
                    <?php if( $users->front_user->thumbnail_path ): ?>
                        <img src="/<?php echo noh( $users->front_user->thumbnail_path ); ?>">
                    <?php else: ?>
                    no image
                    <?php endif; ?>
                </td>
            </tr>

            <tr>
                <th>利用端末OS</th>
                <td><?php echo noh( ( $users['front_user']['os'] ) ? $users['front_user']['os'] : '-' ); ?></td>
            </tr>
            <tr>
                <th>利用端末ID</th>
                <td><?php echo noh( ( $users['front_user']['device_token'] ) ? $users['front_user']['device_token'] : '-' ); ?></td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td><?php echo noh( ( $users['front_user']['created'] ) ?  $users['front_user']['created']->format('Y年m月d日 H:i:s') : '--' ); ?></td>
            </tr>


        </table>
    </div>

    <div class="text_c btn_area">
        <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
            <?php if( $users->front_user->auth_flg === 0 || $users->front_user->reapply_flg === 1 ): ?>
                <?php echo noh( $this->Form->create() ); ?>
                    <?php echo noh( $this->Form->hidden( 'id', [ 'value' => $users['id'] ] ) ); ?>
                    <?php echo noh( $this->Form->button( '承認', [ 'label' => false, 'name' => 'approve', 'class' => 'btn submit' ] ) ); ?>
                    <?php //echo noh( $this->Form->button( '却下', [ 'label' => false, 'name' => 'reject',  'class' => 'btn submit' ] ) ); ?>
                <?php echo noh( $this->Form->end() ); ?>
            <?php endif; ?>
            <?php if( ( $users->front_user->auth_flg === 0 || $users->front_user->reapply_flg === 1 ) && $users->front_user->reject_comment === null ): ?>
                <div class="reject_area">
                <?php echo noh( $this->Form->create($posts,['action' => 'rejectApplication'] ) ); ?>
                    <?php echo noh( $this->Form->hidden( 'id', [ 'value' => $users['id'] ] ) ); ?>
                    <p style="text-align:left;"><label>コメント</label></p>
                    <?php echo noh( $this->Form->textarea( 'コメント', [ 'name' => 'reject_comment', 'cols' => '50', 'rows' => '2'  ] ) ); ?>
                    <?php echo noh( $this->Form->button( '却下', [ 'name' => 'reject', 'class' => 'btn submit', 'style' => 'width:100px;' ] ) ); ?>
                <?php echo noh( $this->Form->end() ); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <?php if( in_array( 12, $valid_service_menuns, true ) ): ?> 
        <div class="clearfix contents_title">
            <h2 class="left"> 投稿 </h2>
        </div>
        <div class="list_table">
            <table>
                <tr>
                    <th>ID</th>
                    <th>画像</th>
                    <th style="width: 45%;">内容</th>
                    <th>投稿日</th>
                    <th>表示</th>
                    <th></th>
                </tr>
                <?php if ( isset( $posts ) ): ?>
                    <?php foreach( $posts as $key => $val ): ?>
                        <tr>
                            <td><?php echo h( $val['id'] ); ?></td>
                            <td>
                                <?php if( isset( $val['rel_post_files'][0] ) ): ?>
                                    <img src="/<?php echo noh( $val['rel_post_files'][0]['file']['file_path'] ); ?>">
                                <?php else: ?>
                                    no image
                                <?php endif; ?>
                            </td>
                            <td style="width: 45%; ">
                                <div style="word-wrap: break-word; white-space: normal;">
                                    <?php echo nl2br( h( $val['content'] ) ); ?>
                                </div>
                            </td>
                            <td><?php echo noh( $val->created->format('Y/m/d H:i:s') ); ?></td>
                            <td>
                                <?php echo noh( ( $val['display_flg'] === 1 ) ? '表示中' : '非表示' ); ?>
                            </td>
                            <td class="">
                                <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                                    <?php if( $val['display_flg'] === 1 ): ?>
                                        <a data-action="<?php echo noh( $this->Url->build( ['action' => 'offDispalayPost', $val->id ]) ); ?>" data-type="hidden" class="btn jsConfirm" tabIndex="-1"><i class="fas fa-toggle-off"></i> 非表示にする</a>
                                    <?php else: ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>

            <table>
        </div>
    <?php endif; ?>

</div>
