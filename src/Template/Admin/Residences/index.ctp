<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo h( $title_for_layout ); ?> </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">

        <?php echo noh( $this->Form->create( null, [ 'type' => 'get', 'autocomplete' => 'off' ] ) ); ?>
        <table>
            <tr>
                <th style="width: 15%;">建物</th>
                <td style="width: 30%;">
                    <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
                        <?php echo noh( $this->Form->select('buildings', $mt_buildings, [ 'empty' => '建物を選択', 'default' => $this->request->getQuery('buildings' ), 'class' => 'autoSubmit' ] ) ); ?>
                    <?php else: ?>
                        <?php echo h( $mt_buildings[$login_user['admin_user']['building_id']] ); ?>
                    <?php endif; ?>
                </td>
                <th style="width: 15%;">部屋番号</th>
                <td style="width: 30%;"> <?php echo noh( $this->Form->text('room_number', [ 'placeholder' => '部屋番号を入力', 'default' => $this->request->getQuery('room_number' ), 'class' => 'autoSubmit' ] ) ); ?></td>
                <td style="width: 10%;" class="text_c" rowspan="2"> <?php echo noh( $this->Form->button( '検索', [ 'class' => 'btn', 'style' => 'margin: 0;' ] ) ); ?> </td>
            </tr>
            <tr>
                <td style="width: 30%;" colspan="4">
                    <div class="checkbox">
                        <?php echo noh( $this->Form->checkbox( 'unautholized', ['hiddenField' => false, 'id' => 'unautholized', 'default' => $this->request->getQuery('unautholized' ), 'class' => 'autoSubmit' ] ) ); ?>
                        <label for="unautholized">未承認/再申請のみ</label>
                    </div>
                </td>
            </tr>
        </table>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

    <?php if ( $data ): ?>

        <div class="list_table">
            <div class="page-counter">
                <?php echo noh( $this->Paginator->counter(
                    '{{page}} / {{pages}} ページ ( 全 {{count}} 戸 )'
                ) ); ?>
            </div>
            <table>
                <tr>
                    <th style="width: 8%;">部屋番号</th>
                    <th style="padding: 0;">
                        <div class="dl_table nest" style="table-layout: fixed;">
                            <dl>
                                <dd style="width: 20%;">ログインID</dd>
                                <dd style="width: 10%;">名前</dd>
                                <dd style="width: 10%;">タイプ</dd>
                                <dd style="width: 10%;">状態</dd>
                                <dd style="width: 10%;">登録日</dd>
                                <dd style="width: 30%;">&nbsp;</dd>
                            </dl>
                        </div>
                    </th>
                </tr>

                <?php foreach( $data as $key => $val ): ?>
                    <tr>
                        <td>
                            <?php echo h( $val->room_number ); ?>
                        </td>
                        <td style="padding: 0;">
                            <div class="dl_table nest" style="table-layout: fixed;">
                                <?php if( count( $val->front_users ) === 0  ): ?>
                                    <p>データがありません</p>
                                <?php else: ?>
                                    <?php foreach( $val->front_users as $val2 ): ?>
                                        <dl class="<?php if( $val2->leave_flg === 1 ): ?>leaved<?php endif; ?>">
                                            <dd style="width: 20%; overflow-wrap: break-word;">
                                                <?php echo h( $val2->user->login_id ); ?>
                                            </dd>
                                            <dd style="width: 10%;"><?php echo h( $val2->name ); ?></dd>
                                            <dd style="width: 10%;"><?php echo ( isset( $val2->type ) ) ? noh( $mt_customer_type[ $val2->type ] ) : '無し'; ?></dd>
                                            <dd style="width: 10%;" <?php if ( $val2->auth_flg === 0 ): ?>class="unapproved"<?php endif; ?>>
                                                <?php if( $val2->user->lock_flg === 1 ): ?>
                                                凍結中
                                                <?php elseif( $val2->leave_flg === 1 ): ?>
                                                退去済み
                                                <?php elseif( $val2->reject_comment ): ?>
                                                却下
                                                <?php elseif( $val2->reapply_flg === 1 ): ?>
                                                再申請
                                                <?php elseif( $val2->auth_flg === 1 ): ?>
                                                承認済み
                                                <?php else: ?>
                                                未承認
                                                <?php endif; ?>
                                            </dd>
                                            <dd style="width: 10%;"><?php echo noh( ( $val2->created ) ? $val2->created->format( 'Y/m/d' ) : '--' ); ?></dd>
                                            <dd style="width: 30%;">
                                                <a href="<?php echo noh( $this->Url->build( ['action' => 'detail',  $val2->user->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 詳細</a>
                                                <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                                                    <?php if( $val2->auth_flg === 1 && $val2->leave_flg === 0 ): ?>
                                                        <a href="<?php echo noh( $this->Url->build( ['action' => 'luggage', $val2->user->id, $val2->id, '?' => $this->request->getQuery() ]) ); ?>" class="btn"><i class="fa fa-suitcase-rolling"></i> 荷物預かり通知</a>
                                                    <?php else: ?>
                                                        <span class="btn off"><i class="fa fa-suitcase-rolling"></i> 荷物預かり通知</span>
                                                    <?php endif; ?>

                                                    <?php if( $val2->leave_flg === 0 ): ?>
                                                        <a href="<?php echo noh( $this->Url->build( ['action' => 'toggleLeave', $val2->user->id, $val2->id, '?' => $this->request->getquery() ]) ); ?>" class="btn jsConfirm" data-type="leave"><i class="fas fa-door-open"></i> 退去登録</a>
                                                    <?php else: ?>
                                                        <a href="<?php echo noh( $this->Url->build( ['action' => 'toggleLeave', $val2->user->id, $val2->id, '?' => $this->request->getQuery() ]) ); ?>" class="btn jsConfirm" data-type="leave"><i class="fas fa-door-open"></i> 退去解除</a>
                                                    <?php endif; ?>

                                                <?php endif; ?>

                                                <?php if ( $val->contractant_id === 2 ): // TODO 契約管理で制御できるよにする?>
                                                    <a href="<?php echo noh( $this->Url->build( ['action' => 'editStatus',  $val2->user->id ]) ); ?>" class="btn"><i class="fa exchange-alt"></i> タイプの変更</a>
                                                <?php elseif ( $val->contractant_id === 4
                                                      && $this->request->session()->read('Admin.Auth.admin_user.authority' ) === ADMIN
                                                      && ( $val2->auth_flg === 1 && $val2->reapply_flg === 0 )
                                                ): ?>
                                                    <a href="<?php echo noh( $this->Url->build( ['action' => 'message', $val2->user->id, $val2->id, '?' => $this->request->getQuery() ]) ); ?>" class="btn" data-type="leave" target="_blank"><i class="fas fa-comments"></i> メッセージ</a>
                                                <?php endif; ?>
                                            </dd>
                                        </dl>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>

        <div class="text_c admin_pager" style="margin-bottom: 30px;">
            <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
            <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
            <?php echo noh( $this->Paginator->numbers() ); ?>
            <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
            <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
        </div>

        <div class="list_table">
            <h3>■管理用ユーザー</h3>
            <table style="table-layout: fixed;">
                <tr>
                <th style="width: 20%;">ログインID</th>
                <th style="width: 10%;">名前</th>
                <th style="width: 10%;">状態</th>
                <th style="width: 10%;">登録日</th>
                <th style="width: 30%;">&nbsp;</th>
                </tr>

                <?php foreach( $admin_front_users as $key => $val ): ?>
                    <tr>
                        <td> <?php echo h( $val->user->login_id ); ?> </td>
                        <td> <?php echo h( $val->name ); ?> </td>
                        <td <?php if ( $val->auth_flg === 0 ): ?>class="unapproved"<?php endif; ?>>
                            <?php if( $val->user->lock_flg === 1 ): ?>
                            凍結中
                            <?php elseif( $val->leave_flg === 1 ): ?>
                            退去済み
                            <?php elseif( $val->reject_comment ): ?>
                            却下
                            <?php elseif( $val->reapply_flg === 1 ): ?>
                            再申請
                            <?php elseif( $val->auth_flg === 1 ): ?>
                            承認済み
                            <?php else: ?>
                            未承認
                            <?php endif; ?>
                        </td>
                        <td> <?php echo h( $val->created ); ?></td>
                        <td>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'detail',  $val->user->id ]) ); ?>" class="btn">
                                <i class="fa fa-edit"></i> 詳細
                            </a>

                            <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) ): ?>
                                <?php if( $val->auth_flg === 1 && $val->leave_flg === 0 ): ?>
                                    <a href="<?php echo noh( $this->Url->build( ['action' => 'luggage', $val->user->id, $val->id, '?' => $this->request->getQuery() ]) ); ?>" class="btn">
                                        <i class="fa fa-suitcase-rolling"></i> 荷物預かり通知
                                    </a>
                                <?php else: ?>
                                    <span class="btn off"><i class="fa fa-suitcase-rolling"></i> 荷物預かり通知</span>
                                <?php endif; ?>

                                <?php if( $val->leave_flg === 0 ): ?>
                                    <a href="<?php echo noh( $this->Url->build( ['action' => 'toggleLeave', $val->user->id, $val->id, '?' => $this->request->getquery() ]) ); ?>" class="btn jsConfirm" data-type="leave">
                                        <i class="fas fa-door-open"></i> 退去登録
                                    </a>
                                <?php else: ?>
                                    <a href="<?php echo noh( $this->Url->build( ['action' => 'toggleLeave', $val->user->id, $val->id, '?' => $this->request->getQuery() ]) ); ?>" class="btn jsConfirm" data-type="leave">
                                        <i class="fas fa-door-open"></i> 退去解除
                                    </a>
                                <?php endif; ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
 
    <?php else: ?>
       <p>建物を選択してください</p>
    <?php endif; ?>
</div>
<?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
<script>
$(function(){
    // 一つ目を初期値にする
    var buildingsSelect = $('[name=buildings]');
    if( buildingsSelect.val() === '' ) 
    {
        buildingsSelect.find('option').eq(1).prop('selected', true).change();
    }
});
</script>
<?php endif; ?>
