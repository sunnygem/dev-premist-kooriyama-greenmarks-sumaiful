<div class="front_user">
    <div class="clearfix contents_title">
        <h2 class="left"> <?php echo noh( $title_for_layout ); ?> 申請の却下 </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create() ); ?>
        <?php echo noh( $this->Form->hidden( 'id', [ 'value' => $users['id'] ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>ユーザーID</th>
                    <td><?php echo h( $users['front_user']['id'] ); ?></td>
                </tr>

                <tr>
                    <th>名前</th>
                    <td><?php echo h( $users['front_user']['name'] ); ?></td>
                </tr>
                <?php if ( isset( $users['front_user']['type'] ) ): ?>
                <tr>
                    <th>タイプ</th>
                    <td>
                    <?php echo h( $mt_customer_type[$users['front_user']['type']] ); ?>
                    </td>
                </tr>
                <?php endif; ?>

                <tr>
                    <th>住居</th>
                    <td>
                        <?php echo h( $mt_buildings[$users['front_user']['building_id']] ); ?><br>
                        <?php echo h( $users['front_user']['room_number'] ); ?>
                    </td>
                </tr>

                <tr>
                    <th>状態</th>
                    <td>
                        <?php if( $users->lock_flg === 1 ): ?>
                        凍結中
                        <?php elseif( $users->front_user->leave_flg === 1 ): ?>
                        退去済み
                        <?php elseif( $users->front_user->reject_comment ): ?>
                        却下
                        <div class="memo">
                            <?php echo h( $users->front_user->reject_comment ); ?>
                        </div>
                        <?php elseif( $users->front_user->reapply_flg === 1 ): ?>
                        再申請
                        <?php elseif( $users->front_user->auth_flg === 1 ): ?>
                        承認済み
                        <?php if( $users['valid_date'] ) echo noh( '<br>承認日' . $users['valid_date']->format( 'Y年m月d日' ) ); ?>
                        <?php else: ?>
                        未承認
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <th>登録日時</th>
                    <td><?php echo noh( ( $users['front_user']['created'] ) ?  $users['front_user']['created']->format('Y年m月d日 H:i:s') : '--' ); ?></td>
                </tr>

                <tr>
                    <th>却下理由</th>
                    <td>
                        <?php echo noh( $this->Form->textarea( 'reject_comment' ) ); ?>
                        <?php if( isset( $error['reject_comment'] ) ) echo noh( '<p class="error">' . $error['reject_comment'] . '</p>' ); ?>
                    </td>
                </tr>

            </table>
        </div>

        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'detail', $users->id]) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '却下', [ 'label' => false, 'name' => 'reject',  'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>

</div>
