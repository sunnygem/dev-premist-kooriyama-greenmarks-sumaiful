<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $users['id'] ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( $users, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $users->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $users->id ] ) ); ?>
        <div class="form_table">
            <table>
                 <tr>
                    <th>建物</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'front_user.building_id', $mt_buildings, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['building_id'] ) ) echo noh( '<p class="error">' . $error['building_id'] . '</p>' ); ?>
                    </td>
                 </tr>

                 <tr>
                    <th>居住者タイプ</th>
                    <td>
                        <div class="radio inline">
                            <?php echo noh( $this->Form->radio( 'front_user.type', $mt_customer_type, [  ] ) ); ?>
                        </div>
                        <?php if( isset( $error['type'] ) ) echo noh( '<p class="error">' . $error['type'] . '</p>' ); ?>
                    </td>
                 </tr>

                 <tr>
                    <th>ニックネーム</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'front_user.nickname', [ 'label' => false, 'placeholder' => 'ログインID' ] ) ); ?>
                        <?php if( isset( $error['nickname'] ) ) echo noh( '<p class="error">' . $error['nickname'] . '</p>' ); ?>
                    </td>
                 </tr>

                 <tr>
                    <th>ログインID</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'login_id', [ 'label' => false, 'placeholder' => 'ログインID' ] ) ); ?>
                        <?php if( isset( $error['login_id'] ) ) echo noh( '<p class="error">' . $error['login_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>パスワード</th>
                    <td>
                        <?php echo noh( $this->Form->password( 'password', [ 'label' => false, 'placeholder' => 'パスワード' ] ) ); ?>
                        <?php if( isset( $error['password'] ) ) echo noh( '<p class="error">' . $error['password'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>確認用パスワード</th>
                    <td>
                        <?php echo noh( $this->Form->password( 'password_confirm', [ 'label' => false, 'placeholder' => '確認用にパスワードを入力' ] ) ); ?>
                        <?php if( isset( $error['password_confirm'] ) ) echo noh( '<p class="error">' . $error['password_confirm'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>有効フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'front_user.auth_flg', [ 'id' => 'auth_flg', 'default' => 0 ] ) ); ?>
                            <label for="auth_flg">有効</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>有効期限</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'expire_date', [ 'default' => null, 'class' => 'datepicker', 'placeholder' => '有効期限を設定'] ) ); ?>
                        <?php if( isset( $error['expire_date'] ) ) echo noh( '<p class="error">' . $error['expire_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <?php /*
                <tr>
                    <th>メールアドレス</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'email', [ 'placeholder' => 'email@example.com' ] ) ); ?>
                        <?php if( isset( $error['email'] ) ) echo noh( '<p class="error">' . $error['email'] . '</p>' ); ?>
                    </td>
                </tr>
                */ ?>



            </table>
        </div>
        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
