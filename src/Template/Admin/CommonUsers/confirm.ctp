<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $users['id'] ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>建物</th>
                <td><?php echo h( $mt_buildings[$users['front_user']['building_id']] ); ?></td>
            </tr>

            <tr>
                <th>タイプ</th>
                <td><?php echo h( $mt_customer_type[$users['front_user']['type']] ); ?></td>
            </tr>

            <tr>
                <th>ニックネーム</th>
                <td><?php echo h( $users['front_user']['nickname'] ); ?></td>
            </tr>

            <tr>
                <th>ログインID</th>
                <td><?php echo h( $users['login_id'] ); ?></td>
            </tr>

            <tr>
                <th>パスワード</th>
                <td><?php echo h( str_repeat('*', strlen( $users['password'] ) ) ); ?></td>
            </tr>

            <tr>
                <th>有効</th>
                <td><?php echo noh( ( $users['front_user']['auth_flg'] ) ? '有効' : '無効' ); ?></td>
            </tr>

            <tr>
                <th>有効期限</th>
                <td><?php echo noh( ( $users['expire_date'] ) ? date( 'Y/m/d' , strtotime( $users['expire_date'] ) ) : '-' ); ?></td>
            </tr>
 
            <?php /*
            <tr>
                <th>メールアドレス</th>
                <td><?php echo h( $users['email'] ); ?></td>
            </tr>
            */ ?>

        </table>
    </div>

    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</div>
