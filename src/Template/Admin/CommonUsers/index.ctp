<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo h( $title_for_layout ); ?></h2>
        <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( [ 'action' => 'edit'  ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
        <?php endif; ?>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>ログインID</th>
                <th>ニックネーム</th>
                <th>タイプ</th>
                <th>有効フラグ(承認フラグ)</th>
                <th>有効期限</th>
                <th>作成日</th>
                <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
                    <th>&nbsp;</th>
                <?php endif; ?>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class=""><?php echo h( $val->user->id ); ?></td>
                    <td class=""><?php echo h( $val->user->login_id ); ?></td>
                    <td class=""><?php echo h( $val->nickname ); ?></td>
                    <td class=""><?php echo noh( ( $val->type !== false ) ? $mt_customer_type[$val->type] : '-' ); ?></td>
                    <td class=""><?php echo noh( ( $val->auth_flg === 1 ) ? '有効' : '無効' ); ?></td>
                    <td class="">
                        <?php echo noh( ( $val->user->expire_date ) ? $val->user->expire_date->format( 'Y/m/d H:i:s' ) : '--' ); ?>
                    </td>
                    <td class="">
                        <?php echo noh( ( $val->user->created ) ? $val->user->created->format( 'Y/m/d' ) : '--' ); ?>
                    </td>
                    <?php if( in_array( $login_user['admin_user']['authority'], [SYSTEM_ADMIN, ADMIN], true ) ): ?>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit', $val->user_id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                    </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>

    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
