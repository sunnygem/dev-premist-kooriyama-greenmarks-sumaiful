<div class="content">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?= $title_for_layout; ?> 詳細
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $data->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>報告種類</th>
                    <td>
                        <?php if( $data->type === 'privacy' ): ?>
                            プライバシーの侵害
                        <?php elseif( $data->type === 'other' ): ?>
                            その他
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <th>建物の公開範囲</th>
                    <td>
                        <?php foreach( $data->presentation_buildings as $val ): ?>
                            <?php echo h( $val->building->name ); ?><br>
                        <?php endforeach; ?>
                    </td>
                </tr>

                <tr>
                    <th>報告者</th>
                    <td>
                        <?php if( $data->user->front_user !== null ): ?>
                            <a href="<?php echo noh( $this->Url->build(['controller' => 'Residences', 'action' => 'detail', $data->user->id ]) ); ?>" target="_blank"><?php echo noh( $data->user->front_user->name ); ?> <i class="fas fa-external-link-alt"></i></a>
                        <?php else: ?>
                            削除済みユーザー
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th>報告日時</th>
                    <td>
                        <?php echo noh( $data->created->format( 'Y/m/d H:i:s') ); ?>
                    </td>
                </tr>
            </table>

            <table>
                <caption>報告内容</caption>
                <?php if( $data->post !== null ): ?>
                    <tr>
                        <th>対象</th>
                        <td>投稿</td>
                    </tr>
                    <tr>
                        <th>対応</th>
                        <td>
                            <?php if( $data->checked === null ): ?>
                                未対応
                            <?php else: ?>
                                <?php echo $data->checked->format( 'Y/m/d H:i:s' ); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th>報告時投稿内容</th>
                        <td>
                            <pre>
                                <?php echo h(  $data->content ); ?>
                            </pre>
                        </td>
                    </tr>
                    <tr>
                        <th>現在の内容</th>
                        <td>
                           <div><?php echo noh( ( $data->post->display_flg === 1 ) ? '表示中' : '非表示' ); ?></div>
                           <div>投稿日時：<?php echo noh( $data->post->created->format('Y/m/d H:i:s') ); ?></div>
                           <div>投稿内容：<pre><?php echo noh( $data->post->content ); ?></pre></div>
                           <div>投稿画像：<?php if( isset( $data->post->rel_post_files[0] )): ?> <img src="/<?php echo noh( $data->post->rel_post_files[0]->file->file_path ); ?>"><?php endif; ?></div>
                        </td>
                    </tr>


                <?php elseif( $data->file !== null ): ?>
                    <tr>
                        <th>対象</th>
                        <td>画像</td>
                    </tr>
                    <tr>
                        <th>状態</th>
                        <td><?php echo ( $data->file->display_flg === 1 ) ? '表示中' : '非表示'; ?></td>
                    </tr>
                    <tr>
                        <th>アルバム</th>
                        <td><?php echo $data->album_title; ?></td>
                    </tr>
                    <tr>
                        <th>画像</th>
                        <td>
                            <?php if( isset( $data->file->file_path )): ?> 
                               <img src="/<?php echo noh( $data->file->file_path ); ?>"></div>
                           <?php endif; ?>
                        </td>
                    </tr>
                <?php endif; ?>


            </table>
        </div>
        <div>
                        <?php  if( $data->checked === null ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $data->id, 0 ]) ); ?>" class="btn"><i class="far fa-square"></i> 対応しない</a>
                        <?php else: ?>
                            <span class="btn">対応済み</span>
                        <?php endif; ?>
                        <?php if ( isset( $data->post ) ): ?>
                            <?php if ( $data->post->display_flg === 0 ): ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $data->id, 2 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 表示する</a>
                            <?php else: ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $data->id, 1 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 非表示にする</a>
                            <?php endif; ?>
                        <?php elseif ( isset( $data->file ) ): ?>
                            <?php if ( $data->file->display_flg === 0 ): ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $data->id, 2 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 表示する</a>
                            <?php else: ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $data->id, 1 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 非表示にする</a>
                            <?php endif; ?>
                        <?php endif; ?>
        <div>
        <?php /*
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
        */ ?>

    <?php echo noh( $this->Form->end() ); ?>
</div>
