<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?= $title_for_layout; ?></h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>建物の表示範囲</th>
                <th>対象</th>
                <th>種類</th>
                <th>対応</th>
                <th>公開日</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class="title"><?php echo h( $val->id ); ?></td>
                    <td class="">
                    <?php if( $val->presentation_buildings ): ?>
                        <?php foreach( $val->presentation_buildings as $val2 ): ?>
                            <?php echo h( $val2->building->name ); ?><br>
                        <?php endforeach; ?>
                    <?php else: ?>
                        登録なし
                    <?php endif; ?>
                    </td>
                    <td class="">
                        <?php if( $val->file_id !== null ): ?>
                           アルバム写真
                        <?php elseif( $val->post_id !== null ): ?>
                            投稿
                        <?php endif; ?>
                    </td>
                    <td class=""><?php echo h( $val->type ); ?></td>
                    <td class=""><?php echo noh( ( $val->checked ) ? $val->checked->format('Y/m/d H:i:s') : '未対応' ); ?></td>
                    <td class=""><?php echo noh( ( $val->created ) ? $val->created->format('Y/m/d H:i:s') : '--' ); ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'detail',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 確認</a>
                    </td>
                    <td class="">
                        <?php  if( $val->checked === null ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $val->id, 0 ]) ); ?>" class="btn"><i class="far fa-square"></i> 対応しない</a>
                        <?php else: ?>
                            <span class="btn">対応済み</span>
                        <?php endif; ?>
                        <?php if ( isset( $val->post ) ): ?>
                            <?php if ( $val->post->display_flg === 0 ): ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $val->id, 2 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 表示する</a>
                            <?php else: ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $val->id, 1 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 非表示にする</a>
                            <?php endif; ?>
                        <?php elseif ( isset( $val->file ) ): ?>
                            <?php if ( $val->file->display_flg === 0 ): ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $val->id, 2 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 表示する</a>
                            <?php else: ?>
                                <a href="<?php echo noh( $this->Url->build( ['action' => 'check', $val->id, 1 ]) ); ?>" class="btn"><i class="far fa-check-square"></i> 非表示にする</a>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
