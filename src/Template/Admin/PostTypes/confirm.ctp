<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            契約者<?php if( isset( $informations->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td><?php echo noh( ( $informations['display_flg'] === '1' ) ? '表示する' : '表示しない' ); ?></td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td><?php echo h( $informations['title'] ); ?></td>
            </tr>
            <tr>
                <th>サムネイル</th>
                <td>
                    <?php
                        if( isset( $informations['uploaded']['image']['file_path'] ) )
                        {
                            echo  noh(  '<img src="' . DS . $informations['uploaded']['image']['file_path'] . '">' );
                        }
                        else
                        {
                            echo noh( '--' );
                        }
                    ?>
                </td>
            </tr>

            <tr>
                <th>公開日</th>
                <td><?php echo h( $informations['release_date'] ); ?></td>
            </tr>
            <tr>
                <th>公開終了日</th>
                <td><?php echo h( $informations['close_date'] ); ?></td>
            </tr>

            <tr>
                <th>外部リンク</th>
                <td><?php echo h( ( strlen( $informations['link_url'] ) > 0 ) ? $informations['link_url']  : '-- ' ); ?></td>
            </tr>

            <tr>
                <th>PDF</th>
                <td>
                    <?php
                        if( isset( $informations['uploaded']['pdf']['file_path'] ) )
                        {
                            echo noh( '<a href="' . $this->Url->build( DS . $informations['uploaded']['pdf']['file_path']) . '" target="_blank"><span class="far fa-file-pdf"></span></a> '. $informations['uploaded']['pdf']['name'] );
                        }
                        else
                        {
                            echo noh( '--' );
                        }
                    ?>
                </td>
            </tr>

        </table>
    </div>
    <div class="text_c">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
