<div class="posts">
    <div class="clearfix contents_title">
        <h2 class="left">ポストカテゴリー管理</h2>
        <div class="right"> </div>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <?php echo noh( $this->Form->create( $data, [] ) ); ?>
            <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id' ) ); ?>
            <?php echo noh( $this->Form->hidden( 'contractant_service_menu_id', [ 'value' => $service_menu_id ] ) ); ?>
            <table>
                <tr>
                    <th>ラベル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'label', ['placeholder' => 'ラベルを入力'] ) ); ?>
                        <?php if( isset( $error['label'] ) ) echo noh( '<p class="error">' . $error['label'] . '</p>' ); ?>
                    </td>
                </tr>
                <tr>
                    <th>表示</th>
                    <td>
                        <?php echo noh( $this->Form->radio( 'display_flg', [ '1' => '表示', '0' => '非表示'], [ 'default' => '1' ]  ) ); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php echo noh( $this->Form->button( '登録',  [ 'name' => 'complete', 'class' => 'btn', 'tabIndex' => '-1' ] ) ); ?>
                    </td>
                </tr>

            </table>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>ラベル</th>
                <th>表示</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $category as $key => $val ): ?>
                <tr>
                    <td class="title"><?php echo h( $val->id ); ?></td>
                    <td class=""><?php echo h( $val->label ); ?></td>
                    <td class=""><?php echo noh( ( $val->display_flg ) ? '表示' : '非表示' ); ?></td>
                    <td class="">
                        <?php if( $val->id === $data->id ): ?>
                            <a class="btn " disabled="disabled"><i class="fa fa-edit"></i> 編集中</a>
                        <?php else: ?>
                            <a href="<?php echo noh( $this->Url->build( [ $val->contractant_service_menu_id, $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <?php endif; ?>

                        <a href="<?php echo noh( $this->Url->build( ['action' => 'categoryDelete', $val->contractant_service_menu_id, $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>

    <div class="text_c btn_area">
        <a href="<?php echo noh( $this->Url->build([ 'controller' => 'ServiceMenus', 'action' => 'edit', $service_menu_id] ) ); ?>" class="btn btn_back">戻る</a>
    </div>
</div>
