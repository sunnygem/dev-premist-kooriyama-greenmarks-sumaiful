<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $users->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $users->id ] ) ); ?>
        <div class="list_table">
            <table>
                <tr>
                    <td>最低入力数を指定</td>
                    <td>
                        <?php echo noh( $this->Form->text( 'password_length', [ 'placeholder' => '文字数を入力' ] ) ); ?>
                        <?php if( isset( $error['password_length'] ) ) echo noh( '<p class="error">' . $error['password_length'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <td>有効期限を設定</td>
                    <td>
                        <?php echo noh( $this->Form->text( 'valid_term', [ 'label' => false, 'placeholder' => '日数を入力' ] ) ); ?>&nbsp;日
                        <?php if( isset( $error['valid_term'] ) ) echo noh( '<p class="error">' . $error['valid_term'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <td>パスワードの修正を制御</td>
                    <td>
                        過去&nbsp;<?php echo noh( $this->Form->text( 'prohibition_num', [ 'label' => false, 'empty' => '回数を入力' ] ) ); ?>&nbsp;回までのパスワードは使用不可
                        <?php if( isset( $error['prohibition_num'] ) ) echo noh( '<p class="error">' . $error['prohibition_num'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <td>連続失敗ロック</td>
                    <td>
                        <?php echo noh( $this->Form->text( 'lock_num', [ 'label' => false, 'empty' => '回数を入力' ] ) ); ?>&nbsp;回でロック
                        <?php if( isset( $error['lock_num'] ) ) echo noh( '<p class="error">' . $error['lock_num'] . '</p>' ); ?>
                    </td>
                </tr>
                <tr>
                    <td>解除までの時間</td>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_hour', [ 'label' => false, 'empty' => '時間を入力' ] ) ); ?>&nbsp;時間
                        <?php if( isset( $error['release_hour'] ) ) echo noh( '<p class="error">' . $error['release_hour'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <td>数字</td>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'include_number_flg', [ 'id' => 'include_number_flg', 'default' => 1 ] ) ); ?>
                            <label for="include_number_flg">含める</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>記号</td>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'include_symbol_flg', [ 'id' => 'include_symbol_flg', 'default' => 0 ] ) ); ?>
                            <label for="include_symbol_flg">含める</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>ログイン保持時間</td>
                    <td>
                        <?php echo noh( $this->Form->text( 'login_expires', [ 'label' => false, 'placeholder' => '時間を入力' ] ) ); ?>&nbsp;時間
                        <span>※デフォルトは24時間です。</span>
                        <?php if( isset( $error['login_expires'] ) ) echo noh( '<p class="error">' . $error['login_expires'] . '</p>' ); ?>
                    </td>
                </tr>



<?php /* 管理画面では初回ログインは強制(特にチェックもしない)
                <tr>
                    <td>初回ログイン</td>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'require_password_reset_flg', [ 'id' => 'require_password_reset_flg', 'default' => 1 ] ) ); ?>
                            <label for="require_password_reset_flg">パスワード再設定を必須にする</label>
                        </div>
                    </td>
                </tr>
*/ ?>
            </table>
            <?php echo noh( $this->Form->hidden( 'require_password_reset_flg', ['value' => 1] ) ); ?>
        </div>
        <div class="text_c">
            <a href="<?php echo noh( $this->Url->build([ 'action' => 'index' ]) ); ?>" class="btn btn_back btn_large">戻る</a>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit', 'tabIndex' => '-1' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
