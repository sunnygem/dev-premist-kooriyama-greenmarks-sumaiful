<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            管理<?php if( isset( $users['id'] ) ): ?> ユーザー編集 <?php else: ?> ユーザー登録 <?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>ログインID</th>
                <td><?php echo h( $users['login_id'] ); ?></td>
            </tr>

            <tr>
                <th>名前</th>
                <td><?php echo h( $users['admin_user']['name'] ); ?></td>
            </tr>

            <tr>
                <th>Eメール</th>
                <td><?php echo h( $users['email'] ); ?></td>
            </tr>

            <?php if( isset( $users['password'] ) ): ?>
            <tr>
                <th>パスワード</th>
                <td><?php echo h( $users['password'] ); ?></td>
            </tr>
            <?php endif; ?>

            <tr>
                <th>権限</th>
                <td><?php echo h( $mt_authority[$users['admin_user']['authority']] ); ?></td>
            </tr>

            <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
            <tr>
                <th>建物</th>
                <td>
                    <?php echo noh( $mt_buildings[$users['admin_user']['building_id']] ); ?><br>
                </td>
            </tr>
            <?php endif; ?>

            <tr>
                <th>有効</th>
                <td><?php echo noh( ( $users['admin_user']['valid_flg'] ) ? '有効' : '無効' ); ?></td>
            </tr>
            <?php /*
            <tr>
                <th>メールアドレス</th>
                <td><?php echo h( $users['admin_user']['email'] ); ?></td>
            </tr>
            */ ?>

        </table>
    </div>

    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</div>
