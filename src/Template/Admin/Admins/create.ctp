<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            管理<?php if( isset( $users->id ) ): ?> ユーザー編集 <?php else: ?> ユーザー登録 <?php endif; ?>
        </h2>
    </div>

    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( $users, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $users->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $users->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>ログインID</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'login_id', [ 'label' => false, 'placeholder' => 'ログインID' ] ) ); ?>
                        <?php if( isset( $error['login_id'] ) ) echo noh( '<p class="error">' . $error['login_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>名前</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'admin_user.name', [ 'label' => false, 'placeholder' => '名前' ] ) ); ?>
                        <?php if( isset( $users['admin_user']['id'] ) ) echo noh( $this->Form->hidden( 'admin_user.id' ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>Eメール</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'email', [ 'label' => false, 'placeholder' => 'Eメール' ] ) ); ?>
                        <?php if( isset( $error['email'] ) ) echo noh( '<p class="error">' . $error['email'] . '</p>' ); ?>
                    </td>
                </tr>

                <?php if( isset( $users->id ) && $users->id === $this->request->session()->read('Admin.Auth.id') ): ?>
                    <tr>
                        <th>パスワード</th>
                        <td>
                            <?php echo noh( $this->Form->password( 'password', [ 'label' => false, 'placeholder' => 'パスワード' ] ) ); ?>
                            <?php if( isset( $error['password'] ) ) echo noh( '<p class="error">' . $error['password'] . '</p>' ); ?>
                        </td>
                    </tr>

                    <tr>
                        <th>パスワード(確認用)</th>
                        <td>
                            <?php echo noh( $this->Form->password( 'password_confirm', [ 'label' => false, 'placeholder' => 'パスワード(確認用)' ] ) ); ?>
                            <?php if( isset( $error['password_confirm'] ) ) echo noh( '<p class="error">' . $error['password_confirm'] . '</p>' ); ?>
                        </td>
                    </tr>
                <?php endif; ?>

                <tr>
                    <th>権限</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'admin_user.authority', $mt_authority, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?>
                        <?php if( isset( $error['authority'] ) ) echo noh( '<p class="error">' . $error['authority'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>有効フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'admin_user.valid_flg', [ 'id' => 'valid_flg', 'default' => 0 ] ) ); ?>
                            <label for="valid_flg">有効</label>
                        </div>
                    </td>
                </tr>
                <?php /*

                <tr>
                    <th>メールアドレス</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'admin_user.email', [ 'placeholder' => 'email@example.com' ] ) ); ?>
                        <?php if( isset( $error['email'] ) ) echo noh( '<p class="error">' . $error['email'] . '</p>' ); ?>
                    </td>
                </tr>
                */ ?>

            </table>
        </div>
        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
