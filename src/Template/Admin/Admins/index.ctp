<div class="">
    <div class="clearfix contents_title">
        <h2 class="left">管理者ユーザー管理</h2>
        <div class="right"> 
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
            <a href="<?php echo noh( $this->Url->build( ['action' => 'security' ]) ); ?>" class="btn"><i class="fas fa-lock"></i> セキュリティ設定</a>
        </div>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="list_table">
        <table>
            <tr>
                <th></th>
                <th>ログインID</th>
                <th>Eメール</th>
                <th>名前</th>
                <th>建物</th>
                <th>有効</th>
                <th>権限</th>
                <th>パスワード</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr>
                    <td class="">
                        <?php if($val->user->id === $login_user['id'] ) echo noh( '★' ); ?>
                    </td>
                    <td class=""><?php echo h( $val->user->login_id ); ?></td>
                    <td class=""><?php echo h( $val->user->email ); ?></td>
                    <td class=""><?php echo h( $val->name ); ?></td>
                    <td class="">
                        <?php echo h( $mt_buildings[$val->building_id] ); ?>
                    </td>
                    <td class=""><?php echo noh( ( $val->valid_flg === 1 ) ? '有効' : '無効' ); ?></td>
                    <td class=""><?php echo noh( $mt_authority[ $val->authority ] ); ?></td>
                    <td> <?php echo noh( ( $val->user->password !== null ) ? '設定済み' : '未設定' ); ?> </td>
                    <td class="">
                        <?php if( in_array( $this->request->Session()->read('Admin.Auth.admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) ): ?>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->user->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->user->id ]) ); ?>" class="btn btn_delete jsConfirm"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>

    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
