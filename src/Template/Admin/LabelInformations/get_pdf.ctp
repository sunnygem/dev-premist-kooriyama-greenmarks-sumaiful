<?php $this->Html->script('pdf/build/pdf', ['block' => true] ); ?>
<canvas id="the-canvas"></canvas>
<script>
    var pdfData = atob( '<?php echo $pdf_data; ?>' );
    //var pdfjsLib = window['pdf/build/pdf'];
    console.log(pdfjsLib);
    pdfjsLib.GlobalWorkerOptions.workerSrc = '/js/pdf/build/pdf.worker.js';

    var loadingTask = pdfjsLib.getDocument({data: pdfData});
    loadingTask.promise.then(function(pdf) {
        console.log('PDF loaded');

        // Fetch the first page
        var pageNumber = 1;
        pdf.getPage(pageNumber).then(function(page) {
            console.log('Page loaded');

            var scale = 1.5;
            var viewport = page.getViewport(scale);

            // Prepare canvas using PDF page dimensions
            var canvas = document.getElementById('the-canvas');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
            canvasContext: context,
            viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.then(function () {
                console.log('Page rendered');
            });
        });
    }, function (reason) {
                                                                                            // PDF loading error
                                                                                              console.error(reason);
                                                                                              });

</script>
