<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo noh( $title_for_layout); ?> <?php if( isset( $informations->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $informations, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

        <?php if( isset( $informations->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $informations->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>公開範囲(任意)</th>
                    <td>
                        <div class="inline">
                            <?php echo noh( $this->Form->select( 'user_types',  $mt_customer_type, [ 'multiple' => 'checkbox', 'id' => 'display_flg' ]) ); ?>
                        </div>
                        <p>※チェックがない場合は全公開となります。</p>
                    </td>
                </tr>

                <tr>
                    <th>タイトル</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'タイトルを入力' ] ) ); ?>
                        <?php if( isset( $error['title'] ) ) echo noh( '<p class="error">' . $error['title'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>画像</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $informations['uploaded']['image'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh( '<img src="' . DS . $informations['uploaded']['image']['file_path'] ) . '">'; ?>
                               <?php if( isset( $informations['uploaded']['image']['id'] ) ) echo $this->Form->hidden( 'uploaded[image][id]', [ 'value' => $informations['uploaded']['image']['id'] ] ); ?>
                               <?php echo $this->Form->hidden( 'uploaded[image][file_path]', [ 'value' => $informations['uploaded']['image']['file_path'] ] ); ?>
                               <?php echo $this->Form->hidden( 'uploaded[image][name]',      [ 'value' => $informations['uploaded']['image']['name'] ] ); ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <span class="file_name"><?php if( isset( $informations['uploaded']['image'] ) ) echo h( $informations['uploaded']['image']['name'] ); ?></span>
                        <?php echo noh( $this->Form->file( 'files[image]', [ 'id' => 'image_file', 'label' => false ] ) ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'release_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker'] ) ); ?>
                        <?php if( isset( $error['release_date'] ) ) echo noh( '<p class="error">' . $error['release_date'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>公開終了日</th>
                    <td><?php echo noh( $this->Form->text( 'close_date',  [ 'placeholder' => '公開終了日を設定', 'class' => 'datepicker']) ); ?></td>
                </tr>

                <tr>
                    <th>本文</th>
                    <td>
                        <?php echo noh( $this->Form->textarea( 'content', [ 'label' => false, 'placeholder' => '本文を入力' ] ) ); ?>
                        <?php if( isset( $error['content'] ) ) echo noh( '<p class="error">' . $error['content'] . '</p>' ); ?>
                    </td>
                </tr>



                <tr class="select_link">
                    <th>
                        <div class="inline">
                            <?php echo noh( $this->Form->radio('type', [ 0 => '外部リンク', 1 => 'pdf' ], [ 'default' => 0, 'hiddenField' => false ]) ); ?>
                        </div>
                    </th>
                    <td>
                        <div class="url_link" data-type="0">
                            <?php echo noh( $this->Form->text( 'link_url',  [ 'placeholder' => 'リンクを設定']) ); ?>
                            <?php if( isset( $error['link_url'] ) ) echo noh( '<p class="error">' . $error['link_url'] . '</p>' ); ?>
                        </div>

                        <div class="file" data-type="0">
                            <label for="pdf_file" class="btn">pdfを選択する</label>
                            <?php if( isset( $informations['uploaded']['pdf'] ) ): // 登録されpdfを表示 ?>
                               <?php if ( isset( $informations['uploaded']['pdf']['id'] ) ) echo $this->Form->hidden( 'uploaded[pdf][id]', [ 'value' => $informations['uploaded']['pdf']['id'] ] ); ?>
                               <?php echo $this->Form->hidden( 'uploaded[pdf][file_path]', [ 'value' => $informations['uploaded']['pdf']['file_path'] ] ); //確認画面用 ?>
                               <?php echo $this->Form->hidden( 'uploaded[pdf][name]',      [ 'value' => $informations['uploaded']['pdf']['name'] ] ); ?>
                            <?php endif; ?>
                            <span class="file_name"><?php if( isset( $informations['uploaded']['pdf'] ) ) echo h( $informations['uploaded']['pdf']['name'] ); ?></span>
                            <?php echo noh( $this->Form->file( 'files[pdf]', [ 'id' => 'pdf_file', 'label' => false ] ) ); ?>
                            <?php if( isset( $error['pdf'] ) ) echo noh( '<p class="error">' . $error['pdf'] . '</p>' ); ?>
                         </div>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c btn_area">
            <a href="<?php echo noh( $this->Url->build(['action' => 'index']) ); ?>" class="btn btn_back">戻る</a>
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
