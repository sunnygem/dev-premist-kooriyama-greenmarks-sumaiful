<header>
    <div class="left">
        <div class="logo">
            <h1>
                <a href="<?php echo $this->Url->build([ 'controller' =>  'Tops', 'action'=> 'index'] ); ?>">
                    <img src="/<?php echo noh( $this->request->Session()->read('ContractantData.file_path') ); ?>" alt="<?php echo h( $app_name ) . ' 管理画面' ; ?>">
                </a>
            </h1>
        </div>
    </div>
    <div class="right">
        <span class="user_name">
            <span class="fa fa-user"></span>&nbsp;<?php echo h( $this->request->Session()->read('Admin.Auth.admin_user.name') ); ?>
        </span>
        <span class="logout">
        <a href="<?php echo $this->Url->build( [ 'controller' => 'logins', 'action' => 'logout' ] ); ?>"><i class="fa fa-sign-out-alt"></i>ログアウト</a> </span>
    </div>
</header>
