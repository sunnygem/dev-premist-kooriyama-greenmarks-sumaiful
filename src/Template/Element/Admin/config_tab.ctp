<nav class="tab_list">
    <ul class="tab_wrap">
        <li class="tab <?php echo ( $this->name === 'Configs' ) ? "selected" : "" ; ?>">
            <a href="<?php echo noh( $this->Url->build(['controller' => 'Configs', 'action' => 'index'] ) ); ?>"><i class="navi_icon fa fa-users"></i> 契約者情報</a>
        </li>

        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority'), [ SYSTEM_ADMIN, ADMIN], true ) ): ?>
        <li class=" tab <?php echo ( $this->name === 'IpLimits' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'IpLimits', 'action' => 'index' ] ); ?>"><i class="fas fa-clipboard-list"></i> IPアドレス制限</a>
        </li>
        <?php endif; ?>
        
        <li class=" tab <?php echo ( $this->name === 'TermsOfServices' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'TermsOfServices', 'action' => 'index' ] ); ?>"><i class="far fa-file-alt"></i> 利用規約</a>
        </li>
        <!--{*
        <li class=" tab <?php echo ( $this->name === 'PrivacyPolicies' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'PrivacyPolicies', 'action' => 'index' ] ); ?>"><i class="far fa-file-alt"></i> プライバシーポリシー</a>
        </li>
        *}-->
    </ul>
</nav>
