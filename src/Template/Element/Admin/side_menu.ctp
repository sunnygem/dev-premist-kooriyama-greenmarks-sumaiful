<nav class="side_menu">
    <ul>
        <li class="<?php echo ( $this->name === 'Tops' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Tops', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-home"></i>ホーム</a>
        </li>

        <?php if( in_array( 14, $valid_service_menuns, true ) ): ?>
            <li class="<?php echo ( $this->name === 'Informations' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Informations', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-info-circle"></i>インフォメーション</a>
            </li>
        <?php endif; ?>

        <?php if( in_array( 13, $valid_service_menuns, true ) ): ?>
            <li class="<?php echo ( $this->name === 'LabelInformations' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'LabelInformations', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-info-circle"></i>インフォメーション</a>
            </li>
        <?php endif; ?>

        <?php if( $unique_parameter === 'willing' ): ?>
        <li class="<?php echo ( $this->name === 'LineOpenChats' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'LineOpenChats', 'action' => 'index' ] ); ?>"><i class="navi_icon fab fa-line"></i>LINEオープンチャット</a>
            <?php if( $this->name === 'LineOpenChats' ): ?>
            <ul class="level2">
                <li class="<?php echo ( in_array( $this->request->getParam('action'), ['index', 'edit', 'confirm'], true ) ) ? "selected" : "" ; ?>">
                    <a href="<?php echo $this->Url->build( [ 'controller' => 'LineOpenChats', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-external-link-alt"></i>URL管理</a>
                </li>
                <li class="<?php echo ( in_array( $this->request->getParam('action'), ['category', 'categoryEdit'], true ) ) ? "selected" : "" ; ?>">
                    <a href="<?php echo $this->Url->build( [ 'controller' => 'LineOpenChats', 'action' => 'category' ] ); ?>"><i class="navi_icon fas fa-tags"></i>カテゴリ管理</a>
                </li>
            </ul>
            <?php endif; ?>
        </li>
        <?php endif; ?>

        <?php if( in_array( 5, $valid_service_menuns, true ) ): //タイプ ?>
            <li class="<?php echo ( $this->name === 'Contents' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Contents', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-file-alt"></i>コンテンツ</a>
            </li>
        <?php endif; ?>

        <?php if( in_array( 15, $valid_service_menuns, true ) ): ?>
            <li class="<?php echo ( $this->name === 'ImageSharings' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'ImageSharings', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-images"></i>アルバム管理</a>
            </li>
        <?php endif; ?>

        <?php if( in_array( 12, $valid_service_menuns, true ) ): ?>
            <li class="<?php echo ( $this->name === 'Posts' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Posts', 'action' => 'index' ] ); ?>"><i class="far fa-comments"></i>投稿管理</a>
            </li>
        <?php endif; ?>

        <?php if( $this->request->Session()->read('ContractantData.unique_parameter') !== 'sumaiful' ) : ?>
            <li class="<?php echo ( $this->name === 'Residences' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Residences', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-user"></i>居住管理</a>
            </li>
        <?php endif; ?>

        <?php if( $unique_parameter === 'willing' ): ?>
            <li class="<?php echo ( $this->name === 'Points' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Points', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-dollar-sign"></i></i>ポイント管理</a>
            </li>
            <?php if( $this->name === 'Points' || $this->name === 'PointsProducts' ): ?>
                <li class="<?php echo ( $this->name === 'PointsProducts' ) ? "selected" : "" ; ?>">
                    <a href="<?php echo $this->Url->build( [ 'controller' => 'PointsProducts', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-dollar-sign"></i></i>ポイント交換商品管理</a>
                </li>
            <?php endif; ?>
            <li class="<?php echo ( $this->name === 'Equipments' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Equipments', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-baseball-ball"></i></i>備品管理</a>
            </li>
            <li class="<?php echo ( $this->name === 'Faqs' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Faqs', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-question"></i></i>Q&A管理</a>
            </li>

            <?php if( $this->name === 'Faqs' || $this->name === 'FaqsQuestions' ): ?>
            <li class="<?php echo ( $this->name === 'FaqsQuestions' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'FaqsQuestions', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-question"></i></i>Questions管理</a>
            </li>
            <?php endif; ?>
        <?php endif; ?>

        <li class="<?php echo ( $this->name === 'CommonUsers' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'CommonUsers', 'action' => 'index' ] ); ?>"><i class="navi_icon far fa-user"></i>共通ユーザー管理</a>
        </li>

        <?php if( in_array( 9, $valid_service_menuns, true ) ): ?>
            <li class="<?php echo ( $this->name === 'Enquetes' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Enquetes', 'action' => 'index' ] ); ?>"><i class="fas fa-clipboard-list"></i>アンケート管理</a>
            </li>
        <?php endif; ?>

        <?php /*
        <li class="<?php echo ( $this->name === 'LuggageStrages' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'LuggageStrages', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-suitcase-rolling"></i>荷物預かり管理</a>
        </li>
        */ ?>

        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true) ): ?>
        <li class="<?php echo ( $this->name === 'ServiceMenus' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'ServiceMenus', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-ellipsis-h"></i>メニュー管理</a>
        </li>
        <?php endif; ?>

        <?php /*
        <li class="<?php echo ( $this->name === 'AppScreens' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'AppScreens', 'action' => 'index' ] ); ?>"><i class="fas fa-mobile-alt"></i>画面管理</a>
        </li>
        */ ?>

<?php /*
        <li class="<?php echo ( $this->name === 'TermsOfServices' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'TermsOfServices', 'action' => 'index' ] ); ?>"><i class="far fa-file-alt"></i>利用規約</a>
        </li>

        <li class="<?php echo ( $this->name === 'PrivacyPolicies' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'PrivacyPolicies', 'action' => 'index' ] ); ?>"><i class="far fa-file-alt"></i>プライバシーポリシー</a>
        </li>
*/ ?>

        <?php
            if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true)
            && $this->request->Session()->read('ContractantData.unique_parameter') !== 'sumaiful') :
        ?>
        <li class="<?php echo ( $this->name === 'Presentations' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Presentations', 'action' => 'index' ] ); ?>"><i class="fas fa-exclamation-triangle"></i>ユーザー報告管理</a>
        </li>
        <?php endif; ?>

        <?php if( in_array( $this->request->session()->read('Admin.Auth.admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true) ): ?>
        <li class="<?php echo ( $this->name === 'Admins' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Admins', 'action' => 'index' ] ); ?>"><i class="navi_icon fas fa-user-cog"></i>管理ユーザー</a>
        </li>
        <?php endif; ?>

        <?php if( $login_user['admin_user']['building_id'] === 0 ): ?>
            <li class="<?php echo ( $this->name === 'Configs' ) ? "selected" : "" ; ?>">
                <a href="<?php echo $this->Url->build( [ 'controller' => 'Configs', 'action' => 'index' ] ); ?>"><i class="fas fa-cog"></i> 設定</a>
            </li>
        <?php endif; ?>

    </ul>
</nav>
