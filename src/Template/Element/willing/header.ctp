<header>
    <div class="header_wrap clearfix">
        <h1>
            <a href="<?php echo noh( $this->Url->build('/') ); ?>">
                <?php echo noh ( $this->Html->image("img_logo.png", ["alt" => "willing"]) ); ?>
            </a>
        </h1>
        <?php if( $this->name !== 'Accounts' ): ?>
            <?php if( $login_user['front_user']['common_user_flg'] === 0 ): ?>
            <div class="userdata">
                <a class="photo_wrap" href="<?php echo noh( $this->Url->build([ 'controller' => 'Mypages', 'action' => 'index' ]) ); ?>">
                    <div class="photo" style="background-image:url(<?php echo DS . $login_user['front_user']['thumbnail_path']; ?>);">
                        <?php /* echo noh( $this->Html->image( DS . $login_user['front_user']['thumbnail_path'], ["alt" => "willing"]) );*/ ?>
                        <?php /*echo $this->Html->link(
                            $this->Html->image("img_user.png", array("alt" => "willing")),
                            "/Mypages",
                            ['escape' => false]
                        );*/ ?>
                    </div>
                </a>
                <div class="name <?php if( $login_user['front_user']['leave_flg'] === 1 ): ?>leaved<?php endif; ?>"><?php echo h( $login_user['front_user']['nickname'] ); ?></div>
                <div class="bt_box">
                    <a href="<?php echo noh( $this->Url->build([ 'controller' => 'Messages', 'action' => 'index' ] ) ); ?>" class="bt_message <?php if( $notification['message'] === 1 ): ?>active<?php endif; ?>"><?php echo _('Message'); ?></a>
                    <a href="<?php echo noh( $this->Url->build([ 'controller' => 'Activities', 'action' => 'index' ] ) ); ?>" class="bt_favorite <?php if( $notification['activity'] === 1 ): ?>active<?php endif; ?>"><?php echo _('Activity'); ?></a>
                    <?php if( $login_user['front_user']['leave_flg'] === 0 ): ?>
                        <a href="javascript: void(0)" class="jsConfirmBaggage bt_gift <?php if( $notification['baggage'] === 1 ): ?>active<?php endif; ?>"><?php echo _('Gift'); ?></a>
                    <?php endif; ?>
                    <a href="<?php echo noh( $this->Url->build([ 'controller' => 'LineOpenChats', 'action' => 'index' ] ) ); ?>" class="bt_line_open_chat <?php if( $this->name === 'LineOpenChats' ) echo 'active'; ?>"><?php echo _('Line Open Chat'); ?></a>
                    <a href="<?php echo noh( $this->Url->build([ 'controller' => 'Equipments', 'action' => 'index' ] ) ); ?>" class="bt_equipment <?php if( $this->name === 'Equipments' ) echo 'active'; ?>"><?php echo _('Bihin'); ?></a>
                </div>
            </div>
            <?php endif; ?>
            <div class="menu_box">
                <a class="bt_menu <?php if( ( $this->name === 'Informations' || $this->name === 'Communities' || $this->name === 'Messages' ) && $this->request->action === 'index' ) echo 's_menu' ; ?>" tabIndex="-1"><span>MENU</span></a>

                <?php if( $login_user['front_user']['common_user_flg'] === 0 ): ?>
                <div class="action <?php if( ( $this->name === 'Informations' || $this->name === 'Communities' || $this->name === 'Messages' ) && $this->request->action === 'index' ) echo 's_menu' ; ?>">
                    <?php
                        if( $this->name === 'Posts' ){
                            echo $this->Html->link( 'Posts' , 'posts' , [ 'class' => 'bt_post active' ] ); 
                        } else {
                            echo $this->Html->link( 'Posts' , 'posts' , [ 'class' => 'bt_post' ] ); 
                        }
                    ?>
                    <?php
                        if( $this->name === 'Communities' ){
                            echo $this->Html->link( 'Community' , 'communities' , [ 'class' => 'bt_community active' ] ); 
                        } else {
                            echo $this->Html->link( 'Community' , 'communities' , [ 'class' => 'bt_community' ] ); 
                        }
                    ?>
                </div>
                <div class="point">
                    <span><?php echo noh( number_format( $user_point ) ); ?></span>Pt.
                </div>
                <?php endif; ?>

                <?php if( $display_search_form === true ): ?>
                    <div class="search">
                        <?php echo $this->Form->create( null, [ 'id' => 'search', 'type' => 'get', 'url' => [ '?' => $this->request->getQuery() ] ] ); ?>
                            <?php if( $this->name === 'Communities' ) echo $this->Form->hidden( 'post_category_id', [ 'default' => $this->request->getQuery('post_category_id') ] ); ?>
                            <?php if( $this->request->getQuery('category') !== null ) echo $this->form->hidden( 'category', [ 'default' => $this->request->getquery('category') ] ); ?>
                            <?php echo $this->form->text( 'search', [ 'placeholder' =>  __('Search'), 'default' => $this->request->getQuery('search') ]); ?>
                        <?php echo $this->Form->end(); ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</header>
<?php /*
<div class="menu_bg" style="display:none;"></div>
*/ ?>
<div id="menu" class="">
    <nav>
        <ul>
            <?php if( $login_user['front_user']['common_user_flg'] === 0 ): ?>
            <li>
                <a href="<?php echo noh( $this->Url->build(['controller' => 'Accounts', 'action' => 'edit']) ); ?>" tabIndex="-1"><?php echo __('プロフィール変更'); ?></a>
            </li>
            <?php endif; ?>
            <li>
                <a href="<?php echo noh( $this->Url->build( $privacy_url) ); ?>" target="blank" tabIndex="-1"><?php echo __('プライバシーポリシー'); ?></a>
            </li>
            <li>
                <a href="<?php echo noh( $this->Url->build( ['controller' => 'faqs', 'action' => 'index']) ); ?>" target="blank" tabIndex="-1"><?php echo __('Q&A'); ?></a>
            </li>
            <li>
                <a href="<?php echo noh( $this->Url->build( ['controller' => 'Terms', 'action' => 'index']) ); ?>" target="blank" tabIndex="-1"><?php echo __('利用規約'); ?></a>
            </li>
            <li>
                <a href="<?php echo noh( $this->Url->build( ['controller' => 'Logins', 'action' => 'logout']) ); ?>" tabIndex="-1"><?php echo __('ログアウト'); ?></a>
            </li>
        </ul>
    </nav>
</div>

