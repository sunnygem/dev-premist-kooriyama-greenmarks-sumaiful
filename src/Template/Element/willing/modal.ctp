<div id="gift_modal" class="modal_wrap">
    <div class="modal_content">
        <div class="inner">
            <p>
                <?php echo __('荷物を受け取りますか？'); ?>
            </p>
            <form  data-action="<?php echo $this->Url->build(['controller' => 'Tops', 'action' => 'receiveBaggage']); ?>">
                <?php echo $this->Form->hidden('user_id',       ['value' => $login_user['id']]); ?>
                <?php echo $this->Form->hidden('front_user_id', ['value' => $login_user['front_user']['id']]); ?>
                <div class="btn">
                    <button class="jsReceiveBaggase" type="button"><?php echo __('受け取り済み'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="func_modal" class="modal_wrap">
    <div class="modal_content">
        <div class="inner post_menu">
            <ul>
                <?php /* 20190523 暫定で利用させない
                <li class="my_func">
                    <a href="javascript: void(0);"><?php echo __('編集する') ?></a>
                </li>
                */ ?>
                <?php if( $this->name !== 'Albums'): ?>
                    <li class="item message">
                        <a href="javascript: void(0);" class="bt_message"><?php echo __('メッセージ') ?></a>
                    </li>
                <?php endif; ?>
                <li class="item report">
                    <a href="javascript: void(0);" class="bt_report"><?php echo __('報告する') ?></a>
                </li>
                <li class="item delete">
                    <?php echo $this->Form->create( null );?>
                        <?php echo $this->Form->hidden('type', ['value' => 'delete']); ?>
                        <a href="javascript: void(0);" class="fnPostDelete" data-msg="<?php echo __('本当に削除しますか？'); ?>"><?php echo __('削除する') ?></a>
                    <?php echo $this->Form->end(); ?>
                </li>
                <li>
                    <a href="javascript: void(0);" class="bt_cancel"><?php echo __('キャンセル') ?></a>
                </li>
            </ul>
        </div>
        <div class="inner report_menu">
            <ul>
                <li class="report">
                    <a href="javascript: void(0);" class="report_item"
                    data-report-type="privacy"
                    data-msg="<?php echo __("この投稿を報告します。\nよろしいですか？"); ?>"
                    ><?php echo __('プライバシーの侵害') ?></a>
                </li>
                <li class="report">
                    <a href="javascript: void(0);" class="report_item"
                    data-report-type="other"
                    data-msg="<?php echo __("この投稿を報告します。\nよろしいですか？"); ?>"
                    ><?php echo __('その他') ?></a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="bt_cancel"><?php echo __('キャンセル') ?></a>
                </li>
            </ul>
        </div>
    </div>

</div>

<div id="activity_modal" class="modal_wrap">
    <div class="modal_content">
        <div class="inner">
            <ul class="list">
            </ul>
        </div>
    </div>
</div>
