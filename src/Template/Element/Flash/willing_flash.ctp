<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="flash_wrap" onclick="this.classList.add('hidden')">
    <div class="flash_bg">
        <div class="message success"><?= $message ?></div>
    </div>
</div>
