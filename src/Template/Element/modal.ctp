<div class="modal unsubscribe" id="unsubscribe">
   <div class="title">
       退会しますか？
   </div>
   <p class="txt">
       にどこまでも応えていくということです<br>
       これまでの新築マンションがいわばお仕着せの洋服
   </p>
   <div class="btn">
       <button class="cancel">
           <img src="<?php echo APP_URL . '/img/common/header_menu_button_cancel.png'?>">
       </button>
       <button class="modal-complete" data-target="unsubscribe_complete">
           <img src="<?php echo APP_URL . '/img/common/header_menu_button_logout.png'?>">
       </button>
   </div>
</div>

<div class="modal logout" id="logout">
   <div class="title">
       ログアウトしますか？
   </div>
   <p class="txt">
       にどこまでも応えていくということです<br>
       これまでの新築マンションがいわばお仕着せの洋服
   </p>
   <div class="btn">
       <button class="cancel">
           <img src="<?php echo APP_URL . '/img/common/header_menu_button_cancel.png'?>">
       </button>
       <button class="modal-complete" data-target="logout_complete">
           <img src="<?php echo APP_URL . '/img/common/header_menu_button_logout.png'?>">
       </button>
   </div>
</div>

<div class="modal complete" id="unsubscribe_complete">
   <div class="title">
       退会完了しました
   </div>
   <div class="logo">
       <img src="<?php echo APP_URL . '/img/common/header_logo.png'; ?>">
   </div>
   <div class="txt">
       にどこまでも応えていくということです<br>
       これまでの新築マンショ<br>
       ンがいわばお仕着せの洋服だと<br>
       したらインプレそれはあなたの理想と
   </div>
   <div class="border">
   </div>
</div>

<div class="modal complete" id="logout_complete">
   <div class="title">
       ログアウト完了しました
   </div>
   <div class="logo">
       <img src="<?php echo APP_URL . '/img/common/header_logo.png'; ?>">
   </div>
   <div class="txt">
       にどこまでも応えていくということです<br>
       これまでの新築マンショ<br>
       ンがいわばお仕着せの洋服だと<br>
       したらインプレそれはあなたの理想と
   </div>
   <div class="border">
   </div>
</div>

<div class="modal disable" id="disable">
   <div class="border">
   </div>
   <div class="txt">
        ご入居後に<br>
        ご利用できる機能です。
   </div>
   <div class="border">
   </div>
</div>
