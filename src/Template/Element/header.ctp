<header>
    <div class="header_wrap">
        <div class="left">
            <div class="menu_trigger">
                <span class="">
                    <img src="<?php echo APP_URL . '/img/common/header_menu_open.png'; ?>">
                </span>
            </div>
            <?php if( $this->name !== 'Tops' ): ?>
            <div class="apartment_logo">
                <img src="<?php echo APP_URL . '/img/common/header_apartment_logo.png'; ?>">
            </div>
            <?php endif; ?>
        </div>
        <div class="header_logo">
            <img src="<?php echo APP_URL . '/img/common/header_logo.png'; ?>">
        </div>
    </div>
</header>
<div id="menu">
    <div class="menu_wrap">
        <div class="menu_header">
            <div class="name">
                USER NAME
            </div>
            <div class="header_logo">
                <img src="<?php echo APP_URL . '/img/common/header_logo.png'; ?>">
            </div>
        </div>
        <div class="border"></div>
        <div class="menu_list">
            <div class="setting">
                <ul>
                    <li><a href="/settings/change_profile">プロフィール変更</a></li>
                    <!--li><a href="/settings/change_mail">メールアドレス変更</a></li-->
                    <!--li><a href="/settings/change_password">パスワード変更</a></li-->
                    <li><a href="/settings/approval_key">家族用承認キー発行</a></li>
                </ul>
            </div>
            <div class="document">
                <ul>
                    <li><a href="/documents/faqs">Q&A</a></li>
                    <li><a href="/documents/terms">利用規約</a></li>
                    <li><a href="/documents/privacy_policy">プライバシーポリシー</a></li>
                    <li><a href="/documents/license">ライセンス表記</a></li>
                </ul>
            </div>
            <div class="operation">
                <ul>
                    <li><a href="#" class="modal-open" data-target="unsubscribe">退会する</a></li>
                    <li><a href="#" class="modal-open" data-target="logout">ログアウト</a></li>
                </ul>
            </div>
        </div>
        <ul>
            <li><a href="/home-kits">HOME KIT</a></li>
            <li><a href="/concierges">CONCIERGE</a></li>
            <li><a href="/more-sumaifuls">MORE SUMAIFUL</a></li>
            <li><a href="/communications">COMMUNICATION</a></li>
        </ul>
        <div class="menu_close" id="menu_close">
            <a href="#">
                <img src="<?php echo APP_URL . '/img/common/header_menu_close.png'; ?>">
            </a>
        </div>
    </div>
</div>
