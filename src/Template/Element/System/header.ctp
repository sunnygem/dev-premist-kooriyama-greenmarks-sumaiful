<header>
    <div class="left">
        <div class="logo">
            <h1>
                <a href="<?php echo $this->Url->build([ 'controller' =>  'Tops', 'action'=> 'index'] ); ?>">
                    システム画面
                </a>
            </h1>
        </div>
    </div>
    <div class="right">
        <!--span class="user_name">
            ログインID：<?php // echo $consultant_login_data['login_id']; ?>
        </span-->
        <span class="logout">
        <a href="<?php echo $this->Url->build( [ 'controller' => 'logins', 'action' => 'logout' ] ); ?>"><i class="fa fa-sign-out-alt"></i>ログアウト</a> </span>
    </div>
</header>
