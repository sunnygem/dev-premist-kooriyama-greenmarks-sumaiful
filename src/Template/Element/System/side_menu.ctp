<nav class="side_menu">
    <ul>
        <li class="<?php echo ( $this->name === 'Tops' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Tops', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-home"></i>ホーム</a>
        </li>

        <li class="<?php echo ( $this->name === 'ServiceMenus' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'ServiceMenus', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-list-ul"></i>メニュー管理</a>
        </li>

        <li class="<?php echo ( $this->name === 'Contractants' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Contractants', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-handshake"></i>契約管理</a>
        </li>

        <?php /*
        <li class="<?php echo ( $this->name === 'Informations' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Informations', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-info-circle"></i>インフォメーション</a>
        </li>
        */ ?>

        <li class="<?php echo ( $this->name === 'Users' ) ? "selected" : "" ; ?>">
            <a href="<?php echo $this->Url->build( [ 'controller' => 'Users', 'action' => 'index' ] ); ?>"><i class="navi_icon fa fa-user-cog"></i>管理者</a>
        </li>
    </ul>
</nav>
