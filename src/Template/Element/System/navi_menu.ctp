<div class="navi_wrap">
    <nav>
        <ul class="navi">
            <li class="<?php echo noh( $this->request->action === 'index' ) ? "active" : "" ; ?>">
                <a href="<?php echo noh( $this->Url->build( [ 'action' => 'index' ] ) ); ?>">メニュー一覧</a>
            </li>
            <li class="<?php echo noh( $this->request->action === 'types' ) ? "active" : "" ; ?>">
                <a href="<?php echo noh( $this->Url->build( [ 'action' => 'types' ] ) ); ?>">タイプ設定</a>
            </li>
        </ul>
    </nav>
</div>
