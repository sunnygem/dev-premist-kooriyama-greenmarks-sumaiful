<div class="concierge enquete">
    <div class="title">
        <p class="date"><?php echo noh( $menu_enquetes->release_date->format( 'Y.m.d' ) ); ?></p>
        <h2><?php echo noh( $menu_enquetes->title ); ?></h2>
    </div>
    <div class="enquete_blc">
        <h3 class="num">
            <div>QUESTION</div>
            <?php echo sprintf( '%02d', $page ); ?>
        </h3>
        <p class="question">
            <?php echo nl2br( h( $enquete_question->question ) ); ?>
        </p>
        <div class="answer">
            <?php echo $this->Form->create(null); ?>
            <?php echo $this->Form->hidden( 'page', ['value' => $page ] ); ?>
            <?php
                switch( $enquete_question->type ): 
                case 'text': ?>
                <div class="text_box">
                    <?php echo $this->Form->text( 'answer[' . $page . '][]', ['placeholder' => $enquete_question->placeholder ] ); ?>
                </div>
            <?php break; ?>
            <?php case 'textarea': ?>
                <div class="text_box">
                    <?php echo $this->Form->textarea( 'answer[' . $page . '][]', ['placeholder' => $enquete_question->placeholder ] ); ?>
                </div>
            <?php break; ?>
            <?php case 'radio': ?>
                <?php echo $this->Form->hidden( 'answer[' . $page . ']' ); ?>
                <?php $options = explode( '|', $enquete_question->item ); ?>
                <div class="btn">
                    <?php foreach( $options as $key => $val ): ?>
                        <input type="radio" name="answer[<?php echo $page; ?>][]" id="answer_<?php echo $key; ?>" value="<?php echo h( $val ); ?>">
                        <label for="answer_<?php echo $key; ?>"><?php echo h( $val ); ?></label>
                        <?php /*
                        <button type="button">
                            <input type="radio" name="answer[<?php echo $page; ?>]" id="answer_<?php echo $key; ?>" value="<?php echo h( $val ); ?>">
                            <?php echo h( $val ); ?>
                        </button>
                        */ ?>
                    <?php endforeach; ?>
                </div>
            <?php break; ?>
            <?php case 'select': ?>
                <?php echo $this->Form->hidden( 'answer[' . $page . ']' ); ?>
                <?php $options = explode( '|', $enquete_question->item ); ?>
                <div class="<?php if( $enquete_question->multiple_flg === 1 ): ?>check_box<?php else: ?>radio_box<?php endif; ?>">
                    <ul>
                        <?php foreach( $options as $key => $val ): ?>
                            <li>
                                <?php if( $enquete_question->multiple_flg === 1 ): ?>
                                    <input type="checkbox" name="answer[<?php echo $page; ?>][]" id="answer_<?php echo $key; ?>" value='<?php echo h( $val ); ?>'>
                                    <label for="answer_<?php echo $key; ?>"><?php echo h( $val ); ?></label>
                                <?php else: ?>
                                    <input type="radio" name="answer[<?php echo $page; ?>][]" id="answer_<?php echo $key; ?>" value="<?php echo h( $val ); ?>">
                                    <label for="answer_<?php echo $key; ?>"><?php echo h( $val ); ?></label>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php break; ?>
            <?php case 'checkbox': ?>
                <?php echo $this->Form->hidden( 'answer[' . $page . ']' ); ?>
                <?php $options = explode( '|', $enquete_question->item ); ?>
                <div class="check_box">
                    <ul>
                        <?php foreach( $options as $key => $val ): ?>
                            <li>
                                <input type="checkbox" name="answer[<?php echo $page; ?>][]" id="answer_<?php echo $key; ?>" value='<?php echo h( $val ); ?>'>
                                <label for="answer_<?php echo $key; ?>"><?php echo h( $val ); ?></label>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php break; ?>
            <?php endswitch; ?>
        </div>
        <div class="btn clearfix control">
            <?php if( $page > 1 ): ?>
                <?php echo $this->Form->button('Back', ['name' => 'back', 'class' => 'left']); ?>
            <?php endif; ?>
            <?php echo $this->Form->button('OK',   ['name' => 'next', 'class' => 'right' ]); ?>
            <?php /*
            <button>
                <img src="<?php echo APP_URL . '/img/concierge/concierge_enquete_button01.png';?>">
            </button>
            */ ?>
        </div>

        <?php echo $this->Form->end(); ?>
    </div>

<?php /*
    <div class="enquete_blc">
        <h3 class="num">
            01
        </h3>
        <p class="question">
            にどこまでも応えていくということです。これまでの新築マンションがいわばお仕着せの洋服だとしたらインプレそれはあなたの理想とする住まいを実現するための個性にどこまでも応えていくとい
        </p>
        <div class="answer">
            <div class="radio_box value">
                <ul>
                    <li>
                        <input type="radio" name="test3" id="test3_1"><label for="test3_1">1</label>
                    </li>
                    <li>
                        <input type="radio" name="test3" id="test3_2"><label for="test3_2">2</label>
                    </li>
                    <li>
                        <input type="radio" name="test3" id="test3_3"><label for="test3_3">3</label>
                    </li>
                    <li>
                        <input type="radio" name="test3" id="test3_4"><label for="test3_4">4</label>
                    </li>
                    <li>
                        <input type="radio" name="test3" id="test3_5"><label for="test3_5">5</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="btn">
            <button>
                <img src="<?php echo APP_URL . '/img/concierge/concierge_enquete_button01.png';?>">
            </button>
        </div>
    </div>
*/ ?>

<div class="loader hidden"></div>
</div>
