<div class="concierge enquete">
    <div class="article_list">
        <ul>
            <?php foreach( $menu_enquetes as $val ): ?>
            <li>
                <a href="<?php echo $this->Url->build([ 'action' => 'answer', $val->id ]); ?>">
                    <p class="date"><?php echo $val->release_date->format( 'Y.m.d' ); ?><span>UP</span></p>
                    <p class="title"><?php echo $val->title; ?></p>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
