<style>
    li {
        display:inline-block;
        max-width:300px;
        margin:20px;
    }
    li > a {
        text-decoration:underline;
    }
    div.link { 
        text-align:center;
        padding:20px;
    }
</style>
<div class="form_logo">
    <?php echo $this->Html->image( 'img_logo.png' ); ?>
</div>
<div class="form_wrap">
    <div class="form_content">
        <?= $this->Flash->render(); ?>
        <p>パスワードを設定しました。アプリを起動し、ログインしてください。</p>
        <p>Set a password. Launch the app and login.</p>
        <br>
        <p>ログインIDは、申請時のメールアドレスになります。</p>
        <p>Login ID is the email address at the time of application.</p>
    </div>
</div>
<div class="link">
    <p>アプリは、下記↓よりDLできます。</p>
    <ul>
        <li>iOSの方は<a href="https://itunes.apple.com/jp/app/apple-store/id1462447916?mt=8">こちら</a></li>
        <li>Androidの方は<a href="https://play.google.com/store/apps/details?id=com.willing.cv">こちら</a></li>
    </ul>
    <p>PCの方は<a style="text-decoration:underline;" href="https://<?php echo $this->Session->read( 'ContractantData.domain' ) . '/logins'; ?>">こちら</a></p>
</div>
