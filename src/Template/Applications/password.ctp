<div class="form_logo">
    <?php echo $this->Html->image( 'img_logo.png' ); ?>
</div>
<div class="form_wrap">
    <div class="form_content">
        <?php echo nl2br( noh( $this->Flash->render() ) ); ?>

        <?php echo noh( $this->Form->create( $data, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php echo noh( $this->Form->hidden( 'id' ) ); ?>

        <div class="form">
            <table>
                <tr>
                    <td colspan="2">
                        <div style="text-align:center;">任意のパスワードを設定してください。</div>
                        <div style="text-align:center;">Set a password of your choice.</div>
                    </td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td>
                        <?php echo noh( $this->Form->input( 'password', [ 'label' => false, 'placeholder' => '英数字8文字以上' ] ) ); ?>
                    </td>
                </tr>
            </table>
        </div>

        <div class="text_c">
            <?php echo noh( $this->Form->button( 'OK', [ 'label' => false, 'name' => 'send', 'class' => 'btn submit active' ] ) ); ?>
        </div>

        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
