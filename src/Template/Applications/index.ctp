
<?php echo noh( $this->Form->create( $users, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>

<div class="form_logo">
    <?php echo $this->Html->image( 'willing_logo.png' ); ?>
</div>
<?php echo nl2br( noh( $this->Flash->render() ) ); ?>
<div class="form_wrap">
    <div class="form_content">
        <div class="form">
            <table>
                <tr>
                    <th>Full Name<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->input( 'front_user.name', [ 'label' => false, 'placeholder' => 'フルネーム' ] ) ); ?></td>
                </tr>
                <tr>
                    <th>Email<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->email( 'email', [ 'label' => false, 'placeholder' => 'メールアドレス' ] ) ); ?></td>
                </tr>
                <tr>
                    <th>Building Name<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->select( 'front_user.building_id', $buildings, [ 'label' => false, 'placeholder' => 'Building Name' ] ) ); ?><span></span></td>
                </tr>
                <tr>
                    <th>Room No.<span class="require">*</span></th>
                    <td><?php echo noh( $this->Form->text( 'front_user.room_number', [ 'label' => false, 'placeholder' => '部屋番号' ] ) ); ?></td>
                </tr>
                <tr>
                    <th style="vertical-align:middle;">再申請<br>Reapply</th>
                    <td><?php echo noh( $this->Form->checkbox( 'front_user.reapply_flg', [ 'label' => false, 'id' => 'reapply' ] ) ); ?><label for="reapply"></label>
                        <p class="fs13">メールアドレス・パスワードを忘れた方はこちらにチェックを入れて再申請して下さい。<br>
                        If you forgot your email or password, please check this box and apply again.</p>
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align:middle;">利用規約<br>Terms of Use</th>
                    <td><?php echo noh( $this->Form->checkbox( 'front_user.reapply_flg', [ 'label' => false, 'id' => 'reapply' ] ) ); ?><label for="reapply"></label>
                        <p class="fs13">メールアドレス・パスワードを忘れた方はこちらにチェックを入れて再申請して下さい。<br>
                        If you forgot your email or password, please check this box and apply again.</p>
                    </td>
                </tr>
            </table>
        </div>

        <div class="text_c">
            <?php echo noh( $this->Form->button( 'OK', [ 'label' => false, 'name' => 'send', 'class' => 'btn submit active' ] ) ); ?>
        </div>
    </div>
</div>

<?php echo noh( $this->Form->end() ); ?>

<p><a href="/applications/help" target="_blank">メールが届かない場合はこちら</a></p>
