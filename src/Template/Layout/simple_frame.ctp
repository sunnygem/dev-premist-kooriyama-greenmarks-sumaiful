<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>
        <?php if( $this->name === 'Tops' ) : ?>
            willing
        <?php else: ?>
        <?= $this->fetch('title') ?> | <?php echo noh( $title_for_layout ); ?>
        <?php endif; ?>
    </title>

    <?php
        if( $favicon !== false )
        {
            echo noh(  $this->Html->meta('icon', $favicon ) );
        }
        else
        {
            echo noh(  $this->Html->meta('icon') );
        }
    ?>

    <?php echo noh( $this->Html->script([ '/js/jquery-3.2.1.min', '/js/jquery.easing.1.3']) ); ?>
    <?php /*if ( ENV !== 'pro' ): ?>
        <?php echo noh( $this->Html->script([ '/js/vue/vue']) ); ?>
    <?php else: ?>
        <?php echo noh( $this->Html->script([ '/js/vue/vue.min']) ); ?>
    <?php endif;*/ ?>
    <?php echo noh( $this->Html->script([ '/js/common', 'script' ], ['defer' => true]) ); ?>

    <?php echo noh( $this->Html->css( 'style' ) ); ?>

</head>
<body>
    <div id="frame">
        <?= $this->Flash->render('flash', ['element' => 'Flash/willing_flash']) ?>
        <section class="container">
            <div class="contents">
                <?php echo noh( $this->fetch('content') ); ?>
            </div>
        </section><!-- /.container -->
    </div>
</body>
</html>
