<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html lang=<?php echo noh( $lang ); ?>>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>
        <?php if( $this->name === 'Tops' ) : ?>
            willing
        <?php else: ?>
        <?= $this->fetch('title') ?> | willing
        <?php endif; ?>
    </title>
    <?php
        if( $favicon !== false )
        {
            echo noh(  $this->Html->meta('icon', $favicon ) );
        }
        else
        {
            echo noh(  $this->Html->meta('icon') );
        }
    ?>

    <?php echo noh( $this->Html->script([ '/js/jquery-3.2.1.min', '/js/jquery.easing.1.3', 'jquery.easingscroll', '/js/object-assign']) ); ?>
    <?php if ( ENV !== 'pro' ): ?>
        <?php echo noh( $this->Html->script([ '/js/vue/vue']) ); ?>
    <?php else: ?>
        <?php echo noh( $this->Html->script([ '/js/vue/vue.min']) ); ?>
    <?php endif; ?>
    <?php echo noh( $this->Html->script([ '/js/common', 'common', 'auto_paging'], ['defer' => true]) ); ?>

    <?php
       if( in_array( $this->name, [ 'Applications', 'Terms', 'Faqs'], true ) )
       {
           echo noh( $this->Html->css( '/css/common' ) );
           echo noh( $this->Html->css( 'application' ) );
       }
       else
       {
           echo noh( $this->Html->css( 'style' ) );
       }
    ?>

</head>
<body id="<?php echo noh( $this->fetch('body_id') ); ?>">
    <?php //if( $this->name !== 'Settings' ) echo $this->element('header') ?>
    <?php if ( in_array( $this->name, ['Applications', 'Terms', 'Faqs'], true ) ): ?>
        <section id="top">
            <div class="container">
                <div class="contents form">
                    <?php echo $this->fetch('content') ?>
                </div>
            </div><!--/.container -->
        </section>
    <?php else: ?>
        <section id="top" class="outline">
        <?php if ( $this->name === 'Logins' ): ?>
            <header>
                <h1><a href="/"><?php echo noh( $this->Html->image( 'img_logo.png', ['alt' => 'willing']) );?></a></h1>
            </header>
            <?php echo noh( $this->fetch('content') ); ?>
        <?php else: ?>
            <?= $this->Flash->render('flash', ['element' => 'Flash/willing_flash']) ?>
            <?php echo $this->element('willing/header'); ?>
            <div class="main">
                <?php echo noh( $this->fetch('content') ); ?>
            </div><!-- /main -->

            <div class="modal_bg"></div>
            <?php if( $login_user['front_user']['common_user_flg'] === 0 ): ?>
                <?php echo $this->element('willing/modal'); ?>
            <?php endif; ?>

        <?php endif; ?>
        </section>
    <?php endif; ?>
    <?php //if( $this->name === 'Settings' ) echo $this->fetch('content') ?>

</body>
</html>
