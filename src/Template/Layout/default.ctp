<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <script type="text/javascript" src="<?php echo APP_URL . '/js/jquery-3.2.1.min.js' ; ?>"></script>
    <script type="text/javascript" src="<?php echo APP_URL . '/js/common.js?' . time(); ?>"></script>
    <?php if ( ENV !== 'pro' ): ?>
        <script type="text/javascript" src="<?php echo APP_URL . '/js/vue/vue.js?' . time(); ?>"></script>
    <?php else: ?>
        <script type="text/javascript" src="<?php echo APP_URL . '/js/vue.min.js?' . time(); ?>"></script>
    <?php endif; ?>

    <link rel="stylesheet" href="<?php echo APP_URL . '/css/common.css?' . time(); ?>">
    

</head>
<body>
    <?= $this->Flash->render() ?>
    <div class="container">
        <?php if( $this->name !== 'Settings' ) echo $this->element('header') ?>
        <div class="contents">
            <?php if( $this->name !== 'Settings' ) echo $this->fetch('content') ?>
        </div>
        <?php if( $this->name === 'Settings' ) echo $this->fetch('content') ?>
    </div>
    <?php echo $this->element('modal'); ?>
</body>
</html>
