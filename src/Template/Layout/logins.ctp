<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php echo noh( $this->Html->script([ '/js/jquery-3.2.1.min',  'common']) ); ?>
    <?php echo noh( $this->Html->css( 'style' ) ); ?>
    <?php echo noh( $this->Html->css( 'https://fonts.googleapis.com/css?family=Noto+Serif+JP' ) ); ?>

</head>
<body id="<?php echo noh( $this->fetch('body_id') ); ?>">
    <?= $this->Flash->render() ?>
    <div class="container">
        <div class="contents login">
            <div class="logo">
                <?php echo $this->Html->image('img_logi.png');?>
            </div>
            <?php echo $this->fetch('content') ?>
        </div>
    </div>
    <?php echo $this->element('modal'); ?>
</body>
</html>

