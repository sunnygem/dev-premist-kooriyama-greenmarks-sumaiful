<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?php
        if( $favicon !== false )
        {
            echo noh(  $this->Html->meta('icon', $favicon ) );
        }
        else
        {
            echo noh(  $this->Html->meta('icon') );
        }
    ?>

    <?php /*= $this->Html->css('base.css') */ ?>
    <?php echo noh( $this->Html->css(['system/system', '/font/fontawesome/css/all', 'system/jquery.datetimepicker.min' ]) ); ?>
    <?php echo noh( $this->Html->script(['jquery-3.2.1.min', 'system/jquery.datetimepicker.full.min', 'system/common', 'ckeditor/ckeditor', 'startCkeditor' ]) ); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <?php
        $ContractantData = $this->request->Session()->read( 'ContractantData');
        if( isset( $ContractantData['theme_color'] ) && $ContractantData['theme_color'] !== null )
        {
            $theme_color = $ContractantData['theme_color'];
        }
        else
        {
            $theme_color = '#737373';
        }

    ?>
    <style>
        a{
            color: <?php echo $theme_color; ?>;
        }
        .btn,
        .side_menu ul > li.selected > a,
        .radio input[type=radio]:checked + label:after,
        .radio input[type=checkbox]:checked + label:after,
        .checkbox input[type=checkbox]:checked + label:after,
        .tab_list .tab_wrap .tab.selected
        {
           background-color: <?php echo $theme_color; ?>;
        }
    </style>

</head>
<body>
    <div id="main_wrap">
        <?php if( $this->name === 'Logins' ): ?>
            <main class="contents">
                <?php echo $this->fetch('content'); ?>
            </main><!--/ .contents -->
            <footer>since <?php echo noh( $since_year ); ?>~</footer>
        <?php else: ?>
            <main class="contents_wrap">
            <?php echo $this->element( 'Admin/header' ); ?>
            <?php echo $this->element( 'Admin/side_menu' ); ?>
                <div class="contents">
                    <?php echo $this->fetch('content'); ?>
                </div><!--/ .contents -->
            </main><!--/ .contents_wrap -->
        <?php endif; ?>
    </div><!--/#main_wrap -->

</body>
</html>
