<div class="">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo h( $title_for_layout ); ?></h2>
        <div class="right"> 
            <?php echo noh( $this->Html->link( '契約管理に戻る', [ 'controller' => 'Contractants', 'action' => 'index' ], [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit', $contractant->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
    </div>

    <div class="form_table">
        <table>
           <tr>
               <th>契約名</th>
               <td>
                   <?php echo h( $contractant->name ); ?>
               </td>
           </tr>
           <tr>
               <th>ドメイン</th>
               <td>
                   <?php echo h( $contractant->domain ); ?>
               </td>
           </tr>
        </table>
    </div>

    <div class="text_c">
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>表示フラグ</th>
                <th>名前</th>
                <th>住所</th>
                <th>アイコン</th>
                <th>部屋番号管理</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr class="<?php if( $val->display_flg === 0 ) echo 'display_hidden'; ?>">
                    <td class="title"><?php echo h( $val->id ); ?></td>
                    <td class=""><?php echo noh( ( $val->display_flg ) ? '表示': '非表示' ); ?></td>
                    <td class=""><?php echo h( $val->name); ?></td>
                    <td class="">
                        〒<?php echo h( $val->zip ); ?><br>
                        <?php echo h( $val->address ); ?>
                    </td>
                    <td class=""><?php echo noh( ( $val->icon_path ) ? '〇' : '×'); ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'roomNumber', $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 部屋番号 管理</a>
                    </td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $contractant->id, $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'delete', $contractant->id, $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
