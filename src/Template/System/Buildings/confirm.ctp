<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?><?php if( isset( $data->id ) ): ?> 編集 <?php else: ?>登録 <?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>表示フラグ</th>
                <td> <?php echo h( ( $data['display_flg'] ) ? '表示' : '非表示' ); ?> </td>
            </tr>
            <tr>
                <th>建物名</th>
                <td> <?php echo h( $data['name'] ); ?> </td>
            </tr>
            <tr>
                <th>建物名(英語)</th>
                <td> <?php echo h( $data['name_en'] ); ?> </td>
            </tr>
            <tr>
                <th>建物名(かな)※検索用</th>
                <td> <?php echo h( $data['name_kana'] ); ?> </td>
            </tr>

            <tr>
                <th>住所</th>
                <td>
                    〒<?php echo h( $data['zip'] ); ?><br>
                    <?php echo h( $data['address'] ); ?>
                </td>
            </tr>
            <tr>
                <th>アイコン画像</th>
                <td class="file">
                    <?php
                        if( isset( $data['files']['buildings']['file_path'] ) ) // 登録されたサムネを表示 
                        {
                            echo noh( '<img src="' . DS . $data['files']['buildings']['file_path'] ) . '">';
                        }
                        elseif( isset( $data['icon_path'] ) ) // 登録されたサムネを表示 
                        {
                            echo noh( '<img src="' . DS . $data['icon_path'] ) . '">';
                        }
                        else
                        {
                            echo noh( '--' );
                        }
                    ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="text_c btn_area">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit', $contractant->id] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</div>
