<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?>
            <?php if( isset( $data->id ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <?php echo noh( $this->Form->create( $data, [ 'enctype' => 'multipart/form-data', 'novalidate' => true, 'autocomplete' => 'offm' ] ) ); ?>
        <?php if( isset( $data->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $data->id ] ) ); ?>
        <div class="form_table">
            <table>

                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>建物名</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => '物件名' ] ) ); ?>
                        <?php if( $this->Form->isFieldError('name') ) echo noh( $this->Form->error('name') ); ?>
                    </td>
                </tr>

                <tr>
                    <th>建物名(英語)</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name_en', [ 'label' => false, 'placeholder' => '物件名(英語)' ] ) ); ?>
                        <?php if( $this->Form->isFieldError('name_en') ) echo noh( $this->Form->error('name_en') ); ?>
                    </td>
                </tr>

                <tr>
                    <th>建物名(かな)※検索用</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name_kana', [ 'label' => false, 'placeholder' => '物件名(かな)' ] ) ); ?>
                        <?php if( $this->Form->isFieldError('name_kana') ) echo noh( $this->Form->error('name_kana') ); ?>
                    </td>
                </tr>


                <tr>
                    <th>住所</th>
                    <td>
                        <div style="margin-bottom: 1rem;">
                            〒<?php echo noh( $this->Form->text( 'zip', [ 'label' => false, 'placeholder' => '郵便番号', 'style' => 'width: 10rem;', 'maxlength' => 7  ] ) ); ?>
                            <?php if( $this->Form->isFieldError('zip') ) echo noh( $this->Form->error('zip') ); ?>
                        </div>

                        <div>
                            <?php echo noh( $this->Form->text( 'address', [ 'label' => false, 'placeholder' => '物件の住所' ] ) ); ?>
                            <?php if( $this->Form->isFieldError('address') ) echo noh( $this->Form->error('address') ); ?>
                        </div>

                    </td>
                </tr>

                <tr>
                    <th>アイコン画像</th>
                    <td class="file">
                        <div style="preview">
                            <?php if( isset( $data['icon_path'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh( '<img src="' . DS . $data['icon_path'] ) . '">'; ?>
                               <?php echo noh( $this->Form->hidden('icon_path') ); ?>
                            <?php endif; ?>
                        </div>
                        <label for="icon_path" class="btn">アイコン画像を選択する</label>
                        <?php echo noh( $this->Form->file( 'files[buildings]', [ 'id' => 'icon_path' ] ) ); ?>
                        <?php if( isset( $error['icon_path'] ) ) echo noh( '<p class="error">' . $error['icon_path'] . '</p>' ); ?>
                        <?php if( $this->Form->isFieldError('icon_path') ) echo noh( $this->Form->error('icon_path') ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
