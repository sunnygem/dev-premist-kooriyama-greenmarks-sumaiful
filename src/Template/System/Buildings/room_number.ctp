<?php echo noh( $this->Html->script( 'system/file_upload', ['block' => true] ) ); ?>
<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php echo h( $title_for_layout ); ?>
            <?php if( isset( $data->id ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <p><?php echo $this->Html->link('サンプルファイルDL', '/file/room_number_sample.xlsx'); ?>
    <?php echo noh( $this->Form->create( null, [ 'novalidate' => true, 'autocomplete' => 'offm' ] ) ); ?>
        <?php echo noh( $this->Form->hidden( 'building_id', [ 'value' => $building->id] ) ); ?>

        <?php echo $this->Form->hidden('drag_data'); ?>

        <div id="accept_area">
            <p><i class="fas fa-paperclip"></i> ここにエクセルファイルをD&amp;Dしてください。</p>
        </div>

        <div class="text_c">
            <?php echo noh( $this->Form->button( 'アップロードする', [ 'label' => false, 'name' => 'upload', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>

    <div class="form_table">
        <?php if( $room_numbers->isEmpty() ): ?>
            <p>登録がありません</p>
        <?php else: ?>
            <table>
               <?php foreach( $room_numbers as $val ): ?>
               <tr>
                    <th>部屋番号</th>
                    <td><?php echo h( $val->room_number ); ?></td>
                </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>

</div>
