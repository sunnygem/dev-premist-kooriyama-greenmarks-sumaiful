<div class="information">
    <div class="clearfix contents_title">
        <h2 class="left">
            インフォメーション<?php if( isset( $informations->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $informations, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $informations->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $informations->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>表示フラグ</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'display_flg',  [ 'id' => 'display_flg', 'default' => 0 ]) ); ?>
                            <label for="display_flg">表示する</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>タイトル</th>
                    <td><?php echo noh( $this->Form->text( 'title', [ 'label' => false, 'placeholder' => 'タイトルを入力' ] ) ); ?></td>
                </tr>

                <tr>
                    <th>サムネイル</th>
                    <td class="file">
                        <div class="preview">
                            <?php if( isset( $informations['image_preview'] ) ): // 登録されたサムネを表示 ?>
                               <?php echo noh( $this->Html->image( $informations->image_preview ) ); ?>
                               <?php echo $this->Form->hidden( 'image_preview', [ 'value' => $informations->images_preview ] ); //確認画面用 ?>
                               <?php echo $this->Form->hidden( 'image_id',      [ 'value' => $informations->image_id ] ); //確認画面用 ?>
                            <?php endif; ?>
                        </div><!--/.preview -->
                        <label for="image_file" class="btn">画像を選択する</label>
                        <span class="file_name"><?php echo h( $informations->images[0]['name'] ); ?></span>
                        <?php echo noh( $this->Form->file( 'image_file', [ 'id' => 'image_file', 'label' => false ] ) ); ?>
                    </td>
                </tr>
                <tr>
                    <th>契約者</th>
                    <td><?php echo noh( $this->Form->select( 'contractant_id', $contractants, [ 'label' => false, 'empty' => '選択してください' ] ) ); ?></td>
                </tr>

                <tr>
                    <th>公開日</th>
                    <td><?php echo noh( $this->Form->text( 'release_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker'] ) ); ?></td>
                </tr>

                <tr>
                    <th>公開終了日</th>
                    <td><?php echo noh( $this->Form->text( 'close_date',  [ 'placeholder' => '公開終了日を設定', 'class' => 'datepicker']) ); ?></td>
                </tr>

                <tr>
                    <th>外部リンク</th>
                    <td><?php echo noh( $this->Form->text( 'url_link',  [ 'placeholder' => 'リンクを設定']) ); ?></td>
                </tr>
                <tr>
                    <th>PDF</th>
                    <td class="file">
                        <label for="pdf_file" class="btn">pdfを選択する</label>
                        <?php if( isset( $informations['pdf_preview'] ) ): // 登録されたサムネを表示 ?>
                            <span class="file_name"><?php echo h( $informations->pdfs[0]['name'] ); ?></span>
                        <?php endif; ?>
                        <?php echo noh( $this->Form->file( 'pdf_file', [ 'id' => 'pdf_file', 'label' => false ] ) ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
