<div class="menu">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo h( $title_for_layout ); ?></h2>
    </div>

    <div class="clearfix sub_title">
        <h2 class="left">
            メニュー<?php if( isset( $menus->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>

    <?php echo noh( $this->Form->create( $menus, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $menus->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $menus->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>メニュー名</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => 'メニュー名' ] ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>概要</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'description', [ 'label' => false, 'placeholder' => '概要を入力してください' ] ) ); ?>
                        <?php if( isset( $error['description'] ) ) echo noh( '<p class="error">' . $error['description'] . '</p>' ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="btn_area">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
