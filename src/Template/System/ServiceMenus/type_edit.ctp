<div class="menu_type">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo h( $title_for_layout ); ?></h2>
    </div>

    <div class="clearfix sub_title">
        <h2 class="left">
            メニュータイプ<?php if( isset( $types->id ) ): ?> 編集 <?php else: ?> 登録 <?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <?php echo noh( $this->Form->create( $types, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $types->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $types->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>名前</th>
                    <td><?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => '名前を入力' ] ) ); ?></td>
                </tr>

                <tr>
                    <th>説明</th>
                    <td><?php echo noh( $this->Form->textarea( 'description', [ 'label' => false, 'placeholder' => '簡単な説明を入力' ] ) ); ?></td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
