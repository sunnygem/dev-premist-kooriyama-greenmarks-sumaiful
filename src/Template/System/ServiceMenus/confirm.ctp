<div class="menu">
    <div class="clearfix contents_title">
        <h2 class="left"><?php echo h( $title_for_layout ); ?></h2>
    </div>

    <div class="clearfix sub_title">
        <h2 class="left">
            メニュー<?php if( isset( $menus->id ) ): ?>編集<?php else: ?>登録<?php endif; ?> 確認
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <div class="form_table">
        <table>
            <tr>
                <th>名前</th>
                <td><?php echo h( $menus->name ); ?></td>
            </tr>

            <tr>
                <th>該当</th>
                <td><?php echo noh( $menus->description ); ?></td>
            </tr>

        </table>
    </div>

    <div class="text_c">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn btn_back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>

</div>
