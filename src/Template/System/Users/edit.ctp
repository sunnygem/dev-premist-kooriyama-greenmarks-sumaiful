<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            <?php if( isset( $users->id ) ): ?> ユーザー編集 <?php else: ?> ユーザー登録 <?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <?php echo noh( $this->Form->create( $users, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'offm' ] ) ); ?>
        <?php if( isset( $users->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $users->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>名前</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'system_user.name', [ 'label' => false, 'placeholder' => '名前' ] ) ); ?>
                        <?php if( isset( $users['system_user']['id'] ) ) echo noh( $this->Form->hidden( 'system_user.id' ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?>
                    </td>
                </tr>


                <tr>
                    <th>ログインID</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'login_id', [ 'label' => false, 'placeholder' => 'ログインID' ] ) ); ?>
                        <?php if( isset( $error['login_id'] ) ) echo noh( '<p class="error">' . $error['login_id'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>パスワード</th>
                    <td>
                        <?php echo noh( $this->Form->password( 'password', [ 'label' => false, 'placeholder' => 'passsword' ] ) ); ?>
                        <?php if( isset( $error['password'] ) ) echo noh( '<p class="error">' . $error['password'] . '</p>' ); ?>
                    </td>
                </tr>

                <tr>
                    <th>パスワード(確認用)</th>
                    <td>
                        <?php echo noh( $this->Form->password( 'password_confirm', [ 'label' => false, 'placeholder' => 'passsword(確認用)' ] ) ); ?>
                        <?php if( isset( $error['password_confirm'] ) ) echo noh( '<p class="error">' . $error['password_confirm'] . '</p>' ); ?>
                    </td>
                </tr>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
