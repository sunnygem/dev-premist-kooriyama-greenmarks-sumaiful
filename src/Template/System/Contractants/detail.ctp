<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left"> 契約者情報 </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>ドメイン</th>
                <td><?php echo h( $contracts->domain ); ?></td>
            </tr>
            <tr>
                <th>契約者名</th>
                <td><?php echo noh( $contracts->name ); ?></td>
            </tr>
            <tr>
                <th>ステータス</th>
                <td><?php echo noh( $mt_contractant_status[$contracts->status_id] ); ?></td>
            </tr>
            <tr>
                <th>識別子</th>
                <td><?php echo noh( $contracts->unique_parameter ); ?></td>
            </tr>
            <tr>
                <th>契約締結日</th>
                <td><?php echo noh( $contracts->contract_date ); ?></td>
            </tr>
            <tr>
                <th>サービス開始日</th>
                <td><?php echo noh( $contracts->service_start_date ); ?></td>
            </tr>
            <tr>
                <th>サービス終了日</th>
                <td><?php echo noh( $contracts->service_end_date ); ?></td>
            </tr>

            <tr>
                <th>テーマカラー</th>
                <td>
                    <div style=" width: 2rem; height: 1rem; background: <?php echo h( $contracts->theme_color ); ?>"></div>
                </td>
            </tr>
            <tr>
                <th>Android PUSH API KEY</th>
                <td><?php echo noh( $contracts->android_push_api_key ); ?></td>
            </tr>


            <tr>
                <th>メニュー</th>
                <td>
                    <?php if( isset( $contracts->service_menu_id ) && count( $contracts->service_menu_id ) > 0 ): ?>
                        <ul>
                        <?php foreach( $contracts->service_menu_id as $val ): ?>
                            <li><?php echo h( $option_service_menus[$val] ); ?></li>
                        <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        --
                    <?php endif; ?>

                </td>
            </tr>

        </table>
    </div>

    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>ログインID</th>
                <th>パスワード</th>
                <th>有効</th>
                <th>作成日</th>
                <th>更新日</th>
                <th>削除日</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $contracts->admin_users as $val ): ?>

            <tr>
                <td><?php echo noh( $val->id ); ?></td>
                <td><?php echo h( $val->login_id ); ?></td>
                <td><?php echo noh( ( $val->password === null ) ? '未設定': '設定済み' ); ?></td>
                <td><?php echo noh( ( $val->valid_flg === 1 ) ? '有効' : '無効' ); ?></td>
                <td><?php echo h( $val->created ); ?></td>
                <td><?php echo h( $val->modified ); ?></td>
                <td><?php echo h( $val->deleted ); ?></td>
                <td>&nbsp;</td>
            </tr>
            <?php endforeach; ?>

        </table>
    </div>

</div>
