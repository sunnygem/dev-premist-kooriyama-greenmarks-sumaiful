<div class="">
    <div class="clearfix contents_title">
        <h2 class="left">契約者管理</h2>
        <div class="right">
            <a href="<?php echo noh( $this->Url->build( ['action' => 'edit' ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 新規作成</a>
        </div>
    </div>
    <?php echo noh( $this->Flash->render() ); ?>
    <div class="list_table">
        <table>
            <tr>
                <th>ID</th>
                <th>契約者名</th>
                <th>ドメイン</th>
                <th>識別子</th>
                <th>契約ステータス</th>
                <th>契約日</th>
                <th>サービス開始日</th>
                <th>サービス終了日</th>
                <th>メニュー</th>
                <th>建物</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $data as $key => $val ): ?>
                <tr class="<?php if( $val->status_id === 2 ) echo 'display_hidden'; ?>">
                    <td class=""><?php echo h( $val->id); ?></td>
                    <td class="title"><?php echo h( $val->name); ?></td>
                    <td class=""><?php echo h( $val->domain ); ?></td>
                    <td class=""><?php echo h( $val->unique_parameter ); ?></td>
                    <td class=""><?php echo h( $mt_contractant_status[$val->status_id] ); ?></td>
                    <td class=""><?php echo h( $val->contract_date->format('Y/m/d') ); ?></td>
                    <td class=""><?php echo h( $val->service_start_date->format('Y/m/d') ); ?></td>
                    <td class=""><?php echo h( $val->service_end_date->format('Y/m/d') ); ?></td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'selectMenu',   $val->id ]) ); ?>" class="btn"><i class="fa fa-ellipsis-h"></i> 編集</a>
                    </td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( [ 'controller' => 'Buildings', 'action' => 'index', $val->id ]) ); ?>" class="btn"><i class="fa fa-building"></i> 建物管理</a>
                    </td>
                    <td class="">
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'detail',   $val->id ]) ); ?>" class="btn"><i class="fa fa-file"></i> 詳細</a>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'edit',   $val->id ]) ); ?>" class="btn"><i class="fa fa-edit"></i> 編集</a>
                        <a href="<?php echo noh( $this->Url->build( ['action' => 'delete', $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-times"></i> 削除</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
    <div class="text_c admin_pager">
        <?php echo noh( $this->Paginator->first( '最初へ' ) ); ?>
        <?php echo noh( $this->Paginator->prev( '前へ' ) ); ?>
        <?php echo noh( $this->Paginator->numbers() ); ?>
        <?php echo noh( $this->Paginator->next( '次へ' ) ); ?>
        <?php echo noh( $this->Paginator->last( '最後へ' ) ); ?>
    </div>
</div>
