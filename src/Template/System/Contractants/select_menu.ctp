<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left"> 契約者情報 </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>ドメイン</th>
                <td><?php echo h( $contracts->domain ); ?></td>
            </tr>
            <tr>
                <th>契約者名</th>
                <td><?php echo noh( $contracts->name ); ?></td>
            </tr>
            <tr>
                <th>ステータス</th>
                <td><?php echo noh( $mt_contractant_status[$contracts->status_id] ); ?></td>
            </tr>

        </table>
    </div>

    <div>
        <?php echo noh( $this->Form->create( null, [] ) ); ?>
            <?php echo noh( $this->Form->select('service_menu_id', $option_service_menus, [ 'empty' => '追加するメニューを選択' ]) ); ?>
            <?php echo noh( $this->Form->button( '登録する', ['class' => 'btn', 'name' => 'add' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php //if( isset( $error['service_menu_id'] ) ) echo noh( '<p class="error">' . $error['service_menu_id'] . '</p>' ); ?>
    </div>

    <div class="list_table">
        <table>
            <tr>
                <th>管理ID</th>
                <th>メニュー</th>
                <th>ラベル</th>
                <th>有効</th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach( $service_menus as $val ): ?>
                <tr>
                    <td><?php echo h( $val->id ); ?></td>
                    <td><?php echo h( $val->service_menu->name ); ?></td>
                    <td><?php echo h( $val->label ); ?></td>
                    <td><?php echo noh( ( $val->valid_flg === 1 ) ? '有効' : '無効' ); ?></td>
                    <td class="">
                        <?php if( $val->valid_flg === 1 ): ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'invalidMenu', $val->id ]) ); ?>" class="btn btn_delete"><i class="fa fa-stop-circle"></i> 無効化</a>
                        <?php else: ?>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'validMenu',   $val->id ]) ); ?>" class="btn"><i class="fa fa-play-circle"></i> 有効化</a>
                            <a href="<?php echo noh( $this->Url->build( ['action' => 'deleteMenu',  $contracts->id, $val->id ]) ); ?>" class="btn btn_delete remove_contractant_menu" tabIndex="-1"><i class="fa fa-times"></i> 削除</a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
