<div class="user">
    <div class="clearfix contents_title">
        <h2 class="left">
            契約者<?php if( isset( $contracts->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>
    <div class="msg">
        <?php echo noh( $this->Flash->render() ); ?>
    </div>
    <div class="form_table">
        <table>
            <tr>
                <th>ドメイン</th>
                <td><?php echo h( $contracts->domain ); ?></td>
            </tr>
            <tr>
                <th>契約者名</th>
                <td><?php echo noh( $contracts->name ); ?></td>
            </tr>
            <tr>
                <th>ステータス</th>
                <td><?php echo noh( $mt_contractant_status[$contracts->status_id] ); ?></td>
            </tr>
            <tr>
                <th>識別子(変更不可)</th>
                <td><?php echo noh( $contracts->unique_parameter ); ?></td>
            </tr>
            <tr>
                <th>契約締結日</th>
                <td><?php echo noh( $contracts->contract_date ); ?></td>
            </tr>
            <tr>
                <th>サービス開始日</th>
                <td><?php echo noh( $contracts->service_start_date ); ?></td>
            </tr>
            <tr>
                <th>サービス終了日</th>
                <td><?php echo noh( $contracts->service_end_date ); ?></td>
            </tr>
            <tr>
                <th>テーマカラー</th>
                <td>
                    <?php echo h( $contracts->theme_color ); ?>
                    <div style="display: inline-block; width: 2em; height: 1em; background: <?php echo h( $contracts->theme_color ); ?>"></div>
                </td>
            </tr>
            <tr>
                <th>Android PUSH API KEY</th>
                <td><?php echo noh( $contracts->android_push_api_key ); ?></td>
            </tr>

<?php /*
            <tr>
                <th>メニュー</th>
                <td>
                    <?php if( isset( $contracts->service_menu_id ) && count( $contracts->service_menu_id ) > 0 ): ?>
                        <ul>
                        <?php foreach( $contracts->service_menu_id as $val ): ?>
                            <li><?php echo h( $option_service_menus[$val] ); ?></li>
                        <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        --
                    <?php endif; ?>

                </td>
            </tr>
*/ ?>
        </table>
    </div>
    <div class="text_c">
        <?php echo noh( $this->Form->create( null, [ 'url' => ['action' => 'edit'] ] ) ); ?>
            <?php echo noh( $this->Form->button( '戻る', [ 'label' => false, 'name' => 'back', 'class' => 'btn back' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
        <?php echo noh( $this->Form->create() ); ?>
            <?php echo noh( $this->Form->button( '登録', [ 'label' => false, 'name' => 'complete', 'class' => 'btn submit' ] ) ); ?>
        <?php echo noh( $this->Form->end() ); ?>
    </div>
</div>
