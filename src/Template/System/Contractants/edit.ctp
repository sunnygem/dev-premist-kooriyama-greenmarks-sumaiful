<div class="contractant">
    <div class="clearfix contents_title">
        <h2 class="left">
            契約者<?php if( isset( $contracts->id ) ): ?>編集<?php else: ?>登録<?php endif; ?>
        </h2>
    </div>

    <?php echo noh( $this->Flash->render() ); ?>

    <?php echo noh( $this->Form->create( $contracts, [ 'novalidate' => true, 'enctype' => 'multipart/form-data', 'autocomplete' => 'off' ] ) ); ?>
        <?php if( isset( $contracts->id ) ) echo noh( $this->Form->hidden( 'id', [ 'value' => $contracts->id ] ) ); ?>
        <div class="form_table">
            <table>
                <tr>
                    <th>ドメイン</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'domain', [ 'label' => false, 'placeholder' => '契約ドメインを入力' ] ) ); ?>
                        <?php if( isset( $error['domain'] ) ) echo noh( '<p class="error">' . $error['domain'] . '</p>' ); ?> 
                    </td>
                </tr>

                <tr>
                    <th>契約者名</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'name', [ 'label' => false, 'placeholder' => '契約者名を入力' ] ) ); ?>
                        <?php if( isset( $error['name'] ) ) echo noh( '<p class="error">' . $error['name'] . '</p>' ); ?> 
                    </td>
                </tr>

                <tr>
                    <th>ステータス</th>
                    <td>
                        <?php echo noh( $this->Form->select( 'status_id', $mt_contractant_status, [ 'label' => false, 'empty' => '選択してください', 'default' => 0 ] ) ); ?>
                    </td>
                </tr>

                <tr>
                    <th>識別子(変更不可)</th>
                    <td>
                        <?php if( isset( $contracts['id'] ) ): ?>
                            <?php echo noh( $this->Form->text( 'unique_parameter', [ 'label' => false, 'placeholder' => '識別子を入力', 'disabled' => true ] ) ); ?>
                            <?php echo noh( $this->Form->hidden( 'unique_parameter' ) ); ?>
                        <?php else: ?>
                            <?php echo noh( $this->Form->text( 'unique_parameter', [ 'label' => false, 'placeholder' => '識別子を入力' ] ) ); ?>
                            <?php if( isset( $error['unique_parameter'] ) ) echo noh( '<p class="error">' . $error['unique_parameter'] . '</p>' ); ?> 
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <th>契約締結日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'contract_date',  [ 'default' => date( 'Y/m/d' ), 'class' => 'datepicker'] ) ); ?>
                        <?php if( isset( $error['contract_date'] ) ) echo noh( '<p class="error">' . $error['contract_date'] . '</p>' ); ?> 
                    </td>
                </tr>
                <tr>
                    <th>サービス開始日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'service_start_date',  ['class' => 'datepicker', 'placeholder' => 'サービス開始日を選択', 'class' => 'datepicker']) ); ?>
                        <?php if( isset( $error['service_start_date'] ) ) echo noh( '<p class="error">' . $error['service_start_date'] . '</p>' ); ?> 
                    </td>
                </tr>
                <tr>
                    <th>サービス終了日</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'service_end_date',  ['class' => 'datepicker', 'placeholder' => 'サービス終了日を選択', 'class' => 'datepicker']) ); ?>
                        <?php if( isset( $error['service_end_date'] ) ) echo noh( '<p class="error">' . $error['service_end_date'] . '</p>' ); ?> 
                    </td>
                </tr>
                <tr>
                    <th>自動更新</th>
                    <td>
                        <div class="checkbox">
                            <?php echo noh( $this->Form->checkbox( 'auto_update_flg',  [ 'id' => 'auto_update', 'default' => 1 ]) ); ?>
                            <label for="auto_update">あり</label>
                        </div>
                    </td>
                </tr>

                <tr>
                    <th>テーマカラー</th>
                    <td>
                        <?php echo noh( $this->Form->color( 'theme_color',  [ 'default' => 'null' ]) ); ?>
                    </td>
                </tr>

                <tr>
                    <th>Android PUSH API KEY</th>
                    <td>
                        <?php echo noh( $this->Form->text( 'android_push_api_key', [ 'label' => false, 'placeholder' => 'API KEY' ] ) ); ?>
                    </td>
                </tr>


<?php /*
                <tr>
                    <th>メニュー</th>
                    <td>
                        <div class="duplicate_select">
                            <div class="selected_item drop_area">
                                <ul>
                                    <?php if( isset( $contracts['service_menu_id'] ) && count( $contracts['service_menu_id'] ) > 0 ): ?>
                                        <?php foreach( $contracts['service_menu_id'] as $val ): ?>
                                        <li>
                                            <?php echo h( $option_service_menus[$val] ); ?>
                                            <?php echo noh( $this->Form->hidden( 'service_menu_id[]', ['value' => $val ] ) ); ?>
                                            <span class="delete fas fa-times"></span>
                                        </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="selection drag_items">
                                <ul>
                                    <?php foreach( $option_service_menus as $key => $val ): ?>
                                        <li data-value="<?php echo h( $key ); ?>"><?php echo h( $val ); ?></li>
                                    <?php endforeach; ?>

                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
*/ ?>

            </table>
        </div>
        <div class="text_c">
            <?php echo noh( $this->Form->button( '確認', [ 'label' => false, 'name' => 'confirm', 'class' => 'btn submit' ] ) ); ?>
        </div>
    <?php echo noh( $this->Form->end() ); ?>
</div>
