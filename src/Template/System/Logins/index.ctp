<div class="login_box">
<?php /*
    <div class="text_c logo">
        <h1><?php echo $this->Html->image( 'system/logo.png', [ 'alt' => '不動産 システム画面' ] ); ?></h1>
    </div>
*/ ?>
    <?php echo $this->Form->create( null, [ 'class' => 'form-signin' ] ); ?>
        <label for="login_id">ID</label>
        <?php echo $this->Form->input( 'login_id', [ 'type' => 'text', 'label' => false ] ); ?>

        <label for="password">パスワード</label>
        <?php echo $this->Form->input( 'password', [ 'type' => 'password', 'label' => false ] ); ?>

        <?php echo $this->Flash->render(); ?>

        <div class="checkbox">
            <?php echo $this->Form->checkbox('auto_login', ['id' => 'auto_login', 'default' => '' ] );?>
            <label for="auto_login">ログイン状態を保持する</label>
        </div>

        <?php echo $this->Form->submit( 'ログイン', [ 'class' => 'btn btn-large btn-success' ] ); ?>
        <?php  if ( ENV === 'dev' || ENV === 'test' ): ?>
            <p> admin / 共通パス </p>
        <?php endif;  ?>
    <?php echo $this->Form->end(); ?>
    <p><?php echo $this->Html->link('パスワードを忘れた方はこちら', ['action' => 'remind']); ?></p>
</div><!--/ login_box -->
