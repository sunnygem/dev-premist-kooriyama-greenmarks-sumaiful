<div class="communication">
    <h2 class="contents_logo">COMMUNICATION</h2>
    <div class="thread_list">
        <ul>
            <?php if ( isset( $threads ) ): ?>
                <?php foreach( $threads as $key => $val ): ?>
                    <li>
                        <a href="<?php echo noh( APP_URL ); ?>/communications/thread/<?php echo h( $contractant_id . '/' . $chat_group_id . '/' . $contractant_service_menu_id . '/' . $val->id ); ?>">
                            <div>
                                <div class="img">
                                    <div class="icon"></div>
                                    <p class="name"><?php echo h( $val->user->nickname ); ?></p>
                                </div>
                                <div class="txt">
                                    <div class="date"><?php echo h( $val->date->format( 'Y.n.j' ) ); ?><span class="category"><?php echo h( $val->contractant_service_menu->label ); ?></span></div>
                                    <p class="title"><?php echo h( $val->title ); ?></p>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="new_thread">
    <div>
        <button>
            <a href="<?php echo APP_URL . '/communications/new-thread/' . $contractant_id . '/' . $chat_group_id . '/' . $contractant_service_menu_id; ?>"><img src="<?php echo APP_URL . '/img/communication/communication_button03.png'; ?>"></a>
        </button>
    </div>
</div>
