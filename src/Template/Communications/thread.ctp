<?php

    echo noh( $this->Form->hidden( 'domain',                      [ 'value' => APP_DOMAIN,                   'id' => 'domain' ] ) );
    echo noh( $this->Form->hidden( 'wss',                         [ 'value' => WSS,                          'id' => 'wss' ] ) );
    echo noh( $this->Form->hidden( 'contractant_id',              [ 'value' => $contractant_id,              'id' => 'contractant_id' ] ) );
    echo noh( $this->Form->hidden( 'chat_group_id',               [ 'value' => $chat_group_id,               'id' => 'chat_group_id' ] ) );
    echo noh( $this->Form->hidden( 'contractant_service_menu_id', [ 'value' => $contractant_service_menu_id, 'id' => 'contractant_service_menu_id' ] ) );
    echo noh( $this->Form->hidden( 'thread_id',                   [ 'value' => $thread_id,                   'id' => 'thread_id' ] ) );
    echo noh( $this->Form->hidden( 'user_id',                     [ 'value' => $user_id,                     'id' => 'user_id' ] ) );
    echo noh( $this->Form->hidden( 'nickname',                    [ 'value' => $userData->nickname,          'id' => 'nickname' ] ) );
    echo noh( $this->Form->hidden( 'number',                      [ 'value' => 1,                            'id' => 'number' ] ) ); // 初期は1ページ目
    echo noh( $this->Form->hidden( 'limit',                       [ 'value' => CHAT_MESSAGE_LIMIT,           'id' => 'limit' ] ) );
?>
<?php /*
    <input type="text" id="chat_message" value="">
    <input type="button" type="button" id="chat_send" value="送信">
    */ ?>

<div class="chat_wrap" id="chat_wrap">
    <div class="communication thread">
        <div class="title">
            <span class="category">集まる</span>
            <h2>代官山ラヴァー好きな好きなに聞いた</h2>
        </div>
        <div class="msg">
            <ul>
                <?php if ( $data !== null ): ?>
                    <?php foreach( $data as $key => $val ): ?>
                        <li <?php if ( $val->user_id === (int)$user_id ) echo 'class="own"'; ?> >
                            <div class="msg_content">
                                <div class="img">
                                    <div class="icon"></div>
                                    <div class="name"><?php echo h( $val->user->nickname ); ?></div>
                                </div>
                                <div class="txt_wrap">
                                    <div class="txt"><?php echo h( $val->message ); ?></div>
                                    <div class="date"><?php echo h( $val->created->format( 'Y.m.d H:i' ) ); ?></div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <ul>
                <chat-list v-for="list in lists"
                    v-bind:own="list.own"
                    v-bind:img="list.img"
                    v-bind:name="list.name"
                    v-bind:message="list.message"
                    v-bind:date="list.date">
                </chat-list>
            </ul>

        </div>
    </div>
    <div class="msg_send_wrap">
        <div class="msg_send" id="chat_message_box">
            <div class="txt">
                <textarea v-model="message"></textarea>
            </div>
            <div class="btn">
                <button  type="button" v-on:click="chatSend">
                    <img src="<?php echo APP_URL . '/img/communication/communication_button04.png'; ?>">
                </button>
            </div>
        </div>
    </div>
</div>
<?php echo noh( $this->Html->script( APP_URL . '/js/chat.js?' . time() ) ); ?>
