<div class="communication">
    <h2 class="contents_logo">COMMUNICATION</h2>
    <div>
        <ul class="menu">
            <?php foreach( $service_menus as $val ): ?>
            <li>
                <a href="<?php echo $this->Url->build( [ 'action' => 'thread_list', $chat_groups->contractant_id, $chat_group_members->chat_group_id, $val->id ]); ?>">
                    <?php echo $val->label; ?>
                </a>
                <span class="new">00</span>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
