<?php echo h( $name ) . '様'; ?>

willingアプリの登録申請を受け付けました。
管理側で確認を行い、承認メールをお送りいたします。
今しばらくお待ちください。

3営業日以内に連絡がない場合は、「<?php echo h( $building['name'] ); ?>」フロントへ直接お声がけください。
※なお、このメールは送信専用になります。このメールに返信しても対応できません。



Dear <?php echo h( $name ); ?>

I received a registration application for the willing app.
The manager will confirm and send an approval email.
Please wait a moment now.

If you do not contact us within 3 business days, please ask the <?php echo h( $building['name_en'] ); ?> Management Office for support.
※In addition, this email is for sending only. It is not possible to respond to this email.
