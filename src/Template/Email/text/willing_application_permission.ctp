<?php echo h( $name ); ?>様

willingアプリの利用が承認されました。

下記アドレスより、パスワードの設定を行ってください。

<?php echo $url; ?>

※このメールは送信専用になります。このメールに返信しても対応できません。


Dear <?php echo h( $name ); ?>

Your use of the willing app has been approved.

Please set the password from the following address.

<?php echo $url; ?>

※This email is for sending only. It is not possible to respond to this email.
