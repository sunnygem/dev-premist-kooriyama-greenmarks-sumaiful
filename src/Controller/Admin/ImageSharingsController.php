<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException; 
use Cake\View\Exception\MissingTemplateException;

class ImageSharingsController extends AdminAppController
{
    public $ImageSharings;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->ImageSharings         = TableRegistry::getTableLocator()->get('ImageSharings');
        $this->ImageSharingBuildings = TableRegistry::getTableLocator()->get('ImageSharingBuildings');
        $this->Files                 = TableRegistry::getTableLocator()->get('Files');
        $this->RelImagesharingFiles  = TableRegistry::getTableLocator()->get('RelImagesharingFiles');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );

        $title_for_layout = '画像管理';
        $this->set( compact( 'title_for_layout' ) );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'ImageSharings.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
            ]
        ];
        $this->paginate['contain'] = [ 'ImageSharingBuildings.Buildings' ];

        // 物件
        if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
        {
            $this->paginate['join'] = [
                'table'  => 'image_sharing_buildings'
                ,'alias' => 'ImageSharingBuildings'
                ,'type'  => 'LEFT'
                ,'conditions' => [
                    'ImageSharingBuildings.image_sharing_id = ImageSharings.id'
                ]
            ];
            // 全公開もしくはユーザーのbuilding_idに合致するもの
            $this->paginate['finder']['search']['conditions']['ImageSharings.all_building_flg']    = 0;
            $this->paginate['finder']['search']['conditions']['ImageSharingBuildings.building_id'] = $this->_admin_building_id;
            $this->paginate['group'] = 'ImageSharings.id';
        }

        $data = $this->paginate( 'ImageSharings' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) === false )
        {
            throw new BadRequestException('権限がありません');
        }
        elseif( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           $error = $this->ImageSharings->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $data = $this->ImageSharings->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               $this->_session->write( 'Admin.data', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.data') )
            {
                $data = $this->ImageSharings->newEntity( $this->_session->read('Admin.data') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $data = $this->ImageSharings->getEditData( $id );
                $data->buildings = $this->ImageSharingBuildings->getEditDataByImageSharingId( $this->_contractant_id, $id );
            }
            else
            {
                $data = $this->ImageSharings->newEntity();
            }
        }
        $this->set( compact( 'data' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.data' ) ) 
            {
                $data = $this->_session->read('Admin.data');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->ImageSharings->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');
                //$files = $data['files'];
                // filesキーを持っているとアソシエーションsaveが走るので別処理にするため削除。
                unset( $data['files'] );
                $image_sharing_id = $this->ImageSharings->saveData( $data );

                // 公開範囲の保存
                $this->ImageSharingBuildings->deleteAll(['image_sharing_id' => $image_sharing_id]);
                if( $data['buildings'] !== '' && is_array( $data['buildings'] ) )
                {
                    foreach( $data['buildings'] as $val )
                    {
                        $arr_buildings = [
                            'contractant_id'    => $this->_contractant_id
                            ,'image_sharing_id' => $image_sharing_id
                            ,'building_id'      => $val
                        ];
                        $this->ImageSharingBuildings->saveData( $arr_buildings );
                    }
                }

                if( $image_sharing_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.data' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.data') )
        {
            $data = $this->_session->read('Admin.data');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'data' ) );
    }

    public function imageEdit( $id )
    {
        $data = $this->ImageSharings->get( $id );

        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            $request = $this->request->data;

            // 画像をサーバーに移動
            $this->File->saveFile( $request, 'image' );

            // DBへの登録
            foreach( $request['files']['image'] as $val )
            {
                if( isset( $val['file_path'] ) && $val['file_path'] !== null )
                {
                    $file_data = [
                       'contractant_id' => $this->_session->read( 'ContractantData.id' )
                       ,'name'          => $val['name'] 
                       ,'file_path'     => $val['file_path'] 
                       ,'mime_type'     => mime_content_type( WWW_ROOT . DS . $val['file_path'] ) 
                    ];
                    if( isset( $val['id'] ) )
                    {
                        $file_data['id'] = $val['id'];
                    }
                    $file_id = $this->Files->saveData( $file_data );

                    // 中間テーブルの登録
                    if( isset( $file_data['id'] ) === false )
                    {
                        $orderby = $this->RelImagesharingFiles->getOrderby( $request['image_sharing_id'] ) + 1;
                        $this->RelImagesharingFiles->saveData([
                            'image_sharing_id' => $request['image_sharing_id']
                            ,'file_id'         => $file_id
                            ,'contractant_id'  => $this->_session->read( 'ContractantData.id' )
                            ,'orderby'         => $orderby
                        ]);
                    }
                }
            }

            $this->Flash->success( '画像を登録しました。');
            $this->redirect([ $data->id ]);
        }
        else
        {
             $images = $this->RelImagesharingFiles->getDataByImageSharingId( $data->id );
        }
        $this->set( compact( 'data', 'images' ) );
 
    }

    public function deleteAlbum( $id= null )
    {
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) === false )
        {
            throw new BadRequestException('権限がありません');
        }
        elseif( $id !== null )
        {
            // 画像も消す
            $res = $this->RelImagesharingFiles->findByImageSharingId( $id );
            foreach( $res as $val )
            {
                self::imageDelete( $val->id, true );
            }

            $data = $this->ImageSharings->get( $id );
            $this->ImageSharings->delete( $data );

            $this->Flash->success( 'アルバムを削除しました。' );
        }
        else
        {
            $this->Flash->error( 'アルバムの削除に失敗しました。' );
        }
        return $this->redirect([ 'action' => 'index']);

    }

    public function imageDelete( $rel_id=null, $all=false )
    {
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) === false )
        {
            throw new BadRequestException('権限がありません');
        }
        elseif( $rel_id !== null )
        {
            $rel_data = $this->RelImagesharingFiles->get( $rel_id );

            $this->RelImagesharingFiles->deleteData( $rel_data->id );
            $this->Files->deleteData( $rel_data->file_id );

            if( $all === false )
            {
                $this->Flash->success( '画像を削除しました。' );
            }
        }
        else
        {
            if( $all === false )
            {
                $this->Flash->error( '画像の削除に失敗しました。' );
            }
        }

        if( $all === false )
        {
            return $this->redirect([ 'action' => 'imageEdit', $rel_data->image_sharing_id ]);
        }
    }
}
