<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException; 
use Cake\View\Exception\MissingTemplateException;

class FaqsQuestionsController extends AdminAppController
{ 
    public $paginate = [ 'finder' => 'search' ];
 
    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        
        if( $this->_unique_parameter !== 'willing' )
        {
            throw new NotFoundException();
        }
        else
        {
            $title_for_layout = 'Q&A管理';

            $this->loadComponent( 'Common' );
            $this->loadComponent( 'File' );
            
            $this->_mt_faq_category =  Configure::read('mt_faq_category');
            $mt_faq_category = $this->_mt_faq_category;

            $this->set( compact( 'title_for_layout' , 'mt_faq_category') );
        }
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'FaqsQuestions.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [
                    //'Faqs.release_date' => 'DESC'
                    'FaqsQuestions.id'    => 'ASC'
                ]
            ]
        ];

        $data = $this->paginate( 'FaqsQuestions' );
        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $data = [];
        $faq = [];
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            // エラーチェック
            $error = $this->FaqsQuestions->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $equipment = $this->FaqsQuestions->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write( 'Admin.faqsquestions', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.faqsquestions') )
            {
                $faq = $this->FaqsQuestions->newEntity( $this->_session->read('Admin.faqsquestions') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $faq = $this->FaqsQuestions->getEditData( $id );
            }
            else
            {
                $faq = $this->FaqsQuestions->newEntity();
            }
        }
        $this->set( compact( 'faq' ) );
    }

    public function confirm()
    {
        $data = [];
        $faq = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {

            if( $this->_session->check( 'Admin.faqsquestions' ) ) 
            {
                $data = $this->_session->read('Admin.faqsquestions');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }
            // エラーチェック
            $error = $this->FaqsQuestions->validation( $data ); 
            if( count( $error ) > 0 )
            {
                $data = $this->FaqsQuestions->newEntity( $request );
                return $this->redirect(['action' => 'edit']);
           }
            else
            {
                // 登録
                if( isset( $data['contractant_id'] ) === false )
                {
                    $data['contractant_id'] = $this->_session->read('ContractantData.id');
                }

                $id = $this->FaqsQuestions->saveData( $data );

                if( $id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.faqsquestions' );
                // システム画面は完了画面無し
                return $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.faqsquestions') )
        {
            $faq = $this->_session->read('Admin.faqsquestions');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'faq' ) );
    }
    
    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $this->FaqsQuestions->deleteData( $id );
                $this->Flash->success('インフォーメーションを削除しました。');
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }

}
