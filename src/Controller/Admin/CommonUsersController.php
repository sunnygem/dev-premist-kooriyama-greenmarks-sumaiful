<?php

namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
   共通ユーザーを作成します
**/ 
class CommonUsersController extends AdminAppController
{
    public $Users;
    public $PasswordSecurities;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();

       //$this->CustomerUsers = TableRegistry::get('AdminUsers');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $this->Users      = TableRegistry::getTableLocator()->get('Users');
        $this->FrontUsers = TableRegistry::getTableLocator()->get('FrontUsers');
 
        // 暫定
        $mt_customer_type = $this->_mt_residence_type;

        $title_for_layout = '共通ユーザー管理';
        $this->set( compact( 'mt_customer_type', 'title_for_layout' ) );

    }

    // willingのテンプレを分ける
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        if( in_array($this->_unique_parameter, [ 'willing', 'demo_willing' ], true ) )
        {
            $this->loadComponent('Common');
            $action_template = $this->Common->toSnakeCase( $this->request->action );
            $this->viewBuilder()->template( 'willing' . DS . $action_template );
        }
    }

    // 共通ユーザー
    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'order' => [
                    'Users.modified' => 'DESC'
                ]
                ,'conditions' => [
                    'Users.contractant_id' => $this->_session->read( 'ContractantData.id' )
                    ,'FrontUsers.common_user_flg' => 1
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'Users' => [
                'conditions' => [ 'Users.deleted IS' => null ]
             ]
             ,'Buildings'
            ,'EncryptionParameters'
        ];

        // 物件
        if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
        {
            $this->paginate['finder']['search']['conditions']['FrontUsers.building_id'] = $this->_admin_building_id;
        }

        //front_usersから取得しています。
        $data = $this->paginate( 'FrontUsers' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        // system_adminユーザー以外はアクセス不可
        if( in_array( $this->_session->read( 'Admin.Auth.admin_user.authority', true ), [ SYSTEM_ADMIN, ADMIN ], true ) === false )
        {
            throw new ForbiddenException('このページは閲覧できません');
        }

       if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
       {
            $request = $this->request->data;
            $request['contractant_id'] = $this->_contractant_id;

            //  エラーチェック
            $error = $this->Users->common_user_validation( $request ); 
            if( count( $error ) > 0 )
            {
                //$users = $request;
                $users = $this->Users->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write( 'Admin.users', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.users') )
            {
                $users = $this->Users->newEntity( $this->_session->read('Admin.users') );
                $users->password         = null;
                $users->password_confirm = null;
            }
            else
            {
            }
        }
        else
        {
            if( $id !== null )
            {
                $users = $this->Users->getFrontUser( $id, [], null, null, true );
                $users->password = null;
                if( $users->expire_date ) $users->expire_date = $users->expire_date->format( 'Y/m/d' );
                //$front_users = $this->FrontUsers->findByUserId( $id );
                //$users->front_user = $front_users->first();
            }
            else
            {
                $users = $this->Users->newEntity();
            }
        }

        $this->set( compact( 'users' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'Admin.users' ) ) 
           {
               $data = $this->_session->read('Admin.users');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->Users->common_user_validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               // 登録
               $data['front_user']['contractant_id']  = $this->_session->read( 'ContractantData.id' );
               $data['front_user']['common_user_flg'] = 1;
               // todo 廃止
               $data['type'] = 'front'; 

               //更新の時
               if( isset( $data['id'] ) )
               {
                   $front_users = $this->FrontUsers->findByUserId( $data['id'] );
                   $data['front_user']['id'] = $front_users->first()->id;

               }
               else
               {
                   $data['front_user']['name']            = '共通ユーザー'; 
                   $data['front_user']['nickname']        = '共通ユーザー'; 
                   $data['valid_date'] = date( 'Y-m-d H:i:s' );
               }

               if( $data['expire_date'] !== '' )
               {
                   $data['expire_date'] = $data['expire_date'] . ' 23:59:59';
               }

               // 物件
               if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
               {
                   $data['front_user']['building_id'] = $this->_admin_building_id;
               }

               $res = $this->Users->saveData( $data );

               if( $res )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('Admin.users' );
               // システム画面は完了画面無し
               return $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('Admin.users') )
        {
            $users = $this->_session->read('Admin.users');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }

        $this->set( compact( 'users' ) );
    }

}
