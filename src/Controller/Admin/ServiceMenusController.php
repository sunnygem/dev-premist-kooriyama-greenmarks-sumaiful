<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ServiceMenusController extends AdminAppController
{
    public $ServiceMenus;
    public $ContractantServiceMenus;
    public $MenuContactTels;
    public $MenuContactEmails;
    public $MenuContactForms;
    public $MenuLinktypes;
    public $MenuApplications;
    public $MenuManuals;
    public $MenuEnquetes;

    public function initialize()
    {
        parent::initialize();
        $this->ContractantServiceMenus = TableRegistry::get('ContractantServiceMenus');
        $this->AppScreens        = TableRegistry::get('AppScreens');
        $this->MenuContactTels   = TableRegistry::get('MenuContactTels');
        $this->MenuContactEmails = TableRegistry::get('MenuContactEmails');
        $this->MenuContactForms  = TableRegistry::get('MenuContactForms');
        $this->MenuLinktypes     = TableRegistry::get('MenuLinktypes');
        $this->MenuApplications  = TableRegistry::get('MenuApplications');
        $this->MenuManuals       = TableRegistry::get('MenuManuals');
        $this->MenuEnquetes      = TableRegistry::get('MenuEnquetes');
        $this->Files             = TableRegistry::get('Files');
        $this->RelMenuLinktypeFiles    = TableRegistry::get('RelMenuLinktypeFiles');
        $this->RelMenuApplicationFiles = TableRegistry::get('RelMenuApplicationFiles');
        $this->RelMenuManualFiles      = TableRegistry::get('RelMenuManualFiles');
        $this->MenuCategories      = TableRegistry::get('MenuCategories');

        $this->loadComponent( 'File' );
    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $title_for_layout = 'メニュー管理';
        $this->set( compact( 'title_for_layout' ) );

        $this->set( 'mt_residence_type', $this->_mt_residence_type );

        $screen_list = $this->AppScreens->getFooterListByContractantId( $this->_session->read( 'ContractantData.id' ) );
        $this->set( compact( 'screen_list' ) );
    }

    public function index()
    {
        $data = $this->ContractantServiceMenus->getDataByContractantId( $this->_session->read('ContractantData.id' ) );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $category_id = [];
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            $error = $this->ContractantServiceMenus->validation( $request );
            // todo 各メニューのバリデーション
            if( count( $error ) > 0 )
            {
                $contractant_service_menus = $this->ContractantServiceMenus->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                // メニューのアイコン
                if( isset( $request['files']['service_menu_icon_path'] ) && $request['files']['service_menu_icon_path']['error'] === 0 && $request['files']['service_menu_icon_path']['size'] )
                {
                    $this->File->saveFile( $request, 'service_menu_icon_path' );
                }

                if( isset( $request['menu_detail'] ) && is_array( $request['menu_detail'] ) )
                {
                    foreach( $request['menu_detail'] as $key => $val )
                    {
                        // 画像の保存
                        if( isset( $val['files']['image'] ) )
                        {
                            $this->File->saveFile( $val, 'image' );

                            if( isset( $val['files']['image'] ) && $val['files']['image']['error'] === 0 && $val['files']['image']['size'] > 0 )
                            {
                                $request['uploaded'][$key] = ( isset( $request['uploaded'][$key] ) ) ? array_merge( $request['uploaded'][$key], $val['files']['image'] ) : $val['files']['image'];
                            }
                        }

                        // PDFの保存
                        if( isset( $val['files']['pdf'] ) )
                        {
                            $this->File->saveFile( $val, 'pdf' );
                            if( isset( $val['files']['pdf'] ) && $val['files']['pdf']['error'] === 0 && $val['files']['pdf']['size'] > 0 )
                            {
                                $request['uploaded'][$key] = ( isset( $request['uploaded'][$key] ) ) ? array_merge( $request['uploaded'][$key], $val['files']['pdf'] ) : $val['files']['pdf'];
                            }
                        }
                    }
                }
                $this->_session->write('Admin.contractant_service_menus', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.contractant_service_menus') )
            {
                $contractant_service_menus = $this->ContractantServiceMenus->getDetailById($this->_session->read('Admin.contractant_service_menus.id') );
                $contractant_service_menus = $this->ContractantServiceMenus->patchEntity( $contractant_service_menus, $this->_session->read('Admin.contractant_service_menus') );
                if( isset( $contractant_service_menus['uploaded'] ) )
                {
                    foreach( $contractant_service_menus['uploaded'] as $key => $val )
                    {
                        if( isset( $val['tmp_dir'] ) && $val['tmp_dir'] !== '' )
                        {
                            $contractant_service_menus['uploaded'][$key]['data'] = file_get_contents( $val['tmp_dir'] );
                        }
                    }
                }
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }
        }
        elseif( $id !== null )
        {
            $contractant_service_menus = $this->ContractantServiceMenus->getDetailById( $id );
            //サービスのタイプによってフォームの出し分け。全てmenu_deitalキーとして扱う
            switch( $contractant_service_menus->service_menu_id )
            {
            case 1: // 問合せTEL 
                $contractant_service_menus->menu_detail = $this->MenuContactTels->getDetailByContractantSeriviceMenuId( $id );
                break;
            case 2: // 問い合わせ メール
                $contractant_service_menus->menu_detail = $this->MenuContactEmails->getDetailByContractantSeriviceMenuId( $id );
                break;
            case 3: // 問い合わせ フォーム
                $contractant_service_menus->menu_detail = $this->MenuContactForms->getDetailByContractantSeriviceMenuId( $id );
                break;
            case 4: // リンク集タイプ
                $contractant_service_menus->menu_detail = $this->MenuLinktypes->getDetailByContractantSeriviceMenuId( $id );
                $contractant_service_menus->uploaded = [];
                foreach( $contractant_service_menus->menu_detail as $key => $val )
                {
                    if( count( $val->files ) > 0 )
                    {
                        $file = $val->files[0];
                        if( $file->file_path !== null && $this->File->checkAllowImageMimeType( $file->mime_type ) )
                        {
                            $contractant_service_menus['uploaded'][$key] = [
                                'id'         => $file->id
                                ,'file_path' => $file->file_path
                                ,'name'      => $file->name
                            ];
                        }
                    }
                }
                break;
            case 5: // ブログタイプ
                break;
            case 6: // アプリ
                $contractant_service_menus->menu_detail = $this->MenuApplications->getDetailByContractantSeriviceMenuId( $id );
                $contractant_service_menus->uploaded    = [];
                //foreach( $contractant_service_menus->menu_detail as $key => $val )
                //{
                //    if( count( $val->files ) > 0 )
                //    {
                //        $file = $val->files[0];
                //        if( $file->file_path !== null && $this->File->checkAllowImageMimeType( $file->mime_type ) )
                //        {
                //            $contractant_service_menus['uploaded'][$key] = [
                //                'id'         => $file->id
                //                ,'file_path' => $file->file_path
                //                ,'name'      => $file->name
                //            ];
                //        }
                //    }
                //}
                break;
            case 7: // マニュアル
                $contractant_service_menus->menu_detail = $this->MenuManuals->getDetailByContractantSeriviceMenuId( $id );
                $contractant_service_menus['uploaded']  = [];
                foreach( $contractant_service_menus->menu_detail as $key => $val )
                {
                    if( count( $val->files ) > 0 )
                    {
                        $file = $val->files[0];
                        if( $file->mime_type === 'application/pdf' )
                        {
                            $contractant_service_menus['uploaded'][$key] = [
                                'id'         => $file->id
                                ,'file_path' => $file->file_path
                                ,'name'      => $file->name
                            ];
                        }
                    }
                }

                break;
            case 8: // 予約
                // todo 内容確認
                break;
            case 9: // アンケート
                //$contractant_service_menus->menu_detail = $this->MenuEnquetes->getDataByContractantSeriviceMenuId( $id );

                break;
            case 10: // チャット
                // todo仕様確認
                break;
            case 11: // リンクタイプ
                $menu_detail = $this->MenuLinktypes->getDetailByContractantSeriviceMenuId( $id );
                $contractant_service_menus->menu_detail = $menu_detail->toArray();
                // 単独リンクなので、menu_detailは必ず1つ

                break;
            case 12: // ポストタイプ
                // ポストカテゴリを作成
                break;
            }
        }
        else
        {
            throw new NotFoundException();
        }

        if( in_array( $contractant_service_menus->service_menu_id, [ 7 ], true ) )
        {
            // カテゴリ
            $manual_categories = $this->MenuCategories->getCategoryByServiceMenuId( $id, [
                    'contractant_id' => $this->_contractant_id
                    ,'display_flg'   => 1
                    ,'deleted IS'    => null
            ], true);
            $this->set( compact('manual_categories') );
        }

        $this->set( compact( 'contractant_service_menus' ) );
    }

    public function confirm()
    {
        $menu_detail = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'Admin.contractant_service_menus' ) ) 
           {
               $data = $this->_session->read('Admin.contractant_service_menus');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->ContractantServiceMenus->validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               if( isset( $data['files']['service_menu_icon_path']['file_path'] ) )
               {
                   $data['service_menu_icon_path'] = $data['files']['service_menu_icon_path']['file_path'];
                   unset( $data['files'] );
               }

               // 登録
               $contractant_service_menu_id = $this->ContractantServiceMenus->saveData( $data );

               $contractant_service_menus = $this->ContractantServiceMenus->get( $contractant_service_menu_id );
               $menu_detail_data = [
                   'contractant_service_menu_id' => $contractant_service_menus->id
               ];
               if( isset ( $data['menu_detail']['id'] ) ) $menu_detail_data['id'] = $data['menu_detail']['id'];
               // 各メニュー項目
               switch( $contractant_service_menus->service_menu_id )
               {
               case 1: // 問合せtel
                   $menu_detail_data = array_merge( $menu_detail_data, $data['menu_detail'] );
                   $res_menu_detail = $this->MenuContactTels->saveData( $menu_detail_data );
                   break;

               case 2: // 問合せメール
                   $menu_detail_data['email'] = $data['menu_detail']['email'];
                   $res_menu_detail = $this->MenuContactEmails->saveData( $menu_detail_data );
                   break;

               case 3: // 問合せメール
                   $menu_detail_data['email'] = $data['menu_detail']['email'];
                   $res_menu_detail = $this->MenuContactForms->saveData( $menu_detail_data );
                   break;

               case 4: // リンクタイプ
                   $res_menu_detail;
                   if( $this->_session->check('Admin.contractant_service_menus.uploaded') )
                   {
                       $data['uploaded'] = $this->_session->read('Admin.contractant_service_menus.uploaded');
                   }

                   // 一度edit_flgを立てる
                   $this->MenuLinktypes->setEditFlgByContractantServiceMenuId( $contractant_service_menus->id );
                   foreach( $data['menu_detail'] as $key => $val )
                   {
                       //複数あるので$menu_detail_dataを初期化する
                       $menu_detail_data = [
                           'contractant_service_menu_id' => $contractant_service_menus->id
                           ,'name'     => $val['name']
                           ,'link_url' => $val['link_url']
                           ,'edit_flg' => 0 // 更新対象のデータのフラグは折る
                       ];
                       if( isset( $val['id']  ) ) $menu_detail_data['id'] = $val['id'];
                       $res_menu_detail = $this->MenuLinktypes->saveData( $menu_detail_data );

                       // 画像の登録
                       if( isset( $data['uploaded'][$key]['file_path'] ) )
                       {
                           $file_data = [
                               'contractant_id' => $this->_session->read('ContractantData.id')
                               ,'name'          => $data['uploaded'][$key]['name']
                               ,'file_path'     => $data['uploaded'][$key]['file_path']
                               ,'mime_type'     => mime_content_type( WWW_ROOT . $data['uploaded'][$key]['file_path'] )
                           ];
                           if( isset( $data['uploaded'][$key]['id'] ) ) $file_data['id'] = $data['uploaded'][$key]['id'];

                           $file_id = $this->Files->saveData( $file_data );
                           // 中間テーブルの登録
                           if( isset( $file_data['id'] ) === false )
                           {
                               $this->RelMenuLinktypeFiles->saveData([
                                   'menu_linktype_id'  => $res_menu_detail
                                   ,'file_id'             => $file_id
                                   ,'contractant_id'      => $this->_session->read('ContractantData.id')
                               ]);
                           }
                       }

                   }
                   // edit_flgを立ったままのものは削除対象なので、deletedを書き込む
                   $arr_delete_id = $this->MenuLinktypes->setDeletedOnEditFlg( $contractant_service_menus->id );
                   foreach( $arr_delete_id as $val )
                   {
                       $file_id = $this->RelMenuLinktypeFiles->deleteByMenuApplicationId( $val );
                       if( $file_id ) $this->Files->physiacalDeleteData( $file_id );
                   }

                   break;

               case 6: // アプリ
                   $res_menu_detail;

                   //if( $this->_session->check('Admin.contractant_service_menus.uploaded') )
                   //{
                   //    $data['uploaded'] = $this->_session->read('Admin.contractant_service_menus.uploaded');
                   //}
                   // 一度edit_flgを立てる
                   $this->MenuApplications->setEditFlgByContractantServiceMenuId( $contractant_service_menus->id );
                   foreach( $data['menu_detail'] as $key => $val )
                   {
                       //複数あるので$menu_detail_dataを初期化する
                       $menu_detail_data = [
                           'contractant_service_menu_id' => $contractant_service_menus->id
                           ,'name'              => $val['name']
                           ,'scheme_ios'        => $val['scheme_ios']
                           ,'app_id_ios'        => $val['app_id_ios']
                           ,'store_url_android' => $val['store_url_android']
                           ,'edit_flg'          => 0 // 更新対象のデータのフラグは折る
                       ];
                       if( isset( $val['id']  ) ) $menu_detail_data['id'] = $val['id'];
                       $res_menu_detail = $this->MenuApplications->saveData( $menu_detail_data );

                       // 画像の登録
                       //if( isset( $data['uploaded'][$key]['file_path'] ) )
                       //{
                       //    $file_data = [
                       //        'contractant_id' => $this->_session->read('ContractantData.id')
                       //        ,'name'          => $data['uploaded'][$key]['name']
                       //        ,'file_path'     => $data['uploaded'][$key]['file_path']
                       //        ,'mime_type'     => mime_content_type( WWW_ROOT . $data['uploaded'][$key]['file_path'] )
                       //    ];
                       //    if( isset( $data['uploaded'][$key]['id'] ) ) $file_data['id'] = $data['uploaded'][$key]['id'];

                       //    $file_id = $this->Files->saveData( $file_data );
                       //    // 中間テーブルの登録
                       //    if( isset( $file_data['id'] ) === false )
                       //    {
                       //        $this->RelMenuApplicationFiles->saveData([
                       //            'menu_application_id'  => $res_menu_detail
                       //            ,'file_id'             => $file_id
                       //            ,'contractant_id'      => $this->_session->read('ContractantData.id')
                       //        ]);
                       //    }
                       //}

                   }
                   // edit_flgを立ったままのものは削除対象なので、deletedを書き込む
                   $arr_delete_id = $this->MenuApplications->setDeletedOnEditFlg( $contractant_service_menus->id );
                   foreach( $arr_delete_id as $val )
                   {
                       $file_id = $this->RelMenuApplicationFiles->deleteByMenuApplicationId( $val );
                       if( $file_id ) $this->Files->physiacalDeleteData( $file_id );
                   }
                   break;

               case 7: // マニュアル
                   $res_menu_detail;

                   if( $this->_session->check('Admin.contractant_service_menus.uploaded') )
                   {
                       $data['uploaded'] = $this->_session->read('Admin.contractant_service_menus.uploaded');
                   }
                   // 一度edit_flgを立てる
                   $this->MenuManuals->setEditFlgByContractantServiceMenuId( $contractant_service_menus->id );
                   foreach( $data['menu_detail'] as $key => $val )
                   {
                       //複数あるので$menu_detail_dataを初期化する
                       $menu_detail_data = [
                           'contractant_service_menu_id' => $contractant_service_menus->id
                           ,'equipment_name' => $val['equipment_name']
                           ,'category_id'    => $val['category_id']
                           ,'edit_flg'       => 0 // 更新対象のデータのフラグは折る
                       ];
                       if( isset( $val['id']  ) ) $menu_detail_data['id'] = $val['id'];
                       $res_menu_detail = $this->MenuManuals->saveData( $menu_detail_data );

                       // 画像の登録
                       if( isset( $data['uploaded'][$key]['file_path'] ) )
                       {

                           $file_data = [
                               'contractant_id' => $this->_session->read('ContractantData.id')
                               ,'name'          => $data['uploaded'][$key]['name']
                               ,'file_path'     => $data['uploaded'][$key]['file_path']
                               ,'mime_type'     => mime_content_type( WWW_ROOT . $data['uploaded'][$key]['file_path'] )
                           ];
                           if( isset( $data['uploaded'][$key]['id'] ) ) $file_data['id'] = $data['uploaded'][$key]['id'];

                           $file_id = $this->Files->saveData( $file_data );
                           // 既存ファイルは削除
                           $this->RelMenuManualFiles->updateAll(
                               [
                                   'deleted' => date('Y-m-d H:i:s')
                               ]
                               ,[
                                   'contractant_id'  => $this->_contractant_id
                                   ,'menu_manual_id' => $res_menu_detail
                                   ,'file_id !='     => $file_id
                               ]
                           );
                           // 中間テーブルの登録
                           if( isset( $file_data['id'] ) === false )
                           {

                               $this->RelMenuManualFiles->saveData([
                                   'menu_manual_id'  => $res_menu_detail
                                   ,'file_id'        => $file_id
                                   ,'contractant_id' => $this->_session->read('ContractantData.id')
                               ]);
                           }
                       }

                   }
                   // edit_flgを立ったままのものは削除対象なので、deletedを書き込む
                   $arr_delete_id = $this->MenuManuals->setDeletedOnEditFlg( $contractant_service_menus->id );
                   foreach( $arr_delete_id as $val )
                   {
                       $file_id = $this->RelMenuManualFiles->deleteByMenuManualId( $val );
                       if( $file_id ) $this->Files->physiacalDeleteData( $file_id );
                   }

                   break;

               //case 9: // アンケート
               //    break;

               //case 10: // チャット
               //    break;

               case 11: // 単独リンクタイプ
                   $res_menu_detail;

                   // 一度edit_flgを立てる
                   $this->MenuLinktypes->setEditFlgByContractantServiceMenuId( $contractant_service_menus->id );
                   foreach( $data['menu_detail'] as $key => $val )
                   {
                       //複数あるので$menu_detail_dataを初期化する
                       $menu_detail_data = [
                           'contractant_service_menu_id' => $contractant_service_menus->id
                           ,'link_url' => $val['link_url']
                           ,'edit_flg' => 0 // 更新対象のデータのフラグは折る
                       ];
                       if( isset( $val['id']  ) ) $menu_detail_data['id'] = $val['id'];
                       $res_menu_detail = $this->MenuLinktypes->saveData( $menu_detail_data );
                   }
                   // edit_flgを立ったままのものは削除対象なので、deletedを書き込む
                   $arr_delete_id = $this->MenuLinktypes->setDeletedOnEditFlg( $contractant_service_menus->id );

                   break;

               //case 12: // ポストタイプ
               //     break;

               default:
                   $res_menu_detail = true;
               }


               if( $contractant_service_menu_id && $res_menu_detail )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('Admin.contractant_service_menus' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index', $contractant_service_menu_id ]);
           }
        }
        else if( $this->_session->check('Admin.contractant_service_menus') )
        {
            $contractant_service_menus = $this->_session->read('Admin.contractant_service_menus');

            // カテゴリの登録がある場合
            if( in_array( $contractant_service_menus['service_menu_id'], [ '7' ], true ) )
            {
                $manual_categories = $this->MenuCategories->getCategoryByServiceMenuId( $contractant_service_menus['id'], [
                        'contractant_id' => $this->_contractant_id
                        ,'display_flg'   => 1
                        ,'deleted IS'    => null
                ], true)->toArray();
                $this->set( compact('manual_categories') );
            }

            $this->set( compact( 'menu_detail' ) );
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'contractant_service_menus' ) );
    }

    public function editEnquete( $service_menu_id=null, $id=null )
    {
        $service_menu = $this->ContractantServiceMenus->get( $service_menu_id );
        if( $this->request->is(['post', 'put', 'patch']) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;
            $error = $this->MenuEnquetes->validation( $request );
            if( count( $error ) > 0 )
            {
                $menu_enquete = $this->MenuEnquetes->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write('Admin.menu_enquete', $request );
                return $this->redirect(['action' => 'confirmEnquete', $service_menu_id]);
            }
        }
        elseif( $this->request->is(['post']) && isset( $this->request->data['back'] ) )
        {
            $menu_enquete = $this->MenuEnquetes->newEntity( $this->_session->read('Admin.menu_enquete') );
        }
        // 直打ち対策
        elseif( $service_menu->service_menu_id === 9 && $service_menu->contractant_id === $this->_session->read('ContractantData.id') )
        {
            // 更新
            if( $id !== null )
            {
                $menu_enquete = $this->MenuEnquetes->get( $id );
            }
            // 新規
            else
            {
                $menu_enquete = $this->MenuEnquetes->newEntity();
                $menu_enquete->contractant_service_menu_id = $service_menu_id;
            }
        }
        $this->set( compact( 'menu_enquete' ) );
    }

    public function confirmEnquete( $service_menu_id=null )
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            $data = $this->_session->read('Admin.menu_enquete');
            // エラーチェック
            $error = $this->MenuEnquetes->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'editEnquete', $service_menu_id ]);
            }
            else
            {
                // 保存
               $menu_enquete_id = $this->MenuEnquetes->saveData( $data );

               if( $menu_enquete_id )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('Admin.menu_enquete' );
               // 完了画面無し
               $this->redirect(['action' => 'edit', $data['contractant_service_menu_id'] ]);

            }
        }
        else if( $this->_session->check('Admin.menu_enquete') )
        {
            $menu_enquete = $this->_session->read('Admin.menu_enquete');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'menu_enquete', 'service_menu_id' ) );
    }

    public function deleteEnquete( $service_menu_id=null, $id=null )
    {
        if( $id !== null )
        {
            $this->MenuEnquetes->deleteData( $id );
            $this->Flash->success( 'アンケート削除しました。' );
        }
        else
        {
            $this->Flash->error( 'アンケートの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'edit', $service_menu_id ]);


    }
}
