<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException; 
use Cake\View\Exception\MissingTemplateException;

class ConfigsController extends AdminAppController
{
    public $MtContractantStatuses;
    public $contractants;

    public function initialize()
    {
        parent::initialize();
        $this->MtContractantStatuses = TableRegistry::getTableLocator()->get('MtContractantStatuses');
        $this->Contractants          = TableRegistry::getTableLocator()->get('Contractants');
        $this->Buildings             = TableRegistry::getTableLocator()->get('Buildings');

        $this->loadComponent( 'File' );
    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->_admin_building_id !== null )
        {
            throw new ForbiddenException();
        }

        $mt_contractant_status = $this->MtContractantStatuses->getList();

        $this->contractant = $this->_session->read('ContractantData' );
        $this->set( compact( 'mt_contractant_status' ) );
    }

    public function index()
    {
        $title_for_layout = '設定ー契約者情報';
        $contracts = $this->contractant;
        $buildings = $this->Buildings->getData( $contracts->id );

        $this->set( compact( 'contracts', 'buildings', 'title_for_layout' ) );
    }

    public function contractantEdit()
    {
        $contractants = [];
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) )
        {
            if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
            {
               $request = $this->request->data;

               // エラーチェック
               $error = $this->Contractants->admin_validation( $request ); 
               if( count( $error ) > 0 )
               {
                   $contractants = $this->Contractants->newEntity( $request );
                   $this->set( compact( 'error' ) );
               }
               else
               {
                   // 先に画像をアップ
                   $this->File->saveFile( $request, 'contractants' );

                   $this->_session->write( 'Admin.contractants', $request );
                   $this->redirect(['action' => 'contractantConfirm']);
               }

            }
            // 戻る
            elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
            {
                if( $this->_session->check('Admin.contractants') )
                {
                    $contractants = $this->Contractants->newEntity( $this->_session->read('Admin.contractants') );
                }
            }
            else
            {
                $contractants = $this->contractant;
            }
            
            $title_for_layout = '設定ー契約者情報 編集';
            $this->set( compact( 'contractants', 'title_for_layout' ) );
        }
        else
        {
            THROW NEW BadRequestException('権限がありません');
        }
    }

    public function contractantConfirm()
    {
        $title_for_layout = '設定ー契約者情報 確認';
        $contractants = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.contractants' ) ) 
            {
                $data = $this->_session->read('Admin.contractants');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->Contractants->admin_validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'contractantEdit']);
            }
            else
            {
                if( isset( $data['files']['contractants']['file_path'] ) ) $data['file_path'] = $data['files']['contractants']['file_path'];
                unset( $data['files'] );
                $res = $this->Contractants->saveData( $data );

                if( $res )
                {
                    $msg = '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.contractants' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.contractants') )
        {
            $contractants = $this->_session->read('Admin.contractants');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'contractants', 'title_for_layout' ) );
    }

    public function edit( $id = null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           $error = $this->Contractants->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $contractants = $this->Contractants->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               // 先に画像をアップ
               $this->File->saveFile( $request, 'image' );
               if( isset( $request['uploaded'] ) === false )
               {
                   $request['uploaded'] = [];
               }

               if( $request['files']['image']['error'] === 0 && $request['files']['image']['size'] > 0 ) 
               {
                   $request['uploaded']['image'] = ( isset( $request['uploaded']['image'] ) ) ? array_merge( $request['uploaded']['image'], $request['files']['image'] ) : $request['files']['image'];
               }

               $this->_session->write( 'Admin.contractants', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.contractants') )
            {
                $contractants = $this->Contractants->newEntity( $this->_session->read('Admin.contractants') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $contractants = $this->Contractants->getEditData( $id );
                $contractants->uploaded = [];
                // 既存ファイル
                foreach( $contractants->files as $key => $val )
                {
                    // 画像
                    if( $val->file_path !== null && $this->File->checkAllowImageMimeType( $val->mime_type ) )
                    {
                        $contractants->uploaded['image'] = [
                            'id'         => $val->id
                            ,'file_path' => $val->file_path
                            ,'name'      => $val->name
                        ];
                    }
                }
            }
            else
            {
                //$contractants = $this->Contractants->newEntity();
                throw new ForbiddenException('不正な遷移です');
            }
        }
        $this->set( compact( 'contractants' ) );
    }

    public function favicon()
    {
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) )
        {
            $title_for_layout = 'ファビコン登録';

            if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
            {
                $request = $this->request->data;

                $error = [];
                if ( is_uploaded_file( $request['file']['tmp_name'] ) === true )
                {
                    $favicon   = $request['file']['tmp_name'];
                    $mime_type = mime_content_type( $favicon );

                    // ico形式の判定に揺れがあるので、画像形式は許可する
                    if( in_array( $mime_type, [ 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon'], true ) )
                    {
                        $dir = FAVICON_PATH . DS . $this->_session->read('ContractantData.unique_parameter');
                        if ( is_dir( $dir ) === false ) mkdir( $dir, 0777, true );
                        move_uploaded_file( $favicon, $dir . DS . 'favicon.ico' );

                        $this->Flash->success( 'faviconを登録しました' );
                        return $this->redirect(['action' => 'favicon']);
                    }
                    else
                    {
                        $error['file'] = 'ファビコン形式のファイルを設定してください';
                    }

                }
                else
                {
                    $error['file'] = 'ファビコンを設定してください';
                }
            }

            $this->set( compact( 'title_for_layout', 'error' ) );
        }
        else
        {
            throw new BadRequestException('権限がありません');
        }
    }


}
