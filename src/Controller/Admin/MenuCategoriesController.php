<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class MenuCategoriesController extends AdminAppController
{
    public $MenuCategories;

    public function initialize()
    {
        parent::initialize();
        $this->MenuCategories = TableRegistry::get('MenuCategories');
        $this->ContractantServiceMenus = TableRegistry::get('ContractantServiceMenus');

    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if (in_array($this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN]) === false )
        {
            throw new BadRequestException('権限がありません' );
        }
        $this->loadComponent( 'File' );
    }

    public function index( $service_menu_id=null, $id=null )
    {
        $service_menu = $this->ContractantServiceMenus->get( $service_menu_id );
        //debug( $service_menu );
        $category = $this->MenuCategories->getCategoryByServiceMenuId( $service_menu_id, [ 'contractant_id' => $this->_contractant_id ] );

        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['complete'] ) )
        {
            $request = $this->request->data;

            $data = $this->MenuCategories->newEntity( $request ); 
            if( count( $data->errors() ) === 0 )
            {
                // アイコン
                if( isset( $request['files']['icon_path'] ) && $request['files']['icon_path']['error'] === 0 && $request['files']['icon_path']['size'] )
                {
                    $this->File->saveFile( $request, 'icon_path' );
                    if( isset( $request['files']['icon_path']['file_path'] ) )
                    {
                        $request['icon_path'] = $request['files']['icon_path']['file_path'];
                        unset( $request['files'] );
                    }
                }

                if( isset( $request['id'] ) === false ) $request['orderby']  = $this->MenuCategories->getOrder( $service_menu_id );
                if( $data->isNew() )
                {
                    $request['contractant_id'] = $this->_contractant_id;
                }
                $menu_category_id = $this->MenuCategories->saveData( $request );

                if( $menu_category_id )
                {
                    $this->Flash->success( 'カテゴリーを登録しました' );
                }
                else
                {
                    $this->Flash->error( 'カテゴリーの登録に失敗しました' );
                }
                $this->redirect([ $service_menu_id ]);
            }
        }
        else
        {
            if( $id !== null )
            {
                $data = $this->MenuCategories->get( $id );
            }
            else
            {
                $data = $this->MenuCategories->newEntity();
            }
        }

        $this->set( compact( 'data', 'category', 'service_menu_id', 'service_menu' ) );
    }

    public function categoryDelete( $service_menu_id=null, $id=null )
    {
        if( $id !== null )
        {
            $MenuManuals =  TableRegistry::get('MenuManuals');
            if( $MenuManuals->checkDataExistByCategoryId( $service_menu_id, $id ) )
            {
                $this->Flash->error( 'このカテゴリーを使用しているデータがあるため、削除できません' );
            }
            else
            {
                $this->MenuCategories->deleteData( $id );
                $this->Flash->success( 'カテゴリーを削除しました。' );
            }
        }
        else
        {
            $this->Flash->error( 'カテゴリーの削除に失敗しました。' );
        }
        return $this->redirect([ 'action' => 'index', $service_menu_id ]);
    }

}
