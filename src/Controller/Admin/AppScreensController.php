<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class AppScreensController extends AdminAppController
{
    public $AppScreens;
    public $ServiceMenus;
    public function initialize()
    {
        parent::initialize();
        $this->AppScreens   = TableRegistry::get('AppScreens');
        $this->ServiceMenus = TableRegistry::get('ServiceMenus');
        $this->ContractantServiceMenus = TableRegistry::get('ContractantServiceMenus');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $this->set( 'title_for_layout', '画面管理' );
    }

    public function index()
    {
        $contractant_id = $this->_session->read( 'ContractantData.id' );
        $screens = $this->AppScreens->getScreenByContractantId( $contractant_id );

        $this->set( compact( 'screens' ) );

    }

    // todo メインの画面を登録
    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;
            $error = $this->AppScreens->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $data = $this->AppScreens->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write( 'Admin.data', $request );
                $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.data') )
            {
                $data = $this->AppScreens->newEntity( $this->_session->read('Admin.data') );
            }
            else
            {
            }

        }
        else
        {
            if( $id !== null )
            {
                $data = $this->AppScreens->get( $id );
            }
            else
            {
                $data = $this->AppScreens->newEntity( );
            }
        }
        $this->set( compact( 'data' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'Admin.data' ) ) 
           {
               $data = $this->_session->read('Admin.data');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->AppScreens->validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               // 登録
               $data['contractant_id'] = $this->_session->read( 'ContractantData.id' );
               if( isset( $data['id'] ) === false ) $data['orderby'] = $this->AppScreens->getOrder( $this->_session->read( 'ContractantData.id' ) );

               $res = $this->AppScreens->saveData( $data );

               if( $res )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('Admin.data' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('Admin.data') )
        {
            $data = $this->_session->read('Admin.data');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }

        $this->set( compact( 'data' ) );
    }


    // 並び替え
    public function menus( $id=null )
    {
        if( $id !== null )
        {
            $footer = $this->AppScreens->get( $id );
            //todo 選択済みのメニューを表示
            $selected_service_menus = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_session->read( 'ContractantData.id' ), $id );

            $this->set( compact( 'footer', 'selected_service_menus' ) );
        }
    }

}
