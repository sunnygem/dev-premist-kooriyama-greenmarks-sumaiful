<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class TopsController extends AdminAppController
{
    // クラス内プロパティを宣言
    public $Buildings;
    public $Presentations;

    public function initialize()
    {
        parent::initialize();

    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        // モデルを呼び出し
        $this->Buildings = TableRegistry::getTableLocator()->get('Buildings');
    }

    // ダッシュボード
    public function index()
    {
    //debug( $this->_session->read('Admin' ));
    //debug( $this->Auth->user() );

        $data = $this->request->getQuery('front_users' );

        $this->set( compact( 'data' ) );

        /////// ここから
        // 申請
        $unapproved_user_count = $this->Buildings->getUnapprovedUserPerBuilding( $this->_contractant_id, $this->_admin_building_id );

        // 未対応レポート
        $no_compatible_presentation_count = $this->Buildings->getNonCompatibleCount( $this->_contractant_id, $this->_admin_building_id  );

        $this->set( compact( 'unapproved_user_count', 'no_compatible_presentation_count' ) );
    }
}
