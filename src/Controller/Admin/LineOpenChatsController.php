<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class LineOpenChatsController extends AdminAppController
{
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();

        $this->LineOpenChatCategories = TableRegistry::getTableLocator()->get('LineOpenChatCategories');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->_unique_parameter !== 'willing' )
        {
            throw new NotFoundException();
        }
        else
        {
            $title_for_layout = 'Lineオープンチャット管理';

            $this->loadComponent( 'Common' );
            $this->loadComponent( 'File' );

            $this->set( compact( 'title_for_layout' ) );
        }
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'LineOpenChats.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [
                    'LineOpenChats.release_date' => 'DESC'
                    ,'LineOpenChats.modified'    => 'DESC'
                ]
            ]
        ];

        $this->paginate['contain'] = [
            'LineOpenChatCategories' => [
                'fields' => [
                    'category_name' => 'LineOpenChatCategories.name'
                ]
            ]
        ];
        $data = $this->paginate( 'LineOpenChats' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $categories = $this->LineOpenChatCategories->getOptions();
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            // エラーチェック
            $error = $this->LineOpenChats->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $line_open_chat = $this->LineOpenChats->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                // アイコン画像のサーバー保存
                if( isset( $request['files']['icon_url_path'] ) && $request['files']['icon_url_path']['error'] === 0 && $request['files']['icon_url_path']['size'] )
                {
                    $this->File->saveFile( $request, 'icon_url_path' );
                }

                $this->_session->write( 'Admin.line_open_chats', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.line_open_chats') )
            {
                $line_open_chat = $this->LineOpenChats->newEntity( $this->_session->read('Admin.line_open_chats') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $line_open_chat = $this->LineOpenChats->getEditData( $id );
            }
            else
            {
                $line_open_chat = $this->LineOpenChats->newEntity();
            }
        }
        $this->set( compact( 'line_open_chat', 'categories' ) );
    }

    public function confirm()
    {
        $categories = $this->LineOpenChatCategories->getOptions();
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.line_open_chats' ) ) 
            {
                $data = $this->_session->read('Admin.line_open_chats');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->LineOpenChats->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                if( isset( $data['contractant_id'] ) === false )
                {
                    $data['contractant_id'] = $this->_session->read('ContractantData.id');
                }

                // アイコン画像パスのDB保存
                if( isset( $data['files']['icon_url_path']['file_path'] ) )
                {
                    $data['icon_url_path'] = $data['files']['icon_url_path']['file_path'];
                    unset( $data['files'] );
                }

                $line_open_chat_id = $this->LineOpenChats->saveData( $data );

                if( $line_open_chat_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.line_open_chats' );
                // システム画面は完了画面無し
                return $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.line_open_chats') )
        {
            $line_open_chat = $this->_session->read('Admin.line_open_chats');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'line_open_chat', 'categories' ) );
    }

    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $this->LineOpenChats->deleteData( $id );
                $this->Flash->success('チャット情報を削除しました。');
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }

    public function category()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'LineOpenChatCategories.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [ ]
            ]
        ];

        $data = $this->paginate( 'LineOpenChatCategories' );
        $this->set( compact( 'data' ) );
    }

    public function categoryEdit( $id = null)
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            // エラーチェック
            $error = $this->LineOpenChatCategories->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $line_open_chat_category = $this->LineOpenChatCategories->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            // 登録
            else
            {
                 // カテゴリのアイコン
                 if( isset( $request['files']['icon_url_path'] ) && $request['files']['icon_url_path']['error'] === 0 && $request['files']['icon_url_path']['size'] )
                 {
                     $this->File->saveFile( $request, 'icon_url_path' );

                     if( isset( $request['files']['icon_url_path']['file_path'] ) )
                     {
                         $request['icon_url_path'] = $request['files']['icon_url_path']['file_path'];
                         unset( $request['files'] );
                     }
                 }

                 if( isset( $request['id'] ) === false )
                 {
                     $request['contractant_id'] = $this->_session->read('ContractantData.id');
                     $request['orderby'] = $this->LineOpenChatCategories->getNextOrderby();
                 }

                 if( $this->LineOpenChatCategories->saveData( $request ) )
                 {
                     $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                     $this->Flash->success( $msg );
                 }
                 else
                 {
                     $this->Flash->error( '登録に失敗しました。' );
                 }

                 // システム画面は完了画面無し
                 return $this->redirect(['action' => 'category']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.line_open_chat_categories') )
            {
                $line_open_chat_category = $this->LineOpenChatCategories->newEntity( $this->_session->read('Admin.line_open_chat_categories') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $line_open_chat_category = $this->LineOpenChatCategories->get( $id );
            }
            else
            {
                $line_open_chat_category = $this->LineOpenChatCategories->newEntity();
            }
        }
        $this->set( compact( 'line_open_chat_category' ) );
    }
    
    public function categoryDelete( $id=null )
    {
        // todo 登録されている件数をチェック
        if( $id !== null && in_array( $this->Auth->user('admin_user.authority'),[ SYSTEM_ADMIN, ADMIN ], true ) )
        {
            $this->autoRender = false;
            if( $this->LineOpenChatCategories->checkRegistered( $id ) === true )
            {
                try
                {
                    $this->LineOpenChatCategories->deleteData( $id );
                    $this->Flash->success('カテゴリーを削除しました。');
                }
                catch ( Exception $e )
                {
                    $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
                }
            }
            else
            {
                $this->Flash->error( "紐づいたデータがあるため削除できません。削除する場合は登録を解除してください。" );
            }
            return $this->redirect(['action' => 'category']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }


}
