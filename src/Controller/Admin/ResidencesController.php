<?php

namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ResidencesController extends AdminAppController
{
    public $Users;
    public $Posts;
    public $PasswordSecurities;
    public $LuggageStrages;
    public $paginate = [ 'finder' => 'search', 'limit' => 50 ];

    public function initialize()
    {
        parent::initialize();
        $this->Users           = TableRegistry::getTableLocator()->get('Users');
        $this->FrontUsers      = TableRegistry::getTableLocator()->get('FrontUsers');
        $this->Posts           = TableRegistry::getTableLocator()->get('Posts');
        $this->LuggageStrages  = TableRegistry::getTableLocator()->get('LuggageStrages');
        $this->ChatMemberlists = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
        $this->Threads         = TableRegistry::getTableLocator()->get( 'Threads' );
        //$this->CustomerUsers = TableRegistry::get('AdminUsers');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        // system_adminユーザー以外はアクセス不可
        if(
            in_array( $this->_session->read( 'Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) === false 
            && in_array( $this->request->action, [ 'index', 'detail' ] ) === false 
        )
        {
            throw new ForbiddenException('このページは閲覧できません');
        }

        // 暫定
        $mt_customer_type = $this->_mt_residence_type;

        $title_for_layout = '居住管理';
        $this->set( compact( 'mt_customer_type', 'title_for_layout' ) );

    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'order' => [
                    'BuildingRoomNumbers.room_number' => 'ASC'
                    //'BuildingRoomNumbers.id' => 'ASC'
                    //,'FrontUsers.building_id' => 'ASC'
                ]
                ,'conditions' => [
                    'Buildings.contractant_id' => $this->_session->read( 'ContractantData.id' )
                    ///,'Users.type'          => 'front'
                    ///,'FrontUsers.common_user_flg' => 0
                ]
            ]
        ];

        // ユーザーの建物権限からbuilding_idを判定
        // マスター
        if( $this->_admin_building_id === null )
        {
            $building_id = ( $this->request->getQuery( 'buildings' ) !== null && $this->request->getQuery( 'buildings' ) > 0 ) ? $this->request->getQuery( 'buildings' ) : null;
        }
        else
        {
            $building_id = $this->_admin_building_id;
        }

        // 検索
        if( $building_id !== null )
        {
            // 建物
            $this->paginate['finder'][ 'search']['conditions'][] = [ 'Buildings.id' => $building_id ];

            // 部屋番号
            if( $this->request->getQuery( 'room_number' ) !== null && strlen( $this->request->getQuery( 'room_number' ) ) > 0 )
            {
                $room_number = trim( mb_convert_kana( $this->request->getQuery( 'room_number' ), 'KVas' ) );
                $this->paginate['finder']['search']['conditions'][] = [ 'BuildingRoomNumbers.room_number' => $room_number ];
            }

            $this->paginate['contain'] = [
                'FrontUsers' => [
                    'conditions' => [
                        'FrontUsers.deleted IS'  => null
                        ,'FrontUsers.contractant_id'  => $this->_contractant_id
                    ]
                ]
                ,'FrontUsers.Users' => [
                    'conditions' => [
                        'Users.deleted IS'  => null
                    ]
                ]
            ];
            // 実生に
            if( $this->request->getQuery( 'unautholized' ) !== null && $this->request->getQuery( 'unautholized' ) === '1'  )
            {
                $cond = [];
                if( $building_id !== null ) $cond[ 'building_id'] = $building_id;
                if( $this->request->getQuery( 'room_number' ) !== '' ) $cond[ 'room_number'] = trim( mb_convert_kana( $this->request->getQuery( 'room_number' ), 'KVas' ) );
                $arr_unautholized_front_user_room_id = $this->FrontUsers->getUnautholizedBuildingRoomID( $this->_contractant_id, $cond );
                if( count( $arr_unautholized_front_user_room_id ) > 0 )
                {
                    $this->paginate['finder']['search']['conditions']['BuildingRoomNumbers.id IN'] = $arr_unautholized_front_user_room_id;
                }
                $this->paginate['contain'] = [
                    'FrontUsers' => [
                        'conditions' => [
                            'OR' => [
                                'FrontUsers.auth_flg' => 0
                                ,'FrontUsers.reapply_flg' => 1
                            ]
                            ,'FrontUsers.deleted IS'  => null
                        ]
                    ]
                    ,'FrontUsers.Users' => [
                        'conditions' => [
                            'Users.deleted IS'  => null
                        ]
                    ]
                ];
            }
            //debug( $this->paginate );
            $data = $this->paginate( 'BuildingRoomNumbers' );
        }
        else
        {
            $data = null;
        }
        //debug( $data );

        // 管理者のユーザーを取得
        $admin_conditions = [
             'FrontUsers.building_id' => $building_id
        ];
        $admin_front_users = $this->FrontUsers->getAdminFrontUser( $this->_contractant_id, $admin_conditions);

        $this->set( compact( 'data', 'admin_front_users' ) );
    }

    public function detail( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['approve'] ) )
        {
            $request = $this->request->data;

            // データの存在チェック 
            $users = $this->Users->getFrontUserIncludeName( $request['id'], [ 'FrontUsers.common_user_flg' => 0 ] );

            // 物件を限定されている管理ユーザの場合
            if( $this->_admin_building_id !== null && $this->_admin_building_id !== $users->front_user->building_id )
            {
                throw new ForbiddenException();
            }

//var_dumP($users);
            if( $users )
            {
                // tmp_keyの発行
                $tmp_key = substr( md5( time() . uniqid() ). 0, 12 );
                $data = [
                   'id' => $users->id
                   ,'valid_date' => date( 'Y-m-d H:i:s' )
                   ,'front_user' => [
                       'id' => $users->front_user->id
                       ,'auth_flg'    => 1 // 認証フラグを立てる
                       ,'reapply_flg' => 0 // 再申請フラグは折る
                       ,'reject_comment' => null // 却下コメントを削除
                       ,'tmp_key'  => $tmp_key
                   ]
                ];
                $this->Users->saveData( $data );

                // チャットメンバーリストを作成
                $front_users = $this->FrontUsers->getAllData( $this->_session->read( 'ContractantData.id' ) );
                $res = $this->ChatMemberlists->createList( $this->_session->read( 'ContractantData.id' ), $users->id, $users->front_user->id, $front_users );

                // ユーザーへメール
                parent::app_common();
                //$users['email']   = $users['login_id'];
                $users['url']     = APP_URL . DS . 'applications' . DS . 'password' . DS . $tmp_key;
                $users['tmp_key'] = $tmp_key;
                //$users['from_mail'] = 'info@' . $users->contractant->domain; // TODO 契約者とかに情報を登録できるようにする
                //$users['from_name'] = $users->contractant->name;
                $this->loadComponent( 'Mail' );
                $this->Mail->send_application_permission( $users );
                $this->Flash->success( '承認手続きを完了しました。' );
//                $this->redirect(['action' => 'detail', $users->id ] );
            }
            else
            {
                $this->Flash->error( 'ユーザーが存在しません' );
                $this->redirect(['action' => 'index', '?' => $this->redirect->getQuery() ]);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.users') )
            {
                $users = $this->Users->newEntity( $this->_session->read('Admin.users') );
                $users->password = null;
            }
            else
            {
            }
        }
        else
        {
            if( $id !== null )
            {
                $users = $this->Users->get( $id );
                $users->front_user = $this->FrontUsers->getDataByUserIdAndContractantId( $id, $this->_session->read('ContractantData.id' ) );

                // 物件を限定されている管理ユーザの場合
                if( $this->_admin_building_id !== null && $this->_admin_building_id !== $users->front_user->building_id )
                {
                    throw new ForbiddenException();
                }

                $posts = $this->Posts->getPostsByUserId( $users->id, null, null, true );
                //debug( $posts );
                $this->set( compact( 'posts' ) );
            }
            else
            {
                throw new NotFoundException();
            }
        }
        $this->set( compact( 'users' ) );
    }

    public function editStatus( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) /*&& isset( $this->request->data['approve'] )*/ )
        {
            $request = $this->request->data;
            $users = $this->Users->get( $id );
            $users->front_user = $this->FrontUsers->getDataByUserIdAndContractantId( $id, $this->_session->read('ContractantData.id' ) );

            //debug( $request );exit;

            $error = []; 
            if( $request['front_user']['type'] === '' )
            {
                $error['front_user'] = [ 'type' => '選択して下さい' ];
            }
            if( count( $error ) > 0 )
            {
                //$users = $request;
                $this->set( compact( 'error' ) );
            }
            else
            {
                //$this->_session->write( 'Admin.users', $request );
                //$this->redirect(['action' => 'confirm']);

                unset( $request['complete'] );

                $entitiy = $this->Users->patchEntity( $users, $request, [
                    'associated' => [ 'FrontUsers' ]
                ]);
                $this->Users->save( $entitiy );

                $this->Flash->success( 'タイプを変更しました。' );
                $this->redirect(['action' => 'index']);
            }
        }
        elseif( $id !== null )
        {
            $users = $this->Users->getFrontUser( $id, [ 'FrontUsers.common_user_flg' => 0 ] );
        }
        else
        {
            throw new NotFoundException( 'ユーザーがみつかりません' );
        }
        $this->set( compact( 'users' ) );
    }

    // 申請拒否
    public function rejectApplication()
    {
        if( $this->request->is( [ 'post', 'put' ] ) )
        {
            $request = $this->request->data;
            $users = $this->Users->get( $request['id'] );
            $user_id = $users->id;
            $users->front_user = $this->FrontUsers->getDataByUserIdAndContractantId( $user_id, $this->_session->read('ContractantData.id' ) );

            if( $users->front_user->auth_flg === 0 || $users->front_user->reapply_flg === 1 )
            {

                if( $this->request->is([ 'post', 'put', 'patch' ]) /*&& isset( $this->request->data['approve'] )*/ )
                {
                    $request = $this->request->data;
                    if( $request['reject_comment'] === '' )
                    {
                        $error['reject_comment'] = 'コメントをいれてください';
                        $this->set( compact( 'error' ) );
                    }
                    else
                    {
                        $data = [
                            'id' => $users->front_user->id
                            ,'reject_comment' => $request['reject_comment']
                        ];
                        $res = $this->FrontUsers->saveData( $data );

                        if( $res )
                        {
                            $this->Flash->success( '却下しました' );
                        }
                        else
                        {
                            $this->Flash->error( '却下しました' );
                        }
                        $this->redirect(['action' => 'detail', $users->id]);
                        //$users = $this->Users->get( $user_id );
                    }
                }
                else
                {
                }
            }
            else
            {
                $this->redirect(['action' => 'index']);
            }
        }
        else
        {
            throw new NotFoundException( 'ユーザーがみつかりません' );
        }
        $this->set( compact( 'users' ) );
    }

    // 管理側で投稿の表示を禁止する
    public function offDispalayPost( $id=null )
    {
        if( in_array( 12, $this->_session->read( 'ContractantServiceMenus.data' ) ) )
        {
             $this->autoRender = false;

             $post = $this->Posts->get( $id );
             $data = [
                 'id' => $post->id
                 ,'display_flg' => 0
             ];
             $res = $this->Posts->saveData( $data );
             if( $res )
             {
                  $this->Flash->success( '投稿を非表示にしました。');
             }
             else
             {
                  $this->Flash->error( '処理に失敗しました。');
             }
             $this->Redirect([ 'action' => 'detail', $post->user_id ]);
        }
        else
        {
             throw new ForbiddenException();
        }
    }

    //荷物の預かりを通知
    public function luggage( $user_id=null, $front_user_id=null )
    {
        $this->autoRender = false;
        if( $user_id !== null && $front_user_id !== null )
        {
            // 物件を限定されている管理ユーザの場合
            if( $this->_admin_building_id !== null )
            {
                $front_user = $this->FrontUsers->findById( $front_user_id )->first();
                if( $this->_admin_building_id !== $front_user->building_id )
                {
                    throw new ForbiddenException();
                }
            }

            try
            {
                $data = [
                    'contractant_id' => $this->_session->read( 'ContractantData.id' )
                    ,'user_id'       => $user_id
                    ,'front_user_id' => $front_user_id
                ];
                $res = $this->LuggageStrages->saveData( $data ); 

            }
            catch( \Exception $e )
            {
                echo $e->getMessage();
            }

            if( $res )
            {
               // PUSH通知
                $cmd = SHELL_COMMAND . ' push ' . $this->_session->read('ContractantData.unique_parameter') . ' luggage_strage ' . $front_user_id;
                exec( $cmd, $out, $ret );
                if( $ret === 0 )
                {
                    $this->Flash->success('荷物預かりを登録しました。');
                }
                else
                {
                    $err_msg = '';
                    foreach( $out as $val )
                    {
                        $err_msg .= strip_tags( $val ) . "\n";
                    }
                    $this->Flash->error("PUSH通知の送信に失敗しました。\n\n" . $err_msg );
                }
            }
            else
            {
                 $this->Flash->error('荷物預かり登録に失敗しました。');
            }
            $this->redirect([ 'action' => 'index', '?' => $this->request->getQuery() ]);
            
        }
        else
        {
            throw new NotFoundException( 'ユーザーがみつかりません' );
        }
    }

    //public function delete( $user_id=null, $front_user_id=null )
    //{
    //    if( $user_id !== null && $front_user_id !== null && in_array( $this->Auth->user('admin_user.authority'), [ SYSTEM_ADMIN, ADMIN], true ) )
    //    {
    //        $this->autoRender = false;

    //        try
    //        {
    //            // chat_memberlistsを削除
    //            // selfとpartnerそれぞれ削除
    //            $chat_cond = [
    //                'OR' => [
    //                    [
    //                        'self_user_id' => $user_id
    //                        ,'self_front_user_id' => $front_user_id
    //                    ]
    //                    ,[
    //                        'partner_user_id' => $user_id
    //                        ,'partner_front_user_id' => $front_user_id
    //                    ]
    //                ]
    //            ];
    //            $this->ChatMemberlists->updateAll( [ 'deleted' => date( 'Y-m-d H:i:s' ) ] ,$chat_cond );

    //            $cond = [
    //                'FrontUsers.id'          => $front_user_id
    //                ,'FrontUsers.deleted IS' => null
    //            ];
    //            $user = $this->Users->getFrontUser( $user_id, $cond );

    //            if( $user )
    //            {
    //                 // front_userを削除
    //                 $res = $this->FrontUsers->deleteData( $user->front_user->id );
    //                 $this->Flash->success('ユーザーを削除しました。');
    //            }
    //            else
    //            {
    //                 throw new Exception( 'ユーザーが存在しません。' );
    //            }
    //        }
    //        catch (Exception $e)
    //        {
    //            $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
    //        }
    //        return $this->redirect(['action' => 'index']);
    //    }
    //    else
    //    {
    //        throw new ForbiddenException('不正な操作です');
    //    }
    //}

    public function toggleLeave( $user_id, $front_user_id )
    {
         $front_user = $this->FrontUsers->getDataByIdAndUesrId( $front_user_id, $user_id );

         // 物件を限定されている管理ユーザの場合
         if( $this->_admin_building_id !== null && $this->_admin_building_id !== $front_user->building_id )
         {
             throw new ForbiddenException();
         }

         if( $front_user )
         {
             $data = [
                 'id'         => $front_user->id
                 ,'leave_flg' => ( $front_user->leave_flg === 0 ) ? 1 : 0
             ];
             $res = $this->FrontUsers->saveData( $data );

             if( $res )
             {
                 $this->Flash->success( '居住のステータス変更が完了しました。');
             }
             else
             {
                 $this->Flash->error( '居住ステータス変更に失敗しました。');
             }
         }
         else
         {
             $this->Flash->error( 'ユーザーの取得に失敗しました。');
         }
         $this->redirect([ 'action' => 'index', '?' => $this->request->getQuery() ]);
    }

    public function message( $user_id, $front_user_id )
    {
         $this->autoRender = false;

         // ログインユーザーに有効なfront_userのデータがあるか
         $admin_front_user = $this->FrontUsers->getDataByUserId( $this->_session->read( 'Admin.Auth.id' ) );
         if( $admin_front_user )
         {
             if( $admin_front_user->nickname !== null ) 
             {
                 // PC側にログインがあって、ユーザーが違う場合はセッションを削除
                 if( $this->_session->check('Front') && $this->_session->read('Front.Auth.id') !== $this->_session->read( 'Admin.Auth.id' ) )
                 {
                     $this->_session->delete('Front');
                     
                 }

                 // 管理ユーザーの持つフロントユーザーデータからchat_group_idを取得
                 $thread = $this->Threads->getDataByTitle( $admin_front_user->user_id, $admin_front_user->id, $user_id, $front_user_id );
                 // 管理ユーザーと対象ユーザーのidの組み合わせからチャット画面へリダイレクト
                 $url = '/messages/detail/' . $thread->chat_group_id . DS . $thread->id . DS . $user_id . DS . $front_user_id;
                 header( 'Location: ' . $url );
                 exit;
             }
             else
             {
                 $this->Flash->error( '生徒アカウントでアプリにログインし、ニックネームの登録をしてください。' );
                 $this->redirect(['action' => 'index', '?' => $this->request->getQuery() ]);
             }
         }
         else
         {
             $this->Flash->error( 'メッセージには生徒アカウントが必要です。生徒の登録申請を同じ登録メールアドレスで行ってください。' );
             $this->redirect(['action' => 'index', '?' => $this->request->getQuery() ]);
         }

    }


    public function delete( $front_user_id )
    {
        $res = $this->FrontUsers->getDataById( $front_user_id, $this->_contractant_id );
        if( $res )
        {
           $this->FrontUsers->deleteData( $res->id );
           $this->Flash->success('ユーザーを削除しました');
        }
        else
        {
           $this->Flash->error('ユーザー削除に失敗しました');
        }
        return $this->redirect(['action' => 'index']);
    }

 
}
