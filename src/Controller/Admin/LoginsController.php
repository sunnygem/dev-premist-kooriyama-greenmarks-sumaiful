<?php
namespace App\Controller\Admin;

use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Cake\Auth\DefaultPasswordHasher;

class LoginsController extends AdminAppController
{
    public $Users;
    public $_contractant_data;

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['passwordRemind', 'initUser', 'setPassword', 'resetPassword', 'logout', 'requestApproval' ]);
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $this->Users                  = TableRegistry::get( 'Users' );
        $this->AdminUsers             = TableRegistry::get( 'AdminUsers' );
        $this->PasswordHistories      = TableRegistry::get( 'PasswordHistories' );
        $this->LoginApprovalHistories = TableRegistry::get( 'LoginApprovalHistories' );
        $this->loadComponent( 'Mail' );

        $this->_contractant_data = $this->_session->read( 'ContractantData' );
    }

    public function index()
    {
        // ログインしてたらTOPへ
        if( $this->Auth->user() !== null ) return $this->redirect(['controller' => 'Tops']);
//$hash = new DefaultPasswordHasher; //debug( $hash->hash('admin1234'));
//debug( $this );
        if( $this->request->is( 'post' ) )
        {
            $user = $this->Auth->identify();

            // ログインロックされているかつ解除時間を経過していない
            if( $this->request->getCookie( $this->_contractant_data['unique_parameter'] . '.admin_login_lock', [] ) === '1' && ( $this->request->getCookie( $this->_contractant_data['unique_parameter'] . '.admin_lock_date' ) + ( $this->_security['release_hour'] * 60 * 60 ) > time() ) )
            {
                $this->Flash->error( 'ログインがロックされています。しばらく時間を空けてからまた操作してください。' );
                //return $this->redirect([ 'action' => 'index' ]);
            }
            // admin_userを持つもののみ
            else if( $user['admin_user'] !== null )
            {
                // 有効期限切れ
                if( $user['expire_date'] !== null && time() > strtotime( $user['expire_date'] ) )
                {
                    $this->Flash->error( 'パスワードの有効期限が切れてきます。再設定をしてください。' );
                    return $this->redirect( ['action' => 'resetPassword' ]);
                }
                // ログイン
                elseif( $user )
                {
                    //$cookie = ( $this->request->getCookie( $this->_contractant_data['unique_parameter'], [] ) ) ? json_decode( $this->request->getCookie( $this->_contractant_data['unique_parameter'], [] ), true ) : [];
                    $cookie = ( $this->request->getCookie( $this->_contractant_data['unique_parameter'], [] ) ) ? $this->request->getCookie( $this->_contractant_data['unique_parameter'], true ) : [];
                    //$cookie = ( $this->request->getCookie( 'sumaiful' ) !== '' ) ?  $this->request->getCookie( 'sumaiful' ) : [];
                    //$cookie['admin_login_false_num'] = 0;
                    //$cookie['admin_login_lock']      = 0;
                    if( $this->request->data['auto_login'] === '1' )
                    {
                        $cookie = [];
                        // 期限が設定されてなければ24時間
                        $cookie[] = [
                            'auto_login' => [
                                'date'  => time()
                                ,'id' => $user['id']
                            ]
                        ];
                    }
                    else
                    {
                        //unset( $cookie['auto_login'] );
                    }
                    $this->response = $this->response->withCookie( $this->_contractant_data['unique_parameter'], [
                        'value' => $cookie 
                    ]);

                    $this->Auth->setUser( $user );
                    return $this->redirect( $this->Auth->redirectUrl() );
                }
            }
            // ログイン失敗
            else
            {
                $this->Flash->error( 'ログインに失敗しました。' );

                //$cookie = ( $this->request->getCookie( 'sumaiful' ) !== '' ) ?  $this->request->getCookie( 'sumaiful' ) : [];
                $cookie = json_decode( $this->request->getCookie( $this->_contractant_data['unique_parameter'] ), true );
                // 失敗回数をクッキーに保存
                if ( $this->request->getCookie( $this->_contractant_data['unique_parameter'], '.admin_login_false_num' ) ) 
                {
                    $cookie['admin_login_false_num'] = $this->request->getCookie( $this->_contractant_data['unique_parameter'] . '.admin_login_false_num', 0 ) + 1;
                    $this->response = $this->response->withCookie( $this->_contractant_data['unique_parameter'], [
                        'value' => $cookie
                    ]);
                }
                else
                {
                    $cookie['admin_login_false_num'] = 1;
                    $cookie['admin_login_lock']      = '0';
                    $this->response = $this->response->withCookie( $this->_contractant_data['unique_parameter'], [
                        'value' => $cookie
                    ]);
                }

                // 失敗回数を超えたらロックをかける
                $login_false_num = $cookie['admin_login_false_num'];
                if ( $this->_security['lock_num'] !== null && $login_false_num >= $this->_security['lock_num'] )
                {
                    $cookie['admin_login_lock'] = '1';
                    $cookie['admin_lock_date']  = time();
                    $this->response = $this->response->withCookie( $this->_contractant_data['unique_parameter'], [
                        'value' => $cookie
                    ]);
                }
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    // 初回ログイン
    public function initUser()
    {
        if( $this->request->is( 'post' ) )
        {
            $request = $this->request->data;
            $error   = $this->Users->init_validation( $request, $this->_security );
            if( count( $error ) > 0 )
            {
                $this->set( compact( 'error' ) );
            }
            else
            {
                $admins = $this->Users->getUninitializedUserBy( $request['login_id'] );
                $data = [
                    'id' => $admins->id
                    ,'password' => $request['password']
                ];

                // 有効期限の設定がある場合
                if( $this->_security['valid_term'] !== null ) $data['expire_date'] = date( 'Y-m-d H:i:s', time() + ( $this->_security['valid_term'] * 86400 ) );

                $user_id = $this->Users->saveData( $data );

                // パスワード変更履歴を保存
                $password = [
                    'user_id'    => $user_id
                    ,'password' => $request['password']
                ];
                $password_history_res = $this->PasswordHistories->saveData( $password );

                $res = true;
                if( $user_id && $password_history_res )
                {
                    $this->Flash->success( 'パスワードの設定が完了しました' );
                    // パスワードが設定できたらログイン
                    $user = $this->Auth->identify();
                    if( $user )
                    {
                        $this->Auth->setUser( $user );
                        return $this->redirect( $this->Auth->redirectUrl() );
                    }
                }
                else
                {
                    $this->Flash->error( 'パスワードの設定ができませんでした' );
                }
            }

        }
    }

    // パスワード設定
    public function setPassword( $key=null )
    {
        if( $key !== null )
        {
            if( $this->request->is( ['post', 'put'] ) )
            {
                $msg = [];
                $request = $this->request->data;
                
                //todo 該当ユーザがいない時
                $admins = $this->AdminUsers->getDataByRemindkey( $key, $this->_contractant_id );

                if ( $request['password'] === '' )
                {
                    $msg[] = 'パスワード入力してください。';
                }
                if ( $request['password'] !== $request['password_confirm'] )
                {
                    $msg[] = 'パスワードが一致しません。';
                }

                if ( count( $msg ) === 0 )
                {
                    $data = [
                       'id' => $admins->user->id
                       ,'password' => $request['password']
                       ,'admin_user' => [
                           'id' => $admins->id
                           ,'init_flg' => 1
                           ,'remind_key' => null
                       ]
                    ];
                    $res = $this->Users->saveData( $data );

                    if( $res )
                    {
                        $this->Flash->success( 'パスワードを設定しました。ログインしてください。' );
                        return $this->redirect( [ 'action' => 'index'] );
                    }
                    else
                    {
                        $this->Flash->error( 'パスワードを設定できませんでした。' );
                    }
                }
                else
                {
                    foreach( $msg as $val )
                    {
                        $this->Flash->error( $val );
                    }
                }
            }
        }
        else
        {
            throw new ForbiddenException();
        }

        $this->set( 'title_for_layout', 'パスワード設定' );
    }

    // パスワード設定
    public function resetPassword( $key=null )
    {
        if( $this->request->is( 'post' ) )
        {
            $request = $this->request->data;

            // ユーザーを取得
            $admins = $this->Users->getDataPrePassword( $request['login_id'], $this->_contractant_id );
            if( $admins )
            {
                // パスワードのチェック
                $hasher = new DefaultPasswordHasher; 
                $password_diff = $hasher->check( $request['pre_password'], $admins->password );
                if( $password_diff )
                {

                    $error   = $this->Users->reset_validation( $request, $this->_security );
                    if( count( $error ) > 0 )
                    {
                        $this->set( compact( 'error' ) );
                    }
                    else
                    {
                        // パスワードの修正の制御をチェック
                        if( $this->_security->prohibition_num != null && $this->PasswordHistories->checkProhibitionNum( $admins->id, $request['password'], $this->_security ) === false )
                        {
                            $prohibition_num_res = $this->PasswordHistories->checkProhibitionNum( $admins->id, $request['password'], $this->_security );
                            $this->Flash->error( '以前使用されたパスワードは利用できません' );
                            return $this->redirect([ 'action' => 'resetPassword' ]);
                        }

                        $data = [
                            'id' => $admins->id
                            ,'password' => $request['password']
                        ];

                        // 有効期限の設定がある場合
                        if( $this->_security['valid_term'] !== null ) $data['expire_date'] = date( 'Y-m-d H:i:s', time() + ( $this->_security['valid_term'] * 86400 ) );

                        $user_id = $this->Users->saveData( $data );

                        // パスワード変更履歴を保存
                        $password = [
                            'user_id'    => $user_id
                            ,'password' => $request['password']
                        ];
                        $password_history_res = $this->PasswordHistories->saveData( $password );

                        $res = true;
                        if( $user_id && $password_history_res )
                        {
                            $this->Flash->success( 'パスワードの再設定が完了しました' );
                            // パスワードが設定できたらログイン
                            $user = $this->Auth->identify();
                            if( $user )
                            {
                                $this->Auth->setUser( $user );
                                return $this->redirect( $this->Auth->redirectUrl() );
                            }
                        }
                        else
                        {
                            $this->Flash->error( 'パスワードの設定ができませんでした' );
                        }
                    }
                }
                else
                {
                    $this->Flash->error( '正しいログインIDとパスワードを入力してください。' );
                }
            }
            else
            {
                $this->Flash->error( '正しいログインIDとパスワードを入力してください。' );
            }

        }
        $this->set( 'title_for_layout', 'パスワード再設定' );
    }

    // ログイン期間承認リクエスト
    public function requestApproval()
    {
        // 承認者
        $auth_users = $this->Users->getListAdminAuthUser( $this->_contractant_id );
        if( $this->request->is( 'post' ) && isset( $this->request->data['approve'] ) )
        {
            $request = $this->request->data;
            // バリデーション
            $error = $this->Users->login_approve_validation( $request );
            if( count( $error ) > 0 )
            {
            //    $this->request->data = $request;
                $this->set( compact( 'error' ) );
            }
            // リクエスト送信
            else
            {
                $auth_user = $this->Users->getAdminUser( $request['auth_user_id'] );

                $data = [
                    'contractant_id'        => $this->_contractant_id
                    ,'request_key'          => parent::createTmpKey()
                    ,'client_user_login_id' => $request['login_id']
                    ,'request_date'         => date( 'Y-m-d H:i:s') 
                    ,'authorize_user_id'    => $auth_user->login_id 
                ];
                $this->LoginApprovalHistories->saveData( $data );

                $this->loadComponent( 'Mail' );
                $email = [
                    'to'        => $auth_user['admin_user']['email']
                    ,'auth_user' => $auth_user['login_id']
                    ,'request_user' => $request['login_id']
                    ,'request_term' => $request['login_approval_date']
                    ,'url'          => 'https://' . $this->_contractant_data['domain'] . DS . 'admin/logins/appoval' . DS . $data['request_key']
                ];
                if( $this->Mail->send_login_approval_request($email) ) 
                {
                    $this->Flash->success('承認メールを送信しました。しばらくお待ちください。');
                }
                else
                {
                    $this->Flash->error('承認メールを送信に失敗しました');
                }

                $this->redirect(['action' => 'requestApproval']);
            }
        }
        else
        {
        }
        $this->set( 'title_for_layout', 'ログイン承認期間リクエスト' );
        $this->set( compact( 'auth_users' ) );
    }

    // パスワード忘れ
    public function passwordRemind()
    {
        $title_for_layout = 'パスワードを忘れた場合';
        if( $this->request->is('post') && isset( $this->request->data['email'] ) )
        {
            $request = $this->request->data;
            $admin_user = $this->AdminUsers->getAdminUserByEmail( $request['email'], $this->_contractant_id );
            if( $admin_user )
            {
                $remind_key = substr( md5( uniqid() ). 0, 8 );
                $data = [
                    'id' => $admin_user->id
                    ,'remind_key' => $remind_key
                ];
                $res = $this->AdminUsers->saveData( $data );

                if( $res )
                {
                    $this->Flash->success('メール送信しました。受信したメールより再設定用の手続きを行ってください。');
                    $mail = [
                        'remind_key' => $remind_key
                        ,'email'     => $admin_user->user->email
                        ,'name'      => $admin_user->name
                    ];
                    $this->Mail->send_password_remind( $mail );

                }
                else
                {
                    $this->Flash->error('メールの送信に失敗しました。');
                }
            }
            else
            {
                $this->Flash->error('該当のユーザーは存在しません。');
                
            }
            $this->redirect(['action' => 'passwordRemind']);
        }

        $this->set( compact( 'title_for_layout' ) );
    }

    public function logout()
    {
        // クッキーの削除
        $this->response = $this->response->withExpiredCookie($this->_contractant_data['unique_parameter']); 
        $this->_session->delete('ContractantServiceMenus');
        return $this->redirect( $this->Auth->logout() );
    }
}
