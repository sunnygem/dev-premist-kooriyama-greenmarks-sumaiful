<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class ContentsController extends AdminAppController
{
    public $Contents;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->Contents = TableRegistry::get('Contents');
        $this->Files    = TableRegistry::get('Files');
        $this->RelContentFiles = TableRegistry::get('RelContentFiles');
    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'sort' => [
                    'modified' => 'DESC'
                ]
                ,'conditions' => [
                    'contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
            ]
        ];
        $data = $this->paginate( 'Contents' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $contents = [];
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           // todo 画像エラーチェック
           $error = $this->Contents->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $contents = $this->Contents->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               // 先に画像をアップ
               $this->File->saveFile( $request, 'image' );
               if( $request['files']['image']['error'] === 0 && $request['files']['image']['size'] > 0 ) 
               {
                   $request['uploaded']['image'] = ( isset( $request['uploaded']['image'] ) ) ? array_merge( $request['uploaded']['image'], $request['files']['image'] ) : $request['files']['image'];
               }

               $this->_session->write( 'Admin.contents', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.contents') )
            {
                $contents = $this->Contents->newEntity( $this->_session->read('Admin.contents') );
            }
            //else
            //{
            //}
        }
        else
        {
            if( $id !== null )
            {
                $contents = $this->Contents->getEditData( $id, $this->_session->read('ContractantData.id') );
                $contents->uploaded = [];
                // サムネイル
                foreach( $contents->files as $val  )
                {
                    if( $val->file_path !== null && $this->File->checkAllowImageMimeType( $val->mime_type ) )
                    {
                        $contents->uploaded['image'] = [
                            'id'         => $val->id
                            ,'file_path' => $val->file_path
                            ,'name'      => $val->name
                        ];
                    }
                }
                //debug( $contents );
            }
            else
            {
                $contents = $this->Contents->newEntity();
            }
        }
        $this->set( compact( 'contents' ) );
    }

    public function confirm()
    {
        $contents = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.contents' ) ) 
            {
                $data = $this->_session->read('Admin.contents');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            //  エラーチェック
            $error = $this->Contents->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');
                // filesキーを持っているとアソシエーションsaveが走るので別処理にするため削除。
                unset( $data['files'] );
                $content_id = $this->Contents->saveData( $data );

                // サムネの登録
                if( isset( $data['uploaded'] ) && count( $data['uploaded'] ) > 0 )
                {
                    // 既存は削除
                    $this->RelContentFiles->updateAll(
                        [
                            'deleted' => date('Y-m-d H:i:s')
                        ]
                        ,[
                            'contractant_id' => $this->_contractant_id
                            ,'content_id'    => $content_id
                        ]
                    );

                    foreach( $data['uploaded'] as $val )
                    {
                        if( isset( $val['file_path'] ) && $val['file_path'] !== null )
                        {
                            $file_data = [
                                'contractant_id' => $data['contractant_id']
                                ,'name'          => $val['name'] 
                                ,'file_path'     => $val['file_path']
                                ,'mime_type'     => mime_content_type( WWW_ROOT . DS . $val['file_path'] ) 
                            ];
                            if( isset( $val['id'] ) ) $file_data['id'] = $val['id'];
                            $file_id = $this->Files->saveData( $file_data );

                            // 中間テーブルの登録
                            if( isset( $file_data['id'] ) === false )
                            {
                                $this->RelContentFiles->saveData([
                                    'content_id'      => $content_id
                                    ,'file_id'        => $file_id
                                    ,'contractant_id' => $data['contractant_id']
                                ]);
                            }
                        }
                    }
                }

                if( $content_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.contents' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.contents') )
        {
            $contents = $this->Contents->newEntity( $this->_session->read('Admin.contents') );

        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'contents' ) );
    }

    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $this->Contents->deleteData( $id );
                $this->Flash->success('コンテンツを削除しました。');
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }


}
