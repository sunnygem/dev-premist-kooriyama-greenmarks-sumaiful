<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PostTypesController extends AdminAppController
{
    public $PostTypes;

    public function initialize()
    {
        parent::initialize();
        $this->PostCategories = TableRegistry::get('PostCategories');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $this->loadComponent( 'Common' );
    }

    public function category( $service_menu_id=null, $id=null )
    {
        $category = $this->PostCategories->getCategoryByServiceMenuId( $service_menu_id );

        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['complete'] ) )
        {
            $request = $this->request->data;

            $error = $this->PostCategories->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $data = $this->PostCategories->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                if( isset( $request['id'] ) === false ) $request['orderby']  = $this->PostCategories->getOrder( $service_menu_id );
                $post_category_id = $this->PostCategories->saveData( $request );

                if( $post_category_id )
                {
                    $this->Flash->success( 'カテゴリーを登録しました' );
                }
                else
                {
                    $this->Flash->error( 'カテゴリーの登録に失敗しました' );
                }
                $this->redirect([ $service_menu_id ]);
            }
        }
        else
        {
            if( $id !== null )
            {
                $data = $this->PostCategories->get( $id );
            }
            else
            {
                $data = $this->PostCategories->newEntity();
            }
        }

        $this->set( compact( 'data', 'category', 'service_menu_id' ) );
    }

    public function categoryDelete( $service_menu_id=null, $id=null )
    {
        if( $id !== null )
        {
            $this->PostCategories->deleteData( $id );
            $this->Flash->success( 'カテゴリーを削除しました。' );
        }
        else
        {
            $this->Flash->error( 'カテゴリーの削除に失敗しました。' );
        }
        return $this->redirect([ 'action' => 'category', $service_menu_id ]);

    }



}
