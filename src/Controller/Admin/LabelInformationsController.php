<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class LabelInformationsController extends AdminAppController
{
    public $LanbelInformations;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->LanbelInformations = TableRegistry::get('LabelInformations');
        $this->Files        = TableRegistry::get('Files');
        $this->RelLabelInformationFiles  = TableRegistry::get('RelLabelInformationFiles');
        $this->LabelInformationUserTypes = TableRegistry::get('LabelInformationUserTypes');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );

        // 暫定
        $mt_customer_type = $this->_mt_residence_type;
        $title_for_layout = 'インフォーメーション管理';
        $this->set( compact( 'mt_customer_type', 'title_for_layout' ) );
 
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'order' => [
                    'LabelInformations.modified' => 'DESC'
                ]
                ,'conditions' => [
                    'LabelInformations.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
            ]
        ];
        $this->paginate['contain'] = [ 'LabelInformationUserTypes' ];
        $data = $this->paginate( 'LanbelInformations' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $informations = [];
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           // todo 画像、pdfエラーチェック
           $error = $this->LanbelInformations->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $informations = $this->LanbelInformations->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               // 先に画像をアップ
               $this->File->saveFile( $request, 'image' );
               if( isset( $request['uploaded'] ) === false )
               {
                   $request['uploaded'] = [];
               }

               if( $request['files']['image']['error'] === 0 && $request['files']['image']['size'] > 0 ) 
               {
                   $request['uploaded']['image'] = ( isset( $request['uploaded']['image'] ) ) ? array_merge( $request['uploaded']['image'], $request['files']['image'] ) : $request['files']['image'];
               }

               // 先にPDFをアップ
               $this->File->saveFile( $request, 'pdf' );
               if( $request['files']['pdf']['error'] === 0 && $request['files']['pdf']['size'] > 0 ) 
               {
                   $request['uploaded']['pdf'] = ( isset( $request['uploaded']['pdf'] ) ) ? array_merge( $request['uploaded']['pdf'], $request['files']['pdf'] ) : $request['files']['pdf'];
               }

               $this->_session->write( 'Admin.informations', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.informations') )
            {
                $informations = $this->LanbelInformations->newEntity( $this->_session->read('Admin.informations') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $informations = $this->LanbelInformations->getEditData( $id );
                $informations->uploaded = [];
                // 既存ファイル
                foreach( $informations->files as $key => $val )
                {
                    // 画像
                    if( $val->file_path !== null && $this->File->checkAllowImageMimeType( $val->mime_type ) )
                    {
                        $informations->uploaded['image'] = [
                            'id'         => $val->id
                            ,'file_path' => $val->file_path
                            ,'name'      => $val->name
                        ];
                    }
                    // pdf
                    if( $val->mime_type === 'application/pdf' )
                    {
                        $informations->uploaded['pdf'] = [
                            'id'         => $val->id
                            ,'file_path' => $val->file_path
                            ,'name'      => $val->name
                        ];
                    }
                }
                $informations->user_types = $this->LabelInformationUserTypes->getEditDataByLabelInformationId( $this->_contractant_id, $id );
            }
            else
            {
                $informations = $this->LanbelInformations->newEntity();
            }
        //debug( $informations );
        }
        $this->set( compact( 'informations' ) );
    }

    public function confirm()
    {
        $informations = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.informations' ) ) 
            {
                $data = $this->_session->read('Admin.informations');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }


            // エラーチェック
            $error = $this->LanbelInformations->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');
                //$files = $data['files'];
                // filesキーを持っているとアソシエーションsaveが走るので別処理にするため削除。
                unset( $data['files'] );
                $information_id = $this->LanbelInformations->saveData( $data );

                // 公開範囲の保存
                $this->LabelInformationUserTypes->deleteAll(['label_information_id' => $information_id]);
                if( $data['user_types'] !== '' && is_array( $data['user_types'] ) )
                {
                    foreach( $data['user_types'] as $val )
                    {
                        $arr_user_type = [
                            'contractant_id'        => $this->_contractant_id
                            ,'label_information_id' => $information_id
                            ,'user_type'            => $val
                        ];

                        $this->LabelInformationUserTypes->saveData( $arr_user_type );
                    }
                    
                }

                // ファイルの登録
                if( isset( $data['uploaded'] ) && count( $data['uploaded'] ) > 0 )
                {
                    foreach( $data['uploaded'] as $val )
                    {
                        if( isset( $val['file_path'] ) && $val['file_path'] !== null )
                        {
                            $file_data = [
                               'contractant_id' => $data['contractant_id']
                               ,'name'          => $val['name'] 
                               ,'file_path'     => $val['file_path'] 
                               ,'mime_type'     => mime_content_type( WWW_ROOT . DS . $val['file_path'] ) 
                            ];
                            if( isset( $val['id'] ) ) $file_data['id'] = $val['id'];
                            $file_id = $this->Files->saveData( $file_data );

                            // 中間テーブルの登録
                            if( isset( $file_data['id'] ) === false )
                            {
                                $this->RelLabelInformationFiles->saveData([
                                    'label_information_id'  => $information_id
                                    ,'file_id'        => $file_id
                                    ,'contractant_id' => $data['contractant_id']
                                ]);
                            }
                        }
                    }
                }

                if( $information_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.informations' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.informations') )
        {
            $informations = $this->_session->read('Admin.informations');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'informations' ) );
    }

    // pdfのぷれびゅー
    public function getPdf()
    {
        if( $this->_session->check( 'Admin.informations' ) )
        {
            //todo layout変える
            $informations = $this->_session->read( 'Admin.informations' );
            $pdf_data = base64_encode( file_get_contents( $informations['uploaded']['pdf']['tmp_dir'] ) );
            $this->set( compact('pdf_data') );
        }
    }

    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $this->LabelInformations->deleteData( $id );
                $this->Flash->success('インフォーメーションを削除しました。');
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
    }
}
