<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class PointsProductsController extends AdminAppController
{
    public $PointsProducts;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->PointsProducts = TableRegistry::get('PointsProducts');

    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        
        
        if( $this->_unique_parameter !== 'willing' )
        {
            throw new NotFoundException();
        }
        else
        {
            $title_for_layout = 'ポイント交換商品管理';

            $this->loadComponent( 'Common' );
            $this->loadComponent( 'File' );

            $this->set( compact( 'title_for_layout' ) );
        }
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    //'PointsProducts.contractant_id' => $this->_session->read( 'contractantdata.id' )
                    'PointsProducts.contractant_id' => 4
                ]
                ,'order' => [ 
                    'PointsProducts.modified' => 'DESC'
                ]
            ]
        ];

        $data = $this->paginate( 'PointsProducts' );
        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null)
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            // エラーチェック
            $error = $this->PointsProducts->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $points_products = $this->PointsProducts->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                // アイコン画像のサーバー保存
                if( isset( $request['files']['icon_url_path'] ) && $request['files']['icon_url_path']['error'] === 0 && $request['files']['icon_url_path']['size'] )
                {
                    $this->File->saveFile( $request, 'icon_url_path' );
                }

                $this->_session->write( 'Admin.points_products', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.points_products') )
            {
                $points_products = $this->PointsProducts->newEntity( $this->_session->read('Admin.points_products') );
            }
        }
        else
        {
            
            if( $id !== null )
            {
                $points_products = $this->PointsProducts->getEditData( $id );
            }
            else
            {
                $points_products = $this->PointsProducts->newEntity();
            }
        }
        $this->set( compact( 'points_products' ) );

    }
    
    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.points_products' ) ) 
            {
                $data = $this->_session->read('Admin.points_products');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }
            // エラーチェック
            $error = $this->PointsProducts->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                if( isset( $data['contractant_id'] ) === false )
                {
                    $data['contractant_id'] = $this->_session->read('ContractantData.id');
                }

                // アイコン画像パスのDB保存
                if( isset( $data['files']['icon_url_path']['file_path'] ) )
                {
                    $data['icon_url_path'] = $data['files']['icon_url_path']['file_path'];
                    unset( $data['files'] );
                }

                $data['use_points'] = (int)$data['use_points'];
                $id = $this->PointsProducts->saveData( $data );

                if( $id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.points_products' );
                // システム画面は完了画面無し
                return $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.points_products') )
        {
            $points_products = $this->_session->read('Admin.points_products');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'points_products' ) );
    }
}
