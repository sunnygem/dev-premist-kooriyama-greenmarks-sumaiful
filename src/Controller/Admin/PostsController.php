<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PostsController extends AdminAppController
{
    public $Posts;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->Posts = TableRegistry::get('Posts');
        $this->Files        = TableRegistry::get('Files');

    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $title_for_layout = '投稿管理';

        $this->loadComponent( 'Common' );
        $this->set( compact( 'title_for_layout' ) );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'Posts.contractant_id'  => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [
                    'Posts.created' => 'DESC'
                ]
            ]
        ];

        // 物件
        if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
        {
            $this->paginate['finder']['search']['conditions']['Posts.building_id'] = $this->_admin_building_id;
        }

        $data = $this->paginate( 'Posts' );
        $this->set( compact( 'data' ) );
    }

    public function detial( $id=null )
    {

    }

    //public function delete( $id=null )
    //{
    //    if( $id !== null && in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) )
    //    {
    //        $this->autoRender = false;
    //        try
    //        {
    //            $this->Informations->deleteData( $id );
    //            $this->Flash->success('インフォーメーションを削除しました。');
    //        }
    //        catch ( Exception $e )
    //        {
    //            $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
    //        }
    //        return $this->redirect(['action' => 'index']);
    //    }
    //}
}
