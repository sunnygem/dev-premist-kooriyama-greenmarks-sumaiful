<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class IpLimitsController extends AdminAppController
{
    public $IpLimits;

    public function initialize()
    {
        parent::initialize();
        // system_adminユーザー以外はアクセス不可
        if( in_array( $this->_session->read( 'Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) === false )
        {
            throw new ForbiddenException('このページは閲覧できません');
        }

        $this->IpLimits = TableRegistry::get('IpLimits');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->_admin_building_id !== null )
        {
            throw new ForbiddenException();
        }

        $this->set( 'title_for_layout', 'IPアドレス管理' );
    }

    public function index()
    {
        if( $this->request->is(['post', 'put', 'patch']) && isset( $this->request->data['ip_limits'] ) )
        {
            $request = $this->request->data;
            $error = $this->IpLimits->validation( $request['ip_limits'] );
            if( count( $error ) > 0 )
            {
                $ip_limits = $request['ip_limits'];
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->IpLimits->truncateByContractantId( $this->_session->read('ContractantData.id') );
                foreach( $request['ip_limits'] as $val )
                {
                    if( $val['ip_address'] !== '' )
                    {
                        $val['contractant_id']   = $this->_session->read('ContractantData.id');
                        $val['insert_user_id']   = $this->_session->read('Admin.Auth.id');
                        $val['insert_user_name'] = $this->_session->read('Admin.Auth.admin_user.name');

                        $this->IpLimits->saveData( $val );
                    }
                }
                $this->Flash->success('登録が完了しました。');
                $this->redirect(['action' => 'index']);
            }
        }
        else
        {
            $ip_limits = $this->IpLimits->getIpAddressByContractantId( $this->_session->read( 'ContractantData.id' ) )->toArray();
        }
        $this->set( compact( 'ip_limits' ) );

    }

}
