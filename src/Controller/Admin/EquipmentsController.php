<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class EquipmentsController extends AdminAppController
{
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();

    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->_unique_parameter !== 'willing' )
        {
            throw new NotFoundException();
        }
        else
        {
            $title_for_layout = '備品管理';

            $this->loadComponent( 'Common' );
            $this->loadComponent( 'File' );

            $this->_mt_equipment_category =  Configure::read('mt_equipment_category');
            $mt_equipment_category = $this->_mt_equipment_category;

            $this->set( compact( 'title_for_layout', 'mt_equipment_category' ) );
        }
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'Equipments.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [
                    'Equipments.id' => 'DESC'
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'Buildings' => [
                'fields' => [ 'building_name' => 'Buildings.name' ]
                ,'conditions' => [
                    'Buildings.deleted IS' => null
                ]
            ]
        ];

        if( $this->request->getQuery('search') !== null )
        {
            $arr_search = $this->request->getQuery('search');
        //dump($this->request->getQuery('search'));
            foreach( $arr_search as $key => $val )
            {
                if( $val !== '' )
                {
                    switch( $key )
                    {
                    case 'name':
                        $this->paginate['finder']['search']['conditions']['Equipments.' . $key . ' LIKE'] = '%' . $val . '%';
                        break;
                    default:
                        $this->paginate['finder']['search']['conditions']['Equipments.' . $key] = $val;
                    }
                }
            }
        }

        $data = $this->paginate( 'Equipments' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            // エラーチェック
            $error = $this->Equipments->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $equipment = $this->Equipments->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                // アイコン画像のサーバー保存
                if( isset( $request['files']['icon_url_path'] ) && $request['files']['icon_url_path']['error'] === 0 && $request['files']['icon_url_path']['size'] )
                {
                    $this->File->saveFile( $request, 'icon_url_path' );
                }

                $this->_session->write( 'Admin.equipments', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.equipments') )
            {
                $equipment = $this->Equipments->newEntity( $this->_session->read('Admin.equipments') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $equipment = $this->Equipments->getEditData( $id );
            }
            else
            {
                $equipment = $this->Equipments->newEntity();
            }
        }
        $this->set( compact( 'equipment' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.equipments' ) ) 
            {
                $data = $this->_session->read('Admin.equipments');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->Equipments->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                if( isset( $data['contractant_id'] ) === false )
                {
                    $data['contractant_id'] = $this->_session->read('ContractantData.id');
                }

                // アイコン画像パスのDB保存
                if( isset( $data['files']['icon_url_path']['file_path'] ) )
                {
                    $data['icon_url_path'] = $data['files']['icon_url_path']['file_path'];
                    unset( $data['files'] );
                }

                $equipment_id = $this->Equipments->saveData( $data );

                if( $equipment_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.equipments' );
                return $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.equipments') )
        {
            $equipment = $this->_session->read('Admin.equipments');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'equipment' ) );
    }

    public function lend( $id )
    {
        $equipment = $this->Equipments->getEditData( $id );
        if( $equipment )
        {
            if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['regist'] ) )
            {
                $request = $this->request->data;

                // エラーチェック
                $equipment = $this->Equipments->patchEntity( $equipment, $request, [
                    'validate' => 'lending', 
                    'fieldList' => [ 'expected_return_date' ]
                ] );
                if( count( $equipment->getErrors() ) === 0 )
                {
                    // 貸出し中フラグ
                    $equipment->in_use_flg = 1;

                    $res = $this->Equipments->save( $equipment );
                    if( $res )
                    {
                        $this->Flash->success('返却予定日時を登録しました');
                        return $this->redirect(['action' => 'index']);
                    }
                    else
                    {
                        $this->Flash->error('返却予定日時の登録に失敗しました');
                    }
                }
            }

            $this->set( compact( 'equipment' ) );
        }
        else
        {
            throw new BadRequestException();
        }
    }

    public function returnOf( $id=null )
    {
        $this->autoRender = false;
        $equipment = $this->Equipments->get( $id );
        if( $equipment && $equipment->in_use_flg === 1 )
        {
            $equipment->in_use_flg           = 0;
            $equipment->expected_return_date = null;
            $res =$this->Equipments->save( $equipment );

            if( $res )
            {
                $this->Flash->success('返却処理が完了しました。');
            }
            else
            {
                $this->Flash->error('返却処理の途中でエラーが発生しました。もう一度やり直してください。');
            }
            return $this->redirect(['action' => 'index'] );
        }
        else
        {
            throw new BadRequestException();
        }
        //if( 
        //else
        //{
        //    throw new BadRequestException('権限がありません' );
        //}
    }

    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $equipment = $this->Equipments->get( $id );
                if( $equipment && $equipment->in_use_flg === 0 )
                {
                    $this->Equipments->deleteData( $id );
                    $this->Flash->success('備品情報を削除しました。');
                }
                else
                {
                    $this->Flash->error('使用中に備品はを削除できません。返却処理後に再度実行してください。');
                }
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }


}
