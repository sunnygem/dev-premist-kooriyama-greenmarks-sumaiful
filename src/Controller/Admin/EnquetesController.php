<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class EnquetesController extends AdminAppController
{
    public $ServiceMenus;
    public $ContractantServiceMenus;
    public $MenuEnquetes;
    public $MtFormTypes;

    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->ContractantServiceMenus = TableRegistry::get('ContractantServiceMenus');
        $this->MenuEnquetes      = TableRegistry::get('MenuEnquetes');
        $this->EnqueteAnswers    = TableRegistry::get('EnqueteAnswers');
        $this->MtFormTypes       = TableRegistry::get('MtFormTypes');
    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $title_for_layout = 'アンケート管理';

        $mt_form_types = $this->MtFormTypes->getList();

        $this->set( compact( 'title_for_layout', 'mt_form_types' ) );

        // todo サービスを利用できるかのチェック

        //if( $service_menu_id === null && preg_match( '/^[\d]+$/', $service_menu_id ) === 0 )
        //{
        //     $this->redirect([ 'controller' => 'ServiceMenus', 'action' => 'index' ]);
        //}

    }

    public function index( $service_menu_id=null )
    {
        // todo サービスを利用できるかのチェック

        //if( $service_menu_id === null && preg_match( '/^[\d]+$/', $service_menu_id ) === 0 )
        //{
        //     $this->redirect([ 'controller' => 'ServiceMenus', 'action' => 'index' ]);
        //}
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'MenuEnquetes.contractant_id'  => $this->_contractant_id
                ]
                ,'order' => [
                    'MenuEnquetes.id' => 'DESC'
                ]
            ]
        ];

        $enquetes = $this->paginate( 'MenuEnquetes' );
        $this->set( compact( 'service_menu_id', 'enquetes' ) );
    }

    public function edit( $id=null )
    {
        // 登録
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            $error = $this->MenuEnquetes->validation( $request );
            // todo 設問のバリデーション
            if( count( $error ) > 0 )
            {
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write('Admin.menu_enquetes', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.menu_enquetes') )
            {
                $menu_enquete = $this->MenuEnquetes->newEntity( $this->_session->read('Admin.menu_enquetes') );
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }
        }
        else
        {
            if( $id !== null )
            {
                $menu_enquete = $this->MenuEnquetes->getDetailById( $id );
            }
            else
            {
                $menu_enquete = $this->MenuEnquetes->newEntity(['enquete_questions' => []]);
            }
        }

        $this->set( compact( 'menu_enquete' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'Admin.menu_enquetes' ) ) 
           {
               $data = $this->_session->read('Admin.menu_enquetes');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->ContractantServiceMenus->validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               if( isset( $data['id'] ) === false )
               {
                   $data['contractant_id'] = $this->_contractant_id;
                   // todo 廃止
                   $data['contractant_service_menu_id'] = 0;

               }

               // todo 削除の処理
               foreach( $data['enquete_questions'] as $key => $val )
               {
                   if( isset( $val['id'] ) === false )
                   {
                       $data['enquete_questions'][$key]['contractant_id'] = $this->_contractant_id;
                       // todo 廃止
                       $data['enquete_questions'][$key]['contractant_service_menu_id'] = 0;
                   }
                   $data['enquete_questions'][$key]['orderby'] = $key + 1;
               }

               // アソシエーションごと登録
               $res = $this->MenuEnquetes->saveData( $data );

               if( $res )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('Admin.menu_enquetes' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index' ]);
           }
        }
        else if( $this->_session->check('Admin.menu_enquetes') )
        {
            $menu_enquete = $this->_session->read('Admin.menu_enquetes');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'menu_enquete' ) );
    }

    public function editEnquete( $service_menu_id=null, $id=null )
    {
        $service_menu = $this->ContractantServiceMenus->get( $service_menu_id );
        if( $this->request->is(['post', 'put', 'patch']) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;
            $error = $this->MenuEnquetes->validation( $request );
            if( count( $error ) > 0 )
            {
                $menu_enquete = $this->MenuEnquetes->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write('Admin.menu_enquetes', $request );
                return $this->redirect(['action' => 'confirmEnquete', $service_menu_id]);
            }
        }
        elseif( $this->request->is(['post']) && isset( $this->request->data['back'] ) )
        {
            $menu_enquete = $this->MenuEnquetes->newEntity( $this->_session->read('Admin.menu_enquetes') );
        }
        // 直打ち対策
        elseif( $service_menu->service_menu_id === 9 && $service_menu->contractant_id === $this->_session->read('ContractantData.id') )
        {
            // 更新
            if( $id !== null )
            {
                $menu_enquete = $this->MenuEnquetes->get( $id );
            }
            // 新規
            else
            {
                $menu_enquete = $this->MenuEnquetes->newEntity();
                $menu_enquete->contractant_service_menu_id = $service_menu_id;
            }
        }
        $this->set( compact( 'menu_enquete' ) );
    }

    public function confirmEnquete( $service_menu_id=null )
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            $data = $this->_session->read('Admin.menu_enquetes');
            // エラーチェック
            $error = $this->MenuEnquetes->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'editEnquete', $service_menu_id ]);
            }
            else
            {
                // 保存
               $menu_enquete_id = $this->MenuEnquetes->saveData( $data );

               if( $menu_enquete_id )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('Admin.menu_enquetes' );
               // 完了画面無し
               $this->redirect(['action' => 'edit', $data['contractant_service_menu_id'] ]);

            }
        }
        else if( $this->_session->check('Admin.menu_enquetes') )
        {
            $menu_enquete = $this->_session->read('Admin.menu_enquetes');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'menu_enquete', 'service_menu_id' ) );
    }

    public function delete( $id=null )
    {
        if( $id !== null )
        {
            $entitiy = $this->MenuEnquetes->get( $id );
            if( $entitiy->answer_count > 0 )
            {
                $this->Flash->error( '既に回答データがあるため削除はできません。' );
            }
            else
            {
                $this->MenuEnquetes->deleteData( $id );
                $this->Flash->success( 'アンケート削除しました。' );
            }
        }
        else
        {
            $this->Flash->error( 'アンケートの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'index']);


    }

    public function answer( $enquete_id=null )
    {
        $enquete = $this->MenuEnquetes->getDetailById( $enquete_id );

        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'EnqueteAnswers.contractant_id' => $this->_contractant_id
                    ,'EnqueteAnswers.enquete_id'    => $enquete_id
                ]
                ,'order' => [
                    'EnqueteAnswers.created' => 'DESC'
                ]
            ]
        ];
        $this->paginate['contain'] = [ 'EnqueteAnswerItems' ];
        $answers = $this->paginate( 'EnqueteAnswers' );
        //$data = $this->getDetailById( $enquete_id );

        $this->set( compact( 'enquete', 'answers' ) );

    }
}
