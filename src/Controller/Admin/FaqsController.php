<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException; 
use Cake\View\Exception\MissingTemplateException;

class FaqsController extends AdminAppController
{ 
    public $paginate = [ 'finder' => 'search' ];
 
    public function initialize()
    {
        parent::initialize();
        
        $this->FaqsQuestions = TableRegistry::getTableLocator()->get('FaqsQuestions');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        
        if( $this->_unique_parameter !== 'willing' )
        {
            throw new NotFoundException();
        }
        else
        {
            $title_for_layout = 'Q&A管理';

            $this->loadComponent( 'Common' );
            $this->loadComponent( 'File' );
            
            $this->_mt_faq_category =  Configure::read('mt_faq_category');
            $mt_faq_category = $this->_mt_faq_category;

            $this->set( compact( 'title_for_layout' , 'mt_faq_category') );
        }
    }

    public function index()
    { 
        $mt_faq_category = $this->_mt_faq_category;

        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'Faqs.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [
                    'Faqs.id'    => 'ASC'
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'FaqsQuestions' => [
                'fields' => [
                    'faq_category_id' => 'FaqsQuestions.faq_category_id',
                    'question'        => 'FaqsQuestions.question',
                    'question_en'     => 'FaqsQuestions.question_en',
                ]
            ]
        ];
        
        $data = $this->paginate( 'Faqs' );
        $this->set( compact( 'data' , 'mt_faq_category') );
    }

    public function edit( $id=null )
    {
        $data = [];
        $mt_faq_category = $this->_mt_faq_category;
        $category  = $this->FaqsQuestions->getFaqsQuestionsData($this->_session->read( 'ContractantData.id'));
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            // エラーチェック
            $error = $this->Faqs->validation( $request ); 
            if( count( $error ) > 0 )
            {
                $equipment = $this->Faqs->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                // アイコン画像のサーバー保存
                if( isset( $request['files']['icon_url_path'] ) && $request['files']['icon_url_path']['error'] === 0 && $request['files']['icon_url_path']['size'] )
                {
                    $this->File->saveFile( $request, 'icon_url_path' );
                }

                $this->_session->write( 'Admin.faqs', $request );
                return $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.faqs') )
            {
                $faq = $this->Faqs->newEntity( $this->_session->read('Admin.faqs') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $faq = $this->Faqs->getEditData( $id );
            }
            else
            {
                $faq = $this->Faqs->newEntity();
            }
        }
        $this->set( compact( 'faq', 'mt_faq_category', 'category' ) );
    }

    public function confirm()
    {
        $data = [];
        $faq = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {

            if( $this->_session->check( 'Admin.faqs' ) ) 
            {
                $data = $this->_session->read('Admin.faqs');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }
            // エラーチェック
            $error = $this->Faqs->validation( $data ); 
            if( count( $error ) > 0 )
            {
                $data = $this->Faqs->newEntity( $request );
                return $this->redirect(['action' => 'edit']);
           }
            else
            {
                // 登録
                if( isset( $data['contractant_id'] ) === false )
                {
                    $data['contractant_id'] = $this->_session->read('ContractantData.id');
                }

                // アイコン画像パスのDB保存
                if( isset( $data['files']['icon_url_path']['file_path'] ) )
                {
                    $data['icon_url_path'] = $data['files']['icon_url_path']['file_path'];
                    unset( $data['files'] );
                }

                $faq_id = $this->Faqs->saveData( $data );

                if( $faq_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.faqs' );
                // システム画面は完了画面無し
                return $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.faqs') )
        {
            $faq = $this->_session->read('Admin.faqs');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'faq' ) );
    }
    
    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $this->Faqs->deleteData( $id );
                $this->Flash->success('インフォーメーションを削除しました。');
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }

}
