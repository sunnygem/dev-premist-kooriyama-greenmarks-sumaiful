<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PrivacyPoliciesController extends AdminAppController
{
    public $PrivacyPolicies;

    public function initialize()
    {
        parent::initialize();
        $this->PrivacyPolicies = TableRegistry::get('PrivacyPolicies');

    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->_admin_building_id !== null )
        {
            throw new ForbiddenException();
        }

        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );
        $this->set( 'title_for_layout', 'プライバシーポリシー' );
    }

    public function index()
    {
        $data = $this->PrivacyPolicies->getDataByContractantId( $this->_session->read( 'ContractantData.id' ) );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) )
        {
            if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
            {
               $request = $this->request->data;

               // エラーチェック
               $error = $this->PrivacyPolicies->validation( $request ); 
               if( count( $error ) > 0 )
               {
                   $data = $this->PrivacyPolicies->newEntity( $request );
                   $this->set( compact( 'error' ) );
               }
               else
               {
                   $this->_session->write( 'Admin.privacy_policy', $request );
                   $this->redirect(['action' => 'confirm']);
               }
            }
            // 戻る
            elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
            {
                if( $this->_session->check('Admin.informations') )
                {
                    $informations = $this->PrivacyPolicies->newEntity( $this->_session->read('Admin.privacy_policy') );
                }
            }
            else
            {
                $data = $this->PrivacyPolicies->getDataByContractantId( $this->_session->read( 'ContractantData.id' ) );
            }
            $this->set( compact( 'data' ) );
        }
        else
        {
            throw new BadRequestException('権限がありません');
        }
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.privacy_policy' ) ) 
            {
                $data = $this->_session->read('Admin.privacy_policy');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->PrivacyPolicies->validation( $data ); 
            if( count( $error ) > 0 )
            {
                $data = $this->PrivacyPolicies->newEntity( $request );
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');

                $privacy_policy_id = $this->PrivacyPolicies->saveData( $data );

                if( $privacy_policy_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.privacy_policy' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.privacy_policy') )
        {
            $data= $this->_session->read('Admin.privacy_policy');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'data' ) );
    }

}
