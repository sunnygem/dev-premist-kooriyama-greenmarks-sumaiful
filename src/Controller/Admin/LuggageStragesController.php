<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class LuggageStragesController extends AdminAppController
{
    public $Users;
    public $LuggageStrages;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->LuggageStrages = TableRegistry::get('LuggageStrages');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $this->loadComponent( 'Common' );

        if( $this->request->action !== 'index' )
        {
            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
            $user_options = $this->Users->getFrontUserOptions([
                'Users.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ,'FrontUsers.common_user_flg' => 0
            ]);

            $this->set( compact( 'user_options' ) );
        }

        $title_for_layout = '荷物預かり管理';
        $this->set( 'title_for_layout', $title_for_layout );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'LuggageStrages.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
            ]
        ];
        $data = $this->paginate( 'LuggageStrages' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           $error = $this->LuggageStrages->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $data = $this->LuggageStrages->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               $this->_session->write( 'Admin.data', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.data') )
            {
                $data = $this->LuggageStrages->newEntity( $this->_session->read('Admin.data') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $data = $this->LuggageStrages->get( $id );
            }
            else
            {
                $data = $this->LuggageStrages->newEntity();
            }
        }
        $this->set( compact( 'data' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.data' ) ) 
            {
                $data = $this->_session->read('Admin.data');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->LuggageStrages->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');
                //$files = $data['files'];
                // filesキーを持っているとアソシエーションsaveが走るので別処理にするため削除。
                $res = $this->LuggageStrages->saveData( $data );

        
                if( $res )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.data' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.data') )
        {
            $data = $this->_session->read('Admin.data');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'data' ) );
    }

}
