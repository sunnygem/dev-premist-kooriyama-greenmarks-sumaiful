<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class InformationsController extends AdminAppController
{
    public $Informations;
    public $paginate = [ 'finder' => 'search' ];

    public $_mt_information_type;

    public function initialize()
    {
        parent::initialize();
        $this->Informations = TableRegistry::getTableLocator()->get('Informations');
        $this->Files        = TableRegistry::getTableLocator()->get('Files');
        $this->RelInformationFiles = TableRegistry::get('RelInformationFiles');

        $this->InformationBuildings = TableRegistry::getTableLocator()->get('InformationBuildings');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $title_for_layout = 'インフォメーション管理';

        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );

        $this->_mt_information_type = Configure::read('mt_information_type');
        $mt_information_type = $this->_mt_information_type;

        $this->set( compact( 'title_for_layout', 'mt_information_type' ) );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'Informations.contractant_id'         => $this->_session->read( 'ContractantData.id' )
                    //,'RelInformationFiles.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => [
                    'Informations.modified' => 'DESC'
                ]
            ]
        ];

        // 物件
        if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
        {
            $this->paginate['join'] = [
                'table'  => 'information_buildings'
                ,'alias' => 'InformationBuildings'
                ,'type'  => 'LEFT'
                ,'conditions' => [
                    'InformationBuildings.information_id = Informations.id'
                ]
            ];
            // 全公開もしくはユーザーのbuilding_idに合致するもの
            $this->paginate['finder']['search']['conditions']['Informations.all_building_flg']    = 0;
            $this->paginate['finder']['search']['conditions']['InformationBuildings.building_id'] = $this->_admin_building_id;
            $this->paginate['group'] = 'Informations.id';
        }

        $this->paginate['contain'] = [ 'InformationBuildings', 'InformationBuildings.Buildings' ];
        $data = $this->paginate( 'Informations' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           $error = $this->Informations->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $informations = $this->Informations->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               // 先に画像をアップ
               $this->File->saveFile( $request, 'image' );
               if( isset( $request['uploaded'] ) === false )
               {
                   $request['uploaded'] = [];
               }

               if( $request['files']['image']['error'] === 0 && $request['files']['image']['size'] > 0 ) 
               {
                   $request['uploaded']['image'] = ( isset( $request['uploaded']['image'] ) ) ? array_merge( $request['uploaded']['image'], $request['files']['image'] ) : $request['files']['image'];
               }

               $this->_session->write( 'Admin.informations', $request );
               return $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.informations') )
            {
                $informations = $this->Informations->newEntity( $this->_session->read('Admin.informations') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $informations = $this->Informations->getEditData( $id );
                $informations->uploaded = [];
                // 既存ファイル
                if( $informations->rel_information_file )
                {
                    $informations->uploaded['image'] = $informations->rel_information_file->file;
                }
                $informations->buildings = $this->InformationBuildings->getEditDataByInformationId( $this->_contractant_id, $id );
            }
            else
            {
                $informations = $this->Informations->newEntity();
            }
        }
        $this->set( compact( 'informations' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.informations' ) ) 
            {
                $data = $this->_session->read('Admin.informations');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->Informations->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');
                //$files = $data['files'];
                // filesキーを持っているとアソシエーションsaveが走るので別処理にするため削除。
                unset( $data['files'] );
                //dump($data);exit;
                $information_id = $this->Informations->saveData( $data );

                // 公開範囲の保存
                $this->InformationBuildings->deleteAll(['information_id' => $information_id]);
                if( $data['buildings'] !== '' && is_array( $data['buildings'] ) )
                {
                    foreach( $data['buildings'] as $val )
                    {
                        $arr_buildings = [
                            'contractant_id'  => $this->_contractant_id
                            ,'information_id' => $information_id
                            ,'building_id'    => $val
                        ];

                        $this->InformationBuildings->saveData( $arr_buildings );
                    }
                }

                // ファイルの登録
                if( isset( $data['uploaded'] ) && count( $data['uploaded'] ) > 0 )
                {
                    foreach( $data['uploaded'] as $val )
                    {
                        if( isset( $val['file_path'] ) && $val['file_path'] !== null )
                        {
                            $file_data = [
                               'contractant_id' => $data['contractant_id']
                               ,'name'          => $val['name'] 
                               ,'file_path'     => $val['file_path'] 
                               ,'mime_type'     => mime_content_type( WWW_ROOT . DS . $val['file_path'] ) 
                            ];
                            if( isset( $val['id'] ) ) $file_data['id'] = $val['id'];
                            $file_id = $this->Files->saveData( $file_data );

                            // 中間テーブルの登録
                            if( isset( $file_data['id'] ) === false )
                            {
                                $this->RelInformationFiles->saveData([
                                    'information_id'  => $information_id
                                    ,'file_id'        => $file_id
                                    ,'contractant_id' => $data['contractant_id']
                                ]);
                            }
                        }
                    }
                }

                if( $information_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.informations' );
                // システム画面は完了画面無し
                return $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.informations') )
        {
            $informations = $this->_session->read('Admin.informations');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'informations' ) );
    }

    // イベント参加者
    public function participant( $information_id=null )
    {
        $title_for_layout = '参加者一覧';

        $data = $this->Informations->getDataWithJoinUser( $information_id, $this->_contractant_id );
        //debug( $data );
        if( $data )
        {
            $this->set( compact( 'data', 'title_for_layout' ) );
        }
        else
        {
            throw new NotFoundException();
        }
        
    }

    public function delete( $id=null )
    {
        if( $id !== null && ( $this->Auth->user('admin_user.authority') === SYSTEM_ADMIN || $this->Auth->user( 'admin_user.authority' ) === ADMIN ) )
        {
            $this->autoRender = false;
            try
            {
                $this->Informations->deleteData( $id );
                $this->Flash->success('インフォーメーションを削除しました。');
            }
            catch ( Exception $e )
            {
                $this->Flash->error( "削除に失敗しました。\n". $e->getMessage() );
            }
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            throw new BadRequestException('権限がありません' );
        }
    }
}
