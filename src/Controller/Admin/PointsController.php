<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class PointsController extends AdminAppController
{
    public $FrontUsers;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->FrontUsers = TableRegistry::getTableLocator()->get('FrontUsers');

        $this->Buildings = TableRegistry::getTableLocator()->get('Buildings');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $title_for_layout = 'ポイント管理';

        $this->loadComponent( 'Common' );

        $this->set( compact( 'title_for_layout' ) );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'FrontUsers.contractant_id'    => $this->_session->read( 'ContractantData.id' )
                     ,'FrontUsers.building_id IS NOT' => null
                     ,'FrontUsers.auth_flg'   => 1
                     ,'FrontUsers.common_user_flg ' => 0
                     ,'Users.password IS NOT' => null
                     ,'Buildings.display_flg' => 1
                    //,'RelInformationFiles.contractant_id' => $this->_session->read( 'ContractantData.id' )
                ]
                ,'order' => []
            ]
        ];
        $this->paginate['contain'] = [ 'Users', 'EncryptionParameters', 'Buildings' ];

        if ( $this->request->getQuery('search') )
        {
             $arr_search = $this->request->getQuery('search');
             foreach( $arr_search as $key => $val )
             {
                 if( $val !== '' )
                 {
                     switch( $key )
                     {
                     case 'building_id':
                     case 'room_number':
                         $this->paginate['finder']['search']['conditions']['FrontUsers.'.$key] = $val;

                         break;
                     }
                 }
             }
        }
        $data = $this->paginate( 'FrontUsers' );

        $this->set( compact( 'data' ) );
    }

    public function apply( $id=null )
    {
        $data = $this->FrontUsers->getDataById( $id, $this->_contractant_id );
        if( $data === false )
        {
            throw new BadRequestException();
        }
        else if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           $data = $this->FrontUsers->patchEntity( $data, $request, ['validate' => 'apply'] );
           if( (int)$data->apply_point > $data->point )
           {
               $data->setError('apply_point', ['所持ポイントを超えた値が入力されています']);
           }
           else
           {
               $data->point = $data->point - $data->apply_point;
           }

           if( count( $data->getErrors() ) === 0 )
           {
               $this->_session->write( 'Admin.front_user', $data );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('Admin.data') )
            {
                $data = $this->data->newEntity( $this->_session->read('Admin.front_user') );
            }
        }

        $this->set( compact( 'data' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) && $this->_session->check( 'Admin.front_user' ) )
        {
            $front_user = $this->_session->read('Admin.front_user');
            $res = $this->FrontUsers->save( $front_user );
            if( $res )
            {
                $this->Flash->success( '消込が完了しました' );
            }
            else
            {
                $this->Flash->error( '登録に失敗しました。' );
            }

            $this->_session->delete('Admin.front_user' );
            return $this->redirect(['action' => 'index']);
        }
        else if( $this->_session->check('Admin.front_user') )
        {
            $front_user = $this->_session->read('Admin.front_user');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'front_user' ) );
    }

}
