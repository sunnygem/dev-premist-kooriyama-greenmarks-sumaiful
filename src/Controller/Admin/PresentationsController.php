<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PresentationsController extends AdminAppController
{
    public $Presentations;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->Presentations = TableRegistry::getTableLocator()->get('Presentations');
        $this->Files         = TableRegistry::getTableLocator()->get('Files');
        $this->Posts         = TableRegistry::getTableLocator()->get('Posts');
    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $title_for_layout = 'ユーザー報告';
        $this->loadComponent( 'File' );

        $this->set( compact( 'title_for_layout' ) );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'order' => [
                    'Presentations.checked' => 'ASC'
                    ,'Presentations.created' => 'DESC'
                ]
                ,'conditions' => [
                    'Presentations.contractant_id' => $this->_session->read( 'ContractantData.id' )
                    //,'FrontUsers.id IS NOT'        => null
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'Posts.RelPostFiles.Files'
            ,'Posts.Users.FrontUsers'
            ,'Files'
            ,'PresentationBuildings.Buildings'
        ];

        // 物件
        if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
        {
            $this->paginate['join'] = [
                'table'  => 'presentation_buildings'
                ,'alias' => 'PresentationBuildings'
                ,'type'  => 'LEFT'
                ,'conditions' => [
                    'PresentationBuildings.presentation_id = Presentations.id'
                ]
            ];
            // 全公開もしくはユーザーのbuilding_idに合致するもの
            //$this->paginate['finder']['search']['conditions']['Presentations.all_building_flg']    = 0;
            $this->paginate['finder']['search']['conditions']['PresentationBuildings.building_id'] = $this->_admin_building_id;
            $this->paginate['group'] = 'Presentations.id';
        }

        $data = $this->paginate( 'Presentations' );

        $this->set( compact( 'data' ) );
    }

    public function detail( $id=null )
    {
        $data = $this->Presentations->findById( $id );
        $data->contain([
            'Posts.RelPostFiles.Files'
            ,'Posts.Users.FrontUsers'
            ,'Files'
            ,'PresentationBuildings.Buildings'
            ,'Users.FrontUsers.EncryptionParameters'
        ]);
        $data = $data->first();
        $this->set(compact('data'));
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.contents' ) ) 
            {
                $data = $this->_session->read('Admin.contents');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            //  エラーチェック
            $error = $this->Contents->validation( $data ); 
            if( count( $error ) > 0 )
            {
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');
                // filesキーを持っているとアソシエーションsaveが走るので別処理にするため削除。
                unset( $data['files'] );
                $content_id = $this->Contents->saveData( $data );

                // サムネの登録
                if( isset( $data['uploaded'] ) && count( $data['uploaded'] ) > 0 )
                {
                    foreach( $data['uploaded'] as $val )
                    {
                        if( isset( $val['file_path'] ) && $val['file_path'] !== null )
                        {
                            $file_data = [
                                'contractant_id' => $data['contractant_id']
                                ,'name'          => $val['name'] 
                                ,'file_path'     => $val['file_path']
                                ,'mime_type'     => mime_content_type( WWW_ROOT . DS . $val['file_path'] ) 
                            ];
                            if( isset( $val['id'] ) ) $file_data['id'] = $val['id'];
                            $file_id = $this->Files->saveData( $file_data );

                            // 中間テーブルの登録
                            if( isset( $file_data['id'] ) === false )
                            {
                                $this->RelContentFiles->saveData([
                                    'content_id'      => $content_id
                                    ,'file_id'        => $file_id
                                    ,'contractant_id' => $data['contractant_id']
                                ]);
                            }
                        }
                    }
                }

                if( $content_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.contents' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.contents') )
        {
            $contents = $this->Contents->newEntity( $this->_session->read('Admin.contents') );

        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'contents' ) );
    }

    public function check( $id, $hidden_flg=0 )
    {
        $this->autoRender = false;
        $presentation = $this->Presentations->getDataById( $id );
        $data = [
            'id'       => $presentation->id
            ,'checked' => date('Y-m-d H:i:s')
        ];
        $this->Presentations->saveData( $data );

        // 非表示にする
        if( $hidden_flg === '1' )
        {
            if( $presentation->file !== null )
            {
                $model = 'Files';
                $data = [
                    'id' => $presentation->file->id
                    ,'display_flg' => 0
                ];
            }
            elseif( $presentation->post !== null )
            {
                $model = 'Posts';
                $data = [
                    'id' => $presentation->post->id
                    ,'display_flg' => 0
                ];
            }
            $this->{$model}->saveData( $data );
            $this->Flash->success('非表示にしました。');
        }
        else if( $hidden_flg === '2' )
        {
            if( $presentation->file !== null )
            {
                $model = 'Files';
                $data = [
                    'id' => $presentation->file->id
                    ,'display_flg' => 1
                ];
            }
            elseif( $presentation->post !== null )
            {
                $model = 'Posts';
                $data = [
                    'id' => $presentation->post->id
                    ,'display_flg' => 1
                ];
            }
            $this->{$model}->saveData( $data );
            $this->Flash->success('表示にしました。');
        }
        else
        {
            $this->Flash->success('完了しました。');
        }

        $this->redirect(['action' => 'index']);
    }


}
