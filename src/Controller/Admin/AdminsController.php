<?php

namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class AdminsController extends AdminAppController
{
    public $Users;
    public $PasswordSecurities;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        // system_adminユーザー以外はアクセス不可
        if( in_array( $this->_session->read( 'Admin.Auth.admin_user.authority' ), [ SYSTEM_ADMIN, ADMIN ], true ) === false )
        {
            throw new ForbiddenException('このページは閲覧できません');
        }

        $this->Users      = TableRegistry::getTableLocator()->get('Users');
        $this->AdminUsers = TableRegistry::getTableLocator()->get('AdminUsers');
        $this->PasswordSecurities = TableRegistry::getTableLocator()->get('PasswordSecurities');

        $this->loadComponent( 'Mail' );
    }

    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $title_for_layout = '管理ユーザー管理';

        // 暫定
        $mt_authority = [ ADMIN => '管理者', GENERAL => '一般' ];

        // 物件に"全て"をキー0で追加する
        $mt_buildings  = [ 0 => '全て'] + $this->_mt_buildings;
        $this->set( compact( 'mt_authority', 'mt_buildings', 'title_for_layout' ) );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'order' => [
                    'AdminUsers.modified' => 'DESC'
                ]
                ,'conditions' => [
                    'AdminUsers.contractant_id' => $this->_session->read( 'ContractantData.id' )
                    ,'AdminUsers.authority !='  => SYSTEM_ADMIN
                    ,'Users.contractant_id'     => $this->_session->read( 'ContractantData.id' )
                    //,'Users.type'          => 'admin'
                ]
            ]
        ];
        $this->paginate['contain'] = [ 'Users' ];

        // 物件
        if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
        {
            $this->paginate['finder']['search']['conditions']['AdminUsers.building_id'] = $this->_admin_building_id;
        }

        $data = $this->paginate( 'AdminUsers' );

        //if( $this->request->is( 'post' ) )
        //{
        //    if( false )
        //    {
        //        $this->Flash->error('ログインIDかパスワードが不正です');
        //    }
        //    // ログイン
        //    else
        //    {
        //        $this->redirect([ 'controller' => 'Tops', 'action' => 'index' ]);
        //    }
        //}

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $users = [];
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
            $request = $this->request->data;

            //  エラーチェック
            $error = $this->Users->admin_validation( $request ); 
            $error = array_merge( $error, $this->AdminUsers->validation( $request['admin_user'] ) ); 
            if( count( $error ) > 0 )
            {
                //$users = $request;
                $users = $this->Users->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $this->_session->write( 'users', $request );
                $this->redirect(['action' => 'confirm']);
            }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('users') )
            {
                $users = $this->Users->newEntity( $this->_session->read('users') );
                $users->password = null;
                $users->password_confirm = null;

            }
            //else
            //{
            //}
        }
        else
        {
            if( $id !== null )
            {
                $users = $this->Users->getAdminUser( $id );
                $users->password = null;

            }
            else
            {
                $users = $this->Users->newEntity();
                $users->admin_user = $this->AdminUsers->newEntity();
            }
        }

        $this->set( compact( 'users' ) );
    }

    public function confirm()
    {
        $users = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'users' ) ) 
           {
               $data = $this->_session->read('users');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->Users->admin_validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {

               // 編集
               if( isset( $data['id'] ) )
               {
               }
               // 新規
               else
               {
                   // フロントユーザーと同じemailアドレスがポストされた場合、そのuserにadmin_dataを振ら下げる
                   $front_exists = $this->Users->checkExistFrontAndNoAdminByEmail( $data['email'], $this->_contractant_id ); 
                   if( $front_exists )
                   {
                       $data['id'] = $front_exists['id'];
                   }
                   else
                   {
                       $data['contractant_id'] = $this->_session->read( 'ContractantData.id' );
                       $data['type']           = 'admin';

                       if ( $data['login_id'] === null || $data['login_id'] === '' )
                       {
                           $data['login_id'] = $data['email'];
                       }
                   }

                   $data['admin_user']['contractant_id'] = $this->_session->read( 'ContractantData.id' );
                   $remind_key = substr( md5( uniqid() ). 0, 8 );
                   $data['admin_user']['remind_key'] = $remind_key;
               }

               // 有効期限が設定されている
               if( $this->_security['valid_term'] !== null )
               {
                   $data['expire_date'] = date( 'Y-m-d H:i:s', time() + ( $this->_security['valid_term'] * 86400 ) );
               }

               // 物件が限定されている場合は自動的に付与
               if( $this->_admin_building_id !== null && preg_match( '/^[\d]+$/', $this->_admin_building_id ) )
               {
                   $data['admin_user']['building_id'] = $this->_admin_building_id;
               }

               $res = $this->Users->saveData( $data );

               if( $res )
               {
                   // メールの送信
                   if( isset( $data['id'] ) && isset( $front_exists ) === false )
                   {
                       $msg =  '編集が完了しました。';
                   }
                   else
                   {
                       $this->Mail->send_admin_create( $data );
                       $msg = '登録が完了しました。';
                   }
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('users' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('users') )
        {
            $users = $this->_session->read('users');
            if( isset( $users['password'] ) )
            {
                $users['password'] = str_repeat( '●', strlen( $users['password'] ) );
            }
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }

        $this->set( compact( 'users' ) );
    }

    public function delete( $id )
    {
        if( $id !== null )
        {
            // admin_usersのみ削除する
            $this->AdminUsers->deleteDataByUserId( $id );
            $this->Flash->success( 'ユーザーを削除しました。' );
        }
        else
        {
            $this->Flash->error( 'ユーザーの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'index']);

    }

    public function security()
    {
        $this->set( 'title_for_layout', 'セキュリティ設定' );
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['complete'] ) )
        {
            $request = $this->request->data;
            $error = $this->PasswordSecurities->validation( $request );
            if( count( $error ) > 0 )
            {
                $data = $this->PasswordSecurities->newEntity( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                $request['contractant_id']   = $this->_session->read( 'ContractantData.id' );
                $request['insert_user_id']   = $this->_session->read( 'Admin.Auth.id' );
                $request['insert_user_name'] = $this->_session->read( 'Admin.Auth.admin_user.name' );
                $res = $this->PasswordSecurities->saveData( $request );
                if( $res )
                {
                    $this->Flash->success( 'パスワードセキュリティを設定しました' );
                }
                else
                {
                    $this->Flash->error( 'パスワードセキュリティの設定に失敗しました' );
                }
                return $this->redirect(['action' => 'security']);
            }
        }
        else
        {
            $data = $this->PasswordSecurities->getFirstData( $this->_session->read( 'ContractantData.id' ) );
        }

        $this->set( compact( 'data' ) );
    }
}
