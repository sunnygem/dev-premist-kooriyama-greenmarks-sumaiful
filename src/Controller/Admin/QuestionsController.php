<?php
namespace App\Controller\Admin;
use App\Controller\AdminAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException; 
use Cake\View\Exception\MissingTemplateException;

class QuestionsController extends AdminAppController
{
    public $Questions;

    public function initialize()
    {
        parent::initialize();
        $this->Questions = TableRegistry::get('questions');

        $title_for_layout = 'Q&A管理';
        $this->set( compact( 'title_for_layout' ) );
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->_admin_building_id !== null )
        {
            throw new ForbiddenException();
        }

        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );
    }

    public function index()
    {
        $data = $this->Questions->getData( $this->_session->read( 'ContractantData.id' ) );
        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        $data = [];
        if( in_array( $this->Auth->user('admin_user.authority'), [SYSTEM_ADMIN, ADMIN], true ) )
        {
            if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
            {
               $request = $this->request->data;

               // エラーチェック
               // todo 画像、pdfエラーチェック
               $error = $this->Questions->validation( $request ); 
               if( count( $error ) > 0 )
               {
                   $data = $this->Questions->newEntity( $request );
                   $this->set( compact( 'error' ) );
               }
               else
               {
                   $this->_session->write( 'Admin.questions', $request );
                   $this->redirect(['action' => 'confirm']);
               }
            }
            // 戻る
            elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
            {
                if( $this->_session->check('Admin.questions') )
                {
                    $informations = $this->Questions->newEntity( $this->_session->read('Admin.questions') );
                }
            }
            else
            {
                $data = $this->Questions->getDataByContractantId( $this->_session->read( 'ContractantData.id' ) );
            }
            $this->set( compact( 'data' ) );
        }
        else
        {
            throw new BadRequestException('権限がありません');
        }
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            if( $this->_session->check( 'Admin.questions' ) ) 
            {
                $data = $this->_session->read('Admin.questions');
            }
            else
            {
                return $this->redirect(['action' => 'index']);
            }

            // エラーチェック
            $error = $this->Questions->validation( $data ); 
            if( count( $error ) > 0 )
            {
                $data = $this->Questions->newEntity( $request );
                return $this->redirect(['action' => 'edit']);
            }
            else
            {
                // 登録
                $data['contractant_id'] = $this->_session->read('ContractantData.id');

                $question_id = $this->Questions->saveData( $data );

                if( $question_id )
                {
                    $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                    $this->Flash->success( $msg );
                }
                else
                {
                    $this->Flash->error( '登録に失敗しました。' );
                }

                $this->_session->delete('Admin.questions' );
                // システム画面は完了画面無し
                $this->redirect(['action' => 'index']);
            }
        }
        else if( $this->_session->check('Admin.questions') )
        {
            $data= $this->_session->read('Admin.questions');
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'data' ) );
    }

}
