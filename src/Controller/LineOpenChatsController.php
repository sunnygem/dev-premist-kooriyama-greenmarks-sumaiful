<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class LineOpenChatsController extends FrontAppController
{
    public $paginate = [ 'finder' => 'front' ];

    public function initialize()
    {
        parent::initialize();

        $this->LineOpenChatCategories = TableRegistry::getTableLocator()->get('LineOpenChatCategories');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );

    }

    public function index()
    {
        $this->_display_search_form = true;

        $categories = $this->LineOpenChatCategories->getDataForFront( $this->_contractant_id );

        $conditions = [
            'LineOpenChats.contractant_id' => $this->_session->read( 'ContractantData.id' )
        ];
        // タイトル検索
        if( $this->request->getQuery('search') !== null )
        {
            $search = trim( mb_convert_kana( $this->request->getQuery('search'), 'as' ) );
            $searchs = explode(' ',  $search);

            $arr_search = [];
            foreach( $searchs as $val ) 
            {
                $arr_search[] = [
                    'OR' => [
                        'LineOpenChats.title LIKE' => '%' . $val . '%'
                        ,'LineOpenChats.title_en LIKE' => '%' . $val . '%'
                    ]
                ];
            }
            $conditions[] = [
                'AND' => $arr_search
            ];
        }
        // カテゴリの絞り込み
        if( $this->request->getQuery('category') !== null )
        {
             $conditions['LineOpenChats.line_open_chat_category_id'] = $this->request->getQuery('category');
        }

        $this->paginate['finder'] = [
            'front' => [
                'conditions' => $conditions
            ]
        ];
        $data = $this->paginate( 'LineOpenChats' );
        $this->set( compact( 'data', 'categories' ) );
    }

    //public function category()
    //{
    //    $this->paginate['finder'] = [
    //        'front' => [
    //            'conditions' => [
    //                'LineOpenChatCategories.contractant_id' => $this->_session->read( 'ContractantData.id' )
    //            ]
    //            ,'order' => [ ]
    //        ]
    //    ];

    //    $data = $this->paginate( 'LineOpenChatCategories' );
    //    $this->set( compact( 'data' ) );
    //}

}
