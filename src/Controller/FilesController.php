<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class FilesController extends AppController
{
    public $Files;
    public $MenuApplications;
    public $MenuManuals;
    public $Informations;

    public function initialize()
    {
        parent::initialize();
        $this->app_common();

        $this->Files = TableRegistry::get( 'Files' );
        $this->Informations = TableRegistry::get( 'Informations' );
        $this->loadComponent( 'Common' );
    }
    public function beforeFilter(Event $event)
    {
        $this->app_screen_id = 3;
    }

    public function index( $id=null)
    {
        $this->autoRender = false;
        if( $id !== null )
        {
            $files = $this->Files->get( $id );
            if( $files )
            {
                // PDFを出力する
                header( "Content-Type: " . $files->mime_type );
                // ファイルを読み込んで出力
                readfile( WWW_ROOT . $files->file_path );
                exit;
            }
        }
        $this->set( compact( 'service_menus' ) );
    }

    public function informationPdf( $id=null )
    {
        $this->autoRender = false;
        if( $id !== null )
        {
            $data = $this->Informations->getDataWithPdf( $id );
            if( $data )
            {
                // PDFを出力する
                header( "Content-Type: " . $data->files[0]->mime_type );
                // ファイルを読み込んで出力
                readfile( WWW_ROOT . $data->files[0]->file_path );
                exit;
            }
        }
        $this->set( compact( 'service_menus' ) );
    }


}
