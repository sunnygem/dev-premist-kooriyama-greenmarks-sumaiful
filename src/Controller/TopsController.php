<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class TopsController extends FrontAppController
{
    public $Informations;
    public $ImageSharings;

    public $paginate;

    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->ImageSharings = TableRegistry::getTableLocator()->get( 'ImageSharings' );
    }
    //public function beforeRender(Event $event)
    //{
    //    parent::beforeRender( $event );

    //}

    public function index()
    {

        $start = true;
        if ( $this->_session->check( 'start' ) === true )
        {
            $start = false;
        }
        else
        {
            $this->_session->write( 'start', true );
        }
        $this->paginate['finder'] = [
            'search' => [
                'order' => [
                    'Informations.release_date' => 'DESC'
                    ,'Informations.created'     => 'DESC'
                ]
                ,'conditions' => [
                    'Informations.contractant_id'   => $this->_session->read( 'ContractantData.id' )
                    ,'Informations.display_flg'     => 1
                    ,'Informations.release_date <=' => date( 'Y-m-d' )
                    ,['OR' => [
                        'Informations.close_date IS'    => NULL
                        ,'Informations.close_date >'    => date( 'Y-m-d H:i:s' )
                    ] ]
                    // 全公開もしくはユーザーのbuilding_idに合致するもの
                    ,['OR' => [
                        'Informations.all_building_flg'       => 1
                        ,'InformationBuildings.building_id' => $this->_login_user['front_user']['building_id']
                    ]]
                ]
                ,'group' => 'Informations.id'
                ,'limit' => 10
            ]
        ];

        // 公開物件を絞る
        $this->paginate['join'] = [
            'table'  => 'information_buildings'
            ,'alias' => 'InformationBuildings'
            ,'type'  => 'LEFT'
            ,'conditions' => [
                'InformationBuildings.information_id = Informations.id' 
                //,'OR' => [
                //    'InformationBuildings.building_id'     => $this->_login_user['front_user']['building_id']
                //    //,'InformationBuildings.building_id IS' => null
                //]
            ]
        ];

        $this->paginate['contain'] = [
            'InformationBuildings' => [
                'fields' => [
                    'InformationBuildings.information_id'
                ]
            ]
            ,'InformationBuildings.Buildings' => [
                'fields' => [
                    'Buildings.name'
                    ,'Buildings.icon_path'
                ]
            ]
        ];
        $informations = $this->paginate( 'Informations' );
        //debug( $informations );

        $params = [
            'contractant_id' => $this->_session->read( 'ContractantData.id' )
            ,'limit'         => 5
            ,'building_id'   => $this->_login_user['front_user']['building_id']
        ];
        $image_sharings = $this->ImageSharings->getAlbumData( $params );
        $this->set( compact( 'informations', 'image_sharings', 'start' ) );
    }

    // 荷物受け取り
    public function receiveBaggage( )
    {
        if( $this->request->is('ajax') )
        {
            $this->autoRender = false;
            $query_params = $this->request->getQuery();
            if( isset( $query_params['user_id'] ) && isset(  $query_params['front_user_id'] ) )
            {
                $this->LuggageStrages = TableRegistry::getTableLocator()->get( 'LuggageStrages' );
                $param = [
                    'contractant_id' => $this->_contractant_id
                    ,'user_id'       => $query_params['user_id']
                    ,'front_user_id' => $query_params['front_user_id']
                ];
                $res = $this->LuggageStrages->setFlagOn( $param );
                $this->response->body( json_encode( ['result' => $res ], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
            }
        }
    }

    // 報告
    public function setPresentation()
    {
        if( $this->request->is('ajax') )
        {
            $this->autoRender = false;
            $complete_msg =  __('通信にに失敗しました。');
            $query_params = $this->request->getQuery();
            if( isset( $query_params['user_id'] ) && isset( $query_params['front_user_id'] ) && isset( $query_params['target_type'] ) )
            {
                $res = false;

                // セッションデータとuser_idを照会してbuilding_idも取得
                if( preg_match('/^[0-9]+$/', $query_params['user_id'] ) && (int)$query_params['user_id'] === $this->_login_user['id'] )
                {
                    $this->Presentations = TableRegistry::getTableLocator()->get( 'Presentations' );
                    switch( $query_params['target_type'] )
                    {
                    case 'post':
                        $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
                        $posts = $this->Posts->findById( $query_params['target_id'] );
                        if( $posts->isEmpty() === false )
                        {
                            $post = $posts->first();
                            $data = [
                                'contractant_id' => $this->_contractant_id
                                ,'type'          => $query_params['type']
                                ,'user_id'       => $query_params['user_id']
                                ,'post_id'       => $post->id
                                ,'content'       => $post->content
                                // ポストが持つ建物情報も保存する
                                ,'presentation_buildings' => [
                                    [
                                        'contractant_id' => $post->contractant_id
                                        ,'building_id'   => $post->building_id
                                    ]
                                ]
                            ];
                            $res = $this->Presentations->saveData( $data );
                        }
                        break;
                    case 'file':
                        // ファイルのチェック
                        $this->Files = TableRegistry::getTableLocator()->get( 'Files' );
                        $files = $this->Files->findById( $query_params['target_id'] );
                        if( $files->isEmpty() === false )
                        {
                            //$file = $files->contain([
                            //    'RelImagesharingFiles'
                            //]) ->first();
                            $file = $this->Files->getImageSharingData( $query_params['target_id'] );
                            $data = [
                                'contractant_id' => $this->_contractant_id
                                ,'type'          => $query_params['type']
                                ,'user_id'       => $query_params['user_id']
                                //,'building_id'   => $building_id
                                ,'file_id'       => $file->id
                                ,'album_title'   => $file->rel_imagesharing_file->image_sharing->title
                            ];

                            // アルバムが持つ建物情報も保存する
                            $imageSharingBuilsings = TableRegistry::getTableLocator()->get( 'ImageSharingBuildings' );
                            $image_sharing_buildings = $imageSharingBuilsings->getEditDataByImageSharingId( $this->_contractant_id, $file->rel_imagesharing_file->image_sharing_id );
                            if( count( $image_sharing_buildings ) > 0 )
                            {
                                foreach( $image_sharing_buildings as $val )
                                {
                                    $data['presentation_buildings'][] = [
                                        'contractant_id' => $this->_contractant_id
                                        ,'building_id'   => $val
                                    ];
                                }
                            }
                            // 登録がない場合、全建物なので、建物マスターを登録
                            else
                            {
                                $Buildings = TableRegistry::getTableLocator()->get('Buildings');
                                $arr_buildings = $Buildings->getOptions( $this->_contractant_id );
                                foreach( $arr_buildings as $key =>$val )
                                {
                                    $data['presentation_buildings'][] = [
                                        'contractant_id' => $this->_contractant_id
                                        ,'building_id'   => $key
                                    ];
                                }
                            }

                            $res = $this->Presentations->saveData( $data );
                        }
                        break;
                    default:
                    }
                    $complete_msg = ( $res ) ? __("ご報告ありがとうございました。") : __('報告データの保存に失敗しました。');
                }
                else
                {
                    $complete_msg = __("Error");
                }
            }
            $this->response->body( json_encode( ['result' => $complete_msg ], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
        }
    }


}
