<?php

namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

//use PhpOffice\PhpSpreadsheet\Reader\Xlsx as Reader;
require_once ROOT . '/vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once ROOT . '/vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class BuildingsController extends SystemAppController
{
    public $Buildings;
    
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();

        $this->Contractants = TableRegistry::get('Contractants');
        $this->Buildings    = TableRegistry::get('Buildings');
        $this->BuildingRoomNumbers = TableRegistry::get('BuildingRoomNumbers');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        if( $this->request->is('ajax') === false && in_array( $this->request->action, ['index', 'edit', 'confirm'], true ) )
        {
            // 契約idは必須
            if( isset( $this->request->params['pass'] ) )
            {
                $this->contractant = $this->Contractants->get( $this->request->params['pass'][0] );
                $this->set( 'contractant', $this->contractant );
            }
            else
            {
                throw new ForbiddenException();
            }
        }

        $this->set( 'title_for_layout', '建物管理' );
    }

    public function index( $contractant_id=null )
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'contractant_id' => $contractant_id
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'buildingroomnumbers' => [
                'fields' => [
                    'count' => 'count( * )'
                    ,'building_id'
                ]
            ]
        ];
        $data = $this->paginate( 'Buildings' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $contractant_id=null, $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           //  エラーチェック
           $data = $this->Buildings->newEntity( $request );
           if( count( $data->errors() ) === 0 )
           {
               // 先に画像をアップ
               $this->loadComponent( 'File' );
               $this->File->saveFile( $request, 'buildings' );

               $this->_session->write( 'System.data', $request );
               $this->redirect(['action' => 'confirm', $contractant_id ]);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('System.data') )
            {
                $data = $this->Buildings->newEntity( $this->_session->read('System.data') );
            }
        }
        else
        {
            if( $id !== null )
            {
                $data = $this->Buildings->get( $id );
            }
            else
            {
                $data = $this->Buildings->newEntity();
            }
        }

        $this->set( compact( 'data' ) );
    }

    public function confirm( $contractant_id=null )
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'System.data' ) )
           {
               $data = $this->_session->read('System.data');
           }
           else
           {
               return $this->redirect(['action' => 'index', $contractant_id ]);
           }

           // エラーチェック
           $error = $this->Buildings->newEntity( $data );
           if( count( $error->errors() ) > 0 )
           {
               return $this->redirect(['action' => 'edit', $contractant_id ]);
           }
           else
           {
               // システムユーザーのcontractant_idは0
               if( isset( $data['id'] ) === false )
               {
                   $data['contractant_id'] = $this->contractant->id;
               }

               if( isset( $data['files']['buildings']['file_path'] ) ) $data['icon_path'] = $data['files']['buildings']['file_path'];
               unset( $data['files'] );

               // 登録
               $res = $this->Buildings->saveData( $data );

               if( $res )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('System.data' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index', $this->contractant->id ]);
           }
        }
        else if( $this->_session->check('System.data') )
        {
            $data = $this->_session->read('System.data');
        }
        else
        {
            return $this->redirect(['action' => 'index', $contractant_id ]);
        }

        $this->set( compact( 'data' ) );
    }

    public function delete( $id )
    {
        if( $id !== null )
        {
            $this->Buildings->deleteData( $id );
            $this->Flash->success( 'ユーザーを削除しました。' );
        }
        else
        {
            $this->Flash->error( 'ユーザーの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'index']);


    }

    public function roomNumber( $building_id=null )
    {

        $building = $this->Buildings->getDataById( $building_id ); 

        $room_numbers = $this->BuildingRoomNumbers->getDataByBuildingId( $building_id );

        if ( $this->request->is( [ 'post', 'put' ] ) )
        {
            $request = $this->request->data;

            if( isset( $request['drag_data'] ) && $request['drag_data'] !== '' )
            {
                $data = json_decode( $request['drag_data'], true );

                $arr = [];
                foreach( $data as $val )
                {
                    $arr[] = [
                        'building_id'     => $building->id
                        ,'contractant_id' => $building->contractant_id
                        ,'room_number'    => $val[0]
                    ];
                }

                $res = $this->BuildingRoomNumbers->uploadSaveData( $arr );
                if( $res )
                {
                    $this->Flash->success( 'データを登録しました。' );
                }
                else
                {
                    $this->Flash->error( 'データの登録に失敗しました。' );
                }

            }
            else
            {
                $this->Flash->error( 'ファイルが選択されていません。' );
            }

            return $this->redirect( [ $building_id ] );
        }

        $this->set( compact( 'building', 'room_numbers' ) );

        $this->set( 'title_for_layout', '建物管理-部屋番号管理' );
    }

    public function roomNumberUpload( $target=null )
    {
        if ( $this->request->is( [ 'post', 'put' ] ) )
        {
            $data = json_decode( $this->request->data['upload_data'], true );
            $this->agencies->syncstart();
            $this->agencies->uploadsavedata( $data );
            $this->agencies->syncend();
        }
        $this->flash->success( 'アップロードしました。' );
        return $this->redirect( '/admin/settings/agency' );
    }

    public function roomNumberDragUpload()
    {
        $this->autoRender = false;
        if ( $this->request->is( 'ajax' ) )
        {
            $data = [];
            $tmp_file = $this->request->data['upload_file']['tmp_name'];
            $reader = \PHPExcel_IOFactory::createReader('Excel2007');
            $excel = $reader->load( $tmp_file );
            $excel->setActiveSheetIndex( 0 ); // シート数は取得しない
            $sheet = $excel->getActiveSheet()->toArray(null,true,true,true);
            $i = 0;
            foreach( $sheet as $key => $val )
            {
                if ( $key === 1 ) continue;
                $j = 0;
                foreach( $val as $val2 )
                {
                    $data[$i][$j] = $val2;
                    $j++;
                }
                $i++;
            }
            $data = json_encode( $data );
            $this->response->body( $data );
        }
    }

}
