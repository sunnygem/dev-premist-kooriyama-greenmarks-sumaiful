<?php
namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class TopsController extends SystemAppController
{
    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
    }

    // ダッシュボード
    public function index()
    {
    }
}
