<?php

namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ServiceMenusController extends SystemAppController
{
    public $ServiceMenus;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->ServiceMenus     = TableRegistry::get('ServiceMenus');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
        $this->set( 'title_for_layout', 'メニュー管理' );
    }

    /**
     *
     * メニュー
     *
     **/
    public function index()
    {
        $data = $this->paginate( 'ServiceMenus' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // todo エラーチェック
           // todo 各メニュー専用のコントローラとテーブルが準備されているかのチェックをする
           $error = $this->ServiceMenus->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $menus = $this->ServiceMenus->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               $this->_session->write( 'System.menus', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('System.menus') )
            {
                $menus = $this->ServiceMenus->newEntity( $this->_session->read('System.menus') );
                $menus->password = null;
            }
            else
            {
            }
        }
        else
        {
            if( $id !== null )
            {
                $menus = $this->ServiceMenus->get( $id );
                $menus->password = null;
                //$this->request->data( 'id', $menus->id );
                //$this->request->data( 'login_id', $menus->login_id );
            }
            else
            {
                $menus = $this->ServiceMenus->newEntity();
            }
        }

        $this->set( compact( 'menus' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'System.menus' ) ) 
           {
               $data = $this->_session->read('System.menus');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->ServiceMenus->validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               if( isset( $data['id'] ) === false )
               {
                   // orderbyを取得
                   $orderby = $this->ServiceMenus->getOrder();
                   $data['orderby'] = $orderby;
               }

               // 登録
               $res = $this->ServiceMenus->saveData( $data );

               if( $res )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('System.menus' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('System.menus') )
        {
            $menus = $this->ServiceMenus->newEntity( $this->_session->read('System.menus') );
            $menus->password = null;
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }

        $this->set( compact( 'menus' ) );
    }

    public function delete( $id=null )
    {
        if( $id !== null )
        {
            // todo 契約者に紐づいているデータは消せない仕様
            //$this->SystemUsers->deleteData( $id );
            $this->Flash->success( 'ユーザーを削除しました。(作成中)' );
        }
        else
        {
            $this->Flash->error( 'ユーザーの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'index']);


    }



//    /**
//     *
//     * メニュータイプ
//     *
//     **/
//    public function types()
//    {
//        $data = $this->paginate( 'MenuTypes' );
//        //debug( $data );
//        $this->set( compact( 'data' ) );
//    }
//
//    public function typeEdit( $id=null )
//    {
//        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
//        {
//           $request = $this->request->data;
//
//           // todo エラーチェック
//           $error = $this->MenuTypes->validation( $request ); 
//           if( count( $error ) > 0 )
//           {
//               $types = $this->MenuTypes->newEntity( $request );
//               $this->set( compact( 'error' ) );
//           }
//           else
//           {
//               $this->_session->write( 'System.types', $request );
//               $this->redirect(['action' => 'typeConfirm']);
//           }
//        }
//        // 戻る
//        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
//        {
//            if( $this->_session->check('System.types') )
//            {
//                $types = $this->MenuTypes->newEntity( $this->_session->read('System.types') );
//            }
//            else
//            {
//            }
//        }
//        else
//        {
//            if( $id !== null )
//            {
//                $types = $this->MenuTypes->get( $id );
//            }
//            else
//            {
//                $types = $this->MenuTypes->newEntity();
//            }
//        }
//
//        $this->set( compact( 'types' ) );
//    }
//
//    public function typeConfirm()
//    {
//        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
//        {
//           if( $this->_session->check( 'System.types' ) ) 
//           {
//               $data = $this->_session->read('System.types');
//           }
//           else
//           {
//               return $this->redirect(['action' => 'types']);
//           }
//
//           // todo エラーチェック
//           $error = $this->MenuTypes->validation( $data ); 
//           if( count( $error ) > 0 )
//           {
//               return $this->redirect(['action' => 'typeEdit']);
//           }
//           else
//           {
//               if( isset( $data['id'] ) === false )
//               {
//                   // orderbyを取得
//                   $orderby = $this->MenuTypes->getOrder();
//                   $data['orderby'] = $orderby;
//               }
//             
//               // 登録
//               $res = $this->MenuTypes->saveData( $data );
//
//               if( $res )
//               {
//                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
//                   $this->Flash->success( $msg );
//               }
//               else
//               {
//                   $this->Flash->error( '登録に失敗しました。' );
//               }
//
//               $this->_session->delete('System.types' );
//               // システム画面は完了画面無し
//               $this->redirect(['action' => 'types']);
//           }
//        }
//        else if( $this->_session->check('System.types') )
//        {
//            $types = $this->MenuTypes->newEntity( $this->_session->read('System.types') );
//        }
//        else
//        {
//            return $this->redirect(['action' => 'types']);
//        }
//
//        $this->set( compact( 'types' ) );
//    }
//
//    public function typeDelete( $id=null )
//    {
//        if( $id !== null )
//        {
//            // todo 契約者に紐づいているデータは消せない仕様
//            //$this->SystemUsers->deleteData( $id );
//            //$this->Flash->success( 'ユーザーを削除しました。' );
//        }
//        else
//        {
//            $this->Flash->error( 'ユーザーの削除に失敗しました。' );
//        }
//        return $this->redirect(['action' => 'index']);
//
//
//    }



}
