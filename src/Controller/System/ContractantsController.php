<?php
namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ContractantsController extends SystemAppController
{
    public $Contractants;
    public $Users;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->Contractants = TableRegistry::get('Contractants');
        $this->ServiceMenus = TableRegistry::get('ServiceMenus');
        $this->Users        = TableRegistry::get('Users');
        $this->MtContractantStatuses      = TableRegistry::get('MtContractantStatuses');
        $this->ContractantServiceMenus = TableRegistry::get('ContractantServiceMenus');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $mt_contractant_status = $this->MtContractantStatuses->getList();
        $option_service_menus = $this->ServiceMenus->getOptions();

        $this->set( compact( 'mt_contractant_status', 'option_service_menus' ) );
    }

    public function index()
    {
        $data = $this->paginate( 'Contractants' );
    //debug( $data );

        $this->set( compact( 'data' ) );

    }

    public function edit( $id=null )
    {
        $contracts = [];
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           $error = $this->Contractants->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $contracts = $this->Contractants->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               $this->_session->write( 'System.contracts', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('System.contracts') )
            {
                $contracts = $this->Contractants->newEntity( $this->_session->read('System.contracts') );
            }
            //else
            //{
            //}
        }
        else
        {
            if( $id !== null )
            {
                $contracts = $this->Contractants->getDetailData( $id );
                $contracts->service_menu_id = $this->ContractantServiceMenus->getServiceMenuIdsByContractId( $id );
            }
            else
            {
                $contracts = $this->Contractants->newEntity();
            }
        }
        //debug( $contracts );
        $this->set( compact( 'contracts' ) );
    }

    public function confirm()
    {
        $contracts = [];
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'System.contracts' ) ) 
           {
               $data = $this->_session->read('System.contracts');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->Contractants->validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               // 登録
               $contractant_id = $this->Contractants->saveData( $data );

               if( $contractant_id )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );

                   // 初期ADMINログインユーザーを作成
                   $admin_login_id = $data['unique_parameter'] . '_admin';
                   if( $this->Users->checkExistInitAdminUser( $admin_login_id ) === false )
                   {
                       $admin_data = [
                           'contractant_id' => $contractant_id
                           ,'login_id'      => $admin_login_id
                           ,'type'          => 'admin'
                           ,'admin_user' => [
                               'contractant_id' => $contractant_id
                               ,'valid_flg'     => 1
                               ,'authority'     => 'system_admin'
                               ,'name'          => '初期ログインユーザー'
                           ]
                       ];
                       $res2 = $this->Users->saveData( $admin_data );

                       if( $res2 )
                       {
                           $this->Flash->success( '初期管理ユーザーを作成しました');
                       }
                       else
                       {
                           $this->Flash->error( '初期管理ユーザーの作成に失敗しました');
                       }
                   }

                   // メニューを登録
                   //if( isset( $data['service_menu_id'] ) && count( $data['service_menu_id'] ) > 0 )
                   //{
                   //    // 有効な既存サービスメニューがあればedit_flgをオン
                   //    $this->ContractantServiceMenus->setEditFlgByContractantId( $contractant_id );

                   //    // 既存レコードをチェック
                   //    foreach( $data['service_menu_id'] as $val )
                   //    {
                   //        $condition = [
                   //            'contractant_id'   => $contractant_id
                   //            ,'service_menu_id' => $val
                   //            ,'edit_flg'        => 1
                   //            ,'deleted IS'      => null
                   //        ];

                   //        $exist = $this->ContractantServiceMenus->checkExistAndBreakEditFlg( $condition ); 
                   //        // なければ新規作成
                   //        if( $exist === false )
                   //        {
                   //            $this->ContractantServiceMenus->saveData([
                   //                'contractant_id'   => $contractant_id
                   //                ,'service_menu_id' => $val
                   //            ]);
                   //        }
                   //    }
                   //    // edit_flgが絶ったままのものは削除対象

                   //}
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('System.contracts' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('System.contracts') )
        {
            $contracts = $this->Contractants->newEntity( $this->_session->read('System.contracts') );
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'contracts' ) );
    }

    public function delete( $id )
    {
        echo 'under construction.';exit;
    }

    public function detail( $id=null )
    {
        $contracts = $this->Contractants->getDetailData( $id );
        $contracts->service_menu_id = $this->ContractantServiceMenus->getServiceMenuIdsByContractId( $id );
        $this->set( compact( 'contracts' ) );
    }

    public function selectMenu( $id=null )
    {
        if( $id !== null )
        {
            $contracts = $this->Contractants->getDetailData( $id );
            $service_menus = $this->ContractantServiceMenus->getDataByContractantId( $id, true );

            if( $this->request->is( 'post' ) && isset( $this->request->data['add'] ) )
            {
                $request = $this->request->data;
                if( $request['service_menu_id'] === '' ) 
                {
                    //$error['service_menu_id'] = '選択してください';
                    //$this->set( compact( 'error' ) );
                    $this->Flash->error( '追加するメニューを選択してください' );
                }
                else
                {
                    $data = [
                        'contractant_id'   => $id
                        ,'service_menu_id' => $request['service_menu_id']
                    ];
                    $res = $this->ContractantServiceMenus->saveData( $data );

                    if( $res )
                    {
                        $this->Flash->success( 'メニューを追加しました' );
                    }
                    else
                    {
                        $this->Flash->error( 'メニューの追加登録に失敗しました' );
                    }
                }
                $this->redirect( [ $id ] );
            }
            $this->set( compact( 'contracts', 'service_menus' ) );
        }
        else
        {
            throw new NotFoundException();
        }
    }

    public function validMenu( $id=null )
    {
        if( $id !== null )
        {
            $service_menus = $this->ContractantServiceMenus->get( $id );

            $res = $this->ContractantServiceMenus->saveData([
                'id'         => $service_menus->id
                ,'valid_flg' => 1
            ]);

            if( $res )
            {
                $this->Flash->success( '有効になりました' );
            }
            else
            {
                $this->Flash->error( '更新に失敗しました' );
            }
            $this->redirect( ['action' => 'select_menu', $service_menus->contractant_id] );
        }
        else
        {
            throw new NotFoundException();
        }
    }
    public function invalidMenu( $id=null )
    {
        if( $id !== null )
        {
            $service_menus = $this->ContractantServiceMenus->get( $id );

            $res = $this->ContractantServiceMenus->saveData([
                'id'         => $service_menus->id
                ,'valid_flg' => 0
            ]);

            if( $res )
            {
                $this->Flash->success( '無効になりました' );
            }
            else
            {
                $this->Flash->error( '更新に失敗しました' );
            }
            $this->redirect([ 'action' => 'select_menu', $service_menus->contractant_id ]);
        }
        else
        {
            throw new NotFoundException();
        }
    }

    public function deleteMenu( $contractant_id=null, $id=null )
    {
        if( $id !== null )
        {
            // todo 契約者に紐づいているデータは消せない仕様
            $this->ContractantServiceMenus->deleteData( $id );
            $this->Flash->success( 'メニューを削除しました。' );
        }
        else
        {
            $this->Flash->error( 'メニューの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'select_menu', $contractant_id ]);

    }

}
