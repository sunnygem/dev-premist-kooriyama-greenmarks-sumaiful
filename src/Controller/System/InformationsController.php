<?php
namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class InformationsController extends SystemAppController
{
    public $Informations;
    public $Contractants;
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->Informations = TableRegistry::get('Informations');
        $this->Contractants = TableRegistry::get('Contractants');
        $this->Files        = TableRegistry::get('Files');
        $this->RelInformationFiles = TableRegistry::get('RelInformationFiles');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $contractants = $this->Contractants->getValidList();
        $this->set( compact('contractants') );

        $this->loadComponent( 'Common' );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'sort' => [
                    'modified' => 'DESC'
                ]
            ]
        ];
        $data = $this->paginate( 'Informations' );

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           // エラーチェック
           // todo 画像、pdfエラーチェック
           $error = $this->Informations->validation( $request ); 
           if( count( $error ) > 0 )
           {
               $informations = $this->Informations->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               if( $request['image_file']['size'] > 0 ) 
               {
                   $request['image_file']['data']      = file_get_contents( $request['image_file']['tmp_name'] );
                   $request['image_file']['mime_type'] = mime_content_type( $request['image_file']['tmp_name'] );
                   $request['image_file']['extension'] = $this->Common->checkFileExtension( $request['image_file'] );

               }

               if( $request['pdf_file']['size'] > 0 ) 
               {
                   $request['pdf_file']['data']      = file_get_contents( $request['pdf_file']['tmp_name'] );
                   $request['pdf_file']['mime_type'] = mime_content_type( $request['pdf_file']['tmp_name'] );
                   $request['pdf_file']['extension'] = $this->Common->checkFileExtension( $request['pdf_file'] );
               }

               $this->_session->write( 'System.informations', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('System.informations') )
            {
                $informations = $this->Informations->newEntity( $this->_session->read('System.informations') );
            }
            //else
            //{
            //}
        }
        else
        {
            if( $id !== null )
            {
                $informations = $this->Informations->getEditData( $id );
                if( count( $informations->images ) > 0  )
                {
                    $informations->image_id      = $informations->images[0]->id;
                    $informations->image_preview = 'data:' . $informations->images[0]->mime_type . ';base64,' . base64_encode( stream_get_contents( $informations->images[0]->data ) );
                }
                if( count( $informations->pdfs ) > 0  )
                {
                    $informations->pdf_id      = $informations->pdfs[0]->id;
                    $informations->pdf_preview = base64_encode( stream_get_contents( $informations->pdfs[0]->data ) );
                }
            }
            else
            {
                $informations = $this->Informations->newEntity();
            }
        }
        $this->set( compact( 'informations' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'System.informations' ) ) 
           {
               $data = $this->_session->read('System.informations');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           //  エラーチェック
           $error = $this->Informations->validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               // 登録
               $information_id = $this->Informations->saveData( $data );

               // 画像の登録
               if( $data['image_file']['error'] === 0 && isset( $data['image_file']['data'] ) )
               {
                   // 既存画像データを削除
                   if( isset( $data['image_id'] ) )
                   {
                       $image_entity = $this->Files->get( $data['image_id'] );
                       $this->Files->delete( $image_entity );
                       // 中間テーブルは、仕様により上記実行時にデフォルトで削除される。
                   }
                   $file_data = $data['image_file'];
                   $file_data['contractant_id'] = $data['contractant_id'];
                   $file_id = $this->Files->saveData( $file_data );
                   $this->RelInformationFiles->saveData([
                       'information_id'  => $information_id
                       ,'file_id'        => $file_id
                       ,'contractant_id' => $data['contractant_id']
                   ]);
               }

               // pdf
               if( $data['pdf_file']['error'] === 0 && isset( $data['pdf_file']['data'] ) )
               {
                   // 既存画像データを削除
                   if( isset( $data['pdf_id'] ) )
                   {
                       $pdf_entity = $this->Files->get( $data['pdf_id'] );
                       $this->Files->delete( $pdf_entity );
                       // 中間テーブルは、仕様により上記実行時にデフォルトで削除される。
                   }
                   $file_data = $data['pdf_file'];
                   $file_data['contractant_id'] = $data['contractant_id'];
                   $file_id = $this->Files->saveData( $file_data );
                   $this->RelInformationFiles->saveData([
                       'information_id'  => $information_id
                       ,'file_id'        => $file_id
                       ,'contractant_id' => $data['contractant_id']
                   ]);
               }

               if( $information_id )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('System.informations' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('System.informations') )
        {
            $informations = $this->Informations->newEntity( $this->_session->read('System.informations') );
            //debug( $informations );
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }
        $this->set( compact( 'informations' ) );
    }

    // pdfのぷれびゅー
    public function getPdf()
    {
        if( $this->_session->check( 'System.informations' ) )
        {
            $informations = $this->_session->read( 'System.informations' );
            $pdf_data = base64_encode( $informations['pdf_file']['data'] );
            $this->set( compact('pdf_data') );
        }

    }

}
