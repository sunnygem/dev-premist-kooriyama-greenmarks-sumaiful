<?php

namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class UsersController extends SystemAppController
{
    public $Users;
    public $SystemUsers;
    
    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
        $this->Users = TableRegistry::get('Users');
        $this->SystemUsers = TableRegistry::get('SystemUsers');
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $this->set( 'title_for_layout', '管理者' );
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'type' => 'system'
                ]
            ]
        ];
        $this->paginate['contain'] = [ 'SystemUsers' ];
        $data = $this->paginate( 'Users' );

        if( $this->request->is( 'post' ) )
        {
            if( false )
            {
                $this->Flash->error('ログインIDかパスワードが不正です');
            }
            // ログイン
            else
            {
                $this->redirect([ 'controller' => 'Tops', 'action' => 'index' ]);
            }
        }

        $this->set( compact( 'data' ) );
    }

    public function edit( $id=null )
    {
        if( $this->request->is([ 'post', 'put', 'patch' ]) && isset( $this->request->data['confirm'] ) )
        {
           $request = $this->request->data;

           //  エラーチェック
           $error = $this->Users->system_validation( $request ); 
           if( count( $error ) > 0 )
           {
               $users = $this->Users->newEntity( $request );
               $this->set( compact( 'error' ) );
           }
           else
           {
               $this->_session->write( 'System.users', $request );
               $this->redirect(['action' => 'confirm']);
           }
        }
        // 戻る
        elseif( $this->request->is( 'post' ) && isset( $this->request->data['back'] ) )
        {
            if( $this->_session->check('System.users') )
            {
                $users = $this->Users->newEntity( $this->_session->read('System.users') );
                $users->password = null;
            }
            //else
            //{
            //}
        }
        else
        {
            if( $id !== null )
            {
                $users = $this->Users->getSystemUser( $id );
                $users->password = null;
            }
            else
            {
                $users = $this->Users->newEntity();
            }
        }

        $this->set( compact( 'users' ) );
    }

    public function confirm()
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
           if( $this->_session->check( 'System.users' ) ) 
           {
               $data = $this->_session->read('System.users');
           }
           else
           {
               return $this->redirect(['action' => 'index']);
           }

           // エラーチェック
           $error = $this->Users->system_validation( $data ); 
           if( count( $error ) > 0 )
           {
               return $this->redirect(['action' => 'edit']);
           }
           else
           {
               // システムユーザーのcontractant_idは0
               $data['type'] = 'system';
               $data['contractant_id'] = 0;

               // 登録
               // アソシエーションごと保存しています
               $user_id = $this->Users->saveData( $data );

               if( $user_id )
               {
                   $msg = ( isset( $data['id'] ) ) ? '編集が完了しました。' : '登録が完了しました。';
                   $this->Flash->success( $msg );
               }
               else
               {
                   $this->Flash->error( '登録に失敗しました。' );
               }

               $this->_session->delete('System.users' );
               // システム画面は完了画面無し
               $this->redirect(['action' => 'index']);
           }
        }
        else if( $this->_session->check('System.users') )
        {
            $users = $this->_session->read('System.users');
            $users['password'] = str_repeat( '●', strlen( $users['password'] ) );
        }
        else
        {
            return $this->redirect(['action' => 'index']);
        }

        $this->set( compact( 'users' ) );
    }

    // パスワード忘れ
    public function remind()
    {
    }
    
    public function delete( $id )
    {
        if( $id !== null )
        {
            $this->Users->deleteData( $id );
            $this->SystemUsers->deleteDataByUserId( $id );
            $this->Flash->success( 'ユーザーを削除しました。' );
        }
        else
        {
            $this->Flash->error( 'ユーザーの削除に失敗しました。' );
        }
        return $this->redirect(['action' => 'index']);


    }

}
