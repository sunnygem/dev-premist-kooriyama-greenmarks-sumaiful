<?php
namespace App\Controller\System;
use App\Controller\SystemAppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class LoginsController extends SystemAppController
{

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['remind']);
    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );
    }

    public function index()
    {
        if( $this->request->is( 'post' ) )
        {
            $user = $this->Auth->identify();
            // ログイン
            if( $user )
            {
                // ログイン保持
                if( $this->getRequest()->data['auto_login'] === '1' )
                {
                    $cookie_key = sha1( uniqid() . mt_rand(1, 999999999) . time() );
                    $this->Cookie->write( 'system.admin_login', $user );
                    $this->Cookie->write( 'system.cookie_key',  $cookie_key );

                    $SystemUsers = TableRegistry::getTableLocator()->get('SystemUsers');
                    $SystemUsers->setCookieKey( $user['system_user']['id'], $cookie_key );
                }

                $this->Auth->setUser( $user );
                return $this->redirect( $this->Auth->redirectUrl() );
            }
        }
    }

    // パスワード忘れ
    public function remind()
    {
    }

    public function logout()
    {
        $this->Cookie->delete('system');
        return $this->redirect( $this->Auth->logout() );
    }
}
