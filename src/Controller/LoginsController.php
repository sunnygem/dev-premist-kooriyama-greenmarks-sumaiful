<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\I18n; 

/**
 *
**/
class LoginsController extends FrontAppController
{
    public $Users;
    public function initialize()
    {
        parent::initialize();

        // 認証不要のアクションを設定
        $this->Auth->allow([ 'logout', 'setLocale' ]);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
    }

    public function index()
    {
        if( $this->request->is( ['post'] ) )
        {
            $user = $this->Auth->identify();
            // ログイン
            if( $user )
            {
                // front_userを持つもののみログイン許可
                if( $user['front_user'] !== null )
                {
                    //if( isset( $this->request->data['auto_login'] ) && $this->request->data['auto_login'] === '1' )
                    //{
                    //    // 期限が設定されてなければ24時間
                    //    $cookie['auto_login'] = [
                    //        'date'  => time()
                    //        ,'id' => $user['id']
                    //    ];
                    //}
                    //else
                    //{
                    //}

                    // アプリ用に必要なデータだけをsetUserする
                    $user = $this->Users->getFrontUser( $user['id'] );
                    $this->Auth->setUser( $user->toArray() );
                    return $this->redirect( $this->Auth->redirectUrl() );
                }
                else
                {
                    $this->Flash->error( 'ログインに失敗しました。' );
                }
            }
            // ログイン失敗
            else
            {
                $this->Flash->error( 'ログインに失敗しました。' );
            }
        }
    }
    //:public function regist()
    //:{
    //:}
    public function logout()
    {
        $this->_session->delete('Front.date_for_point');
        return $this->redirect( $this->Auth->logout() );
    }

    public function setLocale( $locale=null )
    {
        parent::setLocale( $locale );
        $this->redirect(['action' => 'index']);

    }
}
