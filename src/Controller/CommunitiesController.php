<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Cake\Log\Log;
class CommunitiesController extends FrontAppController
{
    public $paginate = [ 'finder' => 'search' ];
    public $Posts;

    public function initialize()
    {
        parent::initialize();

    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
        $this->PostLikes = TableRegistry::getTableLocator()->get( 'PostLikes' );
        $this->PostJoins = TableRegistry::getTableLocator()->get( 'PostJoins' );
        $this->PostComments = TableRegistry::getTableLocator()->get( 'PostComments' );
        $this->PostCommentLikes = TableRegistry::getTableLocator()->get( 'PostCommentLikes' );
        $this->PostActivityLogs = TableRegistry::getTableLocator()->get( 'PostActivityLogs' );
        $this->FrontUsers       = TableRegistry::getTableLocator()->get( 'FrontUsers' );
        $this->Threads          = TableRegistry::getTableLocator()->get( 'Threads' );
    }
//    public function beforeRender(Event $event)
//    {
//        parent::beforeRender( $event );
//
//    }
    public function index()
    {
        $this->_display_search_form = true;
        // 削除
        if( $this->request->is( 'post' ) && isset( $this->request->data['type'] ) && $this->request->data['type'] === 'delete')
        {
            self::delete();
            // パラメーターを引き継いで一覧へ戻る。
            $this->redirect(['action' => 'index', '?' => $this->request->getQueryParams() ]);
        }
        else
        {
            // カテゴリーの絞り込み
            if( isset( $request['post_category_id'] ) && preg_match( '/^[0-9]+$/', $request['post_category_id'] ) === 1 )
            {
                $where['Posts.post_category_id'] = $request['post_category_id'];
            }
            //$posts = $this->Posts->getPosts( $where, $limit, $page );

            $this->paginate['finder'] = [
                'search' => [
                    'conditions' => [
                        'Posts.contractant_id'   => $this->_contractant_id
                        ,'Posts.display_flg'     => 1
                        ,'Users.deleted IS'      => NULL
                        ,'FrontUsers.id IS NOT'  => NULL // front_userが削除されていない
                    ]
                    ,'order' => [
                        'Posts.created' => 'DESC'
                    ]
                    ,'group' => 'Posts.id' // post_buildings用
                ]
            ];
            // 検索
            if( $this->request->getQuery('search') )
            {
                $tmp_str = mb_ereg_replace( "　", " ", trim( $this->request->getQuery('search') ) );
                $tmp_arr = explode( " ", $tmp_str );

                $arr_or = [];
                foreach( $tmp_arr as $key => $val )
                {
                    $arr_or[0][$key] = [ 'Posts.content LIKE' => '%' . $val . '%' ];
                    $arr_or[1][$key] = [ 'FrontUsers.nickname LIKE' => '%' . $val . '%' ] ;
                    $arr_or[2][$key] = [ 'Buildings.name LIKE' => '%' . $val . '%' ] ;
                    $arr_or[3][$key] = [ 'Buildings.name_en LIKE' => '%' . $val . '%' ] ;
                    $arr_or[4][$key] = [ 'Buildings.name_kana LIKE' => '%' . mb_convert_kana( $val, 'cHV' ) . '%' ] ;
                }
                $this->paginate['finder']['search']['conditions']['OR'] = $arr_or;
            }
            // カテゴリー
            if( $this->request->getQuery('post_category_id') )
            {
                $this->paginate['finder']['search']['conditions'][] = ['Posts.post_category_id' => $this->request->getQuery('post_category_id') ];
            }

            $this->paginate['contain'] = [
                'PostCategories'
                ,'Users.FrontUsers'
                ,'Users.FrontUsers.Buildings' => [
                    'fields' => [ 'Buildings.name', 'Buildings.icon_path' ]
                ]
                // 画像
                ,'RelPostFiles' => [
                    'fields' => [ 'RelPostFiles.post_id', 'RelPostFiles.file_id' ]
                ]
                ,'RelPostFiles.Files' => [
                    'fields' => [ 'Files.id', 'Files.file_path' ]
                ]
                //いいね
                ,'PostLikes' => [
                    'fields' => [ 'post_id', 'user_id' ]
                    ,'conditions' => [
                        'PostLikes.deleted IS ' => null 
                        ,'FrontUsers.id IS NOT' => null
                    ]
                ]
                ,'PostLikes.Users' => [
                    'fields' => [ 'id' ]
                ]
                ,'PostLikes.Users.FrontUsers' => [
                    'fields' => [ 'leave_flg', 'nickname', 'thumbnail_path' ]
                ]
                // 参加
                ,'PostJoins' => [
                    'fields' => [ 'post_id', 'user_id' ]
                    ,'conditions' => [
                        'PostJoins.deleted IS' => null
                        ,'FrontUsers.id IS NOT' => null
                    ]
                ]
                ,'PostJoins.Users' => [
                    'fields' => [ 'id' ]
                ]
                ,'PostJoins.Users.FrontUsers' => [
                    'fields' => [ 'leave_flg', 'nickname', 'thumbnail_path' ]
                ]
                ,'PostComments' => [
                    'fields' => [ 'id', 'post_id', 'comment' ]
                    ,'conditions' => [ 
                        'PostComments.deleted IS ' => null
                        ,'FrontUsers.id IS NOT' => null
                    ]
                    ,'sort' => [ 'PostComments.created' => 'DESC' ]
                ]
                ,'PostComments.Users.FrontUsers' => [
                    'fields' => [ 'Users.id', 'FrontUsers.user_id', 'FrontUsers.nickname' ]
                ]
            ];

            // 物件公開範囲
            $this->paginate['join'] = [
                [
                    'table'  => 'post_buildings'
                    ,'alias' => 'PostBuildingsLeftJoin'
                    ,'type'  => 'LEFT'
                    ,'conditions' => [
                        'PostBuildingsLeftJoin.Post_id = Posts.id'
                    ]
                ]
            ];
            $this->paginate['finder']['search']['conditions'][] = [
                 // 自分の投稿か、他人のものであれば全公開か、自分の所属物件対象のもの
                 'OR' => [
                     'Posts.user_id' => $this->_login_user['id']
                     ,[
                         'Posts.user_id !=' => $this->_login_user['id']
                         ,'OR' => [
                             'PostBuildingsLeftJoin.id IS' => null ,
                             'PostBuildingsLeftJoin.building_id' => $this->_login_user['front_user']['building_id']
                         ]
                     ]
                 ]
            ];

            $posts = $this->paginate( 'Posts' );
            //dump( $posts );
            $this->set( compact( 'posts' ) );
        }
    }

    public function detail( $post_id=null )
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['type'] ) && $this->request->data['type'] === 'delete')
        {
            self::delete();
            $this->redirect([ 'controller' => 'Mypages', 'action' => 'index' ]);
        }
        elseif( $post_id !== null )
        {
            $where = [
                'Posts.contractant_id' => $this->_contractant_id
                ,'Posts.id'            => $post_id
                ,'Users.deleted IS'    => NULL
            ];
            $posts = $this->Posts->getPosts( $where );

            $this->set( compact( 'posts' ) );
        }
    }

    public function comment( $post_id=null )
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            $request = $this->request->data;

            $data = [
                'contractant_id' => $this->_contractant_id
                ,'user_id'       => $this->_login_user['id']
                ,'post_id'       => $request['post_id']
                ,'comment'       => $request['comment']
            ];
            if( isset( $request['parent_comment_id'] ) && $request['parent_comment_id'] !== '' ) $data['parent_comment_id'] = $request['parent_comment_id'];
            $res = $this->PostComments->saveData( $data );
            if( $res )
            {
                $posts = $this->Posts->findById( $post_id );
                if( $posts->isEmpty() === false )
                {
                    $post = $posts->first();
                    $log_data = [
                         'contractant_id' => $this->_contractant_id
                        ,'user_id'        => $this->_login_user['id']
                        ,'activity_type'  => 'comment'
                        ,'post_id'        => $post->id
                        ,'target_user_id' => $post->user_id
                    ];
                    $this->PostActivityLogs->saveData( $log_data );
                }

                // コメントポイント
                parent::checkGrantPoint( 'comment', $this->_date_for_point['last_comment_date'] );
            }

            $this->redirect([ $post_id ]);
        }
        else
        {
            $param = [
                'post_id'         => $post_id
                ,'contractant_id' => $this->_contractant_id
            ];

            $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $page  = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 0;

            $comment = $this->PostComments->getComments( $param, $limit, $page );
            $this->set( compact( 'comment', 'post_id' ) );
        }
    }

    public function paging()
    {
        if( $this->request->is(['ajax']) && $this->request->getQuery('page') )
        {
            $this->autoRender = false;

            $page = $this->request->getQuery('page');
            $where = [
                'Posts.contractant_id' => $this->_contractant_id
                // ,'Posts.building_id' => 2
                ,'Users.deleted IS'    => NULL
            ];
            $posts = $this->Posts->getPosts( $where, 20, $page );
            $this->response->body( json_encode( $posts, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
        }
    }

    public function setActivity()
    {
        if( $this->request->is( 'ajax' ) )
        {
            $this->autoRender = false;

            $query_param = $this->request->getQuery();
            $post = $this->Posts->findById( $query_param['target_id'] );

            if( $post->isEmpty() ) exit;

            $post = $post->first();
            if( isset( $query_param['action'] ) )
            {
                $action_data = [
                    'contractant_id'  => $this->_contractant_id
                    ,'user_id'        => $query_param['user_id']
                    ,'post_id' => $post->id
                ];

                $action = $query_param['action'];
                $err_flg = false;
                switch ( $action )
                {
                case 'like' :
                    $res_like = $this->PostLikes->saveData( $action_data );

                    // いいねポイント
                    if( $res_like === 'true' )
                    {
                        parent::checkGrantPoint( 'like', $this->_date_for_point['last_like_date']);
                    }

                    break;
                case 'comment' :
                    $action_data['comment'] = $query_param['comment'];

                    // コメントにコメント
                    if( isset( $query_param['parent_comment_id'] ) ) $action_data['parent_comment_id'] = $query_param['parent_comment_id'];
                    $this->PostComments->saveData( $action_data );

                    // コメントポイント
                    parent::checkGrantPoint( 'comment', $this->_date_for_point['last_comment_date']);

                    break;
                case 'join' :
                    // todo 投稿がイベントかのチェックをした方がいいか？
                    $this->PostJoins->saveData( $action_data );
                    break;
                case 'comment_like' :
                    $action_data['comment_id'] = $query_param['comment_id'];
                    $res_like = $this->PostCommentLikes->saveData( $action_data );

                    // いいねポイント
                    if( $res_like === 'true' )
                    {
                        parent::checkGrantPoint( 'like', $this->_date_for_point['last_like_date'] );
                    }

                    break;
                default :
                    $err_flg = true;
                }

                if( $err_flg === false )
                {
                    $log_data = [
                         'contractant_id' => $this->_contractant_id
                        ,'user_id'        => $query_param['user_id']
                        ,'activity_type'  => $action
                        ,'post_id'        => $post->id
                        ,'target_user_id' => $post->user_id
                    ];
                    $this->PostActivityLogs->saveData( $log_data );
                    $this->response->body( json_encode( ['result' => true], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
                }
                else
                {
                    $this->response->body( json_encode( ['result' => false], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
                }

            }
        }
    }

    public function delete( $data=[] )
    {
        $request = $this->request->data;
        if( isset( $request['target_id'] ) )
        {
            $target_param = [
                'contractant_id' => $this->_contractant_id
                ,'id'            => $request['target_id']
            ];
            $data = $this->Posts->find()->where( $target_param )->first();
            // 自分の投稿かチェック
            if( $data !== null && $data->user_id === $this->_login_user['id'] && $data->front_user_id === $this->_login_user['front_user']['id'] )
            {
                $del_data = [
                    'id' => $data->id
                    ,'deleted' => date('Y-m-d H:i:s')
                ];
                $this->Posts->saveData( $del_data );
                return $this->Flash->success(__('削除しました。') );
            }
            else
            {
                return $this->Flash->error(__('削除に失敗しました。') );
            }

        }
        else
        {
            return $this->Flash->error(__('削除に失敗しました。') );
        }

    }

    //public function message( $user_id, $front_user_id, $target_user_id, $target_front_user_id )
    //{
    //     $this->autoRender = false;
    //     $user        = $this->FrontUsers->checkExistValidUser($user_id, $front_user_id);
    //     $target_user = $this->FrontUsers->checkExistValidUser($target_user_id, $target_front_user_id);

    //     if( $user && $target_user && $user->id !== $target_user->id )
    //     {
    //         $thread = $this->Threads->getDataByTitle(  $user_id, $front_user_id, $target_user_id, $target_front_user_id  );

    //         $url = '/messages/detail/' . $thread->chat_group_id . DS . $thread->id . DS . $target_user_id . DS . $target_front_user_id;
    //         $this->redirect(['controller' => 'Messages', 'action' => 'detail', $thread->chat_group_id, $thread->id, $target_user_id, $target_front_user_id]);
    //     }
    //}
}
