<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Log\Log;

use Cake\Auth\DefaultPasswordHasher;

class ApiController extends AppController
{
    //private $_model;
    //private $_unique_parameter;
    //private $_contractant_id;
    public $_model;
    public $_unique_parameter;
    public $_contractant_id;
    public $_domain;
    public $_access_token;
    public $_access_token_expiration_date;
    public $_refresh_token;
    public $_response_body = true;
    public $_ex_method = [
        'get_token'
        ,'get_refresh_token'
    ];

    public function initialize()
    {
        //Log::debug( 'FILES log ------------------------------------------------------------------------', 'api' );
        //Log::debug( json_decode( json_encode( $_FILES, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
        //Log::debug( 'POST log ------------------------------------------------------------------------', 'api' );
        //Log::debug( json_decode( json_encode( $_POST, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
        Log::debug( 'name -----------------', 'api' );
        Log::debug( file_get_contents("php://input"), 'api' );   

    }
    public function beforeFilter(Event $event)
    {
        $this->autoRender = false;
        $this->response->type( 'application/json' );
        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') API Start ---', 'api');
    }
    public function v1( $version='0', $method='get' )
    {
        // xdebugをoff
        ini_set( 'xdebug.default_enable', 0 );
        $request = $this->request->data;

        Log::debug( 'api method -----------------', 'api' );
        Log::debug( 'method:: ' . $method, 'api' );
        Log::debug( 'api request -----------------', 'api' );
        Log::debug( json_decode( json_encode( $request, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
        //Log::debug( 'FILES log ------------------------------------------------------------------------', 'api' );
        //Log::debug( json_decode( json_encode( $_FILES, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
        //Log::debug( 'POST log ------------------------------------------------------------------------', 'api' );
        //Log::debug( json_decode( json_encode( $_POST, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );

        // access_tokenのチェック
        Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') API Access Token Check  ---', 'api');
        $res = self::check_token( $method, $request );
        if ( $res !== false  || ( isset( $request['unique_parameter'] ) && $request['unique_parameter'] === 'sumaiful' ) )
        {
            Log::debug('check_token $res ---------------------------------', 'api');
            Log::debug($res, 'api');
            if ( $res === true )
            {
                Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') API Access Token OK ---', 'api');
                switch( $version )
                {
                    case '0':
                    default:
                        // ユニークパラメーターのチェック
                        $this->checkUniqueParameter( $method, $request );

                        // アクセスポイント
                        self::check_point( 'access', $request );

                        // 共通の場合
                        if ( in_array( $method, [ 'get', 'save', 'delete' ], true ) )
                        {
                            // パラメータのチェック
                            $res = $this->checkParam( $method, $request );
                            if ( $res === true )
                            {
                                $this->_model = TableRegistry::getTableLocator()->get( $request['model'] );
                                unset( $request['model'] );
                            }
                            else
                            {
                                return self::error_response( '004' );
                            }
                        }
                        $result = $this->setAction( $method, $request );
                        if( ENV === 'dev' )
                        {
                            Log::debug( json_decode( json_encode( $result, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
                        }

                        if ( $this->_response_body === true )
                        {
                            self::response( $result );
                        }

                    break;
                }
            }
            else
            {
                Log::debug(__CLASS__ . ':' . __FUNCTION__ . ' called:(' . __LINE__ . ') API Access Token NG ---', 'api');
                if ( $this->_response_body === true )
                {
                    Log::debug( json_decode( json_encode( $res, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
                    $this->response->body( json_encode( $res, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
                }
            }
        }
        else
        {
            return self::error_response( '005' );
        }
    }

    public function get_token()
    {
        $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
        $this->loadComponent( 'Common' );
        $access_token  = $this->Common->createHash();
        $refresh_token = $this->Common->createHash();
        return $this->Tokens->getToken( $access_token, $refresh_token );
    }

    public function get_refresh_token()
    {
        $request = $this->request->data;
        // refresh_tokenの確認
        if ( isset( $request['refresh_token'] ) ) 
        {
            $this->Tokens     = TableRegistry::getTableLocator()->get( 'Tokens' );
            $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );
            // 有効性の確認
            if ( $this->Tokens->checkRefreshToken( $request['refresh_token'] ) )
            {
                // access_tokenとrefresh_tokenを再発行
                $result = self::get_token();
                // front_userのaccess_tokenとrefresh_tokenを更新
                // 元のrefresh_tokenからaccess_tokenを取得
                $access_token = $this->Tokens->getAccessTokenFromRefreshToken( $request['refresh_token'], $result );
                // access_tokenからユーザーを検索し、該当するユーザーがいたらaccess_tokenを更新する
                $this->FrontUsers->updateAccessToken( $access_token, $result['access_token'] );

                return $result;
            }
            else
            {
                // 再度ログインを要求
                return self::error_response( '002' );
            }
        }
        // リフレッシュトークンが設定されていない場合、再度ログインを要求
        return self::error_response( '002' );
    }

    public function check_token( $method, $request )
    {
        // 除外するmethodの場合は、trueを返す
        if ( in_array( $method, $this->_ex_method, true ) === false )
        {
            $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
            // access_tokenの確認
            if ( isset( $request['access_token'] ) )
            {
                // 有効性の確認
                if ( $this->Tokens->checkAccessToken( $request['access_token'] ) )
                {
                    return true;
                }
                else
                {
                    Log::debug('check token 001 return', 'api');
                    // refresh_tokenを要求
                    return self::error_response( '001' );
                }
            }
            return false;
        }
        return true;
    }
    public function get_encryption_parameters()
    {
        $this->EncryptionParameters = TableRegistry::getTableLocator()->get( 'EncryptionParameters' );
        $this->loadComponent( 'Common' );
        $iv   = $this->Common::createRandomBytes( 16 );
        $salt = $this->Common::createRandomBytes( 32 );
        return $this->EncryptionParameters->getEncryptionParameters( $iv, $salt, true );
    }

    // 正常response
    public function response( $result )
    {
        $data = [];
        $data['data']    = $result;
        $data['code']    = '200';
        $data['status']  = 'success';
        $data['message'] = '';
        //if ( isset( $this->_access_token ) )                 $data['access_token']                 = $this->_access_token;
        //if ( isset( $this->_access_token_expiration_date ) ) $data['access_token_expiration_date'] = $this->_access_token_expiration_date;
        //if ( isset( $this->_refresh_token ) )                $data['refresh_token']                = $this->_refresh_token;
        $this->response->body( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
    }
    public function error_response( $code, $err=null, $msg=null )
    {
        switch( $code )
        {
        case '001':
            $status  = 'request refresh token';
            $message = 'リフレッシュトークンを送信してください。';
            break;
        case '002':
            $status  = 'request login';
            $message = '再度ログインしてください。';
            break;
        case '003':
            $status  = 'invalid data parameter';
            $message = 'dataパラメータが不正です。';
            break;
        case '004':
            $status  = 'invalid unique parameter';
            $message = 'uniqueパラメータが不正です。';
            break;
        case '005':
            $status  = 'invalid access token';
            $message = 'アクセストークンが不正です。';
            break;
        case '006':
            $status  = 'specified method does not exist';
            $message = '指定されたmethodは存在しません。get,save,deleteのいずれかで指定してください';
            break;
        case '007':
            $status  = 'post-type features are not available';
            $message = 'ポストタイプの機能は利用できません。';
            break;
        case '008':
            $status  = 'invalid action';
            $message = 'アクションの値が不正です。';
            break;
        case '009':
            $status  = 'faild to regist data';
            $message = 'データの登録に失敗しました。';
            break;
        case '010':
            $status  = 'please enter your login ID or password';
            $message = 'ログインIDまたはパスワードを入力してください。';
            break;
        case '011':
            $status  = 'invalid encryption_parameter_id';
            $message = '暗号化パラメーターIDが不正です。';
            break;
        case '012':
            $status  = 'login ID or password is incorrect';
            $message = 'ログインIDまたはパスワードが正しくありません。';
            break;
        case '013':
            $status  = 'data is not exist';
            $message = 'データが存在しません。';
            break;
        case '014':
            $status  = 'faild to edit data';
            $message = 'データの編集に失敗しました。';
            break;
        case '015':
            $status  = 'faild to delete data';
            $message = 'データの削除に失敗しました。';
            break;
        case '016':
            $status  = 'faild to get user point';
            $message = 'ポイント取得に失敗しました。';
            break;
        case '017':
            $status  = 'faild to check user point';
            $message = 'ポイントチェック中にエラーが発生しました。';
        case '018':
            $status  = 'faild to get building information';
            $message = '建物の情報取得に失敗しました。';
            break;
        default:
            $status  = 'othre error';
            $message = 'DBエラー等のその他のエラーです。messageを確認してください。';
            break;
        }

        if ( $err !== null )
        {
            $message .= "\n" . json_encode( $err, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  );
        }
        if ( $msg !== null )
        {
            $message = $msg;
        }
        $data = [
            'data'     => null
            ,'code'    => $code
            ,'status'  => $status
            ,'message' => $message
        ];
        $tmp = $data;
        $this->_response_body = false;
        Log::debug( 'error response data ---------------------', 'api' );
        Log::debug( json_decode( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
        $this->response->body( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
        return $tmp;
    }

    public function get( $request )
    {
        $query = $this->_model->find()
            ->where( [
                'contractant_id' => $this->_contractant_id
                ,'deleted IS'    => null
            ] );
        if ( count( $request ) > 0 )
        {
            foreach( $request as $key => $val )
            {
                if ( in_array( $key, [ 'where', 'limit', 'offset', 'page', 'contain', 'fields', 'group', 'having', 'join', 'order' ] ) )
                {
                    $query->{$key}( $val );
                }
            }
        }
        return $query->all();
    }
    public function save( $request )
    {
        return $this->_model->saveData( $request['data'] );
    }
    public function delete( $request )
    {
        $res = $this->_model->deleteData( $request['id'] );
        return ( $res === null ) ? true : false;
    }
    public static function __callStatic( $name, $arguments )
    {
        return self::error_response( '006' );
    }

    private function checkParam( $method, $request )
    {
        $error_msg = '';
        if ( isset ( $request['model'] ) === false || preg_match( '/^[a-zA-Z0-9_-]*$/', $request['model'] ) === 0 )
        {
            $error_msg .= 'modelを指定してください';
        }

        switch( $method )
        {
        case 'get':
            break;
        case 'save':
            if ( isset( $request['data'] ) === false || is_array( $request['data'] ) === false )
            {
                if ( $error_msg !== '' ) $error_msg .= "\n";
                $error_msg .= 'method:saveの場合は、dataをarrayで指定してください。';
            }
            break;
        case 'delete':
            if ( isset( $request['id'] ) === false || preg_match( '/^[\d]+$/', $request['id'] ) === 0 )
            {
                if ( $error_msg !== '' ) $error_msg .= "\n";
                $error_msg .= 'method:deleteの場合は、idを指定してください。';
            }
            break;
        default:
            if ( $error_msg !== '' ) $error_msg .= "\n";
            $error_msg .= 'methodを指定してください。';
            break;
        }
        return ( $error_msg === '' ) ? true : $error_msg;
    }

    private function checkUniqueParameter( $method, $request=[] )
    {
        // 除外するmethodの場合は、trueを返す
        if ( in_array( $method, $this->_ex_method, true ) === false )
        {
            $error_msg = '';
            if ( isset ( $request['unique_parameter'] ) === false || preg_match( '/^[a-zA-Z0-9_-]*$/', $request['unique_parameter'] ) === 0 )
            {
                if ( $error_msg !== '' ) $error_msg .= "\n";
                $error_msg .= 'unique_parameterを指定してください。';
            }
            else
            {
                // unique_parameterからcontractant_idを取得する
                $this->Contractants    = TableRegistry::getTableLocator()->get( 'Contractants' );
                $res = $this->Contractants->getDataByUniqueParameter( $request['unique_parameter'] );
                if ( $res === null )
                {
                    if ( $error_msg !== '' ) $error_msg .= "\n";
                    $error_msg .= 'unique_parameterが不正です。';
                }
                else
                {
                    $this->_contractant_id = $res->id;
                    $this->_domain         = $res->domain;
                }
            }
            return ( $error_msg === '' ) ? true : self::error_response( '004', null, $error_msg );
        }
        return true;
    }

    /**********
     *
     * ページ
     *
    **********/
    public function top()
    {
        $request = $this->request->data;

        $params = [
            'contractant_id' => $this->_contractant_id
        ];

        $data = [];
        $unique_parameter = $request['unique_parameter'];

        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $this->Tokens                  = TableRegistry::getTableLocator()->get( 'Tokens' );
        $this->UserDeviceTokens        = TableRegistry::getTableLocator()->get( 'UserDeviceTokens' );
        $this->Informations            = TableRegistry::getTableLocator()->get( 'LabelInformations' );

        // インフォーメーションサービスが有効になっているかチェック
        $valid_service_menuns = $this->ContractantServiceMenus->getServiceMenuIdsByContractId( $this->_contractant_id );

        // アクセストークンからfront_userを逆引きし、建物IDを取得する
        $front_user = $this->Tokens->getFrontUserByAccessTokens( $request['access_token'] );

        // LabelInformations;
        if( in_array( 13, $valid_service_menuns, true ) )
        {
            // ページング
            if( isset( $request['page'] ) && preg_match( '/^[0-9]+$/', $request['page'] ) === 1 )
            {
                $params['page'] = $request['page'];
            }
            // 一回の件数
            if( isset( $request['limit'] ) && preg_match( '/^[0-9]+$/', $request['limit'] ) === 1 )
            {
                $params['limit'] = $request['limit'];
            }

            if( isset( $request['user_type'] ) )
            {
                $params['user_type'] = $request['user_type'];
            }

            $data['information'] = $this->Informations->getFrontInformations( $params );
        }
        // Informations;
        elseif( in_array( 14, $valid_service_menuns, true ) )
        {
            $params = [
                'limit' => 10
                ,'building_id'  => $front_user['building_id']
                ,'access_token' => $request['access_token']
            ];
            $data['information'] = self::get_informations( $params );
        }

        // 画像共有があるか
        if( in_array( 15, $valid_service_menuns, true ) )
        {
            $params = [
                'contractant_id' => $this->_contractant_id
                ,'limit'         => 5
                ,'building_id'   => $front_user['building_id']
            ];
            $data['image_sharing'] = self::get_image_sharings( $params );
        }

        // デバイストークンの対応
        $device_token = ( isset( $request['device_token'] ) && strlen( $request['device_token'] ) > 0 ) ? $request['device_token'] : '';
        $front_user_id = $this->Tokens->getFrontUserIdByAccessToken( $request['access_token'] );
        $tmp_data = [
            'contractant_id' => $this->_contractant_id
            ,'front_user_id' => $front_user_id
            ,'os'            => $request['os']
            ,'device_token'  => $device_token
        ];
        $this->UserDeviceTokens->saveData( $tmp_data );

        return $data;
    }

    public function homekit( $request )
    {
        $this->app_screen_id = 2;
        $data = [];
        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );

        $data = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_contractant_id, $this->app_screen_id );

        return $data;
    }

    public function concierge( $request )
    {
        $this->app_screen_id = 3;
        $data = [];
        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );

        $user_type = ( isset( $request['user_type'] ) ) ? $request['user_type'] : null;

        // todo user_typeで取得させるメニューを制御
        $data = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_contractant_id, $this->app_screen_id, $user_type )->toArray();

        // 問合せ
        //$contact = $this->ContractantServiceMenus->getContactMenus( $this->_contractant_id, $user_type );
        //if( $contact )
        //{
        //    $data[] = $contact;
        //}

        //アンケートデータ
        $this->MenuEnquetes = TableRegistry::getTableLocator()->get( 'MenuEnquetes' );
        $enquetes = $this->MenuEnquetes->getDataByContractantId( $this->_contractant_id, true );
        foreach( $data as $key => $val )
        {
            if( $val->service_menu_id === 9 )
            {
                $data[$key]['menu_enquetes'] = $enquetes;
            }
        }

        return $data;
    }

    // 設備仕様書の取得メソッド
    public function manuals( $request )
    {
        if( isset( $request['contractant_service_menu_id'] ) )
        {
            // pdf一覧を取得
            if( isset( $request['category_id'] ) )
            {
                $MenuManuals = TableRegistry::getTableLocator()->get( 'MenuManuals' );
                $where = [
                    'MenuManuals.contractant_service_menu_id' => $request['contractant_service_menu_id']
                    ,'MenuManuals.category_id'                => $request['category_id']
                ];

                $select = [
                    'id'
                    ,'equipment_name'
                    ,'created'
                    ,'modified'
                ];
                $res = $MenuManuals->getDetailByContractantSeriviceMenuId( $request['contractant_service_menu_id'], $where, $select );
            }
            // カテゴリ一覧を取得
            else
            {
                $MenuCategories = TableRegistry::getTableLocator()->get( 'MenuCategories' );
                $select = [
                    'id'
                    ,'name'
                    ,'icon_path'
                ];
                $where = [
                    'contractant_id' => $this->_contractant_id
                    ,'display_flg'   => 1
                ];
                $res = $MenuCategories->getCategoryByServiceMenuId( $request['contractant_service_menu_id'], $where, false, $select );

            }
            return $res;
        }
        else
        {
            //エラー
            return self::error_response( '003', $request );
        }
    }

    public function enqueteList( $request )
    {
        $this->app_screen_id = 3;
        $data = [];

        if( isset( $request['service_menu_id'] ) && preg_match( '/^[\d]+$/', $request['service_menu_id'] ) === 1 )
        {
            $this->MenuEnquetes = TableRegistry::getTableLocator()->get( 'MenuEnquetes' );
            $data = $this->MenuEnquetes->getDataByContractantSeriviceMenuId( $request['service_menu_id'], true );
        }

        return $data;
    }

    public function more_sumaiful( $request )
    {
        $data = [];

        $params = [
            'contractant_id' => $this->_contractant_id
        ];
        // ページング
        if( isset( $request['page'] ) && preg_match( '/^[0-9]+$/', $request['page'] ) === 1 )
        {
            $params['page'] = $request['page'];
        }
        // 一回の件数
        if( isset( $request['limit'] ) && preg_match( '/^[0-9]+$/', $request['limit'] ) === 1 )
        {
            $params['limit'] = $request['limit'];
        }

        $this->Contents = TableRegistry::getTableLocator()->get( 'Contents' );
        $data = $this->Contents->getContentsList( $params );
 
        return $data;
    }

    //public function communications( $request )
    //{
    //    $this->app_screen_id = 5;
    //    $data = [];
    //    $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
    //    $this->ChatGroups              = TableRegistry::getTableLocator()->get( 'ChatGroups' );
    //    $this->ChatGroupMembers        = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
    //    $contractant_service_menus     = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_contractant_id, $this->app_screen_id );
    //    $chat_groups        = $this->ChatGroups->getByContractantId( $this->_contractant_id );
    //    $chat_group_members = $this->ChatGroupMembers->getByUserId( $chat_groups->id, $request['user_id'] );
    //    $data['contractant_service_menus'] = $contractant_service_menus;
    //    $data['chat_group_id'] = $chat_group_members->chat_group_id;
    //    return $data;

    //}

    public function threadList( $request )
    {
        $this->Threads = TableRegistry::getTableLocator()->get( 'Threads' );
        $data = $this->Threads->getData( $this->_contractant_id, $request['chat_group_id'], $request['contractant_service_menu_id'] );
        return $data;
    }

    public function thread( $request )
    {
        $this->ChatMessages = TableRegistry::getTableLocator()->get( 'ChatMessages' );
        $data = $this->ChatMessages->getData( $this->_contractant_id );
        return $data;
    }

    // 利用規約
    public function terms_of_service( $request )
    {
        $this->TermsOfServices = TableRegistry::getTableLocator()->get( 'TermsOfServices' );
        return  $this->TermsOfServices->getDataByContractantId( $this->_contractant_id );
    }

    // プライバシーポリシー
    public function privacy_policy( $request )
    {
        $this->PrivacyPolicies = TableRegistry::getTableLocator()->get( 'PrivacyPolicies' );
        return  $this->PrivacyPolicies->getDataByContractantId( $this->_contractant_id );
    }

    public function regist_customer( $request )
    {
        if( isset( $request['data'] ) )
        {
            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
            $data = $request['data'];
            $data['contractant_id'] = $this->_contractant_id;
            $data['type']           = 'front';
            $data['front_user']['contractant_id'] = $this->_contractant_id;

            // 家族認証キーを含む
            if( isset( $request['family_auth_key'] ) )
            {
                // ファミリーキーを持つフロントユーザーを検索
                $res = $this->Users->getFrontUserByFamilyAuthKey( $request['family_auth_key'] );
                if( $res === null )
                {
                    return [ 'error_message' => '正しい家族用承認キーを入力してください。' ];
                }
                else
                {
                    $data['front_user']['parent_user_id'] = $res->id;
                    return $this->Users->registFrontUser( $data );
                }

            }
            // 本人の申請
            else
            {
                // アソシエーションを利用してFrontUsersも登録する
                return $this->Users->registFrontUser( $data );
            }
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    // ログイン用
    public function login( $request )
    {
        try
        {
            //if( isset( $request['data'] ) === false ) throw new \Exception( '003' );

            $data = $request;

            $login_id                = ( isset( $data['login_id'] )                && strlen( $data['login_id'] )                > 0 ) ? $data['login_id']                : '';
            $password                = ( isset( $data['password'] )                && strlen( $data['password'] )                > 0 ) ? $data['password']                : '';
            $encryption_parameter_id = ( isset( $data['encryption_parameter_id'] ) && strlen( $data['encryption_parameter_id'] ) > 0 ) ? $data['encryption_parameter_id'] : '';
            $os                      = ( isset( $data['os'] )                      && strlen( $data['os'] )                      > 0 ) ? $data['os']                      : '';
            $device_token            = ( isset( $data['device_token'] )            && strlen( $data['device_token'] )            > 0 ) ? $data['device_token']            : '';

            if ( $login_id === '' || $password === '' ) throw new \Exception( '010' );

            $this->Users                = TableRegistry::getTableLocator()->get( 'Users' );
            $this->FrontUsers           = TableRegistry::getTableLocator()->get( 'FrontUsers' );
            $this->UserDeviceTokens     = TableRegistry::getTableLocator()->get( 'UserDeviceTokens' );
            $this->EncryptionParameters = TableRegistry::getTableLocator()->get( 'EncryptionParameters' );
            $this->Tokens               = TableRegistry::getTableLocator()->get( 'Tokens' );
            $this->ChatMemberlists      = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
            $this->loadComponent( 'Common' );

            // 暗号パラメータを取得
            $encryption_parameters = $this->EncryptionParameters->getDataById( $encryption_parameter_id );
            if ( $encryption_parameters === false ) throw new \Exception( '011' );
            // パスワードを復号化
            $password = $this->Common->decrypt( $password, Configure::read( 'Security.encryptKey' ), base64_decode( $encryption_parameters->iv ), base64_decode( $encryption_parameters->salt ) );
            $password = str_replace( Configure::read( 'Security.secret' ), '', $password );
            $where = [
                'Users.login_id' => $login_id
                ,'Users.contractant_id'  => $this->_contractant_id
                ,'Users.password IS NOT' => null
                ,'FrontUsers.id IS NOT'  => null
                ,'FrontUsers.auth_flg'   => 1
            ];
            // ユーザー取得
            $res = $this->Users->loginFrontUser( $where );
            $hasher = new DefaultPasswordHasher;
            // パスワードハッシュチェック
            if ( $res && $res->front_user !== null && $hasher->check( $password, $res->password ) )
            {
                // tokenにfront_user_idを登録
                $data = [
                    'access_token' => $data['access_token']
                    ,'front_user_id' => $res->front_user->id
                ];
                $this->Tokens->saveFrontUserId( $data );
                // access_tokenをfront_userに登録
                $data = [
                    'id' => $res->front_user->id
                    ,'access_token' => $data['access_token']
                    ,'os'           => $os
                    ,'device_token' => $device_token
                ];
                $this->FrontUsers->saveData( $data );

                // デバイストークンの保存
                $tmp_data = [
                    'contractant_id' => $this->_contractant_id
                    ,'front_user_id' => $res->front_user->id
                    ,'os'            => $os
                    ,'device_token'  => $device_token
                ];
                $this->UserDeviceTokens->saveData( $tmp_data );


                // チャットメンバーリストを作成
                $front_users = $this->FrontUsers->getAllData( $this->_contractant_id );
                $this->ChatMemberlists->createList( $this->_contractant_id, $res->id, $res->front_user->id, $front_users );

                $res = $res->toArray();
                unset( $res['password'] );
                return $res;
            }
            else
            {
                throw new \Exception( '012' );
            }

        }
        catch( \Exception $e )
        {
            return self::error_response( $e->getMessage() );
        }

    }
    public function login_customer( $request )
    {
        if( isset( $request['data'] ) )
        {
            $data = $request['data'];
            $data['Users.contractant_id'] = $this->_contractant_id;

            $password = $data['password'];
            unset( $data['password'] );

            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
            $res = $this->Users->loginFrontUser( $data );

            $hasher = new DefaultPasswordHasher;
            if( $res && $hasher->check( $password, $res->password ) )
            {
                unset( $res['password'] );
                return $res;
            }
            else
            {
                return self::error_response( '012', $request );
            }
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function get_front_user( $request )
    {
        $this->Users  = TableRegistry::getTableLocator()->get( 'Users' );
        $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
        if( isset( $request['user_id'] ) )
        {
            $res = $this->Users->getFrontUser( $request['user_id'] );
            return ( $res ) ? $res : self::error_response( '013', $request, 'ユーザーが存在しません' );
        }
        else
        {
            $front_user_id = $this->Tokens->getFrontUserIdByAccessToken( $request['access_token'] );
            $res = $this->Users->getFrontUserByAccessToken( $request['access_token'] );
            if ( $front_user_id !== false )
            {
                $res = $this->Users->getFrontUserByFrontUserId( $front_user_id );
                if ( $res !== false )
                {
                    return $res;
                }
                else
                {
                    return self::error_response( '005', $request );
                }
            }
            else
            {
                return self::error_response( '005', $request );
            }
        }
    }

    public function edit_user( $request )
    {
        if( isset( $request['data'] ) )
        {
            $data = $request['data'];

            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
            $user = $this->Users->getFrontUser( $data['user_id'], ['FrontUsers.common_user_flg' => 0] );
            if( $user )
            {
                // 画像の登録
                if( isset( $data['front_user']['files']['front_users'] ) )
                {
                    $data_front_user = $data['front_user'];
                    $this->loadComponent( 'File' );
                    $this->File->saveFile( $data_front_user, 'front_users' );
                    //$data['front_user']['thumbnail_path'] = $this->File->saveBase64Data( $data['front_user']['attachment'] );
                    if( isset( $data_front_user['files']['front_users']['file_path'] ) )
                    {
                        $data['front_user']['thumbnail_path'] = $data_front_user['files']['front_users']['file_path'];
                    }
                    unset( $data['front_user']['files'] );
                }
            
                $data['front_user']['id'] = $user->front_user->id;
                $edit_data = [
                    'id'          => $user->id
                    //,'type'       => 'front'
                    ,'nickname'   => $data['front_user']['nickname'] //暫定対応 20190507 nakagawa
                    ,'front_user' => $data['front_user']
                ];
                $res = $this->Users->saveData( $edit_data );
                return $res;
            }
        }
        else
        {
            return self::error_response( '003', $request );
        }

    }

    public function set_config_push_notification( $request )
    {
        if( isset( $request['data']['front_user_id'] ) )
        {
            if( in_array( $request['data']['target'], [ 'like', 'comment', 'comment_like', 'join', 'baggage', 'message', 'information', 'image_sharing' ], true ) )
            {
                $target = 'push_' . $request['data']['target'] . '_flg';
                $data = [
                    'id'              => $request['data']['front_user_id']
                    ,'contractant_id' => $this->_contractant_id
                    ,'user_id'        => $request['data']['user_id']
                    ,$target          => $request['data']['value']
                ];
                $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );
                $res = $this->FrontUsers->saveData( $data );
                if( $res )
                {
                    return $res;
                }
                else
                {
                    return self::error_response( '009', $request );
                }
            }
            else
            {
                return self::error_response( '003', $request, '許可されていないターゲットの値が指定されています' );
            }
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function generate_family_auth_key( $request )
    {
        if( isset( $request['data']['user_id'] ) )
        {
            $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );

            $duplilcate_flg = true;
            while( $duplilcate_flg  )
            {
                $key = parent::createTmpKey();
                // 重複がなければフラグが折れる
                $duplilcate_flg = $this->FrontUsers->checkDuplicateFamilyAuthKey( $key );
            }

            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
            $user = $this->Users->getFrontUser( $request['data']['user_id'] );

            // 承認キーを保存
            $data = [
                'id' => $user->front_user->id
                ,'family_auth_key' => $key
            ];
            $this->FrontUsers->saveData( $data );

            // キーを返す
            return [ 'family_auth_key' => $key ];
        }
    }


 /********* willing *********/
    public function post_category( $request )
    {
        // ポストタイプが有効になっているか。
        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $res = $this->ContractantServiceMenus->checkValidPostType( $this->_contractant_id );
        if( $res !== null )
        {
            $this->PostCategories = TableRegistry::getTableLocator()->get( 'PostCategories' );
            $res = $this->PostCategories->getCategoryByServiceMenuId( $res->id, [ 'PostCategories.display_flg' => 1 ] );
            return $res;
        }
        else
        {
            return self::error_response( '007' );
        }
    }

    // 物件の取得
    public function get_building_options( $request )
    {
        try
        {
            $this->Buildings = TableRegistry::getTableLocator()->get( 'Buildings' );
            return $this->Buildings->getOptionsForApi( $this->_contractant_id, true, $this->_domain );
        }
        catch( \Exception $e )
        {
            return self::error_response( null, $e->getMessage() );
        }
    }

    // 投稿
    public function community_post( $request )
    {
        // ポストタイプが有効になっているか。
        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $res = $this->ContractantServiceMenus->checkValidPostType( $this->_contractant_id );
        if( $res )
        {
            if( isset( $request['data'] ) )
            {
                $data = $request['data'];
                $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );

                // 編集・削除:
                if( isset( $request['type'] ) )
                {
                    // 対象のエンティティを取得
                    $target_param = [
                        'contractant_id' => $this->_contractant_id
                        ,'id'            => $data['post_id']
                        ,'user_id'       => $data['user_id']
                        ,'front_user_id' => $data['front_user_id']
                    ];
                    $res = $this->Posts->find()->where($target_param)->first();
                    if( $res )
                    {
                        if ( $request['type'] === 'edit' )
                        {
                            $target_data = [
                                'id'       => $res->id
                                ,'content' => $data['content']
                            ];

                            // 公開範囲指定用のパラメータ(API仕様書v1.3.0未満アプリの対応)
                            if( isset( $data['post_buildings']['building_id'] ) )
                            {
                                // 一旦すべてのリレーションを削除
                                $this->PostBuildings = TableRegistry::getTableLocator()->get( 'PostBuildings' );
                                $this->PostBuildings->deleteAll([
                                    'post_id' => $target_data['id'],
                                    'contractant_id' => $this->_contractant_id
                                ]);
                                $target_data['post_buildings']['building_id'] = $data['post_buildings']['building_id'];
                                $target_data['contractant_id'] = $this->_contractant_id;
                            }
                        }
                        elseif ( $request['type'] === 'delete' )
                        {
                            $target_data = [
                                'id'      => $res->id
                                ,'deleted'=> date('Y-m-d H:i:s')
                            ];
                        }
                        else
                        {
                            return self::error_response( '03', $request, 'typeの値が不正です' );
                        }

                        $res2 = $this->Posts->saveData( $target_data );
                        if( $res2 )
                        {
                            return true;
                        }
                        else
                        {
                            if( $request['type'] === 'edit' )
                            {
                                return self::error_response( '014', $request );
                            }
                            elseif( $request['type'] === 'edit' )
                            {
                                return self::error_response( '015', $request );
                            }
                        }
                    }
                    else
                    {
                        return self::error_response( '013', $request );
                    }


                }
                else
                {
                    try
                    {
                        $data['contractant_id'] = $this->_contractant_id;
                        $data['display_flg']    = 1;

                        // 画像の登録
                        if( isset( $data['files']['posts'] ) )
                        {
                            $this->loadComponent( 'File' );
                            $this->File->saveFile( $data, 'posts' );

                            if( isset( $data['files']['posts']['file_path'] ) )
                            {
                                $file_data = [
                                    'contractant_id' => $this->_contractant_id
                                    ,'name'          => $data['files']['posts']['name']
                                    ,'file_path'     => $data['files']['posts']['file_path']
                                ];
                                $this->Files = TableRegistry::getTableLocator()->get( 'Files' );
                                $file_id = $this->Files->saveData( $file_data );
                            }
                            //unset( $data['files'] );
                        }
                        //unset( $data['files'] );

                        $err = $this->Posts->api_validation( $data );
                        if( count( $err ) > 0 )
                        {
                            return self::error_response( '003', $err );
                        }
                        else
                        {
                            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
                            $user = $this->Users->getFrontUser( $data['user_id'] );
                            $data['front_user_id'] = $user->front_user->id;
                            $data['building_id']   = $user->front_user->building_id;

                            // 公開範囲指定用のパラメータがない場合、自分の属する物件IDを範囲とする(API仕様書v1.3.0未満アプリの対応)
                            if( isset( $data['post_buildings']['building_id'] ) === false )
                            {
                                $data['post_buildings']['building_id'][] = $user->front_user->building_id;
                            }

                            $post_id = $this->Posts->saveData( $data );

                            // 画像の中間ファイルの保存
                            if( isset( $file_id ) && $post_id )
                            {
                                $rel_data = [
                                    'contractant_id' => $this->_contractant_id
                                    ,'post_id' => $post_id
                                    ,'file_id' => $file_id
                                ];
                                $this->RelPostFiles = TableRegistry::getTableLocator()->get( 'RelPostFiles' );
                                $this->RelPostFiles->saveData( $rel_data );
                            }

                            // 投稿ポイント
                            self::check_point( 'post', $request );

                            return true;
                        }
                    }
                    catch( \Exception $e )
                    {
                        return self::error_response( null, $e->getMessage() );
                    }
                }
            }
            else
            {
                return self::error_response( '003', $request );
            }
        }
        else
        {
            return self::error_response( '007' );
        }
        
    }

    // ポストタイプに対する行動
    public function execute_activity( $request )
    {
        if( isset( $request['data'] ) )
        {
            $data = $request['data'];

            //post_idから投稿者IDを取得
            $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
            $posts = $this->Posts->findById( $data['post_id'] );

            if( $posts->isEmpty() ) return self::error_response( '009', $request );
            $posts = $posts->first();

            if( isset( $data['action'] ) )
            {
                $action_data = [
                    'contractant_id' => $this->_contractant_id
                    ,'user_id' => $data['user_id']
                    ,'post_id' => $posts->id
                ];

                $action     = $data['action'];
                $err_flg    = false;
                $create_flg = false;
                switch ( $action )
                {
                // イイね
                case 'like' :
                    $this->PostLikes = TableRegistry::getTableLocator()->get( 'PostLikes' );
                    $create_flg = $this->PostLikes->saveData( $action_data );

                    // likeポイント
                    if ( $create_flg === 'true' )
                    {
                        self::check_point( 'like', $request );
                    }

                    break;
                // コメント
                case 'comment' :
                    $this->PostComments = TableRegistry::getTableLocator()->get( 'PostComments' );
                    $action_data['comment'] = $data['comment'];

                    // コメントにコメント
                    if( isset( $data['parent_comment_id'] ) ) $action_data['parent_comment_id'] = $data['parent_comment_id'];
                    $this->PostComments->saveData( $action_data );

                    // コメントポイント
                    self::check_point( 'comment', $request );

                    $create_flg = 'true';
                    break;
                // 参加
                case 'join' :
                    // todo 投稿がイベント化のチェックをした方がいいか？
                    $this->PostJoins = TableRegistry::getTableLocator()->get( 'PostJoins' );
                    $create_flg = $this->PostJoins->saveData( $action_data );
                    break;
                case 'comment_like' :
                    $action_data['comment_id'] = $data['comment_id'];
                    $this->PostCommentLikes = TableRegistry::getTableLocator()->get( 'PostCommentLikes' );
                    $create_flg = $this->PostCommentLikes->saveData( $action_data );

                    // likeポイント
                    if ( $create_flg === 'true' )
                    {
                        self::check_point( 'like', $request );
                    }

                    break;
                default :
                    $err_flg = true;
                }

                if ( $err_flg === true )
                {
                    return self::error_response( '008' );
                }

                if( $create_flg === 'true' ) // 初期化がbool型でなぜstring?
                {
                    // アクティヴィティログにも書き込み
                    $this->PostActivityLogs = TableRegistry::getTableLocator()->get( 'PostActivityLogs' );
                    $log_data = [
                         'contractant_id' => $this->_contractant_id
                        ,'user_id'        => $data['user_id']
                        ,'activity_type'  => $action
                        ,'post_id'        => $posts->id
                        ,'target_user_id' => $posts->user_id
                    ];
                    $res = $this->PostActivityLogs->saveData( $log_data );

                    // この$resはbool? saveDataのリターンはIDでは？
                    if( $res )
                    {
                        // PUSH通知
                        // 自分以外が対象
                        if( (int)$data['user_id'] !== (int)$posts->user_id )
                        {
                            //ターゲットユーザーのPUSH設定をチェック
                            $this->FrontUsers =TableRegistry::getTableLocator()->get( 'FrontUsers' );
                            $push_check = $this->FrontUsers->checkConfigPushNotification( $posts->user_id, $this->_contractant_id, $action );
                            if( $create_flg === 'true' && $push_check )
                            {
                                $cmd = SHELL_COMMAND . ' push ' . $request['unique_parameter'] . ' post_activity ' . $res; // この$resに何を渡している？ID or bool ?
                                exec( $cmd, $out, $ret );
                            }
                        }
                        return true;
                    }
                    else
                    {
                        return self::error_response( '009', $request );
                    }
                }

            }
            else
            {
                return self::error_response( '008', $request );
            }

        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    // インフォメーションに対する行動
    public function execute_activity_information( $request )
    {
        if( isset( $request['data'] ) )
        {
            $data = $request['data'];

            //information_idから投稿者IDを取得
            $this->Informations = TableRegistry::getTableLocator()->get( 'Informations' );
            $informations = $this->Informations->get( $data['information_id'] );

            if( $informations === null ) return self::error_response( '009', $request );

            if( isset( $data['action'] ) )
            {
                $action_data = [
                    'contractant_id'  => $this->_contractant_id
                    ,'user_id'        => $data['user_id']
                    ,'information_id' => $informations->id
                ];

                $action = $data['action'];
                $err_flg = false;
                switch ( $action )
                {
                case 'like' :
                    $this->InformationLikes = TableRegistry::getTableLocator()->get( 'InformationLikes' );
                    $this->InformationLikes->saveData( $action_data );
                    break;
                case 'comment' :
                    $this->InformationComments = TableRegistry::getTableLocator()->get( 'InformationComments' );
                    $action_data['comment'] = $data['comment'];

                    // コメントにコメント
                    if( isset( $data['parent_comment_id'] ) ) $action_data['parent_comment_id'] = $data['parent_comment_id'];
                    $this->InformationComments->saveData( $action_data );
                    break;
                case 'join' :
                    // todo 投稿がイベント化のチェックをした方がいいか？
                    $this->InformationJoins = TableRegistry::getTableLocator()->get( 'InformationJoins' );
                    $this->InformationJoins->saveData( $action_data );
                    break;
                case 'comment_like' :
                    $action_data['comment_id'] = $data['comment_id'];
                    $this->InformationCommentLikes = TableRegistry::getTableLocator()->get( 'InformationCommentLikes' );
                    $this->InformationCommentLikes->saveData( $action_data );
                    break;
                default :
                    $err_flg = true;
                }

                if( $err_flg === false )
                {
                    return true;
                }
                else
                {
                    return self::error_response( '009', $request );
                }

            }
            else
            {
                return self::error_response( '008', $request );
            }

        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    // 荷物受け取り
    public function receive_baggage( $request )
    {
        if( isset( $request['data'] ) && isset( $request['data']['user_id'] ) && isset( $request['data']['front_user_id'] ) )
        {
            $data = $request['data'];
            $this->LuggageStrages = TableRegistry::getTableLocator()->get( 'LuggageStrages' );
            $param = [
                'contractant_id' => $this->_contractant_id
                ,'user_id'       => $data['user_id']
                ,'front_user_id' => $data['front_user_id']
            ];
            return $this->LuggageStrages->setFlagOn( $param );
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function set_presentation( $request )
    {
        $this->Presentations = TableRegistry::getTableLocator()->get( 'Presentations' );
        if( isset( $request['data']['post_id'] ) )
        {
            // 投稿のチェック
            $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
            $posts = $this->Posts->findById( $request['data']['post_id'] );
            if( $posts->isEmpty() === false )
            {
                 $post = $posts->first();
                 $data = [
                     'contractant_id' => $this->_contractant_id
                     ,'type'          => $request['data']['type']
                     ,'user_id'       => $request['data']['user_id']
                     ,'post_id'       => $post->id
                     ,'content'       => $post->content
                     // ポストが持つ建物情報も保存する
                     ,'presentation_buildings' => [
                         [
                             'contractant_id' => $post->contractant_id
                             ,'building_id'   => $post->building_id
                         ]
                     ]
                 ];
                 return  $this->Presentations->saveData( $data );
            }
        }
        elseif( isset( $request['data']['image_sharing_id'] ) )
        {
        }
        elseif( isset( $request['data']['file_id'] ) )
        {
            // 画像のチェック
            $this->Files = TableRegistry::getTableLocator()->get( 'Files' );
            $files = $this->Files->findById( $request['data']['file_id'] );
            if( $files->isEmpty() === false )
            {
                $file = $this->Files->getImageSharingData( $request['data']['file_id'] );
                $data = [
                    'contractant_id' => $this->_contractant_id
                    ,'type'          => $request['data']['type']
                    ,'user_id'       => $request['data']['user_id']
                    ,'file_id'       => $file->id
                    ,'album_title'   => $file->rel_imagesharing_file->image_sharing->title
                ];

                // アルバムが持つ建物情報も保存する
                $imageSharingBuilsings = TableRegistry::getTableLocator()->get( 'ImageSharingBuildings' );
                $image_sharing_buildings = $imageSharingBuilsings->getEditDataByImageSharingId( $this->_contractant_id, $file->rel_imagesharing_file->image_sharing_id );
                if( count( $image_sharing_buildings ) > 0 )
                {
                    foreach( $image_sharing_buildings as $val )
                    {
                        $data['presentation_buildings'][] = [
                            'contractant_id' => $this->_contractant_id
                            ,'building_id'   => $val
                        ];
                    }
                }
                // 登録がない場合、全建物なので、建物マスターを登録
                else
                {
                    $Buildings = TableRegistry::getTableLocator()->get('Buildings');
                    $arr_buildings = $Buildings->getOptions( $this->_contractant_id );
                    foreach( $arr_buildings as $key =>$val )
                    {
                        $data['presentation_buildings'][] = [
                            'contractant_id' => $this->_contractant_id
                            ,'building_id'   => $key
                        ];
                    }
                }

                return  $this->Presentations->saveData( $data );
            }
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function community( $request )
    {
        if( isset( $request['user_id'] ) )
        {
            $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
            $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
            $user = $this->Users->getFrontUser( $request['user_id'] );
            $building_id = $user['front_user']['building_id'];

            $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $page  = ( isset( $request['page'] ) )  ? $request['page']  : 1;

            $where = [
                'Posts.contractant_id' => $this->_contractant_id
                // ,'Posts.building_id' => 2
                ,'Posts.display_flg'    => 1
                ,'Users.deleted IS'     => NULL
                ,'FrontUsers.id IS NOT' => NULL
            ];

            // 検索絞り込み
            if( isset( $request['search'] ) && mb_strlen( trim( $request['search'] ) ) > 0 )
            {
                $tmp_str = mb_ereg_replace( "　", " ", trim( $request['search'] ) );
                $tmp_arr = explode( " ", $tmp_str );

                $arr_or = [];
                foreach( $tmp_arr as $key => $val )
                {
                    $arr_or[0][$key] = [ 'Posts.content LIKE' => '%' . $val . '%' ];
                    $arr_or[1][$key] = [ 'FrontUsers.nickname LIKE' => '%' . $val . '%' ] ;
                    $arr_or[2][$key] = [ 'Buildings.name LIKE' => '%' . $val . '%' ] ;
                    $arr_or[3][$key] = [ 'Buildings.name_en LIKE' => '%' . $val . '%' ] ;
                    $arr_or[4][$key] = [ 'Buildings.name_kana LIKE' => '%' . mb_convert_kana( $val, 'cHV' ) . '%' ] ;
                }
                $where['OR'] = $arr_or;
            }
            // カテゴリーの絞り込み
            if( isset( $request['post_category_id'] ) && preg_match( '/^[0-9]+$/', $request['post_category_id'] ) === 1 )
            {
                $where['Posts.post_category_id'] = $request['post_category_id'];
            }

            // 自分の投稿か、他人のものであれば全公開か、自分の所属物件対象のもの
            $where[] = [
                'OR' => [
                    'Posts.user_id' => $request['user_id']
                    ,[
                        'Posts.user_id !=' => $request['user_id']
                        ,'OR' => [
                            'PostBuildingsLeftJoin.id IS' => null ,
                            'PostBuildingsLeftJoin.building_id' => $building_id
                        ]
                    ]
                ]
            ];

            $res = $this->Posts->getPosts( $where, $limit, $page );
            return $res;
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    // sumaiful
    public function get_label_informations( $request )
    {
        $params = [
            'contractant_id' => $this->_contractant_id
        ];
        if( isset( $request['information_id'] ) && preg_match( '/^[0-9]+$/', $request['information_id'] ) )
        {
            $params['id'] = $request['information_id'];
        }
        else
        {
            $params['limit']  = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $params['page']   = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;
        }

        if( isset( $request['user_type'] ) )
        {
            $params['user_type'] = $request['user_type'];
        }

        $this->LabelInformations = TableRegistry::getTableLocator()->get( 'LabelInformations' );
        return $this->LabelInformations->ggetFrontInformationsetFrontInformations( $params );
    }

    public function get_informations( $request )
    {
        $params = [
            'contractant_id' => $this->_contractant_id
        ];

        // アクセストークンからfront_userを逆引き
        $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
        $front_user  = $this->Tokens->getFrontUserByAccessTokens( $request['access_token'] );

        // リクエストに建物IDがあるか
        if( isset( $request['building_id'] ) )
        {
            $params['building_id'] = $request['building_id'];
        }
        else
        {
            $params['building_id'] = $front_user->building_id;
        }

        if( isset( $request['information_id'] ) && preg_match( '/^[0-9]+$/', $request['information_id'] ) )
        {
            $params['id'] = $request['information_id'];
        }
        else
        {
            $params['limit']  = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $params['page']   = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;
        }

        // 検索絞り込み
        if( isset( $request['search'] ) && mb_strlen( trim( $request['search'] ) ) > 0 )
        {
            $tmp_str = mb_ereg_replace( "　", " ", trim( $request['search'] ) );
            $tmp_arr = explode( " ", $tmp_str );

            $search_arr = [];
            foreach( $tmp_arr as $key => $val )
            {
                $search_arr['OR'][0][$key] = [ 'Informations.title LIKE' => '%' . $val . '%' ];
                $search_arr['OR'][1][$key] = [ 'Informations.content LIKE' => '%' . $val . '%' ];
                $search_arr['OR'][2][$key] = [ 'BuildingsLeftJoin.name LIKE' => '%' . $val . '%' ] ;
                $search_arr['OR'][3][$key] = [ 'BuildingsLeftJoin.name_en LIKE' => '%' . $val . '%' ] ;
                $search_arr['OR'][4][$key] = [ 'BuildingsLeftJoin.name_kana LIKE' => '%' . mb_convert_kana( $val, 'cHV' ) . '%' ] ;
            }
            $params['search'] = $search_arr;
        }

        // カテゴリ
        if( isset( $request['type'] ) )
        {
            $params['type'] = $request['type'];
        }

        // 退去者の判別
        if( $front_user->leave_flg === 1 )
        {
            $params['Informations.reject_leave_readable_flg'] = 0;
        }


        $this->Informations = TableRegistry::getTableLocator()->get( 'Informations' );
        return $this->Informations->getFrontInformations( $params );
    }

    public function get_image_sharings( $request )
    {
        if( isset( $request['building_id'] ) )
        {
            $building_id = $request['building_id'];
        }
        else
        {
            // アクセストークンからfront_userを逆引きし、建物IDを取得する
            $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
            $front_user   = $this->Tokens->getFrontUserByAccessTokens( $request['access_token'] );
            $building_id  = $front_user->building_id;
        }
 
        $params = [
            'contractant_id' => $this->_contractant_id
            ,'building_id'   => $building_id
        ];

        $params['limit']  = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
        $params['page']   = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;

        $this->ImageSharings = TableRegistry::getTableLocator()->get( 'ImageSharings' );
        return  $this->ImageSharings->getAlbumData( $params );
    }

    public function get_notification( $request )
    {
        $data = [
            'message'   => 0
            ,'activity' => 0
            ,'baggage'  => 0
        ];
        if( isset( $request['user_id'] ) )
        {
            // メッセージの未読
            $this->ChatUnreads = TableRegistry::getTableLocator()->get( 'ChatUnreads' );
            $message_count = $this->ChatUnreads->checkUnread( $this->_contractant_id, $request['user_id'], $request['front_user_id'] );
            if( $message_count > 0 )
            {
                $data['message'] = 1;
            }
            // イイねページの未読
            $this->PostActivityLogs = TableRegistry::getTableLocator()->get( 'PostActivityLogs' );
            $activity_count = $this->PostActivityLogs->checkUnread( $request['user_id'] );
            if( $activity_count > 0 )
            {
                $data['activity'] = 1;
            }
            // 荷物預かりの未読
            $this->LuggageStrages = TableRegistry::getTableLocator()->get( 'LuggageStrages' );
            $strage_count = $this->LuggageStrages->checkUnreceive( $request['user_id'] );
            if( $strage_count > 0 )
            {
                $data['baggage'] = 1;
            }
        }
        return $data;
    }

    public function get_comments( $request )
    {
        if( isset( $request['post_id'] ) )
        {
            $param = [
                'post_id'         => $request['post_id']
                ,'contractant_id' => $this->_contractant_id
            ];

            $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $page  = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 0;

            $this->PostComments = TableRegistry::getTableLocator()->get( 'PostComments' );
            $res = $this->PostComments->getComments( $param, $limit, $page );
            return $res;
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function get_posts( $request )
    {
        $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
        if( isset( $request['user_id'] ) )
        {
            //// ユーザーIDで縛るか
            //if( isset( $request['user_id'] ) ) $where['Posts.user_id'] = $request['user_id'];
            // デフォルト20件
            //$limit  = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $page  = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;

            // アクセストークンからログインユーザ情報を取得
            $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
            $front_user = $this->Tokens->getFrontUserByAccessTokens( $request['access_token'] );

            // 本人ページではない場合、物件の公開範囲で絞る
            $where = [];
            if ( $front_user && (int)$request['user_id'] !== (int)$front_user['user_id'] )
            {
                $building_id = $front_user['building_id'];

                // 自分の投稿か、他人のものであれば全公開か、自分の所属物件対象のもの
                $where[] = [
                    'OR' => [
                        'PostBuildingsLeftJoin.id IS' => null ,
                        'PostBuildingsLeftJoin.building_id' => $building_id
                   ]
                ];
            }

            return $this->Posts->getPostsByUserId( $request['user_id'], $limit, $page, false, $where );
        }
        // 単体記事
        elseif( isset( $request['post_id'] ) )
        {
            $params = [
                'Posts.id' => $request['post_id']
                ,'Posts.contractant_id' => $this->_contractant_id
                ,'Posts.display_flg'    => 1
            ];

            return $this->Posts->getPosts( $params );
        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function get_activity( $request )
    {
        if( isset( $request['user_id'] ) )
        {
            // todo 他人の投稿への自分ののアクティビティを取得したい場合はここに
        }
        // 他人からの自分の投稿へのアクティビティ
        elseif( isset( $request['target_user_id'] ) )
        {
            $this->PostActivityLogs = TableRegistry::getTableLocator()->get( 'PostActivityLogs' );
            //  対象のデータに既読フラグを立てる
            $this->PostActivityLogs->FlagOnByUserId( $request['target_user_id'] );

            $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $page  = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;

            $cond = [
                'contractant_id'  => $this->_contractant_id
                ,'target_user_id' => $request['target_user_id']

            ];
            return $this->PostActivityLogs->getDataByTargetUserId( $cond, $limit, $page );

        }
        else
        {
            return self::error_response( '003', $request );
        }
    }

    public function get_memberlist( $request )
    {
        try
        {
            if ( isset( $request['user_id'] )       === false || preg_match( '/^[\d]+$/', $request['user_id'] )       === 0 ) return self::error_response( '003', $request, 'user_idを指定してください' );
            if ( isset( $request['front_user_id'] ) === false || preg_match( '/^[\d]+$/', $request['front_user_id'] ) === 0 ) return self::error_response( '003', $request, 'front_user_idを指定してください' );

            $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
            $page  = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;

            //// 投稿orインフォメーションのイイねしたユーザー
            //if( isset( $request['target_id'] ) && isset( $request['type'] ) )
            //{
            //    if( $request['type'] === 'post_likes' )
            //    {
            //        $target_model = 'PostLikes';
            //    }
            //    elseif( $request['type'] === 'information_likes' )
            //    {
            //        $target_model = 'InformationLikes';
            //    }
            //    $this->{$target_model} = TableRegistry::getTableLocator()->get( $target_model );
            //    $data = $this->{$target_model}->getMessageMemberListByUsersById( $this->_contractant_id, $request['target_id'], $limit, $page );
            //}
            //// メッセージページのユーザー
            //else
            //{
                $this->Users           = TableRegistry::getTableLocator()->get( 'Users' );
                $this->ChatMemberlists = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
                $where = [
                    'ChatMemberlists.contractant_id'      => $this->_contractant_id
                    ,'ChatMemberlists.self_user_id'       => $request['user_id']
                    ,'ChatMemberlists.self_front_user_id' => $request['front_user_id']
                    ,'ChatMemberlists.deleted IS'         => null 
                ];
                // 検索
                if( isset( $request['search'] ) ) 
                {
                    // 全角スペースを半角スペースに変換してスライス
                    $tmp = explode( ' ', mb_convert_kana( $request['search'], 's', 'UTF-8' ) );
                    foreach( $tmp as $key => $val )
                    {
                        if ( strlen( trim( $val ) ) > 0 )
                        {
                            $where['OR'] = [
                                'PartnerFrontUser.nickname LIKE'         => '%' . $val . '%'
                                ,'PartnerFrontUser.biography LIKE'       => '%' . $val . '%'
                                ,'PartnerFrontUser.university_name LIKE' => '%' . $val . '%'
                                ,'PartnerFrontUser.undergraduate LIKE'   => '%' . $val . '%'
                                ,'PartnerFrontUser.club_name LIKE'       => '%' . $val . '%'
                                ,'PartnerFrontUser.hobby LIKE'           => '%' . $val . '%'
                                ,'Buildings.name LIKE'                   => '%' . $val . '%'
                                ,'Buildings.name_en LIKE'                => '%' . $val . '%'
                                ,'Buildings.name_kana LIKE'              => '%' . mb_convert_kana( $val, 'cHV' ) . '%'
                            ];
                        }
                    }
                }
                //// 未読のユーザーを取得する
                //if ( (int)$page === 1 )
                //{
                //    // 未読リストからthread_idを取得、chat_group_idから相手のfront_user_idを取得
                //    // でリストを作成する
                //    $this->ChatUnreads = TableRegistry::getTableLocator()->get( 'ChatUnreads' );
                //    $unread_users = $this->ChatUnreads->getMemberlist( $this->_contractant_id, $request['user_id'], $request['front_user_id'] );
                //    if ( count( $unread_users ) > 0 )
                //    {
                //        $tmp_where = $where;
                //        foreach( $unread_users as $key => $val )
                //        {
                //            $where['OR']['FrontUsers.id']  = $val;
                //            $tmp_where['FrontUsers.id !='] = $val;
                //        }
                //        $data1 = $this->Users->getFrontUser( 'all', $where, $limit, $page );
                //        $data2 = $this->Users->getFrontUser( 'all', $tmp_where, $limit, $page );
                //        $data = [];
                //        foreach( $data1->toArray() as $key => $val )
                //        {
                //            $val['unread_flg'] = 1;
                //            $data[] = $val;
                //        }
                //        foreach( $data2->toArray() as $key => $val )
                //        {
                //            $data[] = $val;
                //        }
                //        $data = self::get_chat_group( $request['user_id'], $request['front_user_id'], $data );
                //    }
                //    else
                //    {
                //        // ユーザー取得
                //        $data = $this->Users->getFrontUser( 'all', $where, $limit, $page );
                //        $data = self::get_chat_group( $request['user_id'], $request['front_user_id'], $data->toArray() );
                //    }
                //}
                //else
                //{
                    // ユーザー取得
                    //$data = $this->Users->getFrontUser( 'all', $where, $limit, $page );
                    $data = $this->ChatMemberlists->getList( $where, $limit, $page );
                    $data = self::get_chat_group( $request['user_id'], $request['front_user_id'], $data );
                //}
            //}

            return $data;
        }
        catch( \Exception $e )
        {
            return self::error_response( $e->getMessage() );
        }
    }

    public function get_message( $request )
    {
        $this->ChatMessages     = TableRegistry::getTableLocator()->get( 'ChatMessages' );
        $this->ChatUnreads      = TableRegistry::getTableLocator()->get( 'ChatUnreads' );
        $this->ChatGroupMembers = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->ChatMemberlists  = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
        $contractant_service_menu_id = ( $this->_contractant_id === 4 ) ? 58 : 0; // willingの場合は固定(58) ほかはいったん0にしています。

        $limit = ( isset( $request['limit'] ) ) ? $request['limit'] : GENERAL_PAGE_LIMIT;
        $page  = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;
        $data  = $this->ChatMessages->getData( $this->_contractant_id, $request['chat_group_id'], $contractant_service_menu_id, $request['thread_id'], $page, $limit );
        // 既読処理
        $this->ChatUnreads->deleteChatUnreadData( $this->_contractant_id, $request['chat_group_id'], $request['thread_id'], $request['user_id'], $request['front_user_id'] );
        $chat_group_members = $this->ChatGroupMembers->getChatGroupMembers( $this->_contractant_id, $request['chat_group_id'], $request['user_id'], $request['front_user_id'] );
        foreach( $chat_group_members as $key => $val )
        {
            $this->ChatMemberlists->saveUnreadFlg( $this->_contractant_id, $request['user_id'], $request['front_user_id'], $val->user_id, $val->front_user_id, 0 );
            //$this->ChatMemberlists->saveUnreadFlg( $this->_contractant_id, $val->user_id, $val->front_user_id, $request['user_id'], $request['front_user_id'], 0 );
        }
        return $data;
    }

    
    public function get_chat_data( $request )
    {
        try
        {
            if( $request['target_user_id'] )
            {
                $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
                $target_user_id = $request['target_user_id'];
                $where = [
                    'Users.contractant_id' => $this->_contractant_id
                    ,'Users.id !=' => $request['user_id']
                    ,'FrontUsers.auth_flg' => 1
                    ,'FrontUsers.common_user_flg' => 0
                ];
                $res[] = $this->Users->getFrontUser( $target_user_id, $where );
                $data = self::get_chat_group( $request['user_id'], $request['front_user_id'], $res );
                if( count( $data ) > 0 )
                {
                    return $data[0];
                }
                else
                {
                    // todo 対象ユーザーが取得できませんでした
                } 
            }
        }
        catch( \Exception $e )
        {
            return self::error_response( $e->getMessage() );
        }
    }

    private function get_chat_group( $user_id, $front_user_id, $data )
    {
        $this->ChatGroups              = TableRegistry::getTableLocator()->get( 'ChatGroups' );
        $this->ChatGroupMembers        = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->Threads                 = TableRegistry::getTableLocator()->get( 'Threads' );

        $contractant_service_menu_id = ( $this->_contractant_id === 4 ) ? 58 : 0; // willingの場合は固定(58) ほかはいったん0にしています。
        foreach( $data as $key => $val )
        {
            // まず、自分のuser_idと相手のuser_idでチャットグループがあるかどうか確認する(複数、共通のチャットグループがないかも確認する)
            $res = $this->ChatGroupMembers->checkForDM( $this->_contractant_id, $user_id, $front_user_id, $val['id'], $val['front_user']['id'] );
            // あれば、chat_group_idを取得(DMの場合は1件のはず)
            // threadを作成
            if ( $res !== false )
            {
//var_dump($res);
                $chat_group_id = $res->first()->chat_group_id;
                // TODO chat_group_idがある時点でthread_idもあるはず
                $thread_id     = $this->Threads->getIdFromChatGroupId( $this->_contractant_id, $chat_group_id, $contractant_service_menu_id, $user_id, $val['id'] );
                $data[$key]['chat_group_id'] = $chat_group_id;
                $data[$key]['thread_id']     = $thread_id;
            }
            // なければ、chat_group,threadを作成
            else
            {
                $name = $user_id . '-' . $front_user_id . 'x' . $val['id'] . '-' . $val['front_user']['id'];
                // チャットグループの登録
                $tmp = [
                    'contractant_id' => $this->_contractant_id
                    ,'name' => $name
                ];
                $chat_group_id = $this->ChatGroups->saveData( $tmp );
                // チャットグループメンバーの登録
                $tmp = [
                    'contractant_id' => $this->_contractant_id
                    ,'chat_group_id' => $chat_group_id
                    ,'user_id'       => $user_id
                    ,'front_user_id' => $front_user_id
                ];
                $this->ChatGroupMembers->saveData( $tmp );
                $tmp = [
                    'contractant_id' => $this->_contractant_id
                    ,'chat_group_id' => $chat_group_id
                    ,'user_id'       => $val['id']
                    ,'front_user_id' => $val['front_user']['id']
                ];
                $this->ChatGroupMembers->saveData( $tmp );
                // threadの登録
                $tmp = [
                    'contractant_id'               => $this->_contractant_id
                    ,'chat_group_id'               => $chat_group_id
                    ,'contractant_service_menu_id' => $contractant_service_menu_id
                    ,'created_user_id'             => $user_id
                    ,'date'                        => date( 'Y-m-d H:i:s' )
                    ,'title'                       => $name
                ];
                $thread_id = $this->Threads->saveData( $tmp );
                $data[$key]['chat_group_id'] = $chat_group_id;
                $data[$key]['thread_id']     = $thread_id;
            }

        }
        return $data;
    }

    /**** LINEオープンチャット ****/
    // コンテンツ取得
    public function get_line_open_chat_contents()
    {
        $request = $this->request->data;

        $this->LineOpenChats = TableRegistry::getTableLocator()->get( 'LineOpenChats' );
        $conditions = [
            'LineOpenChats.contractant_id' => $this->_contractant_id
        ];
        // タイトル検索
        if( isset( $request['search'] ) && $request['search'] !== null )
        {
            $search = trim( mb_convert_kana( $request['search'], 'as' ) );
            $searchs = explode(' ',  $search);

            $arr_search = [];
            foreach( $searchs as $val ) 
            {
                $arr_search[] = [
                    'OR' => [
                        'LineOpenChats.title LIKE' => '%' . $val . '%'
                        ,'LineOpenChats.title_en LIKE' => '%' . $val . '%'
                    ]
                ];
            }
            $conditions[] = [
                'AND' => $arr_search
            ];
        }
        // カテゴリの絞り込み
        if( isset( $request['category'] ) &&  $request['category'] !== null )
        {
            $conditions['LineOpenChats.line_open_chat_category_id'] = $request['category'];
        }

        // おすすめの絞り込み
        if( isset( $request['pickup'] ) &&  $request['pickup'] === '1' )
        {
            $conditions['LineOpenChats.pickup_flg'] = 1;
        }

        $page = ( isset( $request['page'] ) && preg_match( '/^[0-9]+$/', $request['page'] ) ) ? $request['page'] : 1;

        $this->paginate['finder'] = [
            'front' => [
                'conditions' => $conditions
                ,'fields' => [
                     //'full_icon_image_path' => 'CONCAT( ' . APP_URL . DS . ', icon_image_path )'
                     'full_icon_url_path' => "CONCAT( 'https://" . $this->_domain . DS . "', LineOpenChats.icon_url_path )"
                ]
                ,'limit' => GENERAL_PAGE_LIMIT
                ,'page' => $page
            ]
        ];

        try
        {
            $data = $this->paginate( 'LineOpenChats' );
            //Log::debug( 'get_line_open_chat_contents -----------------', 'api' );
            //Log::debug( json_decode( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
            return $data;
        }
        catch ( \Exception $e)
        {
            return [];
        }

    }
 
    // カテゴリ取得
    public function get_line_open_chat_category()
    {
        $this->LineOpenChatCategories = TableRegistry::getTableLocator()->get( 'LineOpenChatCategories' );
        return $this->LineOpenChatCategories->getDataForFront( $this->_contractant_id, $this->_domain );
    }

    // ポイント取得
    public function get_user_point( $request )
    {
        try
        {
            $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
            //Log::debug( $this->Tokens->getFrontUserPoint( $request['access_token'] ), 'api' );
            return $this->Tokens->getFrontUserPoint( $request['access_token'] );
        }
        catch( \Exception $e )
        {
            return self::error_response( '016', $e->getMessage() );
        }
    }

    // ポイント追加
    public function check_point( $action=null, $request )
    {
        //Log::debug( 'check_point==================================', 'api' );
        try
        {
            if( $action !== null )
            {
                $now = new Time();
                $today =  $now->format( 'Y-m-d 00:00:00' );
                
                // キャンペーン期間の取得
                $camptain_date = Configure::read( 'mt_campaign_date' );
                $start_date    = strtotime($camptain_date['start_date']);
                $end_date      = strtotime($camptain_date['end_date']);

                $date_col = 'last_' . $action . '_date';
                $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
                // 日次のチェック : front_user_id
                if ( isset( $request['access_token'] ) )
                {
                    $res = $this->Tokens->checkFrontUserPointDate( $request['access_token'], $date_col, $today );

                    // ポイント加算を実行
                    if ( $res )
                    {
                        $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );

                        
                        // キャンペーン期間中のログイン
                        if ( $action === 'access' && time() >= $start_date && time() <= $end_date )
                        {
                            // ユーザーのポイントが20以下の場合は20p付与t
                            $user_point = $this->FrontUsers->getTotalPoint( $res );

                            $first_campaign_fisrt_login_point = Configure::read( 'mt_first_campaign_fisrt_login_point' );
                            $campain_point = $first_campaign_fisrt_login_point['campaign_point'];

                            if ( $user_point < $campain_point )
                            {                   
                                $this->FrontUsers->setCampaignPoint( $res, $campain_point);
                            }
                        }

                        $this->FrontUsers->setActionPoint( $res, $date_col );
                    }
                }
            }
        }
        catch( \Exception $e )
        {
            return self::error_response( '017', $e->getMessage() );
        }
    }
 
    // 備品リスト
    public function get_equipments()
    {
        $request = $this->request->data;

        // トークンからログインユーザーの建物を逆引きして絞る
        $this->Tokens = TableRegistry::getTableLocator()->get( 'Tokens' );
        $front_user = $this->Tokens->getFrontUserByAccessTokens( $request['access_token'] );
        if( $front_user )
        {
            $building_id = $front_user->building_id;
        }
        else
        {
            return self::error_response( '018' );
        }

        $conditions = [
            'Equipments.contractant_id' => $this->_contractant_id
            ,'Equipments.building_id'   => $building_id
        ];

        // カテゴリの絞り込み
        if( isset( $request['category'] ) && $request['category'] !== null )
        {
            $conditions['Equipments.category_id'] = $request['category'];
        }

        // 名前検索
        if( isset( $request['search'] ) && $request['search'] !== null )
        {
            $search = trim( mb_convert_kana( $request['search'], 'kas' ) );
            $searchs = explode(' ',  $search);

            $arr_search = [];
            foreach( $searchs as $val )
            {
                $arr_search[] = [
                    'Equipments.name LIKE' => '%' . $val . '%'
                ];
            }
            $conditions[] = [
                'AND' => $arr_search
            ];
        }

        $page = ( isset( $request['page'] ) && preg_match( '/^[0-9]+$/', $request['page'] ) ) ? $request['page'] : 1;

        $this->paginate['finder'] = [
            'front' => [
                'conditions' => $conditions
                ,'fields' => [
                     //'full_icon_image_path' => 'CONCAT( ' . APP_URL . DS . ', icon_image_path )'
                     'full_icon_url_path' => "CONCAT( 'https://" . $this->_domain . DS . "', Equipments.icon_url_path )"
                ]
                ,'limit' => GENERAL_PAGE_LIMIT
                ,'page' => $page
            ]
        ];

        try
        {
            $data = $this->paginate( 'Equipments' );
            //Log::debug( json_decode( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
            return $data;
        }
        catch ( \Exception $e)
        {
            Log::debug( json_decode( json_encode( $e->getMessage(), JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT  ), true ), 'api' );
            return [];
        }
    }
    // 備品カテゴリ
    public function get_equipment_category()
    {
        return  Configure::read( 'mt_equipment_category' );
    }

    // 交換商品ポイント
    public function get_points_products()
    {
        $this->PointsProducts = TableRegistry::get('PointsProducts');

        $res['message'] = Configure::read( 'mt_point_popup_message' );
        $res['products'] = $this->PointsProducts->getData( $this->_contractant_id );

        return $res;
    }
    // Q&Aカテゴリ
    public function get_faq_category()
    {
       return  Configure::read( 'mt_faq_category' );
    }

    // Q&A （利用規約と全く同じ作法）
    public function get_faq()
    {
        $this->Questions = TableRegistry::getTableLocator()->get( 'Questions' );
        return  $this->Questions->getData( $this->_contractant_id );
    }
    // Q&A
    public function get_faqs()
    {
        $this->Faqs = TableRegistry::getTableLocator()->get( 'Faqs' );
        $this->FaqsQuestions = TableRegistry::getTableLocator()->get( 'FaqsQuestions' );
        $mt_faq_category = Configure::read( 'mt_faq_category' );

        foreach ($mt_faq_category as $key => $val )
        {    
            $res[$val] = $this->FaqsQuestions->getFaqsQuestionsDataFrontId($this->_contractant_id , $key);
        }

        foreach ( $res as $key => $val )
        {
            foreach ( $val as $key1 => $val2 )
            {
                $count_id = $this->Faqs->getCountFaqsQuestionsId( $this->_contractant_id, $val2->id );
                // questionが一つのものに対し、answerが複数あるもの
                if ( $count_id->count > 2 )
                {
                   $data[$key]['qustion'] = $val2->question;
                   $data[$key]['qustion_en'] = $val2->question_en;
                   $data[$key]['answers'][] = $val2->faqs;
                }
                else
                {
                    $data[$key]['questions'][] = [
                        'qustion' => $val2->question,
                        'qustion_en' => $val2->question_en,
                        'answer' => $val2->faqs['answer'],
                        'answer_en' => $val2->faqs['answer_en'],
                        'icon_url_path' => $val2->faqs['icon_url_path'],
                    ];
                }
            }
        }

        return $data;
    }
}
