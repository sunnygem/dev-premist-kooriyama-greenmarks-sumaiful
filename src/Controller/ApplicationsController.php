<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Console\Shell;

use Cake\Mailer\Email;


use Cake\Http\Cookie\Cookie;
use Cake\Http\Cookie\CookieCollection;

class ApplicationsController extends FrontAppController
{
    public function initialize()
    {
        parent::initialize();
        //parent::app_common();

        $this->Auth->allow();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );

        $this->Users                = TableRegistry::getTableLocator()->get( 'Users' );
        $this->EncryptionParameters = TableRegistry::getTableLocator()->get( 'EncryptionParameters' );
        $this->Buildings            = TableRegistry::getTableLocator()->get( 'Buildings' );
        $this->BuildingRoomNumbers  = TableRegistry::getTableLocator()->get( 'BuildingRoomNumbers' );
        $this->TermsOfServices      = TableRegistry::get('TermsOfServices');
        $this->loadComponent( 'Common' );
    }

    public function index()
    {
        $users = $this->Users->newEntity();
        $buildings = $this->Buildings->getOptions( $this->_contractant_id );
        $term_of_use = $this->TermsOfServices->getDataByContractantId( $this->_contractant_id );
        if ( $this->request->is( 'post' ) )
        {
            $data = $this->request->data;
            // TODO バリデーション
            if ( $this->BuildingRoomNumbers->checkExists( $this->_contractant_id, $data['front_user']['building_id'], $data['front_user']['room_number'] ) ||  $data['front_user']['room_number'] === '999')
            {
                // 管理側ですでに同じメールアドレスのusersデータがあるかをチェックし、存在すればそのusersにfront_userを紐づける
                $admin_exist = $this->Users->checkExistAdminAndNoFrontByEmail( $data['email'], $this->_contractant_id );
                if( $admin_exist )
                {
                    $data['id'] = $admin_exist['id'];
                }
                // 通常の登録申請
                else
                {
                    // 取り急ぎwillingのidを4で設定しておく
                    $data['contractant_id'] = $this->_contractant_id;
                    // ユーザー登録
                    //$data['type'] = 'front';
                    $data['login_id'] = $data['email'];
                }

                $data['front_user']['contractant_id'] = $this->_contractant_id;
                // 管理ユーザーの申請
                if( $data['front_user']['room_number'] === '999' )
                {
                    $data['front_user']['building_room_number_id'] = null;
                    $data['front_user']['admin_flg'] = 1;
                }
                // 通常の申請
                else
                {
                    $data['front_user']['building_room_number_id'] = $this->BuildingRoomNumbers->getIdByContractantIdAndBuildingIdAndRoomNumber( $this->_contractant_id, $data['front_user']['building_id'], $data['front_user']['room_number'] );
                }

                // 暗号化する
                $tmp_data = $data;
                $data = self::execEncrypt( $data );

                $users = $this->Users->newEntity( $data );
                $user_id = $this->Users->registFrontUser( $data );
                //$remind_key = substr( md5( uniqid() ). 0, 8 );
                // メールを送信
                $building = $this->Buildings->getDataById($tmp_data['front_user']['building_id']);
                $tmp_data['building'] = $building;
                $this->loadComponent( 'Mail' );
                $this->Mail->send_application( $tmp_data );

                $this->Flash->success( 'アプリの使用申請を行いました。' . "\n" . 'Applied for using the app.' );
                $this->redirect( '/applications' );
            }
            else
            {
                $this->Flash->error( '該当する部屋番号が見つかりません。' . "\n" . 'No matching room number found.' );
            }

        }
        $this->set( compact( 'users', 'buildings', 'term_of_use' ) );
    }

    public function password( $tmp_key=null )
    {
        if ( strlen( $tmp_key ) > 0 )
        {
            if ( $this->request->is( [ 'post', 'put' ] ) )
            {
                $data = $this->request->data;
                $password = $data['password'];

                if ( preg_match( '/^[!-~]{8,32}$/', $password ) === 1 )
                {
                    $this->Users->registFrontUser( $data );
                    $this->redirect( '/applications/password_complete' );
                    $this->Flash->success( 'パスワードを設定しました。アプリを起動し、ログインしてください。' . "\n" . 'Set a password. Launch the app and login.' );
                }
                else
                {
                    $data = $this->Users->newEntity( $data );
                    $this->Flash->error( 'パスワードは、英数記号8～32文字で入力してください。' . "\n" . 'Please enter a password using 8-32 alphanumeric characters.' );
                }
            }
            else
            {
                $data = $this->Users->getFrontUserByTmpKey( $tmp_key );
                if ( $data === null )
                {
                    throw new ForbiddenException( '不正なアクセスです。' );
                }
                unset( $data['password'] );
            }

            $this->set( compact( 'data' ) );
        }
        else
        {
            throw new ForbiddenException( '不正なアクセスです。' );
        }
    }

    public function passwordComplete()
    {
    }

    public function help()
    {
        $unique_parameter = $this->_session->read( 'ContractantData.unique_parameter' );
        if ( Email::getConfigTransport( $unique_parameter ) !== null )
        {
            $email = Email::getConfigTransport( $unique_parameter )['username'];
        }
        else
        {
            $email = Email::getConfigTransport( 'default' )['username'];
        }
        $this->set( compact( 'email' ) );
    }

    private function execEncrypt( $data )
    {
        $iv   = $this->Common::createRandomBytes( 16 );
        $salt = $this->Common::createRandomBytes( 32 );
        $res  = $this->EncryptionParameters->getEncryptionParameters( $iv, $salt, true );

        $data['encryption_parameter_id'] = $res['id'];
        $data['front_user']['encryption_parameter_id'] = $res['id'];

        $targets = [ 'name' ];
        foreach( $targets as $val )
        {
            if ( isset( $data[$val] ) )
            {
                $data[$val] = $this->Common::encrypt( $data[$val], Configure::read( 'Security.encryptKey' ), base64_decode( $iv ), base64_decode( $salt ) );
            }
            else
            {
                $data['front_user'][$val] = $this->Common::encrypt( $data['front_user'][$val], Configure::read( 'Security.encryptKey' ), base64_decode( $iv ), base64_decode( $salt ) );
            }
        }
        return $data;
    }
}
