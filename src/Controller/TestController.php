<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class TestController extends FrontAppController
{
    public $Informations;
    public $ImageSharings;
    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Informations  = TableRegistry::getTableLocator()->get( 'Informations' );
        $this->ImageSharings = TableRegistry::getTableLocator()->get( 'ImageSharings' );
        $this->ChatGroupMembers = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->ChatMemberlists = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
    }
    //public function beforeRender(Event $event)
    //{
    //    parent::beforeRender( $event );

    //}

    public function index()
    {
        $where = [
            'ChatMemberlists.contractant_id'      => 4
            ,'ChatMemberlists.self_user_id'       => 32
            ,'ChatMemberlists.self_front_user_id' => 13
            ];
        //$data = $this->ChatMemberlists->getList( $where, 20, 1);
        $data = $this->ChatGroupMembers->getChatGroupMembers( 4, 51, 32, 13 );
        var_dump($data);
        $this->set( compact( 'data' ) );
    }
}
