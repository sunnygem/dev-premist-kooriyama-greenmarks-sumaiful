<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class AlbumsController extends FrontAppController
{


    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->ImageSharings = TableRegistry::getTableLocator()->get( 'ImageSharings' );
    }
//    public function beforeRender(Event $event)
//    {
//        parent::beforeRender( $event );
//
//    }

    public function index()
    {
        $params = [
            'contractant_id' => $this->_session->read( 'ContractantData.id' )
            ,'limit'         => 20
            ,'building_id'   => $this->_login_user['front_user']['building_id']
        ];
        $image_sharings = $this->ImageSharings->getAlbumData( $params );
//debug( $image_sharings );
        $this->set( compact( 'image_sharings' ) );
    }

    public function detail( $id=null )
    {
        $params = [
            'contractant_id' => $this->_contractant_id
            ,'id'            => $id
            ,'building_id'   => $this->_login_user['front_user']['building_id']
        ];
        $image_sharings = $this->ImageSharings->getAlbumData( $params );

        if( $image_sharings === null)
        {
            throw new NotFoundException();
        }
//debug( $image_sharings );
        $this->set( compact( 'image_sharings' ) );
    }

}
