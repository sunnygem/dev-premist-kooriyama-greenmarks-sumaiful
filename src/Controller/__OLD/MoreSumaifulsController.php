<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class MoreSumaifulsController extends AppController
{
    public $Contents;
    public function initialize()
    {
        parent::initialize();
        $this->app_common();

        $this->Contents = TableRegistry::get( 'Contents' );
    }
    public function beforeFilter(Event $event)
    {
    }

    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'sort' => [
                    'Contents.modified' => 'DESC'
                ]
                ,'conditions' => [
                    'Contents.contractant_id'   => $this->_session->read( 'ContractantData.id' )
                    ,'Contents.display_flg'     => 1
                    ,'Contents.release_date <=' => date( 'Y-m-d' )
                    ,'OR' => [
                        'Contents.close_date IS'    => NULL
                        ,'Contents.close_date >'    => date( 'Y-m-d H:i:s' )
                    ]
                ]
            ]
        ];
        $contents = $this->paginate( 'Contents' );

        $this->set( compact( 'contents' ) );

    }

    public function article( $id=null )
    {
        $content = $this->Contents->getDetailData( $id );
        //debug( $content );

        $this->set( compact( 'content' ) );
    }
}
