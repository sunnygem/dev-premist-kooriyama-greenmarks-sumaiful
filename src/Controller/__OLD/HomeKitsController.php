<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 *
 * 基本的にアプリ
 *
**/
class HomeKitsController extends AppController
{
    public $app_screen_id;
    public $ContractantServiceMenus;
    public $MenuApplications;

    public function initialize()
    {
        parent::initialize();
        $this->app_common();

        $this->ContractantServiceMenus = TableRegistry::get( 'ContractantServiceMenus' );
        $this->MenuApplications = TableRegistry::get( 'MenuApplications' );
    }
    public function beforeFilter(Event $event)
    {
        $this->app_screen_id = 2;
    }

    public function index()
    {
        $service_menus = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_session->read( 'ContractantData.id' ), $this->app_screen_id );
        $this->set( compact( 'service_menus' ) );
    }

    public function appSelect( $service_menu_id=null)
    {
        if( $service_menu_id !== null )
        {
            $service_menu = $this->ContractantServiceMenus->get( $service_menu_id );
            // home kitはアプリのみ
            switch( $service_menu->service_menu_id )
            {
            case 6: // アプリ
                $menu_data = $this->MenuApplications->getDetailByContractantSeriviceMenuId( $service_menu_id );
                break;
            default :
            }

            $this->set( compact( 'service_menu', 'menu_data' ) );
        }
        else
        {
            throw new NotFoundException('ページが存在しません');
        }

    }
}
