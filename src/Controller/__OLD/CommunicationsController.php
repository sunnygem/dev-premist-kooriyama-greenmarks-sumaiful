<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;


use Cake\Http\Cookie\Cookie;
use Cake\Http\Cookie\CookieCollection;

/**
 *
 * チャット機能
 *
**/
class CommunicationsController extends AppController
{
    public $app_screen_id;
    public $ContractantServiceMenus;

    public function initialize()
    {
        parent::initialize();
        $this->app_common(); // TODO parent::に修正すること

        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $this->ChatGroups              = TableRegistry::getTableLocator()->get( 'ChatGroups' );
        $this->ChatGroupMembers        = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->Threads                 = TableRegistry::getTableLocator()->get( 'Threads' );
        $this->ChatMessages            = TableRegistry::getTableLocator()->get( 'ChatMessages' );
        $this->Users                   = TableRegistry::getTableLocator()->get( 'Users' );
    }

    public function beforeFilter(Event $event)
    {
        $this->app_screen_id = 5;

        $this->_user_id = 11; // TODO 開発用
    }

    public function index()
    {
        $service_menus = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_session->read( 'ContractantData.id' ), $this->app_screen_id );
        $chat_groups        = $this->ChatGroups->getByContractantId( $this->_session->read( 'ContractantData.id') );
        $chat_group_members = $this->ChatGroupMembers->getByUserId( $chat_groups->id, $this->_user_id );
        $this->set( compact( 'service_menus', 'chat_groups', 'chat_group_members' ) );
    }
    public function threadList( $contractant_id, $chat_group_id, $contractant_service_menu_id=null )
    {
        //$service_menu = $this->ContractantServiceMenus->get( $contractant_service_menu_id );
        $threads = $this->Threads->getData( $contractant_id, $chat_group_id, $contractant_service_menu_id );
        // TODO threadを取得する
        //debug( $threads );
        $this->set( compact( 'threads', 'contractant_id', 'chat_group_id', 'contractant_service_menu_id' ) );
    }
    public function thread( $contractant_id, $chat_group_id, $contractant_service_menu_id, $thread_id, $user_id=null )
    {
        
        $user_id = ( $user_id !== null ) ? $user_id : $this->_user_id;
        $userData = $this->Users->get( $user_id );

        $data = $this->ChatMessages->getData( $contractant_id, $chat_group_id, $contractant_service_menu_id, $thread_id );
        $this->set( compact( 'contractant_id', 'chat_group_id', 'contractant_service_menu_id', 'thread_id', 'user_id', 'data', 'userData' ) );

    }
    public function newThread( $contractant_id, $chat_group_id, $contractant_service_menu_id=null )
    {
        $threads = $this->Threads->newEntity();
        $threads->contractant_id              = $contractant_id;
        $threads->chat_group_id               = $chat_group_id;
        $threads->contractant_service_menu_id = $contractant_service_menu_id;
        if ( $this->request->is( [ 'post', 'put' ] ) )
        {
            $request = $this->request->data;
            $request['user_id'] = $this->_user_id;
            $request['date']    = date( 'Y-m-d H:i:s' );
            $this->Threads->saveData( $request );
            return $this->redirect( '/communications/thread-list/' . $contractant_id . '/' . $chat_group_id . '/' . $contractant_service_menu_id );
        }
        $this->set( compact( 'threads' ) );
    }
}
