<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ConciergesController extends AppController
{
    public $app_screen_id;
    public $ContractantServiceMenus;
    public $MenuLinktypes;
    public $MenuApplications;
    public $MenuManuals;
    public $MenuEnquetes;

    public function initialize()
    {
        parent::initialize();
        $this->app_common();

        $this->ContractantServiceMenus = TableRegistry::get( 'ContractantServiceMenus' );
        $this->MenuContactTels         = TableRegistry::get( 'MenuContactTels' );
        $this->MenuContactEmals        = TableRegistry::get( 'MenuContactEmals' );
        $this->MenuContactForms        = TableRegistry::get( 'MenuContactForms' );
        $this->MenuLinktypes           = TableRegistry::get( 'MenuLinktypes' );
        $this->MenuApplications        = TableRegistry::get( 'MenuApplications' );
        $this->MenuManuals             = TableRegistry::get( 'MenuManuals' );
        $this->MenuEnquetes            = TableRegistry::get( 'MenuEnquetes' );
    }
    public function beforeFilter(Event $event)
    {
        $this->app_screen_id = 3;
    }

    public function index()
    {
        $service_menus = $this->ContractantServiceMenus->getDataByAppFooterId( $this->_session->read( 'ContractantData.id' ), $this->app_screen_id );
        //debug($service_menus);
        $this->set( compact( 'service_menus' ) );
    }

    public function menu( $id=null )
    {
        if( $id !== null )
        {
            $service_menu = $this->ContractantServiceMenus->get( $id );
            switch( $service_menu->service_menu_id )
            {
            case 1: //問い合わせTEl
                $menu_detail = $this->MenuContactTels->getDetailByContractantSeriviceMenuId( $service_menu->id );
                $this->setAction( 'contact' );
                break;
            case 2: //問い合わせメール
                $this->setAction( 'contact' );
                break;
            case 3: //問い合わせフォーム
                $this->setAction( 'contact' );
                break;
            case 4: // リンクタイプ
                $menu_detail = $this->MenuLinktypes->getDetailByContractantSeriviceMenuId( $service_menu->id );
                $this->setAction( 'linkSelect' );
                break;
            case 6: //アプリ
                $menu_detail = $this->MenuApplications->getDetailByContractantSeriviceMenuId( $service_menu->id );
                $this->setAction( 'appSelect' );
                break;
            case 7: //マニュアル
                $menu_detail = $this->MenuManuals->getDetailByContractantSeriviceMenuId( $service_menu->id );
                $this->setAction( 'documentList' );
                break;
            case 9: //アンケート
                $menu_detail = $this->MenuEnquetes->getDataByContractantSeriviceMenuId( $service_menu->id, true );
                $this->setAction( 'enqueteList' );
                break;
            }
            //debug( $menu_detail );
            $this->set( compact( 'menu_detail' ) );
        }
        else
        {
            throw new NotFoundException('ページが存在しません');
        }
    }

    public function enqueteList()
    {
    }

    public function enquete( $id=null )
    {
        if( $id !== null )
        {
            $menu_enquete = $this->MenuEnquetes->get( $id );
            $this->set( compact( 'menu_enquete' ) );
        }
    }

    public function appSelect()
    {
    }

    public function linkSelect()
    {
    }

    public function documentList()
    {
    }

    public function contact()
    {
    }
}
