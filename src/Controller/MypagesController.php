<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class MypagesController extends FrontAppController
{
    public $Users;
    public $Posts;

    public $paginate = [ 'finder' => 'search' ];

    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
        $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
    }
    //public function beforeRender(Event $event)
    //{
    //    parent::beforeRender( $event );
    //}

    public function index( $user_id=null )
    {
        if( $user_id === null )
        {
            $mypage_user = $this->_login_user;
            $user_id     = $this->_login_user['id'];
            $front_user_id = $this->_login_user['front_user']['id'];
        }
        elseif( $user_id !== $this->_login_user['id'] )
        {
            $mypage_user   = $this->Users->getFrontUser( $user_id );
            $front_user_id = $mypage_user->front_user->id;
        }
        //$where = [
        //    'Posts.user_id'        => $user_id
        //    ,'Posts.front_user_id' => $front_user_id
        //];

        //$posts = $this->Posts->getPosts( $where, 20, 1 );

        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'Posts.contractant_id' => $this->_contractant_id
                    ,'Posts.display_flg'   => 1
                    ,'Users.id'            => $user_id
                    ,'FrontUsers.id'       => $front_user_id
                    ,'Users.deleted IS'    => NULL
                ]
                ,'order' => [
                    'Posts.created' => 'DESC'
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'PostCategories'
            ,'Users.FrontUsers'
            // 画像
            ,'RelPostFiles' => [
            'fields' => [ 'RelPostFiles.post_id', 'RelPostFiles.file_id' ]
            ]
            ,'RelPostFiles.Files' => [
            'fields' => [ 'Files.id', 'Files.file_path' ]
            ]
            //いいね
            ,'PostLikes' => [
            'fields' => [ 'post_id' ]
            ,'conditions' => ['PostLikes.deleted IS ' => null ]
            ]
            ,'PostLikes.Users' => [
            'fields' => [ 'id' ]
            ]
            ,'PostLikes.Users.FrontUsers' => [
            'fields' => [ 'leave_flg', 'nickname', 'thumbnail_path' ]
            ]
            ,'PostJoins' => [
            'fields' => [ 'post_id' ]
            ,'conditions' => [ 'PostJoins.deleted IS' => null ]
            ]
            ,'PostJoins.Users' => [
            'fields' => [ 'id' ]
            ]
            ,'PostJoins.Users.FrontUsers' => [
            'fields' => [ 'leave_flg', 'nickname', 'thumbnail_path' ]
            ]
            ,'PostComments' => [
            'fields' => [ 'id', 'post_id', 'comment' ]
            ,'conditions' => ['PostComments.deleted IS ' => null ]
            ,'sort' => [ 'PostComments.created' => 'ASC' ]
            ]
            ,'PostComments.Users.FrontUsers' => [
            'fields' => [ 'Users.id', 'FrontUsers.user_id', 'FrontUsers.nickname' ]
            ]
        ];

        // 自分以外の場合
        if( $this->_login_user['id'] !== $mypage_user['id'] )
        {
            // 物件公開範囲
            $this->paginate['join'] = [
                [
                    'table'  => 'post_buildings'
                    ,'alias' => 'PostBuildingsLeftJoin'
                    ,'type'  => 'LEFT'
                    ,'conditions' => [
                        'PostBuildingsLeftJoin.Post_id = Posts.id'
                    ]
                ]
            ];
            $this->paginate['finder']['search']['conditions'][] = [
                 // 全公開か、自分の所属物件対象のもの
                 'OR' => [
                     'Posts.user_id' => $this->_login_user['id']
                     ,[
                         'OR' => [
                             'PostBuildingsLeftJoin.id IS' => null ,
                             'PostBuildingsLeftJoin.building_id' => $this->_login_user['front_user']['building_id']
                         ]
                     ]
                 ]
            ];
        }

        $posts = $this->paginate( 'Posts' );
        //dump($posts );
        $this->set( compact( 'mypage_user', 'posts' ) );

    }
}
