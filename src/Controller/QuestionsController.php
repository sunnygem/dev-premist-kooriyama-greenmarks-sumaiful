<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 *
**/
class QuestionsController extends FrontAppController
{
    public $Terms;
    public function initialize()
    {
        parent::initialize();
        $this->Questions  = TableRegistry::getTableLocator()->get( 'Questions' );
        $this->Auth->allow();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $data = $this->Questions->getDataByContractantId( $this->_contractant_id );
        $this->set( compact( 'data' ) );
    }

}
