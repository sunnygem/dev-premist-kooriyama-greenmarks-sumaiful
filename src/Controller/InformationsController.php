<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class InformationsController extends FrontAppController
{
    public $paginate = [ 'finder' => 'search' ];

    public $Informations;

    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Informations = TableRegistry::getTableLocator()->get( 'Informations' );
        $this->InformationComments     = TableRegistry::getTableLocator()->get( 'InformationComments' );
        $this->InformationCommentLikes = TableRegistry::getTableLocator()->get( 'InformationCommentLikes' );
        $this->InformationLikes        = TableRegistry::getTableLocator()->get( 'InformationLikes' );
        $this->InformationComments     = TableRegistry::getTableLocator()->get( 'InformationComments' );
        $this->InformationJoins        = TableRegistry::getTableLocator()->get( 'InformationJoins' );
    }
//    public function beforeRender(Event $event)
//    {
//        parent::beforeRender( $event );
//
//$this->render('/willing/Tops/index');
//    }
    public function index()
    {
        $this->_display_search_form = true;
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'Informations.contractant_id'  => $this->_contractant_id
                    ,'Informations.display_flg'    => 1
                    ,'Informations.release_date <=' => date( 'Y-m-d' )
                    ,[ 'OR' => [
                        'Informations.close_date IS' => NULL
                        ,'Informations.close_date >' => date( 'Y-m-d H:i:s' )
                    ] ]
                    // 全公開もしくはユーザーのbuilding_idに合致するもの
                    ,[ 'OR' => [
                        'Informations.all_building_flg'       => 1
                        ,'InformationBuildingsLeftJoin.building_id' => $this->_login_user['front_user']['building_id']
                    ] ]
                ]
                ,'order' => [
                    'Informations.release_date' => 'DESC'
                    ,'Informations.created'     => 'DESC'
                ]

                ,'group' => 'Informations.id'
            ]
        ];
        $this->paginate['contain'] = [
            // 投稿者
            'Users.AdminUsers' => [
                'conditions' => [
                'AdminUsers.deleted IS' => null
                ]
            ]
            // サムネイル
            ,'RelInformationFiles.Files' => [
                'conditions' => [
                'Files.deleted IS' => null
                ]
            ]
            ,'InformationBuildings' => [
                'fields' => [
                    'InformationBuildings.information_id'
                ]
                ,'conditions' => [
                    'InformationBuildings.building_id' => $this->Auth->user('front_user.building_id')
                ]
            ]
            ,'InformationBuildings.Buildings' => [
                'fields' => [
                    'Buildings.name'
                    ,'Buildings.icon_path'
                ]
            ]
        ];
        // 公開物件を絞る
        $this->paginate['join'] = [
            [
                'table'  => 'information_buildings'
                ,'alias' => 'InformationBuildingsLeftJoin'
                ,'type'  => 'LEFT'
                ,'fields' => [
                    //'InformationBuildingsLeftJoin.id'
                ]
                ,'conditions' => [
                    'InformationBuildingsLeftJoin.information_id = Informations.id' 
                    //,'OR' => [
                    //    'InformationBuildings.building_id'     => $this->_login_user['front_user']['building_id']
                    //    //,'InformationBuildings.building_id IS' => null
                    //]
                ]
            ]
            ,[
                'table'  => 'buildings'
                ,'alias' => 'BuildingsLeftJoin'
                ,'type'  => 'LEFT'
                ,'conditions' => [
                    'InformationBuildingsLeftJoin.building_id = BuildingsLeftJoin.id' 
                ]
            ]
        ];

        // 検索絞り込み
        $request = $this->request->query;
        if( isset( $request['search'] ) && mb_strlen( trim( $request['search'] ) ) > 0 )
        {
            $tmp_str = mb_ereg_replace( "　", " ", trim( $request['search'] ) );
            $tmp_arr = explode( " ", $tmp_str );

            $search_arr = [];
            foreach( $tmp_arr as $key => $val )
            {
                $search_arr['OR'][0][$key] = [ 'Informations.title LIKE' => '%' . $val . '%' ];
                $search_arr['OR'][1][$key] = [ 'Informations.content LIKE' => '%' . $val . '%' ];
                $search_arr['OR'][2][$key] = [ 'BuildingsLeftJoin.name LIKE' => '%' . $val . '%' ] ;
                $search_arr['OR'][3][$key] = [ 'BuildingsLeftJoin.name_en LIKE' => '%' . $val . '%' ] ;
                $search_arr['OR'][4][$key] = [ 'BuildingsLeftJoin.name_kana LIKE' => '%' . mb_convert_kana( $val, 'cHV' ) . '%' ] ;
            }
            $this->paginate['finder']['search']['conditions'] = array_merge( $this->paginate['finder']['search']['conditions'], $search_arr );
        }

        // 退去者の判別
        if( isset( $this->Auth->user()['front_user']['leave_flg'] ) && $this->Auth->user()['front_user']['leave_flg'] === 1 )
        {
            $this->paginate['finder']['search']['conditions']['Informations.reject_leave_readable_flg '] = 0;
        }

        // カテゴリ
        if( isset( $request['type'] ) )
        {
            // 1.3.0前の後方互換
            switch( $request['type'] )
            {
            case 'general':
                $this->paginate['finder']['search']['conditions'][] = [
                    'OR' => [
                        'Informations.type' => $request['type'],
                        'Informations.event_flg' => 0,
                    ]
                    ,'Informations.type !=' => 'line_open_chat',
                ];
                break;
            case 'event':
                $this->paginate['finder']['search']['conditions'][] = [
                    'OR' => [
                        'Informations.type' => $request['type'],
                        'Informations.event_flg' => 1,
                    ]
                    ,'Informations.type !=' => 'line_open_chat',
                ];
                break;
            case 'line_open_chat':
                $this->paginate['finder']['search']['conditions']['Informations.type'] = $request['type'];
                break;
            }
        }

        $data = $this->paginate( 'Informations' );
        $this->set( compact( 'data' ) );
    }

    public function detail( $id=null )
    {
        $params = [
            'contractant_id' => $this->_session->read( 'ContractantData.id')
            ,'id'            => $id

        ];
        $data = $this->Informations->getFrontInformations( $params );
        //debug ( $data );

        $this->set( compact( 'data' ) );
    }

    public function comment( $information_id=null )
    {
        if( $this->request->is( 'post' ) && isset( $this->request->data['complete'] ) )
        {
            $request = $this->request->data;

            $data = [
                'contractant_id'  => $this->_contractant_id
                ,'user_id'        => $this->_login_user['id']
                ,'information_id' => $request['information_id']
                ,'comment'        => $request['comment']
            ];
            if( isset( $request['parent_comment_id'] ) && $request['parent_comment_id'] !== '' ) $data['parent_comment_id'] = $request['parent_comment_id'];
            $this->InformationComments->saveData( $data );

            $this->redirect([ $information_id ]);
        }
        else
        {
            $params = [
                'contractant_id' => $this->_contractant_id
                ,'information_id' => $information_id
            ];
            $comment = $this->InformationComments->getComments( $params );

            //debug( $comment );
            $this->set( compact( 'comment', 'information_id' ) );
        }
    }

    public function setActivity()
    {
        if( $this->request->is( 'ajax' ) )
        {
            $this->autoRender = false;

            $query_param = $this->request->getQuery();
            $information = $this->Informations->findById( $query_param['target_id'] );

            if( $information->isEmpty() ) exit;

            $information = $information->first();
            if( isset( $query_param['action'] ) )
            {
                $action_data = [
                    'contractant_id'  => $this->_contractant_id
                    ,'user_id'        => $query_param['user_id']
                    ,'information_id' => $information->id
                ];

                $action = $query_param['action'];
                $err_flg = false;
                switch ( $action )
                {
                case 'like' :
                    $this->InformationLikes->saveData( $action_data );
                    break;
                case 'comment' :
                    $action_data['comment'] = $query_param['comment'];

                    // コメントにコメント
                    if( isset( $query_param['parent_comment_id'] ) ) $action_data['parent_comment_id'] = $query_param['parent_comment_id'];
                    $this->InformationComments->saveData( $action_data );
                    break;
                case 'join' :
                    // todo 投稿がイベントかのチェックをした方がいいか？
                    $this->InformationJoins->saveData( $action_data );
                    break;
                case 'comment_like' :
                    $action_data['comment_id'] = $query_param['comment_id'];
                    $res = $this->InformationCommentLikes->saveData( $action_data );
                    break;
                default :
                    $err_flg = true;
                }

                if( $err_flg === false )
                {
                    $this->response->body( json_encode( ['result' => true], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
                }
                else
                {
                    $this->response->body( json_encode( ['result' => false], JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
                }

            }
        }
    }


}
