<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ChattestsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->app_common(); // TODO parent::に修正すること

        $this->ContractantServiceMenus = TableRegistry::getTableLocator()->get( 'ContractantServiceMenus' );
        $this->ChatGroups              = TableRegistry::getTableLocator()->get( 'ChatGroups' );
        $this->ChatGroupMembers        = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->Threads                 = TableRegistry::getTableLocator()->get( 'Threads' );
        $this->ChatMessages            = TableRegistry::getTableLocator()->get( 'ChatMessages' );
        $this->Users                   = TableRegistry::getTableLocator()->get( 'Users' );
    }

    public function beforeFilter(Event $event)
    {
        $this->app_screen_id = 5;

        $this->_user_id = 11; // TODO 開発用
    }
    public function index( $thread_id, $user_id, $front_user_id )
    {
        $contractant_id = 4;
        $chat_group_id  = 31;
        $contractant_service_menu_id = 58;
        //$thread_id = 42;
        //$user_id = 32;
        //$front_user_id = 13;
        $data = $this->ChatMessages->getData( $contractant_id, $chat_group_id, $contractant_service_menu_id, $thread_id );
        $tmp = [];
        foreach( $data as $val )
        {
            array_unshift( $tmp, $val );
        }
        $data = $tmp;

        $this->set( compact( 'contractant_id', 'chat_group_id', 'contractant_service_menu_id', 'thread_id', 'user_id', 'front_user_id', 'data' ) );
    }
}
