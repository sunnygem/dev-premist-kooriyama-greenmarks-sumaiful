<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class AccountsController extends FrontAppController
{
    public $Users;
    public $FrontUsers;
    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Users = TableRegistry::getTableLocator()->get( 'Users' );
    }
    //public function beforeRender(Event $event)
    //{
    //    parent::beforeRender( $event );
    //}

    public function edit()
    {
        if( $this->request->is( ['post', 'put', 'patch']) && isset( $this->request->data['complete'] ) )
        {
            $this->FrontUsers = TableRegistry::getTableLocator()->get( 'FrontUsers' );

            $request = $this->request->data;

            // ファイル
            $this->loadComponent( 'File' );
            $this->File->saveFile( $request, 'front_users' );

            $error = $this->FrontUsers->validation( $request['front_user'] );

            if( count( $error ) > 0 )
            {
                $user = $this->Users->newEntity( $request );
                $this->set( compact( 'user', 'error' ) );
            }
            else
            {
                if( isset( $request['files']['front_users']['file_path'] ) )
                {
                    $request['front_user']['thumbnail_path'] = $request['files']['front_users']['file_path'];
                }
                $res = $this->Users->saveData( $request );

                if( $res )
                {
                    $user = $this->Users->getFrontUser( $res->id );
                    $this->Auth->setUser( $user->toArray() );
                    $this->Flash->success( __('プロフィールを保存しました。') );
                    return $this->redirect([ 'controller' => 'Tops', 'action' => 'index']);
                }
                else
                {
                    $this->Flash->error( 'プロフィールの保存に失敗しました。');
                    return $this->redirect(['action' => 'edit']);
                }

            }
        }
        else
        {

            $cond = [
                'FrontUsers.id'=> $this->_login_user['front_user']['id']
            ];
            $user = $this->Users->getFrontUser( $this->_login_user['id'] );
            //debug($posts );
            $this->set( compact( 'user' ) );
        }

    }
}
