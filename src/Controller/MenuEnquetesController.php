<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class MenuEnquetesController extends FrontAppController
{
    public $paginate = [ 'finder' => 'search' ];
    public $MenuEnquetes;
    public $EnqueteQuestions;

    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );

        if( $this->_session->read('ContractantData.unique_parameter') === 'sumaiful' )
        {
            $this->Auth->allow();
        }

        $this->MenuEnquetes = TableRegistry::getTableLocator()->get( 'MenuEnquetes' );
        $this->EnqueteQuestions = TableRegistry::getTableLocator()->get( 'EnqueteQuestions' );
        $this->EnqueteAnswers   = TableRegistry::getTableLocator()->get( 'EnqueteAnswers' );

        $this->ViewBuilder()->setLayout('simple_frame');
    }
//    public function beforeRender(Event $event)
//    {
//        parent::beforeRender( $event );
//
//$this->render('/willing/Tops/index');
//    }
    public function index()
    {
        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'MenuEnquetes.display_flg' => 1
                    ,'MenuEnquetes.release_date <=' => date('Y-m-d H:i:s')
                    ,'OR' => [
                        ['MenuEnquetes.close_date IS'  => null ]
                        ,['MenuEnquetes.close_date >=' => date('Y-m-d H:i:s')]
                    ]
                ]
                ,'order' => [ 'MenuEnquetes.created' => 'ASC' ]
            ]
        ];

        $menu_enquetes = $this->paginate( 'MenuEnquetes' );

        $this->set( compact( 'menu_enquetes' ) );
    }

    public function answer( $id=null )
    {
        if( $id !== null )
        {
            $menu_enquetes = $this->MenuEnquetes->getDetailById( $id );
            // 設問数のチェック
            $question_count = count( $menu_enquetes->enquete_questions );

            $page = 1;
            if( $this->request->is( 'post' )  )
            {
                $request = $this->request->data;
                $page   = $request['page'];

                // todo バリデーション
                if( isset( $this->request->data['next'] ) )
                {
                    $answer = ( $request['answer'][$request['page']] ) ? $request['answer'][$request['page']] : [];
                    $this->_session->write( 'answer.' . $page, $answer );

                    // todo 回答終了で保存
                    if( $question_count <= $page )
                    {
                        $data = [];
                        // idアドレスを取得
                        $ip_address = $_SERVER[ 'REMOTE_ADDR' ];

                        if( $this->_session->check( 'answer' ) )
                        {
                            $tmp_data = $this->_session->read( 'answer' );
                            ksort( $tmp_data );

                            $data = [
                                'contractant_id' => $this->_contractant_id
                                ,'enquete_id'    => $menu_enquetes->id
                                ,'ip_address'    => $ip_address
                            ];
                            foreach( $tmp_data as $key => $val )
                            {
                                // 回答無し対応
                                if( $val )
                                {
                                    foreach( $val as $key2 => $val2 )
                                    {
                                        $data['enquete_answer_items'][] = [
                                            'contractant_id'       => $this->_contractant_id
                                            ,'enquete_question_id' => $menu_enquetes->enquete_questions[$key-1]->id
                                            ,'answer'              => $val2
                                        ];

                                    }
                                }
                                else
                                {
                                    $data['enquete_answer_items'][] = [
                                        'contractant_id'       => $this->_contractant_id
                                        ,'enquete_question_id' => $menu_enquetes->enquete_questions[$key-1]->id
                                    ];
                                }
                               
                            }
                        }
                        $this->EnqueteAnswers->saveData( $data );

                        $this->_session->delete( 'answer' );
                        return $this->redirect(['action' => 'complete']);
                    }

                    $page = $page + 1;
                }
                elseif( isset( $this->request->data['back'] ) )
                {
                    if( $this->_session->read( 'answer.' . $page ) )
                    {
                        $this->_session->delete( 'answer.' . $page );
                    }
                    $page = $page - 1;
                }
            }
            else
            {
                // 古いセッションが残っていたら消す
                if( $this->_session->check( 'answer' ) )
                {
                    $this->_session->delete( 'answer' );
                }
            }


            $enquete_question = $menu_enquetes->enquete_questions[$page-1];

            //$this->get
            $this->set( compact( 'menu_enquetes', 'page', 'enquete_question' ) );
        }
        else
        {
            throw new NotFoundException();
        }


    }
    public function complete( $id=null )
    {
    }


}
