<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class MessagesController extends FrontAppController
{


    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        parent::app_common();

        $this->Users            = TableRegistry::getTableLocator()->get( 'Users' );
        $this->ChatMemberlists  = TableRegistry::getTableLocator()->get( 'ChatMemberlists' );
        $this->ChatGroups       = TableRegistry::getTableLocator()->get( 'ChatGroups' );
        $this->ChatGroupMembers = TableRegistry::getTableLocator()->get( 'ChatGroupMembers' );
        $this->Threads          = TableRegistry::getTableLocator()->get( 'Threads' );
        $this->FrontUsers       = TableRegistry::getTableLocator()->get( 'FrontUsers' );
        $this->ChatMessages     = TableRegistry::getTableLocator()->get( 'ChatMessages' );

        $this->_contractant_id = $this->_session->read( 'ContractantData.id' );
        $this->_user_id        = $this->_session->read( 'Front.Auth.id' );
        $this->_front_user_id  = $this->_session->read( 'Front.Auth.front_user.id' );

        $this->_contractant_service_menu_id = ( $this->_contractant_id === 4 ) ? 58 : 0; // willingの場合は固定(58) ほかはいったん0にしています。
    }
//    public function beforeRender(Event $event)
//    {
//        parent::beforeRender( $event );
//
//    }

    public function index()
    {
        $this->_display_search_form = true;
    }

    public function detail( $chat_group_id, $thread_id, $user_id, $front_user_id )
    {
        $contractant_id              = $this->_contractant_id;
        $contractant_service_menu_id = $this->_contractant_service_menu_id;
        $front_user_data             = $this->FrontUsers->findById( $front_user_id )->first();

        $data = self::get_message( $chat_group_id, $thread_id, $user_id, $front_user_id );

        $user_id       = $this->_session->read( 'Front.Auth.id' );
        $front_user_id = $this->_session->read( 'Front.Auth.front_user.id' );

        $this->set( compact( 'contractant_id', 'contractant_service_menu_id', 'chat_group_id', 'thread_id', 'user_id', 'front_user_id', 'front_user_data', 'data' ) );
    }

    public function get_message( $chat_group_id, $thread_id, $user_id, $front_user_id )
    {
        if ( $this->request->is( 'ajax' ) )
        {
            $this->autoRender = false;
            $request = $this->request->query;

            $limit         = ( isset( $request['limit'] ) ) ? $request['limit'] : CHAT_MESSAGE_LIMIT;
            $page          = ( isset( $request['page'] ) && $request['page'] > 0 ) ? $request['page'] : 1;
            $chat_group_id = $request['chat_group_id'];
            $thread_id     = $request['thread_id'];
            $user_id       = $request['user_id'];
            $front_user_id = $request['front_user_id'];

        }
        else
        {
            $limit = CHAT_MESSAGE_LIMIT;
            $page  = 1;
        }

        $data  = $this->ChatMessages->getData( $this->_contractant_id, $chat_group_id, $this->_contractant_service_menu_id, $thread_id, $page, $limit );
        // 既読処理
        $this->ChatUnreads->deleteChatUnreadData( $this->_contractant_id, $chat_group_id, $thread_id, $user_id, $front_user_id );
        $chat_group_members = $this->ChatGroupMembers->getChatGroupMembers( $this->_contractant_id, $chat_group_id, $user_id, $front_user_id );
        foreach( $chat_group_members as $key => $val )
        {
            $this->ChatMemberlists->saveUnreadFlg( $this->_contractant_id, $user_id, $front_user_id, $val->user_id, $val->front_user_id, 0 );
        }

        // 順番を逆にする
        if ( $data !== null && count( $data ) > 0 )
        {
            $data = array_reverse( $data->toArray() );
            if ( isset( $request['reverse'] ) ) $data = array_reverse( $data );
        }

        if ( $this->request->is( 'ajax' ) )
        {
            $this->response->body( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
        }
        else
        {
            return $data;
        }

    }

    public function get()
    {
        $this->autoRender = false;

        if ( $this->request->is( 'ajax' ) )
        {
            $request = $this->request->query;

            $page  = (int)$request['page'];
            $limit = CHAT_MESSAGE_LIMIT;
            $where = [
                'ChatMemberlists.contractant_id'      => $this->_contractant_id
                ,'ChatMemberlists.self_user_id'       => $this->_user_id
                ,'ChatMemberlists.self_front_user_id' => $this->_front_user_id
            ];
            // 検索
            if( isset( $request['search'] ) && strlen( $request['search'] ) > 0 ) 
            {
                // 全角スペースを半角スペースに変換してスライス
                $tmp = explode( ' ', mb_convert_kana( $request['search'], 's', 'UTF-8' ) );
                foreach( $tmp as $key => $val )
                {
                    if ( strlen( trim( $val ) ) > 0 )
                    {
                        $where['OR'] = [
                            'PartnerFrontUser.nickname LIKE'         => '%' . $val . '%'
                            ,'PartnerFrontUser.biography LIKE'       => '%' . $val . '%'
                            ,'PartnerFrontUser.university_name LIKE' => '%' . $val . '%'
                            ,'PartnerFrontUser.undergraduate LIKE'   => '%' . $val . '%'
                            ,'PartnerFrontUser.club_name LIKE'       => '%' . $val . '%'
                            ,'PartnerFrontUser.hobby LIKE'           => '%' . $val . '%'
                            ,'Buildings.name LIKE'                   => '%' . $val . '%'
                            ,'Buildings.name_en LIKE'                => '%' . $val . '%'
                            ,'Buildings.name_kana LIKE'              => '%' . mb_convert_kana( $val, 'cHV' ) . '%'
                        ];
                    }
                }
            }
            $data = $this->ChatMemberlists->getList( $where, $limit, $page );
            $data = self::get_chat_group( $this->_user_id, $this->_front_user_id, $data );
            $this->response->body( json_encode( $data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT ) );
        }
    }
    private function get_chat_group( $user_id, $front_user_id, $data )
    {

        foreach( $data as $key => $val )
        {
            // まず、自分のuser_idと相手のuser_idでチャットグループがあるかどうか確認する(複数、共通のチャットグループがないかも確認する)
            $res = $this->ChatGroupMembers->checkForDM( $this->_contractant_id, $user_id, $front_user_id, $val['id'], $val['front_user']['id'] );
            // あれば、chat_group_idを取得(DMの場合は1件のはず)
            // threadを作成
            if ( $res !== false )
            {
//var_dump($res);
                $chat_group_id = $res->first()->chat_group_id;
                // TODO chat_group_idがある時点でthread_idもあるはず
                $thread_id     = $this->Threads->getIdFromChatGroupId( $this->_contractant_id, $chat_group_id, $this->_contractant_service_menu_id, $user_id, $val['id'] );
                $data[$key]['chat_group_id'] = $chat_group_id;
                $data[$key]['thread_id']     = $thread_id;
            }
            // なければ、chat_group,threadを作成
            else
            {
                $name = $user_id . '-' . $front_user_id . 'x' . $val['id'] . '-' . $val['front_user']['id'];
                // チャットグループの登録
                $tmp = [
                    'contractant_id' => $this->_contractant_id
                    ,'name' => $name
                ];
                $chat_group_id = $this->ChatGroups->saveData( $tmp );
                // チャットグループメンバーの登録
                $tmp = [
                    'contractant_id' => $this->_contractant_id
                    ,'chat_group_id' => $chat_group_id
                    ,'user_id'       => $user_id
                    ,'front_user_id' => $front_user_id
                ];
                $this->ChatGroupMembers->saveData( $tmp );
                $tmp = [
                    'contractant_id' => $this->_contractant_id
                    ,'chat_group_id' => $chat_group_id
                    ,'user_id'       => $val['id']
                    ,'front_user_id' => $val['front_user']['id']
                ];
                $this->ChatGroupMembers->saveData( $tmp );
                // threadの登録
                $tmp = [
                    'contractant_id'               => $this->_contractant_id
                    ,'chat_group_id'               => $chat_group_id
                    ,'contractant_service_menu_id' => $this->_contractant_service_menu_id
                    ,'created_user_id'             => $user_id
                    ,'date'                        => date( 'Y-m-d H:i:s' )
                    ,'title'                       => $name
                ];
                $thread_id = $this->Threads->saveData( $tmp );
                $data[$key]['chat_group_id'] = $chat_group_id;
                $data[$key]['thread_id']     = $thread_id;
            }

        }
        return $data;
    }


    public function startMessage( $user_id, $front_user_id, $target_user_id, $target_front_user_id )
    {
         $this->autoRender = false;
         $user        = $this->FrontUsers->checkExistValidUser($user_id, $front_user_id);
         $target_user = $this->FrontUsers->checkExistValidUser($target_user_id, $target_front_user_id);

         if( $user && $target_user && $user->id !== $target_user->id )
         {
             $thread = $this->Threads->getDataByTitle(  $user_id, $front_user_id, $target_user_id, $target_front_user_id  );

             $url = '/messages/detail/' . $thread->chat_group_id . DS . $thread->id . DS . $target_user_id . DS . $target_front_user_id;
             $this->redirect([ 'action' => 'detail', $thread->chat_group_id, $thread->id, $target_user_id, $target_front_user_id]);
         }
    }


}



