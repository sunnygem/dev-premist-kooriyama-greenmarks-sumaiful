<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class EquipmentsController extends FrontAppController
{
    public $paginate = [ 'finder' => 'front' ];

    public function initialize()
    {
        parent::initialize();

    }
    public function beforeFilter( Event $event )
    {
        parent::beforeFilter( $event );

        $this->loadComponent( 'Common' );
        $this->loadComponent( 'File' );

        $this->_mt_equipment_category =  Configure::read('mt_equipment_category');
        $mt_equipment_category = $this->_mt_equipment_category;

        $this->set( compact( 'mt_equipment_category' ) );
    }

    public function index()
    {
        $this->_display_search_form = true;

        $conditions = [
            'Equipments.contractant_id' => $this->_session->read( 'ContractantData.id' )
            ,'Equipments.building_id'   => $this->_login_user['front_user']['building_id']
        ];
        // タイトル検索
        if( $this->request->getQuery('search') !== null )
        {
            $search = trim( mb_convert_kana( $this->request->getQuery('search'), 'kas' ) );
            $searchs = explode(' ',  $search);

            $arr_search = [];
            foreach( $searchs as $val ) 
            {
                $arr_search[] = [
                    'Equipments.name LIKE' => '%' . $val . '%'
                ];
            }
            $conditions[] = [
                'AND' => $arr_search
            ];
        }
        // カテゴリの絞り込み
        if( $this->request->getQuery('category') !== null )
        {
             $conditions['Equipments.category_id'] = $this->request->getQuery('category');
        }

        $this->paginate['finder'] = [
            'front' => [
                'conditions' => $conditions
            ]
        ];
        $data = $this->paginate( 'Equipments' );
        $this->set( compact( 'data' ) );
    }


}
