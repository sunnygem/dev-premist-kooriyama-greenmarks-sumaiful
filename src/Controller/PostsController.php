<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PostsController extends FrontAppController
{
    public $Posts;
    public $Buildings;

    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
        $this->Posts = TableRegistry::getTableLocator()->get( 'Posts' );
        $this->Buildings = TableRegistry::getTableLocator()->get( 'Buildings' );

        $mt_buildings = $this->Buildings->getOptions( $this->_contractant_id, true );
        $this->set(compact('mt_buildings'));
    }

    public function index()
    {
        if( $this->request->is(['post']) )
        {
            $request = $this->request->data;

            // ファイル
            $this->loadComponent( 'File' );
            $this->File->saveFile( $request, 'posts' );

            $error = $this->Posts->validation();
            if( count( $error ) > 0 )
            {
                //$data = $this->Posts->newEntities( $request );
                $this->set( compact( 'error' ) );
            }
            else
            {
                // 公開範囲
                $set_data = [
                    'contractant_id' => $this->_contractant_id
                    ,'display_flg'   => 1
                    ,'user_id'       => $this->_login_user['id']
                    ,'front_user_id' => $this->_login_user['front_user']['id']
                    ,'building_id'   => $this->_login_user['front_user']['building_id']
                    ,'content'       => $request['content']
                    ,'post_category_id'  => $request['post_category_id']
                ];
                if( isset( $request['post_buildings'] ) )
                {
                    $set_data['post_buildings'] = $request['post_buildings'];
                }

                $post_id = $this->Posts->saveData( $set_data );

                // 投稿ポイント
                parent::checkGrantPoint( 'post', $this->_date_for_point['last_post_date'] );

                // 画像
                if( $post_id && isset( $request['files']['posts']['file_path'] ) )
                {
                    // 画像パスの保存
                    $file_data = [
                        'contractant_id' => $this->_contractant_id
                        ,'name'          => $request['files']['posts']['name']
                        ,'file_path'     => $request['files']['posts']['file_path']
                    ];
                    $this->Files = TableRegistry::getTableLocator()->get( 'Files' );
                    $file_id = $this->Files->saveData( $file_data );

                    // 画像の中間ファイルの保存
                    if( $file_id )
                    {
                        $rel_data = [
                            'contractant_id' => $this->_contractant_id
                            ,'post_id' => $post_id
                            ,'file_id' => $file_id
                        ];
                        $this->RelPostFiles = TableRegistry::getTableLocator()->get( 'RelPostFiles' );
                        $this->RelPostFiles->saveData( $rel_data );
                    }
                }

                if( $post_id )
                {
                    $this->Flash->success( __('投稿が完了しました！' ));

                    return $this->redirect(['controller' => 'Communities', 'action' => 'index']);
                }
                else
                {
                    $this->Flash->error( '投稿に失敗しました。');
                }
            }
        }
    }

}
