<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class ActivitiesController extends FrontAppController
{
    public $paginate = [ 'finder' => 'search' ];

    public $PostActivityLogs;

    public function initialize()
    {
        parent::initialize();
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter( $event );
    }
//    public function beforeRender(Event $event)
//    {
//        parent::beforeRender( $event );
//
//    }

    public function index()
    {
        //  対象のデータに既読フラグを立てる
        $this->PostActivityLogs->FlagOnByUserId( $this->_login_user['id'] );

        $this->paginate['finder'] = [
            'search' => [
                'conditions' => [
                    'PostActivityLogs.contractant_id'  => $this->_session->read( 'ContractantData.id' )
                    ,'PostActivityLogs.target_user_id' => $this->_login_user['id']
                    ,'PostActivityLogs.user_id !='     => $this->_login_user['id']
                    ,'FrontUsers.id IS NOT'            => null
                ]
                ,'order' => [
                    'PostActivityLogs.created' => 'DESC'
                ]
            ]
        ];
        $this->paginate['contain'] = [
            'Users.FrontUsers'
            ,'Posts.PostCategories'
        ];
        $data = $this->paginate( 'PostActivityLogs' );
        $this->set( compact( 'data' ) );
    }

}
