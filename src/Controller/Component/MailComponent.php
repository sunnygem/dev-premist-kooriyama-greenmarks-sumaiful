<?php

namespace App\Controller\Component;

use Cake\Event\Event;
use Cake\Controller\Component;
use Cake\Mailer\Email;

class MailComponent extends Component
{
    public $_controller;
    public $_from_mail;
    public function initialize( array $config )
    {
        $this->_controller = $this->_registry->getController();

        $this->_unique_parameter = 'sumaiful';
        if( $this->_controller->_session->check('ContractantData.name') )
        {
            $this->_from_name = $this->_controller->_session->read('ContractantData.name');
            $this->_from_mail = 'system@' . $this->_controller->_session->read('ContractantData.domain');
            $this->_unique_parameter = $this->_controller->_session->read('ContractantData.unique_parameter');
            $this->_admin_url        = 'https://' . $this->_controller->_session->read('ContractantData.domain') . DS;
        }
    }
    
    private function send( $data )
    {
        $mail = new Email();
        // SMTP切り替え対応
        $profile = 'default';
        $transport = 'default';
        if ( ENV === 'pro' && $this->_unique_parameter !== 'sumaiful' )
        {
            $profile   = $this->_unique_parameter;
            $transport = $this->_unique_parameter;
        }
        $mail->setProfile( $profile );
        $mail->setTransport( $transport );

        $mail->template( $data['template'], 'default' )
            ->emailFormat( 'text' )
            ->to( $data['to'] )
            ->from( $data['from'])
            ->returnPath( $data['to'] )
            ->subject( $data['subject'] );
        if ( isset( $data['viewVars'] ) )
        {
            foreach( $data['viewVars'] as $key => $val )
            {
                $mail->viewVars( [ $key => $val ] );
            }
        }
        return $mail->send();
    }

    public function send_test( $data )
    {
        $data = [
            'from' => [ $this->_from_mail => $this->_from_name ]
            ,'subject'  => '[' . $this->_from_name . ']test mail'
            ,'to'       => $data['to']
            ,'template' => 'test'
        ];
        $this->send( $data );
    }
    public function send_login_approval_request( $data )
    {
        $data = [
            'from' => [ $this->_from_mail => $this->_from_name ]
            ,'subject'  => '[' . $this->_from_name . ']ログイン期間申請'
            ,'to'       => $data['to']
            ,'template' => 'login_approval_request'
            ,'viewVars' => [
                'auth_user'     => $data['auth_user']
                ,'request_user' => $data['request_user']
                ,'request_term' => $data['request_term']
                ,'url'          => $data['url']
            ]
        ];
        return $this->send( $data );
    }

    public function send_application( $data )
    {
        $data = [
            'from' => [ $this->_from_mail => $this->_from_name ]
            ,'subject'  => 'willingアプリ使用申請'
            ,'to'       => $data['email']
            ,'template' => 'willing_application'
            ,'viewVars' => [
                'name'      => $data['front_user']['name']
                ,'building' => $data['building']
            ]
        ];
        return $this->send( $data );
    }
    public function send_application_permission( $data )
    {
        $data = [
            'from' => [ $this->_from_mail => $this->_from_name ]
            ,'subject'  => 'willingアプリのパスワードを設定してください。'
            ,'to'       => $data['email']
            ,'template' => 'willing_application_permission'
            ,'viewVars' => [
                'name'     => $data['front_user']['name']
                ,'url'     => $data['url']
            ]
        ];
        return $this->send( $data );
    }

    public function send_admin_create( $data )
    {
        $data = [
            'from' => [ $this->_from_mail => $this->_from_name ]
            ,'subject'  => $this->_from_name . '管理画面のパスワードを設定してください。'
            ,'to'       => $data['email']
            ,'template' => 'admin_create'
            ,'viewVars' => [
                'name'       => $data['admin_user']['name']
                ,'set_url' => $this->_admin_url . 'admin/logins/set-password' . DS . $data['admin_user']['remind_key']
            ]
        ];
        return $this->send( $data );
    }

    public function send_password_remind( $data )
    {
        $data = [
            'from' => [ $this->_from_mail => $this->_from_name ]
            ,'subject'  => $this->_from_name . 'パスワードを再設定してください。'
            ,'to'       => $data['email']
            ,'template' => 'password_remind'
            ,'viewVars' => [
                'name'       => $data['name']
                ,'set_url' => $this->_admin_url . 'admin/logins/set-password' . DS . $data['remind_key']
            ]
        ];
        return $this->send( $data );

    }


}
