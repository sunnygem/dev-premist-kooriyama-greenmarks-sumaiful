<?php

namespace App\Controller\Component;
use Cake\Controller\Component;

class FileComponent extends Component
{
    public $_allow_mime_type = [
        'image/png'
        ,'image/jpeg'
        ,'image/gif'
        ,'application/pdf'
    ];

    public $_allow_image_mime_type = [
        'image/png'
        ,'image/jpeg'
        ,'image/gif'
    ];

    /*
     * $data array  フォームデータ
     * $key  string 画像のキー
     * $target_dir string 保存先のディレクトリ名
     * controller側
     * $this->Image->saveImage( $data, $key, $target_dir )
     *
     */
    public function saveFile( &$data, $key )
    {
        if ( count( $data ) !== 0 )
        {
            // cakeのrequestデータ
            if ( isset( $data['files'][$key]['tmp_name'] ) )
            {
                $data['files'][$key]['file_path'] = self::save( $data, $key );
            }
            else
            {
                foreach( $data['files'][$key] as $k => $v )
                {
                    $data['files'][$key][$k]['file_path'] = self::save( $v, $key, $data, $k );
                }
            }
        }
    }

    // $data array $_FILESのデータ
    //public function convertFileArray( $data=[], $key='images' )
    //{
    //    if ( count( $data ) !== 0 )
    //    {
    //         $arr = [
    //             'name'      => $data['name']['files'][$key]
    //             ,'type'     => $data['type']['files'][$key]
    //             ,'tmp_name' => $data['tmp_name']['files'][$key]
    //             ,'error'    => $data['error']['files'][$key]
    //             ,'size'     => $data['size']['files'][$key]
    //         ];
    //         return $arr;
    //    }
    //    else
    //    {
    //         return false;
    //    }
   //}

    // ファイルの移動
    public function save( $val, $key, $data=null, $k=null )
    {
        $image = ( isset( $val['files'][$key]['tmp_name'] ) ) ? $val['files'][$key] : $val;
        // 配列じゃない場合は、スルー
        if ( is_array( $image ) === false ) return $val;

        if ( $image['error'] === 0 && $image['size'] !== 0 )
        {
            if ( is_uploaded_file( $image['tmp_name'] ) === true )
            {
                //$mime_type = mime_content_type( $image['tmp_name'] );
                $mime_type = self::checkMimeType( $image['tmp_name'] );
                //$dir = 'img' . DS . 'upload' . DS;
                switch( $mime_type )
                {
                case 'image/gif':
                    $dir = 'image';
                    $ext = '.gif';
                    break;
                case 'image/jpeg':
                    $dir = 'image';
                    $ext = '.jpg';
                    break;
                case 'image/png':
                    $dir = 'image';
                    $ext = '.png';
                    break;
                case 'image/svg+xml':
                    $dir = 'image';
                    $ext = '.svg';
                    break;
                case 'application/pdf':
                    $dir = 'pdf';
                    $ext = '.pdf';
                    break;
                default:
                    $dir = 'image';
                    $ext = '.jpg';
                }

                if( $key !== null ) $dir .= DS . $key;

                $file_name = time() . '_' . uniqid( rand() ) . $ext;
                $dir .= $this->getTargetDir( $file_name );
                //if ( is_dir( WWW_ROOT . 'img' . DS . 'upload' . DS . $dir ) === false ) mkdir( WWW_ROOT . 'img' . DS . 'upload' . DS . $dir, 0777, true );
                if ( is_dir( WWW_ROOT . 'upload' . DS . $dir ) === false ) mkdir( WWW_ROOT . 'upload' . DS . $dir, 0777, true );
                move_uploaded_file( $image['tmp_name'], WWW_ROOT . DS . 'upload' . DS .  $dir . DS . $file_name );
                $file_name = 'upload' . DS . $dir . DS . $file_name;

                return $file_name;
            }
        }
        else if ( $image['size'] === 0 && $k !== null && isset( $data['hidden_' . $key][$k] ) )
        {
            return $data['hidden_' . $key][$k];
        }
        else if ( $image['size'] === 0 && isset( $val['hidden_' . $key] ) )
        {
            return $val['hidden_' . $key];
        }
    }

    public function getTargetDir( $file_name )
    {
        $tmp_name = sha1( $file_name );

        return DS . substr( $tmp_name, 0, 2 ) . DS . substr( $tmp_name, 2, 2 );
    }

    public function checkAllowImageMimeType( $mime_type )
    {
        return in_array( $mime_type, $this->_allow_image_mime_type, true );
    }

    /* APIでのBase64データの変換、サーバー保存 */
    public function saveBase64Data( $val )
    {
        $val = str_replace(' ', '+', $val);
        $data = base64_decode( $val );

        $mime_type = finfo_buffer( finfo_open(), $data, FILEINFO_MIME_TYPE );
        if( self::checkAllowImageMimeType( $mime_type ) )
        {
            switch( $mime_type )
            {
            case 'image/gif':
                $dir = 'image';
                $ext = '.gif';
                break;
            case 'image/jpeg':
                $dir = 'image';
                $ext = '.jpg';
                break;
            case 'image/png':
                $dir = 'image';
                $ext = '.png';
                break;
            case 'application/pdf':
                $dir = 'pdf';
                $ext = '.pdf';
                break;
            default:
                $dir = 'image';
                $ext = '.jpg';
                break;
            }

            $file_name = time() . '_' . uniqid( rand() ) . $ext;
            $dir .= $this->getTargetDir( $file_name );

            // ファイルの保存
            if ( is_dir( WWW_ROOT . 'upload' . DS . $dir ) === false ) mkdir( WWW_ROOT . 'upload' . DS . $dir, 0777, true );
            file_put_contents( WWW_ROOT . DS . 'upload' . DS .  $dir . DS . $file_name, $data );
            $file_name = 'upload' . DS . $dir . DS . $file_name;

            return $file_name;
        }
    }



    public function checkFileExist( $file )
    {
        return ( file_exists ( WWW_ROOT . DS . $file ) ) ? true : false;
    }

    // 画像の物理削除
    public function removeFile( $file )
    {
        return unlink( WWW_ROOT . DS . $file );
    }

    private function checkMimeType( $file_name )
    {
        // svgファイルのチェック
        $res = file_get_contents( $file_name );
        if ( preg_match( '/^<svg.*?xmlns="http:\/\/www.w3.org\/2000\/svg\".*?>/i', $res ) === 1 )
        {
            $mime_type = 'image/svg+xml';
        }
        // 通常の画像
        else
        {
            $mime_type = mime_content_type( $file_name );
        }

        return $mime_type;
    }

}
