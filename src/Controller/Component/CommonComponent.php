<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Error\ForbiddenException;

use Cake\Core\Configure;

use Imagick;

/*
 * CommonComponent
 *
 */
class CommonComponent extends Component
{
    // マスターデータの取得
    public function getMaster( $table )
    {
        $table = TableRegistry::get( $table );
        return $table->find( 'list' )->toArray();
    }

    // 契約者情報のチェック
    public function checkContractantByDomain( $domain )
    {
        $this->Contractants = TableRegistry::get( 'Contractants' );
        return $this->Contractants->getDataByDomain( $domain );

    }
    // 契約者情報のチェック
    public function checkContractantByParameter( $unique_parameter )
    {
        $this->Contractants = TableRegistry::get( 'Contractants' );
        return $this->Contractants->getDataByUniqueParameter( $unique_parameter );

    }
    public function moveFileToTmpDir( $file=[] )
    {
        $tmp_file = '';

        if ( $file['error'] === 0 && $file['size'] !== 0 )
        {
            if ( is_uploaded_file( $file['tmp_name'] ) === true )
            {
                $file_name = explode( '/', $file['tmp_name'] );
                $tmp_dir = TMP . 'file'. DS;
                //一時フォルダ作成
                if( is_dir( $tmp_dir )  === false ) mkdir( $tmp_dir, 0777, true );

                move_uploaded_file( $file['tmp_name'], $tmp_dir . $file_name[2] );
                $tmp_file = $tmp_dir . $file_name[2];

            }
        }
        return $tmp_file;
    }
    public function putBinaryToTmpDir( $binary=null, $file_name=null )
    {
        $tmp_file = '';

        $tmp_dir = TMP . 'file'. DS;
        // todo ファイルネームがなければ自動で命名 
        if( $file_name === null ) $file_name = 'tmp_' . time();

        //一時フォルダ作成
        if( is_dir( $tmp_dir )  === false ) mkdir( $tmp_dir, 0777, true );
        $res = file_put_contents( $tmp_dir . $file_name, $binary );

        if( $res )
        {
            $tmp_file = $tmp_dir . $file_name;

        }
        return $tmp_file;
    
        
    }
    public function checkFileExtension( $file=[] )
    {
        $tmp = pathinfo( $file["name"] );
        return $tmp['extension'];
    }

    public function compressImage( $file_path )
    {
        $imagick_img = new \Imagick($photo_file_path);
    }

    // hashを生成
    public function createHash()
    {
        return hash( 'sha256', time() . openssl_random_pseudo_bytes( 256 ) );
    }
    // iv,saltを生成
    public function createRandomBytes( $byte=32 )
    {
        return base64_encode( random_bytes( $byte ) );
    }
    // 暗号化
    public function encrypt( $data, $password, $iv, $salt, $info='' )
    {
        $key = hash_hkdf( 'sha256', $password, 0, $info, $salt );
        $encrypted_data = openssl_encrypt( $data, 'AES-256-CBC', $key, 0, $iv );
        return $encrypted_data;
    }
    public function decrypt( $edata, $password, $iv, $salt, $info='' )
    {
        $key = hash_hkdf( 'sha256', $password, 0, $info, $salt );
        return openssl_decrypt( $edata, 'AES-256-CBC', $key, 0, $iv );
    }

    public function toSnakeCase( $str=null )
    {
        return ltrim(strtolower(preg_replace('/[A-Z]/', '_\0', $str)), '_');
    }

    public function toCamelCase( $str=null )
    {
        return lcfirst(strtr(ucwords(strtr($str, ['_' => ' '])), [' ' => '']));
    }
}
