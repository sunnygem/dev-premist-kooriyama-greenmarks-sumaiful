<?php
namespace App\Controller;
use App\Controller\FrontAppController;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 *
**/
class FaqsController extends FrontAppController
{
    public $Terms;
    public function initialize()
    {
        parent::initialize();
        $this->Faqs = TableRegistry::getTableLocator()->get( 'Faqs' );
        $this->FaqsQuestions = TableRegistry::getTableLocator()->get( 'FaqsQuestions' );
        $this->Auth->allow();
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
                
        $mt_faq_category = Configure::read( 'mt_faq_category' );

        //return $this->FaqsQuestions->getFaqsQuestionsData( $this->_contractant_id );
        $tes = $this->FaqsQuestions->getFaqsQuestionsDataFront();
        $t = 0;$j = 0;$k = 0;$l = 0;$m = 0;
        foreach( $tes as $key => $val )
        {
            switch ( $val->faq_category_id )
            {
                case 1:
                    $tes1['category_name'] = 'アプリ全体について'; 
                    $tes1['contents'][0]['question']       = $val->question;
                    $tes1['contents'][0]['question_en']    = $val->question_en;
                    $tes1['contents'][0]['answers'][$t]    = $val->faqs;
                    $t++;
                    break;
                case 2:
                    $tes2['category_name'] = 'ポイントについて';
                    $tes2['contents'][$j]['question']       = $val->question;
                    $tes2['contents'][$j]['question_en']    = $val->question_en;
                    $tes2['contents'][$j]['answers'][$j]        = $val->faqs;
                    $j++;
                    break;
                case 3:
                    $tes3['category_name'] = '投稿について';
                    $tes3['contents'][$k]['question']       = $val->question;
                    $tes3['contents'][$k]['question_en']    = $val->question_en;
                    $tes3['contents'][$k]['answers'][$k]        = $val->faqs;
                    $k++;
                    break;
                case 4:
                    $tes4['category_name']  = '備品の貸し出しについて';
                    $tes4['contents'][$l]['question']       = $val->question;
                    $tes4['contents'][$l]['question_en']    = $val->question_en;
                    $tes4['contents'][$l]['answers'][$l]        = $val->faqs;
                    $l++;
                    break;
                case 5:
                    $tes5['category_name'] = 'オープンチャットについて';
                    $tes5['contents'][$m]['question']       = $val->question;
                    $tes5['contents'][$m]['question_en']    = $val->question_en;
                    $tes5['contents'][$m]['answers'][$m]        = $val->faqs;
                    $m++;
                    break;
                default:
            }
        }

        $data =  [$tes1,$tes2,$tes3,$tes4,$tes5];

        $this->set( compact( 'data' ) );
    }

}
